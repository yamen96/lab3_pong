`ifndef _my_incl_vh_
`define _my_incl_vh_

//
// Data width of each x and y
//

parameter DATA_WIDTH_COORD = 8;

//
// This file provides useful parameters and types for Lab 3.
// 

parameter SCREEN_WIDTH = 160;
parameter SCREEN_HEIGHT = 120;

// Use the same precision for x and y as it simplifies life
// A new type that describes a pixel location on the screen


typedef struct {
   reg [DATA_WIDTH_COORD-1:0] x;
   reg [DATA_WIDTH_COORD-1:0] y;
} point;

// A new type that describes a velocity.  Each component of the
// velocity can be either + or -, so use signed type

typedef struct {
   reg signed [DATA_WIDTH_COORD-1:0] x;
   reg signed [DATA_WIDTH_COORD-1:0] y;
} velocity;
  
  //Colours.  
parameter BLACK = 3'b000;
parameter BLUE  = 3'b001;
parameter GREEN = 3'b010;
parameter CYAN = 3'b011;
parameter RED = 3'b100;
parameter PURPLE = 3'b101;
parameter YELLOW = 3'b110;
parameter WHITE = 3'b111;

// We are going to write this as a state machine.  The following
// is a list of states that the state machine can be in.

typedef enum int unsigned {INIT = 1 , START = 2, 
              DRAW_TOP_ENTER = 4, DRAW_TOP_LOOP = 8, 
              DRAW_BOTTOM_ENTER = 16, DRAW_BOTTOM_LOOP = 32, IDLE = 64, 
              ERASE_PADDLE_ONE_ENTER = 128, ERASE_PADDLE_ONE_LOOP = 256, 
              DRAW_PADDLE_ONE_ENTER = 512, DRAW_PADDLE_ONE_LOOP = 1024, 
              ERASE_PADDLE_TWO_ENTER = 2048, ERASE_PADDLE_TWO_LOOP = 4096,
              DRAW_PADDLE_TWO_ENTER = 8192, DRAW_PADDLE_TWO_LOOP = 16384,
              ERASE_PUCK = 32768, DRAW_PUCK = 65536} draw_state_type;  

// Here are some parameters that we will use in the code. 
 
// These parameters contain information about the paddle 
parameter PADDLE_WIDTH = 10;  // width, in pixels, of the paddle
parameter PADDLE_ONE_COL = 2;  // col to draw the paddle 
parameter PADDLE_ONE_Y_START = SCREEN_HEIGHT / 2;  // starting y position of the paddle
parameter PADDLE_TWO_COL = SCREEN_WIDTH - 2;  // col to draw the paddle 
parameter PADDLE_TWO_Y_START = SCREEN_HEIGHT / 2;  // starting y position of the paddle

// These parameters describe the lines that are drawn around the  
// border of the screen  
parameter TOP_LINE = 4;
parameter BOTTOM_LINE = SCREEN_HEIGHT - 4;
parameter RIGHT_LINE = SCREEN_WIDTH -4;
parameter LEFT_LINE = 4;

// These parameters describe the starting location for the puck 
parameter FACEOFF_X = SCREEN_WIDTH/2;
parameter FACEOFF_Y = SCREEN_HEIGHT/2;
  
// Starting Velocity
parameter VELOCITY_START_X = 1;
parameter VELOCITY_START_Y = -1;
  
// This parameter indicates how many times the counter should count in the
// START state between each invocation of the main loop of the program.
// A larger value will result in a slower game.  The current setting will    
// cause the machine to wait in the start state for 1/8 of a second between 
// each invocation of the main loop.  The 50000000 is because we are
// clocking our circuit with  a 50Mhz clock. 
  
parameter LOOP_SPEED = 50000000/32;  // 8Hz
  
`endif // _my_incl_vh_