-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.1 Build 190 01/19/2015 SJ Full Version"

-- DATE "10/18/2016 14:31:43"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	lab3_bonus IS
    PORT (
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(8 DOWNTO 0);
	VGA_R : OUT std_logic_vector(9 DOWNTO 0);
	VGA_G : OUT std_logic_vector(9 DOWNTO 0);
	VGA_B : OUT std_logic_vector(9 DOWNTO 0);
	VGA_HS : OUT std_logic;
	VGA_VS : OUT std_logic;
	VGA_BLANK : OUT std_logic;
	VGA_SYNC : OUT std_logic;
	VGA_CLK : OUT std_logic
	);
END lab3_bonus;

-- Design Ports Information
-- KEY[4]	=>  Location: PIN_AE29,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[5]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[6]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[7]	=>  Location: PIN_AK18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[2]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[3]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[5]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[6]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[7]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[8]	=>  Location: PIN_Y27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[9]	=>  Location: PIN_Y26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[0]	=>  Location: PIN_J9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[1]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[3]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[4]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[5]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[6]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[7]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[8]	=>  Location: PIN_E3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[9]	=>  Location: PIN_J12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[0]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[1]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[2]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[3]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[4]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[5]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[7]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[8]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[9]	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_HS	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_VS	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_BLANK	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_SYNC	=>  Location: PIN_AF26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_CLK	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[8]	=>  Location: PIN_AC30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF lab3_bonus IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_VGA_R : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_G : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_B : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL ww_VGA_BLANK : std_logic;
SIGNAL ww_VGA_SYNC : std_logic;
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \KEY[4]~input_o\ : std_logic;
SIGNAL \KEY[5]~input_o\ : std_logic;
SIGNAL \KEY[6]~input_o\ : std_logic;
SIGNAL \KEY[7]~input_o\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \vga_u0|controller|Add1~37_sumout\ : std_logic;
SIGNAL \KEY[8]~input_o\ : std_logic;
SIGNAL \vga_u0|controller|Add0~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~38\ : std_logic;
SIGNAL \vga_u0|controller|Add0~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~18\ : std_logic;
SIGNAL \vga_u0|controller|Add0~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~34\ : std_logic;
SIGNAL \vga_u0|controller|Add0~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~30\ : std_logic;
SIGNAL \vga_u0|controller|Add0~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~14\ : std_logic;
SIGNAL \vga_u0|controller|Add0~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~26\ : std_logic;
SIGNAL \vga_u0|controller|Add0~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~22\ : std_logic;
SIGNAL \vga_u0|controller|Add0~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~10\ : std_logic;
SIGNAL \vga_u0|controller|Add0~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[7]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|Add0~6\ : std_logic;
SIGNAL \vga_u0|controller|Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|always1~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~6\ : std_logic;
SIGNAL \vga_u0|controller|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|LessThan7~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~1_combout\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \Add4~25_sumout\ : std_logic;
SIGNAL \Add3~25_sumout\ : std_logic;
SIGNAL \Selector14~1_combout\ : std_logic;
SIGNAL \Add11~18\ : std_logic;
SIGNAL \Add11~22\ : std_logic;
SIGNAL \Add11~6\ : std_logic;
SIGNAL \Add11~10\ : std_logic;
SIGNAL \Add11~14\ : std_logic;
SIGNAL \Add11~26\ : std_logic;
SIGNAL \Add11~30\ : std_logic;
SIGNAL \Add11~1_sumout\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \Add5~14\ : std_logic;
SIGNAL \Add5~9_sumout\ : std_logic;
SIGNAL \Add6~22\ : std_logic;
SIGNAL \Add6~14\ : std_logic;
SIGNAL \Add6~9_sumout\ : std_logic;
SIGNAL \Selector26~0_combout\ : std_logic;
SIGNAL \Selector23~0_combout\ : std_logic;
SIGNAL \Add5~10\ : std_logic;
SIGNAL \Add5~5_sumout\ : std_logic;
SIGNAL \Add6~10\ : std_logic;
SIGNAL \Add6~5_sumout\ : std_logic;
SIGNAL \Selector25~0_combout\ : std_logic;
SIGNAL \Add5~6\ : std_logic;
SIGNAL \Add5~1_sumout\ : std_logic;
SIGNAL \Add6~6\ : std_logic;
SIGNAL \Add6~1_sumout\ : std_logic;
SIGNAL \Selector9~0_combout\ : std_logic;
SIGNAL \Add5~2\ : std_logic;
SIGNAL \Add5~17_sumout\ : std_logic;
SIGNAL \Add6~2\ : std_logic;
SIGNAL \Add6~17_sumout\ : std_logic;
SIGNAL \Selector8~0_combout\ : std_logic;
SIGNAL \paddle_two_y~0_combout\ : std_logic;
SIGNAL \paddle_two_y~1_combout\ : std_logic;
SIGNAL \paddle_two_y~6_combout\ : std_logic;
SIGNAL \Add5~25_sumout\ : std_logic;
SIGNAL \Add6~25_sumout\ : std_logic;
SIGNAL \Selector14~0_combout\ : std_logic;
SIGNAL \Add6~26\ : std_logic;
SIGNAL \Add6~21_sumout\ : std_logic;
SIGNAL \Add5~26\ : std_logic;
SIGNAL \Add5~21_sumout\ : std_logic;
SIGNAL \Selector28~0_combout\ : std_logic;
SIGNAL \Add5~22\ : std_logic;
SIGNAL \Add5~13_sumout\ : std_logic;
SIGNAL \Selector27~0_combout\ : std_logic;
SIGNAL \Add6~13_sumout\ : std_logic;
SIGNAL \paddle_two_y~4_combout\ : std_logic;
SIGNAL \Add9~18\ : std_logic;
SIGNAL \Add9~13_sumout\ : std_logic;
SIGNAL \Add10~18\ : std_logic;
SIGNAL \Add10~13_sumout\ : std_logic;
SIGNAL \Add10~13_wirecell_combout\ : std_logic;
SIGNAL \Selector30~0_combout\ : std_logic;
SIGNAL \Add9~14\ : std_logic;
SIGNAL \Add9~9_sumout\ : std_logic;
SIGNAL \Add10~14\ : std_logic;
SIGNAL \Add10~9_sumout\ : std_logic;
SIGNAL \Add10~9_wirecell_combout\ : std_logic;
SIGNAL \Add9~10\ : std_logic;
SIGNAL \Add9~5_sumout\ : std_logic;
SIGNAL \Add10~10\ : std_logic;
SIGNAL \Add10~5_sumout\ : std_logic;
SIGNAL \Add10~5_wirecell_combout\ : std_logic;
SIGNAL \Add8~18\ : std_logic;
SIGNAL \Add8~13_sumout\ : std_logic;
SIGNAL \Add8~14\ : std_logic;
SIGNAL \Add8~9_sumout\ : std_logic;
SIGNAL \Add8~9_wirecell_combout\ : std_logic;
SIGNAL \Add8~10\ : std_logic;
SIGNAL \Add8~5_sumout\ : std_logic;
SIGNAL \Add8~5_wirecell_combout\ : std_logic;
SIGNAL \Add7~1_wirecell_combout\ : std_logic;
SIGNAL \puck_velocity.x[0]~DUPLICATE_q\ : std_logic;
SIGNAL \Add7~9_sumout\ : std_logic;
SIGNAL \Add7~10\ : std_logic;
SIGNAL \Add7~21_sumout\ : std_logic;
SIGNAL \Add7~22\ : std_logic;
SIGNAL \Add7~25_sumout\ : std_logic;
SIGNAL \Add7~26\ : std_logic;
SIGNAL \Add7~29_sumout\ : std_logic;
SIGNAL \Add7~30\ : std_logic;
SIGNAL \Add7~1_sumout\ : std_logic;
SIGNAL \Equal9~0_combout\ : std_logic;
SIGNAL \Add7~2\ : std_logic;
SIGNAL \Add7~13_sumout\ : std_logic;
SIGNAL \Add7~17_wirecell_combout\ : std_logic;
SIGNAL \Add7~14\ : std_logic;
SIGNAL \Add7~17_sumout\ : std_logic;
SIGNAL \Equal9~1_combout\ : std_logic;
SIGNAL \Add2~125_sumout\ : std_logic;
SIGNAL \Add2~74\ : std_logic;
SIGNAL \Add2~89_sumout\ : std_logic;
SIGNAL \Add12~18\ : std_logic;
SIGNAL \Add12~22\ : std_logic;
SIGNAL \Add12~6\ : std_logic;
SIGNAL \Add12~10\ : std_logic;
SIGNAL \Add12~14\ : std_logic;
SIGNAL \Add12~25_sumout\ : std_logic;
SIGNAL \Equal6~3_combout\ : std_logic;
SIGNAL \Add12~26\ : std_logic;
SIGNAL \Add12~29_sumout\ : std_logic;
SIGNAL \Selector14~2_combout\ : std_logic;
SIGNAL \Equal6~4_combout\ : std_logic;
SIGNAL \Add11~25_sumout\ : std_logic;
SIGNAL \Add11~29_sumout\ : std_logic;
SIGNAL \Equal4~5_combout\ : std_logic;
SIGNAL \Add11~13_sumout\ : std_logic;
SIGNAL \Add11~9_sumout\ : std_logic;
SIGNAL \Add11~5_sumout\ : std_logic;
SIGNAL \Equal4~0_combout\ : std_logic;
SIGNAL \Selector6~7_combout\ : std_logic;
SIGNAL \Equal4~2_combout\ : std_logic;
SIGNAL \Selector75~0_combout\ : std_logic;
SIGNAL \Add1~14\ : std_logic;
SIGNAL \Add1~17_sumout\ : std_logic;
SIGNAL \state.IDLE~q\ : std_logic;
SIGNAL \Selector6~2_combout\ : std_logic;
SIGNAL \WideOr16~1_combout\ : std_logic;
SIGNAL \Selector6~3_combout\ : std_logic;
SIGNAL \Selector6~4_combout\ : std_logic;
SIGNAL \Selector6~5_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Selector6~6_combout\ : std_logic;
SIGNAL \Selector15~0_combout\ : std_logic;
SIGNAL \Selector15~1_combout\ : std_logic;
SIGNAL \Add0~25_sumout\ : std_logic;
SIGNAL \Selector15~2_combout\ : std_logic;
SIGNAL \Add0~26\ : std_logic;
SIGNAL \Add0~21_sumout\ : std_logic;
SIGNAL \Selector14~3_combout\ : std_logic;
SIGNAL \Add12~30\ : std_logic;
SIGNAL \Add12~1_sumout\ : std_logic;
SIGNAL \Selector65~0_combout\ : std_logic;
SIGNAL \Equal6~2_combout\ : std_logic;
SIGNAL \Selector65~1_combout\ : std_logic;
SIGNAL \Selector65~2_combout\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \draw.x[3]~11_combout\ : std_logic;
SIGNAL \state.DRAW_BOTTOM_ENTER~q\ : std_logic;
SIGNAL \Selector70~0_combout\ : std_logic;
SIGNAL \state.DRAW_BOTTOM_LOOP~q\ : std_logic;
SIGNAL \draw.x[5]~0_combout\ : std_logic;
SIGNAL \draw.y[4]~0_combout\ : std_logic;
SIGNAL \draw.y[4]~1_combout\ : std_logic;
SIGNAL \Add12~17_sumout\ : std_logic;
SIGNAL \Add12~21_sumout\ : std_logic;
SIGNAL \Equal6~1_combout\ : std_logic;
SIGNAL \Selector77~0_combout\ : std_logic;
SIGNAL \Selector77~1_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \WideOr16~0_combout\ : std_logic;
SIGNAL \Add9~6\ : std_logic;
SIGNAL \Add9~2\ : std_logic;
SIGNAL \Add9~29_sumout\ : std_logic;
SIGNAL \Add9~1_sumout\ : std_logic;
SIGNAL \Add10~6\ : std_logic;
SIGNAL \Add10~2\ : std_logic;
SIGNAL \Add10~29_sumout\ : std_logic;
SIGNAL \Add10~29_wirecell_combout\ : std_logic;
SIGNAL \Add9~30\ : std_logic;
SIGNAL \Add9~25_sumout\ : std_logic;
SIGNAL \Add10~30\ : std_logic;
SIGNAL \Add10~25_sumout\ : std_logic;
SIGNAL \Add10~25_wirecell_combout\ : std_logic;
SIGNAL \Add8~1_wirecell_combout\ : std_logic;
SIGNAL \Add8~6\ : std_logic;
SIGNAL \Add8~2\ : std_logic;
SIGNAL \Add8~29_sumout\ : std_logic;
SIGNAL \Add8~29_wirecell_combout\ : std_logic;
SIGNAL \Add8~30\ : std_logic;
SIGNAL \Add8~25_sumout\ : std_logic;
SIGNAL \Selector9~2_combout\ : std_logic;
SIGNAL \Selector9~3_combout\ : std_logic;
SIGNAL \Add0~6\ : std_logic;
SIGNAL \Add0~1_sumout\ : std_logic;
SIGNAL \Selector9~4_combout\ : std_logic;
SIGNAL \Equal4~3_combout\ : std_logic;
SIGNAL \Selector73~0_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_ONE_LOOP~q\ : std_logic;
SIGNAL \Selector72~2_combout\ : std_logic;
SIGNAL \Selector72~3_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_TWO_LOOP~q\ : std_logic;
SIGNAL \Selector72~0_combout\ : std_logic;
SIGNAL \LessThan0~6_combout\ : std_logic;
SIGNAL \LessThan0~7_combout\ : std_logic;
SIGNAL \LessThan0~8_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \LessThan0~4_combout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \LessThan0~5_combout\ : std_logic;
SIGNAL \Selector72~1_combout\ : std_logic;
SIGNAL \Selector67~3_combout\ : std_logic;
SIGNAL \state.DRAW_TOP_ENTER~q\ : std_logic;
SIGNAL \Selector68~0_combout\ : std_logic;
SIGNAL \state.DRAW_TOP_LOOP~q\ : std_logic;
SIGNAL \WideOr16~2_combout\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \Add1~18\ : std_logic;
SIGNAL \Add1~21_sumout\ : std_logic;
SIGNAL \Selector5~1_combout\ : std_logic;
SIGNAL \Selector5~2_combout\ : std_logic;
SIGNAL \Selector5~3_combout\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \Selector5~4_combout\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \Add1~22\ : std_logic;
SIGNAL \Add1~25_sumout\ : std_logic;
SIGNAL \Selector4~0_combout\ : std_logic;
SIGNAL \draw.x[3]~5_combout\ : std_logic;
SIGNAL \draw.x[3]~8_combout\ : std_logic;
SIGNAL \draw.x[3]~7_combout\ : std_logic;
SIGNAL \draw.x[3]~6_combout\ : std_logic;
SIGNAL \draw.x[3]~9_combout\ : std_logic;
SIGNAL \Add1~26\ : std_logic;
SIGNAL \Add1~29_sumout\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \Add1~30\ : std_logic;
SIGNAL \Add1~9_sumout\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \draw.x[3]~1_combout\ : std_logic;
SIGNAL \draw.x[5]~10_combout\ : std_logic;
SIGNAL \Add1~10\ : std_logic;
SIGNAL \Add1~5_sumout\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \Add1~6\ : std_logic;
SIGNAL \Add1~1_sumout\ : std_logic;
SIGNAL \Selector0~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Selector66~0_combout\ : std_logic;
SIGNAL \state.START~q\ : std_logic;
SIGNAL \Selector6~1_combout\ : std_logic;
SIGNAL \Selector72~4_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_ONE_ENTER~q\ : std_logic;
SIGNAL \draw.x[3]~2_combout\ : std_logic;
SIGNAL \draw.x[3]~3_combout\ : std_logic;
SIGNAL \Add1~13_sumout\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \Equal2~0_combout\ : std_logic;
SIGNAL \Selector71~0_combout\ : std_logic;
SIGNAL \state.IDLE~DUPLICATE_q\ : std_logic;
SIGNAL \clock_counter[31]~0_combout\ : std_logic;
SIGNAL \Add2~90\ : std_logic;
SIGNAL \Add2~93_sumout\ : std_logic;
SIGNAL \Add2~94\ : std_logic;
SIGNAL \Add2~97_sumout\ : std_logic;
SIGNAL \Add2~98\ : std_logic;
SIGNAL \Add2~101_sumout\ : std_logic;
SIGNAL \Add2~102\ : std_logic;
SIGNAL \Add2~81_sumout\ : std_logic;
SIGNAL \Add2~82\ : std_logic;
SIGNAL \Add2~85_sumout\ : std_logic;
SIGNAL \Add2~86\ : std_logic;
SIGNAL \Add2~105_sumout\ : std_logic;
SIGNAL \Add2~106\ : std_logic;
SIGNAL \Add2~109_sumout\ : std_logic;
SIGNAL \Add2~110\ : std_logic;
SIGNAL \Add2~113_sumout\ : std_logic;
SIGNAL \Add2~114\ : std_logic;
SIGNAL \Add2~117_sumout\ : std_logic;
SIGNAL \Add2~118\ : std_logic;
SIGNAL \Add2~77_sumout\ : std_logic;
SIGNAL \LessThan0~9_combout\ : std_logic;
SIGNAL \Add2~126\ : std_logic;
SIGNAL \Add2~121_sumout\ : std_logic;
SIGNAL \Add2~122\ : std_logic;
SIGNAL \Add2~17_sumout\ : std_logic;
SIGNAL \Add2~18\ : std_logic;
SIGNAL \Add2~13_sumout\ : std_logic;
SIGNAL \Add2~14\ : std_logic;
SIGNAL \Add2~21_sumout\ : std_logic;
SIGNAL \Add2~22\ : std_logic;
SIGNAL \Add2~25_sumout\ : std_logic;
SIGNAL \Add2~26\ : std_logic;
SIGNAL \Add2~29_sumout\ : std_logic;
SIGNAL \Add2~30\ : std_logic;
SIGNAL \Add2~37_sumout\ : std_logic;
SIGNAL \Add2~38\ : std_logic;
SIGNAL \Add2~41_sumout\ : std_logic;
SIGNAL \Add2~42\ : std_logic;
SIGNAL \Add2~45_sumout\ : std_logic;
SIGNAL \Add2~46\ : std_logic;
SIGNAL \Add2~33_sumout\ : std_logic;
SIGNAL \Add2~34\ : std_logic;
SIGNAL \Add2~49_sumout\ : std_logic;
SIGNAL \Add2~50\ : std_logic;
SIGNAL \Add2~9_sumout\ : std_logic;
SIGNAL \Add2~10\ : std_logic;
SIGNAL \Add2~5_sumout\ : std_logic;
SIGNAL \Add2~6\ : std_logic;
SIGNAL \Add2~53_sumout\ : std_logic;
SIGNAL \Add2~54\ : std_logic;
SIGNAL \Add2~57_sumout\ : std_logic;
SIGNAL \Add2~58\ : std_logic;
SIGNAL \Add2~61_sumout\ : std_logic;
SIGNAL \Add2~62\ : std_logic;
SIGNAL \Add2~65_sumout\ : std_logic;
SIGNAL \Add2~66\ : std_logic;
SIGNAL \Add2~69_sumout\ : std_logic;
SIGNAL \Add2~70\ : std_logic;
SIGNAL \Add2~1_sumout\ : std_logic;
SIGNAL \Add2~2\ : std_logic;
SIGNAL \Add2~73_sumout\ : std_logic;
SIGNAL \Selector65~10_combout\ : std_logic;
SIGNAL \Selector65~11_combout\ : std_logic;
SIGNAL \Selector65~12_combout\ : std_logic;
SIGNAL \Selector65~7_combout\ : std_logic;
SIGNAL \Selector65~8_combout\ : std_logic;
SIGNAL \Selector63~0_combout\ : std_logic;
SIGNAL \Selector65~9_combout\ : std_logic;
SIGNAL \Selector65~4_combout\ : std_logic;
SIGNAL \Selector65~5_combout\ : std_logic;
SIGNAL \Selector65~6_combout\ : std_logic;
SIGNAL \Selector65~13_combout\ : std_logic;
SIGNAL \Add12~5_sumout\ : std_logic;
SIGNAL \LessThan8~1_combout\ : std_logic;
SIGNAL \LessThan8~0_combout\ : std_logic;
SIGNAL \Add12~9_sumout\ : std_logic;
SIGNAL \LessThan8~2_combout\ : std_logic;
SIGNAL \always0~3_combout\ : std_logic;
SIGNAL \always0~1_combout\ : std_logic;
SIGNAL \always0~2_combout\ : std_logic;
SIGNAL \always0~0_combout\ : std_logic;
SIGNAL \always0~4_combout\ : std_logic;
SIGNAL \Add12~13_sumout\ : std_logic;
SIGNAL \LessThan8~4_combout\ : std_logic;
SIGNAL \LessThan8~3_combout\ : std_logic;
SIGNAL \always0~5_combout\ : std_logic;
SIGNAL \Equal10~0_combout\ : std_logic;
SIGNAL \Equal10~1_combout\ : std_logic;
SIGNAL \Selector65~14_combout\ : std_logic;
SIGNAL \Selector81~0_combout\ : std_logic;
SIGNAL \state.DRAW_PUCK~q\ : std_logic;
SIGNAL \draw~0_combout\ : std_logic;
SIGNAL \Selector12~1_combout\ : std_logic;
SIGNAL \Add0~22\ : std_logic;
SIGNAL \Add0~18\ : std_logic;
SIGNAL \Add0~13_sumout\ : std_logic;
SIGNAL \Add4~26\ : std_logic;
SIGNAL \Add4~22\ : std_logic;
SIGNAL \Add4~13_sumout\ : std_logic;
SIGNAL \Selector12~0_combout\ : std_logic;
SIGNAL \Selector12~2_combout\ : std_logic;
SIGNAL \Add0~14\ : std_logic;
SIGNAL \Add0~10\ : std_logic;
SIGNAL \Add0~5_sumout\ : std_logic;
SIGNAL \Selector10~1_combout\ : std_logic;
SIGNAL \Selector10~2_combout\ : std_logic;
SIGNAL \paddle_two_y~2_combout\ : std_logic;
SIGNAL \Add3~10\ : std_logic;
SIGNAL \Add3~5_sumout\ : std_logic;
SIGNAL \Selector10~0_combout\ : std_logic;
SIGNAL \Selector10~3_combout\ : std_logic;
SIGNAL \Equal6~0_combout\ : std_logic;
SIGNAL \Selector79~0_combout\ : std_logic;
SIGNAL \Selector79~1_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_TWO_LOOP~q\ : std_logic;
SIGNAL \draw.x[3]~4_combout\ : std_logic;
SIGNAL \state.ERASE_PUCK~q\ : std_logic;
SIGNAL \Add9~26\ : std_logic;
SIGNAL \Add9~21_sumout\ : std_logic;
SIGNAL \Add10~26\ : std_logic;
SIGNAL \Add10~21_sumout\ : std_logic;
SIGNAL \Add10~21_wirecell_combout\ : std_logic;
SIGNAL \puck_velocity.y[7]~DUPLICATE_q\ : std_logic;
SIGNAL \Add8~26\ : std_logic;
SIGNAL \Add8~21_sumout\ : std_logic;
SIGNAL \Equal7~2_combout\ : std_logic;
SIGNAL \Equal7~1_combout\ : std_logic;
SIGNAL \Add9~17_sumout\ : std_logic;
SIGNAL \Add10~17_sumout\ : std_logic;
SIGNAL \Add10~17_wirecell_combout\ : std_logic;
SIGNAL \Add8~17_sumout\ : std_logic;
SIGNAL \Equal7~0_combout\ : std_logic;
SIGNAL \Equal8~0_combout\ : std_logic;
SIGNAL \Equal8~1_combout\ : std_logic;
SIGNAL \Add10~1_sumout\ : std_logic;
SIGNAL \Add10~1_wirecell_combout\ : std_logic;
SIGNAL \Add8~1_sumout\ : std_logic;
SIGNAL \always0~7_combout\ : std_logic;
SIGNAL \always0~6_combout\ : std_logic;
SIGNAL \always0~8_combout\ : std_logic;
SIGNAL \always0~9_combout\ : std_logic;
SIGNAL \LessThan6~4_combout\ : std_logic;
SIGNAL \LessThan6~3_combout\ : std_logic;
SIGNAL \Add11~21_sumout\ : std_logic;
SIGNAL \LessThan6~1_combout\ : std_logic;
SIGNAL \Add11~17_sumout\ : std_logic;
SIGNAL \LessThan6~0_combout\ : std_logic;
SIGNAL \LessThan6~2_combout\ : std_logic;
SIGNAL \always0~10_combout\ : std_logic;
SIGNAL \Selector46~1_combout\ : std_logic;
SIGNAL \Add13~9_sumout\ : std_logic;
SIGNAL \Selector53~0_combout\ : std_logic;
SIGNAL \puck_velocity.x[0]~0_combout\ : std_logic;
SIGNAL \Add13~10\ : std_logic;
SIGNAL \Add13~21_sumout\ : std_logic;
SIGNAL \Selector46~0_combout\ : std_logic;
SIGNAL \Add13~22\ : std_logic;
SIGNAL \Add13~25_sumout\ : std_logic;
SIGNAL \Add13~26\ : std_logic;
SIGNAL \Add13~29_sumout\ : std_logic;
SIGNAL \Add13~30\ : std_logic;
SIGNAL \Add13~1_sumout\ : std_logic;
SIGNAL \Add13~2\ : std_logic;
SIGNAL \Add13~13_sumout\ : std_logic;
SIGNAL \Add13~14\ : std_logic;
SIGNAL \Add13~17_sumout\ : std_logic;
SIGNAL \Add13~18\ : std_logic;
SIGNAL \Add13~5_sumout\ : std_logic;
SIGNAL \Add7~18\ : std_logic;
SIGNAL \Add7~5_sumout\ : std_logic;
SIGNAL \Equal9~2_combout\ : std_logic;
SIGNAL \Selector65~18_combout\ : std_logic;
SIGNAL \Selector65~15_combout\ : std_logic;
SIGNAL \Selector65~16_combout\ : std_logic;
SIGNAL \Selector65~17_combout\ : std_logic;
SIGNAL \Selector65~19_combout\ : std_logic;
SIGNAL \state.INIT~q\ : std_logic;
SIGNAL \Selector16~0_combout\ : std_logic;
SIGNAL \Add3~26\ : std_logic;
SIGNAL \Add3~22\ : std_logic;
SIGNAL \Add3~13_sumout\ : std_logic;
SIGNAL \Selector20~0_combout\ : std_logic;
SIGNAL \Add3~14\ : std_logic;
SIGNAL \Add3~9_sumout\ : std_logic;
SIGNAL \Add4~14\ : std_logic;
SIGNAL \Add4~9_sumout\ : std_logic;
SIGNAL \Selector19~0_combout\ : std_logic;
SIGNAL \Add4~10\ : std_logic;
SIGNAL \Add4~5_sumout\ : std_logic;
SIGNAL \Selector18~0_combout\ : std_logic;
SIGNAL \Add3~6\ : std_logic;
SIGNAL \Add3~1_sumout\ : std_logic;
SIGNAL \Add4~6\ : std_logic;
SIGNAL \Add4~1_sumout\ : std_logic;
SIGNAL \Selector9~1_combout\ : std_logic;
SIGNAL \Add3~2\ : std_logic;
SIGNAL \Add3~17_sumout\ : std_logic;
SIGNAL \Add4~2\ : std_logic;
SIGNAL \Add4~17_sumout\ : std_logic;
SIGNAL \Selector8~1_combout\ : std_logic;
SIGNAL \paddle_one_y~0_combout\ : std_logic;
SIGNAL \paddle_one_y~1_combout\ : std_logic;
SIGNAL \paddle_one_y~2_combout\ : std_logic;
SIGNAL \Add4~21_sumout\ : std_logic;
SIGNAL \Selector21~0_combout\ : std_logic;
SIGNAL \Add3~21_sumout\ : std_logic;
SIGNAL \Selector13~0_combout\ : std_logic;
SIGNAL \Selector13~4_combout\ : std_logic;
SIGNAL \Selector13~5_combout\ : std_logic;
SIGNAL \Add0~17_sumout\ : std_logic;
SIGNAL \Selector13~6_combout\ : std_logic;
SIGNAL \paddle_two_y~5_combout\ : std_logic;
SIGNAL \Selector13~1_combout\ : std_logic;
SIGNAL \Selector13~2_combout\ : std_logic;
SIGNAL \Selector13~3_combout\ : std_logic;
SIGNAL \Selector13~7_combout\ : std_logic;
SIGNAL \Equal4~1_combout\ : std_logic;
SIGNAL \Equal4~4_combout\ : std_logic;
SIGNAL \Selector65~3_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_ONE_ENTER~q\ : std_logic;
SIGNAL \Selector75~1_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_ONE_LOOP~q\ : std_logic;
SIGNAL \Selector67~1_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_TWO_ENTER~q\ : std_logic;
SIGNAL \Selector8~2_combout\ : std_logic;
SIGNAL \Add0~2\ : std_logic;
SIGNAL \Add0~29_sumout\ : std_logic;
SIGNAL \Selector8~3_combout\ : std_logic;
SIGNAL \Selector67~2_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_TWO_ENTER~q\ : std_logic;
SIGNAL \Selector11~1_combout\ : std_logic;
SIGNAL \Selector11~2_combout\ : std_logic;
SIGNAL \Selector11~0_combout\ : std_logic;
SIGNAL \Add0~9_sumout\ : std_logic;
SIGNAL \paddle_two_y~3_combout\ : std_logic;
SIGNAL \Selector11~3_combout\ : std_logic;
SIGNAL \vga_u0|LessThan3~0_combout\ : std_logic;
SIGNAL \Selector64~1_combout\ : std_logic;
SIGNAL \Selector64~0_combout\ : std_logic;
SIGNAL \Selector64~2_combout\ : std_logic;
SIGNAL \plot~q\ : std_logic;
SIGNAL \draw.x[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|writeEn~0_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~6\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~7\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~2\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~3\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~5_sumout\ : std_logic;
SIGNAL \Selector63~1_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \vga_u0|controller|VGA_R[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_G[0]~0_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_B[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK~q\ : std_logic;
SIGNAL \puck.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|controller|yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \puck.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|controller|xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \puck_velocity.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL clock_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL \puck_velocity.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \draw.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \draw.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL colour : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL paddle_two_y : std_logic_vector(7 DOWNTO 0);
SIGNAL paddle_one_y : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \ALT_INV_draw.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Equal6~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal6~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector9~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector9~2_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_BOTTOM_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_BOTTOM_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_draw~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PUCK~q\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PUCK~q\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_Selector9~1_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_one_y~1_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_one_y~0_combout\ : std_logic;
SIGNAL ALT_INV_paddle_one_y : std_logic_vector(7 DOWNTO 1);
SIGNAL \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_Selector9~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~1_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~0_combout\ : std_logic;
SIGNAL ALT_INV_paddle_two_y : std_logic_vector(7 DOWNTO 1);
SIGNAL \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_WideOr16~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.START~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_draw.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL ALT_INV_colour : std_logic_vector(1 DOWNTO 1);
SIGNAL \vga_u0|ALT_INV_writeEn~0_combout\ : std_logic;
SIGNAL \ALT_INV_plot~q\ : std_logic;
SIGNAL \vga_u0|ALT_INV_LessThan3~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_on_screen~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_LessThan7~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~6_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_one_y~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector53~0_combout\ : std_logic;
SIGNAL ALT_INV_clock_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_Add9~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~1_sumout\ : std_logic;
SIGNAL \ALT_INV_puck_velocity.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_puck_velocity.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_puck.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Add0~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~29_sumout\ : std_logic;
SIGNAL \ALT_INV_puck.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add4~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add3~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add6~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add5~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ : std_logic;
SIGNAL \ALT_INV_puck_velocity.x[0]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.IDLE~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_draw.x[6]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_velocity.y[7]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_KEY[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_Equal7~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal7~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal8~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal8~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal7~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector46~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~18_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~17_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~16_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~15_combout\ : std_logic;
SIGNAL \ALT_INV_Equal9~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~9_combout\ : std_logic;
SIGNAL \ALT_INV_Selector8~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector15~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector15~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~7_combout\ : std_logic;
SIGNAL \ALT_INV_Equal6~4_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~5_combout\ : std_logic;
SIGNAL \ALT_INV_Selector14~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~6_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~5_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~6_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~5_combout\ : std_logic;
SIGNAL \ALT_INV_Equal2~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector72~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector72~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector72~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector72~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~14_combout\ : std_logic;
SIGNAL \ALT_INV_Equal10~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal10~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~13_combout\ : std_logic;
SIGNAL \ALT_INV_Equal9~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal9~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~12_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~11_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~10_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~8_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~7_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~6_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~5_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~4_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~9_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~8_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~7_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~6_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~5_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~4_combout\ : std_logic;
SIGNAL \ALT_INV_always0~10_combout\ : std_logic;
SIGNAL \ALT_INV_always0~9_combout\ : std_logic;
SIGNAL \ALT_INV_always0~8_combout\ : std_logic;
SIGNAL \ALT_INV_always0~7_combout\ : std_logic;
SIGNAL \ALT_INV_always0~6_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~4_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~0_combout\ : std_logic;
SIGNAL \ALT_INV_always0~5_combout\ : std_logic;
SIGNAL \ALT_INV_always0~4_combout\ : std_logic;
SIGNAL \ALT_INV_always0~3_combout\ : std_logic;
SIGNAL \ALT_INV_always0~2_combout\ : std_logic;
SIGNAL \ALT_INV_always0~1_combout\ : std_logic;
SIGNAL \ALT_INV_always0~0_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan8~4_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan8~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan8~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan8~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan8~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector14~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector8~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector14~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector8~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector67~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~3_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector77~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal6~3_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector63~0_combout\ : std_logic;
SIGNAL \ALT_INV_WideOr16~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector64~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector64~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector79~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector75~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~8_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~7_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~6_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~5_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector0~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~3_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~2_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_TOP_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_draw.x[3]~1_combout\ : std_logic;
SIGNAL \ALT_INV_WideOr16~1_combout\ : std_logic;
SIGNAL \ALT_INV_state.INIT~q\ : std_logic;
SIGNAL \ALT_INV_Selector12~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector12~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector11~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector11~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector11~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector10~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector10~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector10~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_two_y~2_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y[4]~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[5]~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_TOP_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.IDLE~q\ : std_logic;
SIGNAL \ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector65~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal6~2_combout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
VGA_R <= ww_VGA_R;
VGA_G <= ww_VGA_G;
VGA_B <= ww_VGA_B;
VGA_HS <= ww_VGA_HS;
VGA_VS <= ww_VGA_VS;
VGA_BLANK <= ww_VGA_BLANK;
VGA_SYNC <= ww_VGA_SYNC;
VGA_CLK <= ww_VGA_CLK;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ <= (colour(1) & colour(1));

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & \draw.x\(3) & \draw.x\(2) & \draw.x\(1) & 
\draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(0);
\vga_u0|VideoMemory|auto_generated|ram_block1a8\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ <= (gnd & \~GND~combout\);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & \draw.x\(3) & \draw.x\(2) & \draw.x\(1) & 
\draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(4) & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2));

\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ <= (gnd & gnd & gnd & \CLOCK_50~input_o\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\(6);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\
& \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\);
\ALT_INV_draw.y\(7) <= NOT \draw.y\(7);
\ALT_INV_Equal6~1_combout\ <= NOT \Equal6~1_combout\;
\ALT_INV_draw.y\(0) <= NOT \draw.y\(0);
\ALT_INV_draw.y\(1) <= NOT \draw.y\(1);
\ALT_INV_draw.y\(2) <= NOT \draw.y\(2);
\ALT_INV_Equal6~0_combout\ <= NOT \Equal6~0_combout\;
\ALT_INV_Selector9~3_combout\ <= NOT \Selector9~3_combout\;
\ALT_INV_Selector9~2_combout\ <= NOT \Selector9~2_combout\;
\ALT_INV_state.DRAW_BOTTOM_ENTER~q\ <= NOT \state.DRAW_BOTTOM_ENTER~q\;
\ALT_INV_state.DRAW_BOTTOM_LOOP~q\ <= NOT \state.DRAW_BOTTOM_LOOP~q\;
\ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\ <= NOT \state.ERASE_PADDLE_ONE_ENTER~q\;
\ALT_INV_draw~0_combout\ <= NOT \draw~0_combout\;
\ALT_INV_state.DRAW_PUCK~q\ <= NOT \state.DRAW_PUCK~q\;
\ALT_INV_state.ERASE_PUCK~q\ <= NOT \state.ERASE_PUCK~q\;
\ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\ <= NOT \state.ERASE_PADDLE_TWO_ENTER~q\;
\ALT_INV_Selector9~1_combout\ <= NOT \Selector9~1_combout\;
\ALT_INV_paddle_one_y~1_combout\ <= NOT \paddle_one_y~1_combout\;
\ALT_INV_paddle_one_y~0_combout\ <= NOT \paddle_one_y~0_combout\;
ALT_INV_paddle_one_y(3) <= NOT paddle_one_y(3);
ALT_INV_paddle_one_y(4) <= NOT paddle_one_y(4);
ALT_INV_paddle_one_y(5) <= NOT paddle_one_y(5);
ALT_INV_paddle_one_y(1) <= NOT paddle_one_y(1);
ALT_INV_paddle_one_y(2) <= NOT paddle_one_y(2);
ALT_INV_paddle_one_y(7) <= NOT paddle_one_y(7);
\ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\ <= NOT \state.DRAW_PADDLE_ONE_ENTER~q\;
ALT_INV_paddle_one_y(6) <= NOT paddle_one_y(6);
\ALT_INV_Selector9~0_combout\ <= NOT \Selector9~0_combout\;
\ALT_INV_paddle_two_y~1_combout\ <= NOT \paddle_two_y~1_combout\;
\ALT_INV_paddle_two_y~0_combout\ <= NOT \paddle_two_y~0_combout\;
ALT_INV_paddle_two_y(3) <= NOT paddle_two_y(3);
ALT_INV_paddle_two_y(4) <= NOT paddle_two_y(4);
ALT_INV_paddle_two_y(5) <= NOT paddle_two_y(5);
ALT_INV_paddle_two_y(1) <= NOT paddle_two_y(1);
ALT_INV_paddle_two_y(2) <= NOT paddle_two_y(2);
ALT_INV_paddle_two_y(7) <= NOT paddle_two_y(7);
\ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\ <= NOT \state.DRAW_PADDLE_TWO_ENTER~q\;
ALT_INV_paddle_two_y(6) <= NOT paddle_two_y(6);
\ALT_INV_WideOr16~0_combout\ <= NOT \WideOr16~0_combout\;
\ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\ <= NOT \state.ERASE_PADDLE_ONE_LOOP~q\;
\ALT_INV_state.ERASE_PADDLE_TWO_LOOP~q\ <= NOT \state.ERASE_PADDLE_TWO_LOOP~q\;
\ALT_INV_state.START~q\ <= NOT \state.START~q\;
\ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\ <= NOT \state.DRAW_PADDLE_TWO_LOOP~q\;
\ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\ <= NOT \state.DRAW_PADDLE_ONE_LOOP~q\;
\ALT_INV_draw.x\(4) <= NOT \draw.x\(4);
\ALT_INV_draw.x\(3) <= NOT \draw.x\(3);
\ALT_INV_draw.x\(2) <= NOT \draw.x\(2);
\ALT_INV_draw.x\(1) <= NOT \draw.x\(1);
\ALT_INV_draw.x\(0) <= NOT \draw.x\(0);
ALT_INV_colour(1) <= NOT colour(1);
\vga_u0|ALT_INV_writeEn~0_combout\ <= NOT \vga_u0|writeEn~0_combout\;
\ALT_INV_plot~q\ <= NOT \plot~q\;
\ALT_INV_draw.x\(5) <= NOT \draw.x\(5);
\ALT_INV_draw.x\(6) <= NOT \draw.x\(6);
\ALT_INV_draw.x\(7) <= NOT \draw.x\(7);
\vga_u0|ALT_INV_LessThan3~0_combout\ <= NOT \vga_u0|LessThan3~0_combout\;
\ALT_INV_draw.y\(3) <= NOT \draw.y\(3);
\ALT_INV_draw.y\(4) <= NOT \draw.y\(4);
\ALT_INV_draw.y\(5) <= NOT \draw.y\(5);
\ALT_INV_draw.y\(6) <= NOT \draw.y\(6);
\vga_u0|controller|ALT_INV_always1~1_combout\ <= NOT \vga_u0|controller|always1~1_combout\;
\vga_u0|controller|ALT_INV_always1~0_combout\ <= NOT \vga_u0|controller|always1~0_combout\;
\vga_u0|controller|ALT_INV_Equal0~1_combout\ <= NOT \vga_u0|controller|Equal0~1_combout\;
\vga_u0|controller|ALT_INV_Equal0~0_combout\ <= NOT \vga_u0|controller|Equal0~0_combout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1);
\vga_u0|controller|ALT_INV_on_screen~1_combout\ <= NOT \vga_u0|controller|on_screen~1_combout\;
\vga_u0|controller|ALT_INV_on_screen~0_combout\ <= NOT \vga_u0|controller|on_screen~0_combout\;
\vga_u0|controller|ALT_INV_LessThan7~0_combout\ <= NOT \vga_u0|controller|LessThan7~0_combout\;
\ALT_INV_paddle_two_y~6_combout\ <= NOT \paddle_two_y~6_combout\;
\ALT_INV_paddle_one_y~2_combout\ <= NOT \paddle_one_y~2_combout\;
\ALT_INV_Selector53~0_combout\ <= NOT \Selector53~0_combout\;
ALT_INV_clock_counter(0) <= NOT clock_counter(0);
ALT_INV_clock_counter(1) <= NOT clock_counter(1);
\ALT_INV_Add9~29_sumout\ <= NOT \Add9~29_sumout\;
\ALT_INV_Add9~25_sumout\ <= NOT \Add9~25_sumout\;
\ALT_INV_Add9~21_sumout\ <= NOT \Add9~21_sumout\;
\ALT_INV_Add9~17_sumout\ <= NOT \Add9~17_sumout\;
\ALT_INV_Add9~13_sumout\ <= NOT \Add9~13_sumout\;
\ALT_INV_Add9~9_sumout\ <= NOT \Add9~9_sumout\;
\ALT_INV_Add9~5_sumout\ <= NOT \Add9~5_sumout\;
\ALT_INV_Add9~1_sumout\ <= NOT \Add9~1_sumout\;
\ALT_INV_Add13~9_sumout\ <= NOT \Add13~9_sumout\;
\ALT_INV_Add10~29_sumout\ <= NOT \Add10~29_sumout\;
\ALT_INV_Add10~25_sumout\ <= NOT \Add10~25_sumout\;
\ALT_INV_Add10~21_sumout\ <= NOT \Add10~21_sumout\;
\ALT_INV_Add10~17_sumout\ <= NOT \Add10~17_sumout\;
\ALT_INV_Add10~13_sumout\ <= NOT \Add10~13_sumout\;
\ALT_INV_Add10~9_sumout\ <= NOT \Add10~9_sumout\;
\ALT_INV_Add10~5_sumout\ <= NOT \Add10~5_sumout\;
\ALT_INV_Add10~1_sumout\ <= NOT \Add10~1_sumout\;
\ALT_INV_puck_velocity.x\(3) <= NOT \puck_velocity.x\(3);
\ALT_INV_puck_velocity.x\(2) <= NOT \puck_velocity.x\(2);
\ALT_INV_puck_velocity.x\(1) <= NOT \puck_velocity.x\(1);
\ALT_INV_puck_velocity.x\(6) <= NOT \puck_velocity.x\(6);
\ALT_INV_puck_velocity.x\(5) <= NOT \puck_velocity.x\(5);
\ALT_INV_puck_velocity.x\(7) <= NOT \puck_velocity.x\(7);
\ALT_INV_puck_velocity.x\(4) <= NOT \puck_velocity.x\(4);
\ALT_INV_puck_velocity.y\(5) <= NOT \puck_velocity.y\(5);
\ALT_INV_puck_velocity.y\(6) <= NOT \puck_velocity.y\(6);
\ALT_INV_puck_velocity.y\(7) <= NOT \puck_velocity.y\(7);
\ALT_INV_puck_velocity.y\(0) <= NOT \puck_velocity.y\(0);
\ALT_INV_puck_velocity.y\(1) <= NOT \puck_velocity.y\(1);
\ALT_INV_puck_velocity.y\(2) <= NOT \puck_velocity.y\(2);
\ALT_INV_puck_velocity.y\(3) <= NOT \puck_velocity.y\(3);
\ALT_INV_puck_velocity.y\(4) <= NOT \puck_velocity.y\(4);
\ALT_INV_puck.y\(7) <= NOT \puck.y\(7);
\ALT_INV_Add0~29_sumout\ <= NOT \Add0~29_sumout\;
\ALT_INV_puck.y\(0) <= NOT \puck.y\(0);
\ALT_INV_Add0~25_sumout\ <= NOT \Add0~25_sumout\;
\ALT_INV_puck.y\(1) <= NOT \puck.y\(1);
\ALT_INV_Add0~21_sumout\ <= NOT \Add0~21_sumout\;
\ALT_INV_puck.y\(2) <= NOT \puck.y\(2);
\ALT_INV_Add0~17_sumout\ <= NOT \Add0~17_sumout\;
\ALT_INV_Add7~29_sumout\ <= NOT \Add7~29_sumout\;
\ALT_INV_Add7~25_sumout\ <= NOT \Add7~25_sumout\;
\ALT_INV_Add7~21_sumout\ <= NOT \Add7~21_sumout\;
\ALT_INV_Add7~17_sumout\ <= NOT \Add7~17_sumout\;
\ALT_INV_Add7~13_sumout\ <= NOT \Add7~13_sumout\;
\ALT_INV_Add7~9_sumout\ <= NOT \Add7~9_sumout\;
\ALT_INV_Add7~5_sumout\ <= NOT \Add7~5_sumout\;
\ALT_INV_Add7~1_sumout\ <= NOT \Add7~1_sumout\;
ALT_INV_clock_counter(30) <= NOT clock_counter(30);
ALT_INV_clock_counter(29) <= NOT clock_counter(29);
ALT_INV_clock_counter(28) <= NOT clock_counter(28);
ALT_INV_clock_counter(27) <= NOT clock_counter(27);
ALT_INV_clock_counter(24) <= NOT clock_counter(24);
ALT_INV_clock_counter(23) <= NOT clock_counter(23);
ALT_INV_clock_counter(22) <= NOT clock_counter(22);
ALT_INV_clock_counter(21) <= NOT clock_counter(21);
ALT_INV_clock_counter(26) <= NOT clock_counter(26);
ALT_INV_clock_counter(25) <= NOT clock_counter(25);
ALT_INV_clock_counter(31) <= NOT clock_counter(31);
ALT_INV_clock_counter(20) <= NOT clock_counter(20);
ALT_INV_clock_counter(18) <= NOT clock_counter(18);
ALT_INV_clock_counter(17) <= NOT clock_counter(17);
ALT_INV_clock_counter(16) <= NOT clock_counter(16);
ALT_INV_clock_counter(15) <= NOT clock_counter(15);
ALT_INV_clock_counter(14) <= NOT clock_counter(14);
ALT_INV_clock_counter(11) <= NOT clock_counter(11);
ALT_INV_clock_counter(9) <= NOT clock_counter(9);
ALT_INV_clock_counter(8) <= NOT clock_counter(8);
ALT_INV_clock_counter(7) <= NOT clock_counter(7);
ALT_INV_clock_counter(10) <= NOT clock_counter(10);
ALT_INV_clock_counter(6) <= NOT clock_counter(6);
ALT_INV_clock_counter(5) <= NOT clock_counter(5);
ALT_INV_clock_counter(4) <= NOT clock_counter(4);
ALT_INV_clock_counter(2) <= NOT clock_counter(2);
ALT_INV_clock_counter(3) <= NOT clock_counter(3);
ALT_INV_clock_counter(12) <= NOT clock_counter(12);
ALT_INV_clock_counter(13) <= NOT clock_counter(13);
ALT_INV_clock_counter(19) <= NOT clock_counter(19);
\ALT_INV_Add8~29_sumout\ <= NOT \Add8~29_sumout\;
\ALT_INV_Add8~25_sumout\ <= NOT \Add8~25_sumout\;
\ALT_INV_Add8~21_sumout\ <= NOT \Add8~21_sumout\;
\ALT_INV_Add8~17_sumout\ <= NOT \Add8~17_sumout\;
\ALT_INV_Add8~13_sumout\ <= NOT \Add8~13_sumout\;
\ALT_INV_Add8~9_sumout\ <= NOT \Add8~9_sumout\;
\ALT_INV_Add8~5_sumout\ <= NOT \Add8~5_sumout\;
\ALT_INV_Add8~1_sumout\ <= NOT \Add8~1_sumout\;
\ALT_INV_Add3~25_sumout\ <= NOT \Add3~25_sumout\;
\ALT_INV_Add4~25_sumout\ <= NOT \Add4~25_sumout\;
\ALT_INV_Add3~21_sumout\ <= NOT \Add3~21_sumout\;
\ALT_INV_Add4~21_sumout\ <= NOT \Add4~21_sumout\;
\ALT_INV_Add4~17_sumout\ <= NOT \Add4~17_sumout\;
\ALT_INV_Add3~17_sumout\ <= NOT \Add3~17_sumout\;
\ALT_INV_Add5~25_sumout\ <= NOT \Add5~25_sumout\;
\ALT_INV_Add6~25_sumout\ <= NOT \Add6~25_sumout\;
\ALT_INV_Add5~21_sumout\ <= NOT \Add5~21_sumout\;
\ALT_INV_Add6~21_sumout\ <= NOT \Add6~21_sumout\;
\ALT_INV_Add6~17_sumout\ <= NOT \Add6~17_sumout\;
\ALT_INV_Add5~17_sumout\ <= NOT \Add5~17_sumout\;
\ALT_INV_Add1~29_sumout\ <= NOT \Add1~29_sumout\;
\ALT_INV_puck.x\(4) <= NOT \puck.x\(4);
\ALT_INV_Add1~25_sumout\ <= NOT \Add1~25_sumout\;
\ALT_INV_puck.x\(3) <= NOT \puck.x\(3);
\ALT_INV_puck.x\(2) <= NOT \puck.x\(2);
\ALT_INV_Add1~21_sumout\ <= NOT \Add1~21_sumout\;
\ALT_INV_puck.x\(1) <= NOT \puck.x\(1);
\ALT_INV_Add1~17_sumout\ <= NOT \Add1~17_sumout\;
\ALT_INV_Add1~13_sumout\ <= NOT \Add1~13_sumout\;
\ALT_INV_puck.x\(0) <= NOT \puck.x\(0);
\ALT_INV_Add1~9_sumout\ <= NOT \Add1~9_sumout\;
\ALT_INV_puck.x\(5) <= NOT \puck.x\(5);
\ALT_INV_Add1~5_sumout\ <= NOT \Add1~5_sumout\;
\ALT_INV_puck.x\(6) <= NOT \puck.x\(6);
\ALT_INV_Add1~1_sumout\ <= NOT \Add1~1_sumout\;
\ALT_INV_puck.x\(7) <= NOT \puck.x\(7);
\ALT_INV_puck.y\(3) <= NOT \puck.y\(3);
\ALT_INV_Add0~13_sumout\ <= NOT \Add0~13_sumout\;
\ALT_INV_Add3~13_sumout\ <= NOT \Add3~13_sumout\;
\ALT_INV_Add4~13_sumout\ <= NOT \Add4~13_sumout\;
\ALT_INV_Add5~13_sumout\ <= NOT \Add5~13_sumout\;
\ALT_INV_Add6~13_sumout\ <= NOT \Add6~13_sumout\;
\ALT_INV_puck.y\(4) <= NOT \puck.y\(4);
\ALT_INV_Add0~9_sumout\ <= NOT \Add0~9_sumout\;
\ALT_INV_Add3~9_sumout\ <= NOT \Add3~9_sumout\;
\ALT_INV_Add4~9_sumout\ <= NOT \Add4~9_sumout\;
\ALT_INV_Add5~9_sumout\ <= NOT \Add5~9_sumout\;
\ALT_INV_Add6~9_sumout\ <= NOT \Add6~9_sumout\;
\ALT_INV_puck.y\(5) <= NOT \puck.y\(5);
\ALT_INV_Add0~5_sumout\ <= NOT \Add0~5_sumout\;
\ALT_INV_Add4~5_sumout\ <= NOT \Add4~5_sumout\;
\ALT_INV_Add3~5_sumout\ <= NOT \Add3~5_sumout\;
\ALT_INV_Add6~5_sumout\ <= NOT \Add6~5_sumout\;
\ALT_INV_Add5~5_sumout\ <= NOT \Add5~5_sumout\;
\ALT_INV_Add11~29_sumout\ <= NOT \Add11~29_sumout\;
\ALT_INV_Add11~25_sumout\ <= NOT \Add11~25_sumout\;
\ALT_INV_Add11~21_sumout\ <= NOT \Add11~21_sumout\;
\ALT_INV_Add11~17_sumout\ <= NOT \Add11~17_sumout\;
\ALT_INV_Add11~13_sumout\ <= NOT \Add11~13_sumout\;
\ALT_INV_Add11~9_sumout\ <= NOT \Add11~9_sumout\;
\ALT_INV_Add11~5_sumout\ <= NOT \Add11~5_sumout\;
\ALT_INV_Add11~1_sumout\ <= NOT \Add11~1_sumout\;
\ALT_INV_Add12~29_sumout\ <= NOT \Add12~29_sumout\;
\ALT_INV_Add12~25_sumout\ <= NOT \Add12~25_sumout\;
\ALT_INV_Add12~21_sumout\ <= NOT \Add12~21_sumout\;
\ALT_INV_Add12~17_sumout\ <= NOT \Add12~17_sumout\;
\ALT_INV_Add12~13_sumout\ <= NOT \Add12~13_sumout\;
\ALT_INV_Add12~9_sumout\ <= NOT \Add12~9_sumout\;
\ALT_INV_Add12~5_sumout\ <= NOT \Add12~5_sumout\;
\ALT_INV_Add12~1_sumout\ <= NOT \Add12~1_sumout\;
\ALT_INV_puck.y\(6) <= NOT \puck.y\(6);
\ALT_INV_Add4~1_sumout\ <= NOT \Add4~1_sumout\;
\ALT_INV_Add3~1_sumout\ <= NOT \Add3~1_sumout\;
\ALT_INV_Add6~1_sumout\ <= NOT \Add6~1_sumout\;
\ALT_INV_Add5~1_sumout\ <= NOT \Add5~1_sumout\;
\ALT_INV_Add0~1_sumout\ <= NOT \Add0~1_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~5_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~1_sumout\;
\vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|user_input_translator|Add1~5_sumout\;
\vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|user_input_translator|Add1~1_sumout\;
\vga_u0|controller|ALT_INV_yCounter\(0) <= NOT \vga_u0|controller|yCounter\(0);
\vga_u0|controller|ALT_INV_yCounter\(1) <= NOT \vga_u0|controller|yCounter\(1);
\vga_u0|controller|ALT_INV_yCounter\(2) <= NOT \vga_u0|controller|yCounter\(2);
\vga_u0|controller|ALT_INV_yCounter\(3) <= NOT \vga_u0|controller|yCounter\(3);
\vga_u0|controller|ALT_INV_yCounter\(4) <= NOT \vga_u0|controller|yCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(0) <= NOT \vga_u0|controller|xCounter\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a8\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\;
\vga_u0|controller|ALT_INV_xCounter\(2) <= NOT \vga_u0|controller|xCounter\(2);
\vga_u0|controller|ALT_INV_xCounter\(3) <= NOT \vga_u0|controller|xCounter\(3);
\vga_u0|controller|ALT_INV_xCounter\(5) <= NOT \vga_u0|controller|xCounter\(5);
\vga_u0|controller|ALT_INV_xCounter\(6) <= NOT \vga_u0|controller|xCounter\(6);
\vga_u0|controller|ALT_INV_xCounter\(1) <= NOT \vga_u0|controller|xCounter\(1);
\vga_u0|controller|ALT_INV_xCounter\(4) <= NOT \vga_u0|controller|xCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(7) <= NOT \vga_u0|controller|xCounter\(7);
\vga_u0|controller|ALT_INV_xCounter\(8) <= NOT \vga_u0|controller|xCounter\(8);
\vga_u0|controller|ALT_INV_xCounter\(9) <= NOT \vga_u0|controller|xCounter\(9);
\vga_u0|controller|ALT_INV_yCounter\(6) <= NOT \vga_u0|controller|yCounter\(6);
\vga_u0|controller|ALT_INV_yCounter\(5) <= NOT \vga_u0|controller|yCounter\(5);
\vga_u0|controller|ALT_INV_yCounter\(8) <= NOT \vga_u0|controller|yCounter\(8);
\vga_u0|controller|ALT_INV_yCounter\(7) <= NOT \vga_u0|controller|yCounter\(7);
\vga_u0|controller|ALT_INV_yCounter\(9) <= NOT \vga_u0|controller|yCounter\(9);
\ALT_INV_puck_velocity.x[0]~DUPLICATE_q\ <= NOT \puck_velocity.x[0]~DUPLICATE_q\;
\ALT_INV_state.IDLE~DUPLICATE_q\ <= NOT \state.IDLE~DUPLICATE_q\;
\ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ <= NOT \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\;
\ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ <= NOT \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\;
\ALT_INV_draw.x[6]~DUPLICATE_q\ <= NOT \draw.x[6]~DUPLICATE_q\;
\ALT_INV_puck_velocity.y[7]~DUPLICATE_q\ <= NOT \puck_velocity.y[7]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[2]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[3]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[7]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[8]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[6]~DUPLICATE_q\;
\ALT_INV_KEY[3]~input_o\ <= NOT \KEY[3]~input_o\;
\ALT_INV_KEY[2]~input_o\ <= NOT \KEY[2]~input_o\;
\ALT_INV_KEY[1]~input_o\ <= NOT \KEY[1]~input_o\;
\ALT_INV_KEY[0]~input_o\ <= NOT \KEY[0]~input_o\;
\ALT_INV_KEY[8]~input_o\ <= NOT \KEY[8]~input_o\;
\ALT_INV_Equal7~2_combout\ <= NOT \Equal7~2_combout\;
\ALT_INV_Equal7~1_combout\ <= NOT \Equal7~1_combout\;
\ALT_INV_Equal8~1_combout\ <= NOT \Equal8~1_combout\;
\ALT_INV_Equal8~0_combout\ <= NOT \Equal8~0_combout\;
\ALT_INV_Equal7~0_combout\ <= NOT \Equal7~0_combout\;
\ALT_INV_Selector46~1_combout\ <= NOT \Selector46~1_combout\;
\ALT_INV_puck_velocity.x\(0) <= NOT \puck_velocity.x\(0);
\ALT_INV_Selector65~18_combout\ <= NOT \Selector65~18_combout\;
\ALT_INV_Selector65~17_combout\ <= NOT \Selector65~17_combout\;
\ALT_INV_Selector65~16_combout\ <= NOT \Selector65~16_combout\;
\ALT_INV_Selector65~15_combout\ <= NOT \Selector65~15_combout\;
\ALT_INV_Equal9~2_combout\ <= NOT \Equal9~2_combout\;
\ALT_INV_LessThan0~9_combout\ <= NOT \LessThan0~9_combout\;
\ALT_INV_Selector8~2_combout\ <= NOT \Selector8~2_combout\;
\ALT_INV_Selector15~1_combout\ <= NOT \Selector15~1_combout\;
\ALT_INV_Selector15~0_combout\ <= NOT \Selector15~0_combout\;
\ALT_INV_Selector6~7_combout\ <= NOT \Selector6~7_combout\;
\ALT_INV_Equal6~4_combout\ <= NOT \Equal6~4_combout\;
\ALT_INV_Equal4~5_combout\ <= NOT \Equal4~5_combout\;
\ALT_INV_Selector14~2_combout\ <= NOT \Selector14~2_combout\;
\ALT_INV_Selector13~6_combout\ <= NOT \Selector13~6_combout\;
\ALT_INV_Selector13~5_combout\ <= NOT \Selector13~5_combout\;
\ALT_INV_Selector13~4_combout\ <= NOT \Selector13~4_combout\;
\ALT_INV_Selector6~6_combout\ <= NOT \Selector6~6_combout\;
\ALT_INV_Selector13~3_combout\ <= NOT \Selector13~3_combout\;
\ALT_INV_Selector13~2_combout\ <= NOT \Selector13~2_combout\;
\ALT_INV_Selector13~1_combout\ <= NOT \Selector13~1_combout\;
\ALT_INV_Selector13~0_combout\ <= NOT \Selector13~0_combout\;
\ALT_INV_paddle_two_y~5_combout\ <= NOT \paddle_two_y~5_combout\;
\ALT_INV_Equal2~0_combout\ <= NOT \Equal2~0_combout\;
\ALT_INV_Selector72~3_combout\ <= NOT \Selector72~3_combout\;
\ALT_INV_Selector72~2_combout\ <= NOT \Selector72~2_combout\;
\ALT_INV_Selector72~1_combout\ <= NOT \Selector72~1_combout\;
\ALT_INV_Selector72~0_combout\ <= NOT \Selector72~0_combout\;
\ALT_INV_Selector65~14_combout\ <= NOT \Selector65~14_combout\;
\ALT_INV_Equal10~1_combout\ <= NOT \Equal10~1_combout\;
\ALT_INV_Equal10~0_combout\ <= NOT \Equal10~0_combout\;
\ALT_INV_Selector65~13_combout\ <= NOT \Selector65~13_combout\;
\ALT_INV_Equal9~1_combout\ <= NOT \Equal9~1_combout\;
\ALT_INV_Equal9~0_combout\ <= NOT \Equal9~0_combout\;
\ALT_INV_Selector65~12_combout\ <= NOT \Selector65~12_combout\;
\ALT_INV_Selector65~11_combout\ <= NOT \Selector65~11_combout\;
\ALT_INV_Selector65~10_combout\ <= NOT \Selector65~10_combout\;
\ALT_INV_LessThan0~8_combout\ <= NOT \LessThan0~8_combout\;
\ALT_INV_LessThan0~7_combout\ <= NOT \LessThan0~7_combout\;
\ALT_INV_LessThan0~6_combout\ <= NOT \LessThan0~6_combout\;
\ALT_INV_LessThan0~5_combout\ <= NOT \LessThan0~5_combout\;
\ALT_INV_LessThan0~4_combout\ <= NOT \LessThan0~4_combout\;
\ALT_INV_LessThan0~3_combout\ <= NOT \LessThan0~3_combout\;
\ALT_INV_LessThan0~2_combout\ <= NOT \LessThan0~2_combout\;
\ALT_INV_LessThan0~1_combout\ <= NOT \LessThan0~1_combout\;
\ALT_INV_LessThan0~0_combout\ <= NOT \LessThan0~0_combout\;
\ALT_INV_Selector65~9_combout\ <= NOT \Selector65~9_combout\;
\ALT_INV_Selector65~8_combout\ <= NOT \Selector65~8_combout\;
\ALT_INV_Selector65~7_combout\ <= NOT \Selector65~7_combout\;
\ALT_INV_Selector65~6_combout\ <= NOT \Selector65~6_combout\;
\ALT_INV_Selector65~5_combout\ <= NOT \Selector65~5_combout\;
\ALT_INV_Selector65~4_combout\ <= NOT \Selector65~4_combout\;
\ALT_INV_always0~10_combout\ <= NOT \always0~10_combout\;
\ALT_INV_always0~9_combout\ <= NOT \always0~9_combout\;
\ALT_INV_always0~8_combout\ <= NOT \always0~8_combout\;
\ALT_INV_always0~7_combout\ <= NOT \always0~7_combout\;
\ALT_INV_always0~6_combout\ <= NOT \always0~6_combout\;
\ALT_INV_LessThan6~4_combout\ <= NOT \LessThan6~4_combout\;
\ALT_INV_LessThan6~3_combout\ <= NOT \LessThan6~3_combout\;
\ALT_INV_LessThan6~2_combout\ <= NOT \LessThan6~2_combout\;
\ALT_INV_LessThan6~1_combout\ <= NOT \LessThan6~1_combout\;
\ALT_INV_LessThan6~0_combout\ <= NOT \LessThan6~0_combout\;
\ALT_INV_always0~5_combout\ <= NOT \always0~5_combout\;
\ALT_INV_always0~4_combout\ <= NOT \always0~4_combout\;
\ALT_INV_always0~3_combout\ <= NOT \always0~3_combout\;
\ALT_INV_always0~2_combout\ <= NOT \always0~2_combout\;
\ALT_INV_always0~1_combout\ <= NOT \always0~1_combout\;
\ALT_INV_always0~0_combout\ <= NOT \always0~0_combout\;
\ALT_INV_LessThan8~4_combout\ <= NOT \LessThan8~4_combout\;
\ALT_INV_LessThan8~3_combout\ <= NOT \LessThan8~3_combout\;
\ALT_INV_LessThan8~2_combout\ <= NOT \LessThan8~2_combout\;
\ALT_INV_LessThan8~1_combout\ <= NOT \LessThan8~1_combout\;
\ALT_INV_LessThan8~0_combout\ <= NOT \LessThan8~0_combout\;
\ALT_INV_Selector14~1_combout\ <= NOT \Selector14~1_combout\;
\ALT_INV_Selector8~1_combout\ <= NOT \Selector8~1_combout\;
\ALT_INV_Selector14~0_combout\ <= NOT \Selector14~0_combout\;
\ALT_INV_Selector8~0_combout\ <= NOT \Selector8~0_combout\;
\vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ <= NOT \vga_u0|controller|VGA_VS1~0_combout\;
\vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ <= NOT \vga_u0|controller|VGA_HS1~0_combout\;
\ALT_INV_Selector5~3_combout\ <= NOT \Selector5~3_combout\;
\ALT_INV_Selector5~2_combout\ <= NOT \Selector5~2_combout\;
\ALT_INV_Selector5~1_combout\ <= NOT \Selector5~1_combout\;
\ALT_INV_Selector5~0_combout\ <= NOT \Selector5~0_combout\;
\ALT_INV_Selector67~1_combout\ <= NOT \Selector67~1_combout\;
\ALT_INV_Selector65~3_combout\ <= NOT \Selector65~3_combout\;
\ALT_INV_Equal4~4_combout\ <= NOT \Equal4~4_combout\;
\ALT_INV_Selector6~4_combout\ <= NOT \Selector6~4_combout\;
\ALT_INV_Selector6~3_combout\ <= NOT \Selector6~3_combout\;
\ALT_INV_Selector6~2_combout\ <= NOT \Selector6~2_combout\;
\ALT_INV_Selector6~1_combout\ <= NOT \Selector6~1_combout\;
\ALT_INV_Selector6~0_combout\ <= NOT \Selector6~0_combout\;
\ALT_INV_Selector77~0_combout\ <= NOT \Selector77~0_combout\;
\ALT_INV_Equal6~3_combout\ <= NOT \Equal6~3_combout\;
\ALT_INV_Equal4~3_combout\ <= NOT \Equal4~3_combout\;
\ALT_INV_Selector63~0_combout\ <= NOT \Selector63~0_combout\;
\ALT_INV_WideOr16~2_combout\ <= NOT \WideOr16~2_combout\;
\ALT_INV_Selector64~1_combout\ <= NOT \Selector64~1_combout\;
\ALT_INV_Selector64~0_combout\ <= NOT \Selector64~0_combout\;
\ALT_INV_Selector79~0_combout\ <= NOT \Selector79~0_combout\;
\ALT_INV_Selector75~0_combout\ <= NOT \Selector75~0_combout\;
\ALT_INV_draw.x[3]~8_combout\ <= NOT \draw.x[3]~8_combout\;
\ALT_INV_draw.x[3]~7_combout\ <= NOT \draw.x[3]~7_combout\;
\ALT_INV_draw.x[3]~6_combout\ <= NOT \draw.x[3]~6_combout\;
\ALT_INV_draw.x[3]~5_combout\ <= NOT \draw.x[3]~5_combout\;
\ALT_INV_draw.x[3]~4_combout\ <= NOT \draw.x[3]~4_combout\;
\ALT_INV_Selector0~0_combout\ <= NOT \Selector0~0_combout\;
\ALT_INV_draw.x[3]~3_combout\ <= NOT \draw.x[3]~3_combout\;
\ALT_INV_draw.x[3]~2_combout\ <= NOT \draw.x[3]~2_combout\;
\ALT_INV_state.DRAW_TOP_ENTER~q\ <= NOT \state.DRAW_TOP_ENTER~q\;
\ALT_INV_draw.x[3]~1_combout\ <= NOT \draw.x[3]~1_combout\;
\ALT_INV_WideOr16~1_combout\ <= NOT \WideOr16~1_combout\;
\ALT_INV_state.INIT~q\ <= NOT \state.INIT~q\;
\ALT_INV_Selector12~1_combout\ <= NOT \Selector12~1_combout\;
\ALT_INV_Selector12~0_combout\ <= NOT \Selector12~0_combout\;
\ALT_INV_paddle_two_y~4_combout\ <= NOT \paddle_two_y~4_combout\;
\ALT_INV_Selector11~2_combout\ <= NOT \Selector11~2_combout\;
\ALT_INV_Selector11~1_combout\ <= NOT \Selector11~1_combout\;
\ALT_INV_Selector11~0_combout\ <= NOT \Selector11~0_combout\;
\ALT_INV_paddle_two_y~3_combout\ <= NOT \paddle_two_y~3_combout\;
\ALT_INV_Selector10~2_combout\ <= NOT \Selector10~2_combout\;
\ALT_INV_Selector10~1_combout\ <= NOT \Selector10~1_combout\;
\ALT_INV_Selector10~0_combout\ <= NOT \Selector10~0_combout\;
\ALT_INV_paddle_two_y~2_combout\ <= NOT \paddle_two_y~2_combout\;
\ALT_INV_draw.y[4]~0_combout\ <= NOT \draw.y[4]~0_combout\;
\ALT_INV_draw.x[5]~0_combout\ <= NOT \draw.x[5]~0_combout\;
\ALT_INV_state.DRAW_TOP_LOOP~q\ <= NOT \state.DRAW_TOP_LOOP~q\;
\ALT_INV_state.IDLE~q\ <= NOT \state.IDLE~q\;
\ALT_INV_Equal0~1_combout\ <= NOT \Equal0~1_combout\;
\ALT_INV_Equal0~0_combout\ <= NOT \Equal0~0_combout\;
\ALT_INV_Equal1~2_combout\ <= NOT \Equal1~2_combout\;
\ALT_INV_Equal1~1_combout\ <= NOT \Equal1~1_combout\;
\ALT_INV_Equal1~0_combout\ <= NOT \Equal1~0_combout\;
\ALT_INV_Selector65~2_combout\ <= NOT \Selector65~2_combout\;
\ALT_INV_Equal4~2_combout\ <= NOT \Equal4~2_combout\;
\ALT_INV_Equal4~1_combout\ <= NOT \Equal4~1_combout\;
\ALT_INV_Equal4~0_combout\ <= NOT \Equal4~0_combout\;
\ALT_INV_Selector65~1_combout\ <= NOT \Selector65~1_combout\;
\ALT_INV_Selector65~0_combout\ <= NOT \Selector65~0_combout\;
\ALT_INV_Equal6~2_combout\ <= NOT \Equal6~2_combout\;

-- Location: IOOBUF_X40_Y81_N53
\VGA_R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(0));

-- Location: IOOBUF_X38_Y81_N2
\VGA_R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(1));

-- Location: IOOBUF_X26_Y81_N59
\VGA_R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(2));

-- Location: IOOBUF_X38_Y81_N19
\VGA_R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(3));

-- Location: IOOBUF_X36_Y81_N36
\VGA_R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(4));

-- Location: IOOBUF_X22_Y81_N19
\VGA_R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(5));

-- Location: IOOBUF_X22_Y81_N2
\VGA_R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(6));

-- Location: IOOBUF_X26_Y81_N42
\VGA_R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(7));

-- Location: IOOBUF_X89_Y25_N22
\VGA_R[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(8));

-- Location: IOOBUF_X89_Y25_N5
\VGA_R[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(9));

-- Location: IOOBUF_X4_Y81_N19
\VGA_G[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(0));

-- Location: IOOBUF_X4_Y81_N2
\VGA_G[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(1));

-- Location: IOOBUF_X20_Y81_N19
\VGA_G[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(2));

-- Location: IOOBUF_X6_Y81_N2
\VGA_G[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(3));

-- Location: IOOBUF_X10_Y81_N59
\VGA_G[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(4));

-- Location: IOOBUF_X10_Y81_N42
\VGA_G[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(5));

-- Location: IOOBUF_X18_Y81_N42
\VGA_G[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(6));

-- Location: IOOBUF_X18_Y81_N59
\VGA_G[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(7));

-- Location: IOOBUF_X8_Y81_N36
\VGA_G[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(8));

-- Location: IOOBUF_X12_Y81_N19
\VGA_G[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(9));

-- Location: IOOBUF_X40_Y81_N36
\VGA_B[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(0));

-- Location: IOOBUF_X28_Y81_N19
\VGA_B[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(1));

-- Location: IOOBUF_X20_Y81_N2
\VGA_B[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(2));

-- Location: IOOBUF_X36_Y81_N19
\VGA_B[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(3));

-- Location: IOOBUF_X28_Y81_N2
\VGA_B[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(4));

-- Location: IOOBUF_X36_Y81_N2
\VGA_B[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(5));

-- Location: IOOBUF_X40_Y81_N19
\VGA_B[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(6));

-- Location: IOOBUF_X32_Y81_N19
\VGA_B[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(7));

-- Location: IOOBUF_X34_Y81_N93
\VGA_B[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(8));

-- Location: IOOBUF_X89_Y23_N22
\VGA_B[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(9));

-- Location: IOOBUF_X36_Y81_N53
\VGA_HS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_HS~q\,
	devoe => ww_devoe,
	o => ww_VGA_HS);

-- Location: IOOBUF_X34_Y81_N42
\VGA_VS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_VS~q\,
	devoe => ww_devoe,
	o => ww_VGA_VS);

-- Location: IOOBUF_X38_Y81_N53
\VGA_BLANK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_BLANK~q\,
	devoe => ww_devoe,
	o => ww_VGA_BLANK);

-- Location: IOOBUF_X86_Y0_N53
\VGA_SYNC~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_VGA_SYNC);

-- Location: IOOBUF_X38_Y81_N36
\VGA_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_VGA_CLK);

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: PLLREFCLKSELECT_X0_Y21_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT\ : cyclonev_pll_refclk_select
-- pragma translate_off
GENERIC MAP (
	pll_auto_clk_sw_en => "false",
	pll_clk_loss_edge => "both_edges",
	pll_clk_loss_sw_en => "false",
	pll_clk_sw_dly => 0,
	pll_clkin_0_src => "clk_0",
	pll_clkin_1_src => "ref_clk1",
	pll_manu_clk_sw_en => "false",
	pll_sw_refclk_src => "clk_0")
-- pragma translate_on
PORT MAP (
	clkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\,
	clkout => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	extswitchbuf => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\);

-- Location: FRACTIONALPLL_X0_Y15_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL\ : cyclonev_fractional_pll
-- pragma translate_off
GENERIC MAP (
	dsm_accumulator_reset_value => 0,
	forcelock => "false",
	mimic_fbclk_type => "gclk_far",
	nreset_invert => "true",
	output_clock_frequency => "300.0 mhz",
	pll_atb => 0,
	pll_bwctrl => 4000,
	pll_cmp_buf_dly => "0 ps",
	pll_cp_comp => "true",
	pll_cp_current => 10,
	pll_ctrl_override_setting => "false",
	pll_dsm_dither => "disable",
	pll_dsm_out_sel => "disable",
	pll_dsm_reset => "false",
	pll_ecn_bypass => "false",
	pll_ecn_test_en => "false",
	pll_enable => "true",
	pll_fbclk_mux_1 => "glb",
	pll_fbclk_mux_2 => "fb_1",
	pll_fractional_carry_out => 32,
	pll_fractional_division => 1,
	pll_fractional_division_string => "'0'",
	pll_fractional_value_ready => "true",
	pll_lf_testen => "false",
	pll_lock_fltr_cfg => 25,
	pll_lock_fltr_test => "false",
	pll_m_cnt_bypass_en => "false",
	pll_m_cnt_coarse_dly => "0 ps",
	pll_m_cnt_fine_dly => "0 ps",
	pll_m_cnt_hi_div => 6,
	pll_m_cnt_in_src => "ph_mux_clk",
	pll_m_cnt_lo_div => 6,
	pll_m_cnt_odd_div_duty_en => "false",
	pll_m_cnt_ph_mux_prst => 0,
	pll_m_cnt_prst => 1,
	pll_n_cnt_bypass_en => "false",
	pll_n_cnt_coarse_dly => "0 ps",
	pll_n_cnt_fine_dly => "0 ps",
	pll_n_cnt_hi_div => 1,
	pll_n_cnt_lo_div => 1,
	pll_n_cnt_odd_div_duty_en => "false",
	pll_ref_buf_dly => "0 ps",
	pll_reg_boost => 0,
	pll_regulator_bypass => "false",
	pll_ripplecap_ctrl => 0,
	pll_slf_rst => "false",
	pll_tclk_mux_en => "false",
	pll_tclk_sel => "n_src",
	pll_test_enable => "false",
	pll_testdn_enable => "false",
	pll_testup_enable => "false",
	pll_unlock_fltr_cfg => 2,
	pll_vco_div => 2,
	pll_vco_ph0_en => "true",
	pll_vco_ph1_en => "true",
	pll_vco_ph2_en => "true",
	pll_vco_ph3_en => "true",
	pll_vco_ph4_en => "true",
	pll_vco_ph5_en => "true",
	pll_vco_ph6_en => "true",
	pll_vco_ph7_en => "true",
	pll_vctrl_test_voltage => 750,
	reference_clock_frequency => "50.0 mhz",
	vccd0g_atb => "disable",
	vccd0g_output => 0,
	vccd1g_atb => "disable",
	vccd1g_output => 0,
	vccm1g_tap => 2,
	vccr_pd => "false",
	vcodiv_override => "false",
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	coreclkfb => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	ecnc1test => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\,
	nresync => GND,
	refclkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftdonein => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	fbclk => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	tclk => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	vcoph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\);

-- Location: PLLRECONFIG_X0_Y19_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG\ : cyclonev_pll_reconfig
-- pragma translate_off
GENERIC MAP (
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftenm => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\);

-- Location: PLLOUTPUTCOUNTER_X0_Y20_N1
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 6,
	dprio0_cnt_lo_div => 6,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "25.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 6)
-- pragma translate_on
PORT MAP (
	nen0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\,
	tclk0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	up0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	vco0ph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0));

-- Location: CLKCTRL_G6
\vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0),
	outclk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\);

-- Location: LABCELL_X35_Y67_N30
\vga_u0|controller|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add1~37_sumout\,
	cout => \vga_u0|controller|Add1~38\);

-- Location: IOIBUF_X89_Y25_N55
\KEY[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(8),
	o => \KEY[8]~input_o\);

-- Location: LABCELL_X36_Y67_N0
\vga_u0|controller|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~37_sumout\ = SUM(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add0~38\ = CARRY(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add0~37_sumout\,
	cout => \vga_u0|controller|Add0~38\);

-- Location: FF_X36_Y67_N2
\vga_u0|controller|xCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~37_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(0));

-- Location: LABCELL_X36_Y67_N3
\vga_u0|controller|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~17_sumout\ = SUM(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))
-- \vga_u0|controller|Add0~18\ = CARRY(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(1),
	cin => \vga_u0|controller|Add0~38\,
	sumout => \vga_u0|controller|Add0~17_sumout\,
	cout => \vga_u0|controller|Add0~18\);

-- Location: FF_X36_Y67_N5
\vga_u0|controller|xCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~17_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(1));

-- Location: LABCELL_X36_Y67_N6
\vga_u0|controller|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~33_sumout\ = SUM(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))
-- \vga_u0|controller|Add0~34\ = CARRY(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(2),
	cin => \vga_u0|controller|Add0~18\,
	sumout => \vga_u0|controller|Add0~33_sumout\,
	cout => \vga_u0|controller|Add0~34\);

-- Location: FF_X36_Y67_N8
\vga_u0|controller|xCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~33_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(2));

-- Location: LABCELL_X36_Y67_N9
\vga_u0|controller|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~29_sumout\ = SUM(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))
-- \vga_u0|controller|Add0~30\ = CARRY(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	cin => \vga_u0|controller|Add0~34\,
	sumout => \vga_u0|controller|Add0~29_sumout\,
	cout => \vga_u0|controller|Add0~30\);

-- Location: FF_X36_Y67_N10
\vga_u0|controller|xCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~29_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(3));

-- Location: LABCELL_X36_Y67_N12
\vga_u0|controller|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~13_sumout\ = SUM(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))
-- \vga_u0|controller|Add0~14\ = CARRY(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(4),
	cin => \vga_u0|controller|Add0~30\,
	sumout => \vga_u0|controller|Add0~13_sumout\,
	cout => \vga_u0|controller|Add0~14\);

-- Location: FF_X36_Y67_N14
\vga_u0|controller|xCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~13_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(4));

-- Location: LABCELL_X36_Y67_N15
\vga_u0|controller|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~25_sumout\ = SUM(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))
-- \vga_u0|controller|Add0~26\ = CARRY(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	cin => \vga_u0|controller|Add0~14\,
	sumout => \vga_u0|controller|Add0~25_sumout\,
	cout => \vga_u0|controller|Add0~26\);

-- Location: FF_X36_Y67_N16
\vga_u0|controller|xCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~25_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(5));

-- Location: LABCELL_X36_Y67_N18
\vga_u0|controller|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~21_sumout\ = SUM(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))
-- \vga_u0|controller|Add0~22\ = CARRY(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(6),
	cin => \vga_u0|controller|Add0~26\,
	sumout => \vga_u0|controller|Add0~21_sumout\,
	cout => \vga_u0|controller|Add0~22\);

-- Location: FF_X36_Y67_N20
\vga_u0|controller|xCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~21_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(6));

-- Location: LABCELL_X36_Y67_N21
\vga_u0|controller|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~9_sumout\ = SUM(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))
-- \vga_u0|controller|Add0~10\ = CARRY(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => \vga_u0|controller|Add0~22\,
	sumout => \vga_u0|controller|Add0~9_sumout\,
	cout => \vga_u0|controller|Add0~10\);

-- Location: FF_X36_Y67_N22
\vga_u0|controller|xCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~9_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(7));

-- Location: LABCELL_X36_Y67_N24
\vga_u0|controller|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~5_sumout\ = SUM(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))
-- \vga_u0|controller|Add0~6\ = CARRY(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(8),
	cin => \vga_u0|controller|Add0~10\,
	sumout => \vga_u0|controller|Add0~5_sumout\,
	cout => \vga_u0|controller|Add0~6\);

-- Location: FF_X36_Y67_N26
\vga_u0|controller|xCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(8));

-- Location: LABCELL_X36_Y67_N45
\vga_u0|controller|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~1_combout\ = ( !\vga_u0|controller|xCounter\(6) & ( (\vga_u0|controller|xCounter\(0) & (!\vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(0),
	datac => \vga_u0|controller|ALT_INV_xCounter\(5),
	datad => \vga_u0|controller|ALT_INV_xCounter\(1),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(6),
	combout => \vga_u0|controller|Equal0~1_combout\);

-- Location: FF_X36_Y67_N23
\vga_u0|controller|xCounter[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~9_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[7]~DUPLICATE_q\);

-- Location: LABCELL_X36_Y67_N27
\vga_u0|controller|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~1_sumout\ = SUM(( \vga_u0|controller|xCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(9),
	cin => \vga_u0|controller|Add0~6\,
	sumout => \vga_u0|controller|Add0~1_sumout\);

-- Location: FF_X36_Y67_N28
\vga_u0|controller|xCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~1_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(9));

-- Location: LABCELL_X36_Y67_N36
\vga_u0|controller|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (!\vga_u0|controller|xCounter[7]~DUPLICATE_q\ & (\vga_u0|controller|xCounter\(9) & (\vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000100000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\,
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_xCounter\(3),
	datad => \vga_u0|controller|ALT_INV_xCounter\(2),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|Equal0~0_combout\);

-- Location: LABCELL_X36_Y67_N51
\vga_u0|controller|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~2_combout\ = (\vga_u0|controller|xCounter\(8) & (\vga_u0|controller|Equal0~1_combout\ & \vga_u0|controller|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datac => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	datad => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	combout => \vga_u0|controller|Equal0~2_combout\);

-- Location: FF_X35_Y67_N37
\vga_u0|controller|yCounter[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~29_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[2]~DUPLICATE_q\);

-- Location: LABCELL_X35_Y67_N6
\vga_u0|controller|always1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~1_combout\ = ( !\vga_u0|controller|yCounter\(0) & ( (\vga_u0|controller|yCounter[2]~DUPLICATE_q\ & (\vga_u0|controller|yCounter\(3) & (!\vga_u0|controller|yCounter\(1) & !\vga_u0|controller|yCounter\(4)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000000100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\,
	datab => \vga_u0|controller|ALT_INV_yCounter\(3),
	datac => \vga_u0|controller|ALT_INV_yCounter\(1),
	datad => \vga_u0|controller|ALT_INV_yCounter\(4),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(0),
	combout => \vga_u0|controller|always1~1_combout\);

-- Location: LABCELL_X35_Y67_N54
\vga_u0|controller|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~9_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~6\ ))
-- \vga_u0|controller|Add1~10\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|Add1~6\,
	sumout => \vga_u0|controller|Add1~9_sumout\,
	cout => \vga_u0|controller|Add1~10\);

-- Location: LABCELL_X35_Y67_N57
\vga_u0|controller|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(9),
	cin => \vga_u0|controller|Add1~10\,
	sumout => \vga_u0|controller|Add1~1_sumout\);

-- Location: FF_X35_Y67_N59
\vga_u0|controller|yCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~1_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(9));

-- Location: LABCELL_X35_Y67_N0
\vga_u0|controller|always1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~0_combout\ = ( !\vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(5) & (\vga_u0|controller|yCounter\(9) & (!\vga_u0|controller|yCounter\(6) & !\vga_u0|controller|yCounter\(8)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(5),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter\(6),
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|always1~0_combout\);

-- Location: LABCELL_X35_Y67_N12
\vga_u0|controller|always1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~2_combout\ = ( \vga_u0|controller|Equal0~1_combout\ & ( (\vga_u0|controller|always1~1_combout\ & (\vga_u0|controller|xCounter\(8) & (\vga_u0|controller|always1~0_combout\ & \vga_u0|controller|Equal0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_always1~1_combout\,
	datab => \vga_u0|controller|ALT_INV_xCounter\(8),
	datac => \vga_u0|controller|ALT_INV_always1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	combout => \vga_u0|controller|always1~2_combout\);

-- Location: FF_X35_Y67_N31
\vga_u0|controller|yCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~37_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(0));

-- Location: LABCELL_X35_Y67_N33
\vga_u0|controller|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~33_sumout\ = SUM(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))
-- \vga_u0|controller|Add1~34\ = CARRY(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(1),
	cin => \vga_u0|controller|Add1~38\,
	sumout => \vga_u0|controller|Add1~33_sumout\,
	cout => \vga_u0|controller|Add1~34\);

-- Location: FF_X35_Y67_N35
\vga_u0|controller|yCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~33_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(1));

-- Location: LABCELL_X35_Y67_N36
\vga_u0|controller|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~29_sumout\ = SUM(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))
-- \vga_u0|controller|Add1~30\ = CARRY(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	cin => \vga_u0|controller|Add1~34\,
	sumout => \vga_u0|controller|Add1~29_sumout\,
	cout => \vga_u0|controller|Add1~30\);

-- Location: FF_X35_Y67_N38
\vga_u0|controller|yCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~29_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(2));

-- Location: LABCELL_X35_Y67_N39
\vga_u0|controller|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~25_sumout\ = SUM(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))
-- \vga_u0|controller|Add1~26\ = CARRY(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	cin => \vga_u0|controller|Add1~30\,
	sumout => \vga_u0|controller|Add1~25_sumout\,
	cout => \vga_u0|controller|Add1~26\);

-- Location: FF_X35_Y67_N41
\vga_u0|controller|yCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~25_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(3));

-- Location: LABCELL_X35_Y67_N42
\vga_u0|controller|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~21_sumout\ = SUM(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))
-- \vga_u0|controller|Add1~22\ = CARRY(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|Add1~26\,
	sumout => \vga_u0|controller|Add1~21_sumout\,
	cout => \vga_u0|controller|Add1~22\);

-- Location: FF_X35_Y67_N43
\vga_u0|controller|yCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~21_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(4));

-- Location: LABCELL_X35_Y67_N45
\vga_u0|controller|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~13_sumout\ = SUM(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))
-- \vga_u0|controller|Add1~14\ = CARRY(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(5),
	cin => \vga_u0|controller|Add1~22\,
	sumout => \vga_u0|controller|Add1~13_sumout\,
	cout => \vga_u0|controller|Add1~14\);

-- Location: FF_X35_Y67_N46
\vga_u0|controller|yCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~13_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(5));

-- Location: LABCELL_X35_Y67_N48
\vga_u0|controller|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~17_sumout\ = SUM(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))
-- \vga_u0|controller|Add1~18\ = CARRY(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(6),
	cin => \vga_u0|controller|Add1~14\,
	sumout => \vga_u0|controller|Add1~17_sumout\,
	cout => \vga_u0|controller|Add1~18\);

-- Location: FF_X35_Y67_N50
\vga_u0|controller|yCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(6));

-- Location: LABCELL_X35_Y67_N51
\vga_u0|controller|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~5_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))
-- \vga_u0|controller|Add1~6\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|Add1~18\,
	sumout => \vga_u0|controller|Add1~5_sumout\,
	cout => \vga_u0|controller|Add1~6\);

-- Location: FF_X35_Y67_N53
\vga_u0|controller|yCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~5_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(7));

-- Location: FF_X35_Y67_N55
\vga_u0|controller|yCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~9_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(8));

-- Location: FF_X35_Y67_N49
\vga_u0|controller|yCounter[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[6]~DUPLICATE_q\);

-- Location: FF_X35_Y67_N40
\vga_u0|controller|yCounter[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~25_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[3]~DUPLICATE_q\);

-- Location: FF_X36_Y67_N25
\vga_u0|controller|xCounter[8]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[8]~DUPLICATE_q\);

-- Location: LABCELL_X37_Y67_N0
\vga_u0|controller|controller_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~9_sumout\ = SUM(( !\vga_u0|controller|yCounter[2]~DUPLICATE_q\ $ (!\vga_u0|controller|xCounter\(7)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~10\ = CARRY(( !\vga_u0|controller|yCounter[2]~DUPLICATE_q\ $ (!\vga_u0|controller|xCounter\(7)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~11\ = SHARE((\vga_u0|controller|yCounter[2]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(7)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|controller|controller_translator|Add1~9_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~10\,
	shareout => \vga_u0|controller|controller_translator|Add1~11\);

-- Location: LABCELL_X37_Y67_N3
\vga_u0|controller|controller_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~13_sumout\ = SUM(( !\vga_u0|controller|yCounter[3]~DUPLICATE_q\ $ (!\vga_u0|controller|xCounter[8]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~10\ ))
-- \vga_u0|controller|controller_translator|Add1~14\ = CARRY(( !\vga_u0|controller|yCounter[3]~DUPLICATE_q\ $ (!\vga_u0|controller|xCounter[8]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~10\ ))
-- \vga_u0|controller|controller_translator|Add1~15\ = SHARE((\vga_u0|controller|yCounter[3]~DUPLICATE_q\ & \vga_u0|controller|xCounter[8]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000100010001000100000000000000000110011001100110",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\,
	datab => \vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~10\,
	sharein => \vga_u0|controller|controller_translator|Add1~11\,
	sumout => \vga_u0|controller|controller_translator|Add1~13_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~14\,
	shareout => \vga_u0|controller|controller_translator|Add1~15\);

-- Location: LABCELL_X37_Y67_N6
\vga_u0|controller|controller_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~17_sumout\ = SUM(( !\vga_u0|controller|xCounter\(9) $ (!\vga_u0|controller|yCounter\(4) $ (\vga_u0|controller|yCounter\(2))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~18\ = CARRY(( !\vga_u0|controller|xCounter\(9) $ (!\vga_u0|controller|yCounter\(4) $ (\vga_u0|controller|yCounter\(2))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~19\ = SHARE((!\vga_u0|controller|xCounter\(9) & (\vga_u0|controller|yCounter\(4) & \vga_u0|controller|yCounter\(2))) # (\vga_u0|controller|xCounter\(9) & ((\vga_u0|controller|yCounter\(2)) # 
-- (\vga_u0|controller|yCounter\(4)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter\(4),
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	cin => \vga_u0|controller|controller_translator|Add1~14\,
	sharein => \vga_u0|controller|controller_translator|Add1~15\,
	sumout => \vga_u0|controller|controller_translator|Add1~17_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~18\,
	shareout => \vga_u0|controller|controller_translator|Add1~19\);

-- Location: LABCELL_X37_Y67_N9
\vga_u0|controller|controller_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~21_sumout\ = SUM(( !\vga_u0|controller|yCounter[3]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(5)) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~18\ ))
-- \vga_u0|controller|controller_translator|Add1~22\ = CARRY(( !\vga_u0|controller|yCounter[3]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(5)) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( \vga_u0|controller|controller_translator|Add1~18\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~23\ = SHARE((\vga_u0|controller|yCounter[3]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter[3]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(5),
	cin => \vga_u0|controller|controller_translator|Add1~18\,
	sharein => \vga_u0|controller|controller_translator|Add1~19\,
	sumout => \vga_u0|controller|controller_translator|Add1~21_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~22\,
	shareout => \vga_u0|controller|controller_translator|Add1~23\);

-- Location: LABCELL_X37_Y67_N12
\vga_u0|controller|controller_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~25_sumout\ = SUM(( !\vga_u0|controller|yCounter[6]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~22\ ))
-- \vga_u0|controller|controller_translator|Add1~26\ = CARRY(( !\vga_u0|controller|yCounter[6]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( \vga_u0|controller|controller_translator|Add1~22\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~27\ = SHARE((\vga_u0|controller|yCounter[6]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|controller_translator|Add1~22\,
	sharein => \vga_u0|controller|controller_translator|Add1~23\,
	sumout => \vga_u0|controller|controller_translator|Add1~25_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~26\,
	shareout => \vga_u0|controller|controller_translator|Add1~27\);

-- Location: LABCELL_X37_Y67_N15
\vga_u0|controller|controller_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~29_sumout\ = SUM(( !\vga_u0|controller|yCounter\(5) $ (!\vga_u0|controller|yCounter\(7)) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( \vga_u0|controller|controller_translator|Add1~26\ ))
-- \vga_u0|controller|controller_translator|Add1~30\ = CARRY(( !\vga_u0|controller|yCounter\(5) $ (!\vga_u0|controller|yCounter\(7)) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( \vga_u0|controller|controller_translator|Add1~26\ ))
-- \vga_u0|controller|controller_translator|Add1~31\ = SHARE((\vga_u0|controller|yCounter\(5) & \vga_u0|controller|yCounter\(7)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_yCounter\(5),
	datad => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|controller_translator|Add1~26\,
	sharein => \vga_u0|controller|controller_translator|Add1~27\,
	sumout => \vga_u0|controller|controller_translator|Add1~29_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~30\,
	shareout => \vga_u0|controller|controller_translator|Add1~31\);

-- Location: LABCELL_X37_Y67_N18
\vga_u0|controller|controller_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~33_sumout\ = SUM(( !\vga_u0|controller|yCounter[6]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~34\ = CARRY(( !\vga_u0|controller|yCounter[6]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(8)) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( \vga_u0|controller|controller_translator|Add1~30\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~35\ = SHARE((\vga_u0|controller|yCounter[6]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(8)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000011001100000000000000000011001111001100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~30\,
	sharein => \vga_u0|controller|controller_translator|Add1~31\,
	sumout => \vga_u0|controller|controller_translator|Add1~33_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~34\,
	shareout => \vga_u0|controller|controller_translator|Add1~35\);

-- Location: LABCELL_X37_Y67_N21
\vga_u0|controller|controller_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|controller_translator|Add1~34\,
	sharein => \vga_u0|controller|controller_translator|Add1~35\,
	sumout => \vga_u0|controller|controller_translator|Add1~37_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~38\,
	shareout => \vga_u0|controller|controller_translator|Add1~39\);

-- Location: LABCELL_X37_Y67_N24
\vga_u0|controller|controller_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~2\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~3\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|controller_translator|Add1~38\,
	sharein => \vga_u0|controller|controller_translator|Add1~39\,
	sumout => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~2\,
	shareout => \vga_u0|controller|controller_translator|Add1~3\);

-- Location: FF_X37_Y67_N26
\vga_u0|VideoMemory|auto_generated|address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0));

-- Location: FF_X37_Y67_N38
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0));

-- Location: LABCELL_X36_Y67_N30
\vga_u0|controller|on_screen~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~0_combout\ = ( !\vga_u0|controller|xCounter\(1) & ( !\vga_u0|controller|xCounter\(4) & ( (!\vga_u0|controller|xCounter\(6) & (!\vga_u0|controller|xCounter\(2) & (!\vga_u0|controller|xCounter\(3) & 
-- !\vga_u0|controller|xCounter\(5)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(6),
	datab => \vga_u0|controller|ALT_INV_xCounter\(2),
	datac => \vga_u0|controller|ALT_INV_xCounter\(3),
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	datae => \vga_u0|controller|ALT_INV_xCounter\(1),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|on_screen~0_combout\);

-- Location: LABCELL_X35_Y67_N3
\vga_u0|controller|LessThan7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|LessThan7~0_combout\ = ( \vga_u0|controller|yCounter\(7) & ( (!\vga_u0|controller|yCounter\(9) & ((!\vga_u0|controller|yCounter\(5)) # ((!\vga_u0|controller|yCounter[6]~DUPLICATE_q\) # (!\vga_u0|controller|yCounter\(8))))) ) ) # ( 
-- !\vga_u0|controller|yCounter\(7) & ( !\vga_u0|controller|yCounter\(9) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110010001100110011001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(5),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(7),
	combout => \vga_u0|controller|LessThan7~0_combout\);

-- Location: LABCELL_X36_Y67_N39
\vga_u0|controller|on_screen~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~1_combout\ = ( \vga_u0|controller|xCounter\(8) & ( (!\vga_u0|controller|xCounter\(9) & \vga_u0|controller|LessThan7~0_combout\) ) ) # ( !\vga_u0|controller|xCounter\(8) & ( (\vga_u0|controller|LessThan7~0_combout\ & 
-- ((!\vga_u0|controller|xCounter[7]~DUPLICATE_q\) # ((!\vga_u0|controller|xCounter\(9)) # (\vga_u0|controller|on_screen~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011101111000000001110111100000000110011000000000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\,
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_on_screen~0_combout\,
	datad => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_xCounter\(8),
	combout => \vga_u0|controller|on_screen~1_combout\);

-- Location: CLKCTRL_G5
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: LABCELL_X36_Y63_N0
\Add4~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~25_sumout\ = SUM(( paddle_one_y(1) ) + ( VCC ) + ( !VCC ))
-- \Add4~26\ = CARRY(( paddle_one_y(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(1),
	cin => GND,
	sumout => \Add4~25_sumout\,
	cout => \Add4~26\);

-- Location: LABCELL_X37_Y63_N0
\Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~25_sumout\ = SUM(( paddle_one_y(1) ) + ( VCC ) + ( !VCC ))
-- \Add3~26\ = CARRY(( paddle_one_y(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(1),
	cin => GND,
	sumout => \Add3~25_sumout\,
	cout => \Add3~26\);

-- Location: LABCELL_X36_Y63_N36
\Selector14~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector14~1_combout\ = ( paddle_one_y(1) & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((\paddle_one_y~2_combout\) # (\Add4~25_sumout\))) ) ) ) # ( !paddle_one_y(1) & ( \KEY[2]~input_o\ & ( (\Add4~25_sumout\ & 
-- (\state.DRAW_PADDLE_ONE_ENTER~q\ & !\paddle_one_y~2_combout\)) ) ) ) # ( paddle_one_y(1) & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((\paddle_one_y~2_combout\) # (\Add3~25_sumout\))) ) ) ) # ( !paddle_one_y(1) & ( !\KEY[2]~input_o\ & ( 
-- (\Add3~25_sumout\ & (\state.DRAW_PADDLE_ONE_ENTER~q\ & !\paddle_one_y~2_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000111100000101000000000000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add4~25_sumout\,
	datab => \ALT_INV_Add3~25_sumout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => ALT_INV_paddle_one_y(1),
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector14~1_combout\);

-- Location: MLABCELL_X39_Y64_N30
\Add11~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~17_sumout\ = SUM(( paddle_one_y(1) ) + ( VCC ) + ( !VCC ))
-- \Add11~18\ = CARRY(( paddle_one_y(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_one_y(1),
	cin => GND,
	sumout => \Add11~17_sumout\,
	cout => \Add11~18\);

-- Location: MLABCELL_X39_Y64_N33
\Add11~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~21_sumout\ = SUM(( !paddle_one_y(2) ) + ( GND ) + ( \Add11~18\ ))
-- \Add11~22\ = CARRY(( !paddle_one_y(2) ) + ( GND ) + ( \Add11~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(2),
	cin => \Add11~18\,
	sumout => \Add11~21_sumout\,
	cout => \Add11~22\);

-- Location: MLABCELL_X39_Y64_N36
\Add11~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~5_sumout\ = SUM(( !paddle_one_y(3) ) + ( VCC ) + ( \Add11~22\ ))
-- \Add11~6\ = CARRY(( !paddle_one_y(3) ) + ( VCC ) + ( \Add11~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(3),
	cin => \Add11~22\,
	sumout => \Add11~5_sumout\,
	cout => \Add11~6\);

-- Location: MLABCELL_X39_Y64_N39
\Add11~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~9_sumout\ = SUM(( !paddle_one_y(4) ) + ( GND ) + ( \Add11~6\ ))
-- \Add11~10\ = CARRY(( !paddle_one_y(4) ) + ( GND ) + ( \Add11~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(4),
	cin => \Add11~6\,
	sumout => \Add11~9_sumout\,
	cout => \Add11~10\);

-- Location: MLABCELL_X39_Y64_N42
\Add11~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~13_sumout\ = SUM(( !paddle_one_y(5) ) + ( GND ) + ( \Add11~10\ ))
-- \Add11~14\ = CARRY(( !paddle_one_y(5) ) + ( GND ) + ( \Add11~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(5),
	cin => \Add11~10\,
	sumout => \Add11~13_sumout\,
	cout => \Add11~14\);

-- Location: MLABCELL_X39_Y64_N45
\Add11~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~25_sumout\ = SUM(( paddle_one_y(6) ) + ( GND ) + ( \Add11~14\ ))
-- \Add11~26\ = CARRY(( paddle_one_y(6) ) + ( GND ) + ( \Add11~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(6),
	cin => \Add11~14\,
	sumout => \Add11~25_sumout\,
	cout => \Add11~26\);

-- Location: MLABCELL_X39_Y64_N48
\Add11~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~29_sumout\ = SUM(( paddle_one_y(7) ) + ( GND ) + ( \Add11~26\ ))
-- \Add11~30\ = CARRY(( paddle_one_y(7) ) + ( GND ) + ( \Add11~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_one_y(7),
	cin => \Add11~26\,
	sumout => \Add11~29_sumout\,
	cout => \Add11~30\);

-- Location: MLABCELL_X39_Y64_N51
\Add11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~1_sumout\ = SUM(( GND ) + ( GND ) + ( \Add11~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Add11~30\,
	sumout => \Add11~1_sumout\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: FF_X34_Y65_N50
\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector67~2_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\);

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: LABCELL_X35_Y62_N6
\Add5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~13_sumout\ = SUM(( !paddle_two_y(3) ) + ( VCC ) + ( \Add5~22\ ))
-- \Add5~14\ = CARRY(( !paddle_two_y(3) ) + ( VCC ) + ( \Add5~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(3),
	cin => \Add5~22\,
	sumout => \Add5~13_sumout\,
	cout => \Add5~14\);

-- Location: LABCELL_X35_Y62_N9
\Add5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~9_sumout\ = SUM(( !paddle_two_y(4) ) + ( VCC ) + ( \Add5~14\ ))
-- \Add5~10\ = CARRY(( !paddle_two_y(4) ) + ( VCC ) + ( \Add5~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(4),
	cin => \Add5~14\,
	sumout => \Add5~9_sumout\,
	cout => \Add5~10\);

-- Location: LABCELL_X36_Y62_N3
\Add6~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~21_sumout\ = SUM(( !paddle_two_y(2) ) + ( GND ) + ( \Add6~26\ ))
-- \Add6~22\ = CARRY(( !paddle_two_y(2) ) + ( GND ) + ( \Add6~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(2),
	cin => \Add6~26\,
	sumout => \Add6~21_sumout\,
	cout => \Add6~22\);

-- Location: LABCELL_X36_Y62_N6
\Add6~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~13_sumout\ = SUM(( !paddle_two_y(3) ) + ( GND ) + ( \Add6~22\ ))
-- \Add6~14\ = CARRY(( !paddle_two_y(3) ) + ( GND ) + ( \Add6~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(3),
	cin => \Add6~22\,
	sumout => \Add6~13_sumout\,
	cout => \Add6~14\);

-- Location: LABCELL_X36_Y62_N9
\Add6~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~9_sumout\ = SUM(( !paddle_two_y(4) ) + ( GND ) + ( \Add6~14\ ))
-- \Add6~10\ = CARRY(( !paddle_two_y(4) ) + ( GND ) + ( \Add6~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(4),
	cin => \Add6~14\,
	sumout => \Add6~9_sumout\,
	cout => \Add6~10\);

-- Location: LABCELL_X36_Y62_N42
\Selector26~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector26~0_combout\ = ( paddle_two_y(4) & ( \Add6~9_sumout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (((!\Add5~9_sumout\ & !\KEY[0]~input_o\)) # (\paddle_two_y~6_combout\))) ) ) ) # ( !paddle_two_y(4) & ( \Add6~9_sumout\ & ( 
-- (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\Add5~9_sumout\ & (!\paddle_two_y~6_combout\ & !\KEY[0]~input_o\))) ) ) ) # ( paddle_two_y(4) & ( !\Add6~9_sumout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ((!\Add5~9_sumout\) # ((\KEY[0]~input_o\) # 
-- (\paddle_two_y~6_combout\)))) ) ) ) # ( !paddle_two_y(4) & ( !\Add6~9_sumout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\paddle_two_y~6_combout\ & ((!\Add5~9_sumout\) # (\KEY[0]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001010000010001010101010101000000000000000100010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_Add5~9_sumout\,
	datac => \ALT_INV_paddle_two_y~6_combout\,
	datad => \ALT_INV_KEY[0]~input_o\,
	datae => ALT_INV_paddle_two_y(4),
	dataf => \ALT_INV_Add6~9_sumout\,
	combout => \Selector26~0_combout\);

-- Location: LABCELL_X36_Y62_N51
\Selector23~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector23~0_combout\ = ( \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ ) # ( !\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ( !\state.INIT~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.INIT~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	combout => \Selector23~0_combout\);

-- Location: FF_X36_Y62_N44
\paddle_two_y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector26~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(4));

-- Location: LABCELL_X35_Y62_N12
\Add5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~5_sumout\ = SUM(( !paddle_two_y(5) ) + ( VCC ) + ( \Add5~10\ ))
-- \Add5~6\ = CARRY(( !paddle_two_y(5) ) + ( VCC ) + ( \Add5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(5),
	cin => \Add5~10\,
	sumout => \Add5~5_sumout\,
	cout => \Add5~6\);

-- Location: LABCELL_X36_Y62_N12
\Add6~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~5_sumout\ = SUM(( !paddle_two_y(5) ) + ( GND ) + ( \Add6~10\ ))
-- \Add6~6\ = CARRY(( !paddle_two_y(5) ) + ( GND ) + ( \Add6~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(5),
	cin => \Add6~10\,
	sumout => \Add6~5_sumout\,
	cout => \Add6~6\);

-- Location: LABCELL_X36_Y62_N36
\Selector25~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector25~0_combout\ = ( paddle_two_y(5) & ( \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ( ((!\KEY[0]~input_o\ & (!\Add5~5_sumout\)) # (\KEY[0]~input_o\ & ((!\Add6~5_sumout\)))) # (\paddle_two_y~6_combout\) ) ) ) # ( !paddle_two_y(5) & ( 
-- \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ( (!\KEY[0]~input_o\ & ((!\Add5~5_sumout\) # ((\paddle_two_y~6_combout\)))) # (\KEY[0]~input_o\ & (((!\paddle_two_y~6_combout\ & !\Add6~5_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010111100100011001011111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add5~5_sumout\,
	datab => \ALT_INV_KEY[0]~input_o\,
	datac => \ALT_INV_paddle_two_y~6_combout\,
	datad => \ALT_INV_Add6~5_sumout\,
	datae => ALT_INV_paddle_two_y(5),
	dataf => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	combout => \Selector25~0_combout\);

-- Location: FF_X36_Y62_N38
\paddle_two_y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector25~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(5));

-- Location: LABCELL_X35_Y62_N15
\Add5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~1_sumout\ = SUM(( paddle_two_y(6) ) + ( VCC ) + ( \Add5~6\ ))
-- \Add5~2\ = CARRY(( paddle_two_y(6) ) + ( VCC ) + ( \Add5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(6),
	cin => \Add5~6\,
	sumout => \Add5~1_sumout\,
	cout => \Add5~2\);

-- Location: LABCELL_X36_Y62_N15
\Add6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~1_sumout\ = SUM(( paddle_two_y(6) ) + ( GND ) + ( \Add6~6\ ))
-- \Add6~2\ = CARRY(( paddle_two_y(6) ) + ( GND ) + ( \Add6~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(6),
	cin => \Add6~6\,
	sumout => \Add6~1_sumout\,
	cout => \Add6~2\);

-- Location: LABCELL_X36_Y62_N24
\Selector9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~0_combout\ = ( paddle_two_y(6) & ( \Add6~1_sumout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (((\Add5~1_sumout\ & !\paddle_two_y~6_combout\)) # (\KEY[0]~input_o\))) ) ) ) # ( !paddle_two_y(6) & ( \Add6~1_sumout\ & ( 
-- (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\paddle_two_y~6_combout\ & ((\KEY[0]~input_o\) # (\Add5~1_sumout\)))) ) ) ) # ( paddle_two_y(6) & ( !\Add6~1_sumout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ((!\paddle_two_y~6_combout\ & 
-- (\Add5~1_sumout\ & !\KEY[0]~input_o\)) # (\paddle_two_y~6_combout\ & ((\KEY[0]~input_o\))))) ) ) ) # ( !paddle_two_y(6) & ( !\Add6~1_sumout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (\Add5~1_sumout\ & (!\paddle_two_y~6_combout\ & 
-- !\KEY[0]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000000100000000010100010000010100000001000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_Add5~1_sumout\,
	datac => \ALT_INV_paddle_two_y~6_combout\,
	datad => \ALT_INV_KEY[0]~input_o\,
	datae => ALT_INV_paddle_two_y(6),
	dataf => \ALT_INV_Add6~1_sumout\,
	combout => \Selector9~0_combout\);

-- Location: FF_X36_Y62_N26
\paddle_two_y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector9~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(6));

-- Location: LABCELL_X35_Y62_N18
\Add5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~17_sumout\ = SUM(( paddle_two_y(7) ) + ( VCC ) + ( \Add5~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(7),
	cin => \Add5~2\,
	sumout => \Add5~17_sumout\);

-- Location: LABCELL_X36_Y62_N18
\Add6~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~17_sumout\ = SUM(( paddle_two_y(7) ) + ( GND ) + ( \Add6~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(7),
	cin => \Add6~2\,
	sumout => \Add6~17_sumout\);

-- Location: LABCELL_X35_Y62_N24
\Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~0_combout\ = ( paddle_two_y(7) & ( \KEY[0]~input_o\ & ( \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ ) ) ) # ( !paddle_two_y(7) & ( \KEY[0]~input_o\ & ( (\Add6~17_sumout\ & (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & !\paddle_two_y~6_combout\)) ) 
-- ) ) # ( paddle_two_y(7) & ( !\KEY[0]~input_o\ & ( (\Add5~17_sumout\ & (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & !\paddle_two_y~6_combout\)) ) ) ) # ( !paddle_two_y(7) & ( !\KEY[0]~input_o\ & ( (\Add5~17_sumout\ & 
-- (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & !\paddle_two_y~6_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000000011000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add5~17_sumout\,
	datab => \ALT_INV_Add6~17_sumout\,
	datac => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datad => \ALT_INV_paddle_two_y~6_combout\,
	datae => ALT_INV_paddle_two_y(7),
	dataf => \ALT_INV_KEY[0]~input_o\,
	combout => \Selector8~0_combout\);

-- Location: FF_X35_Y62_N26
\paddle_two_y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector8~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(7));

-- Location: LABCELL_X37_Y62_N6
\paddle_two_y~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~0_combout\ = ( paddle_two_y(1) & ( paddle_two_y(4) & ( (!paddle_two_y(3) & (!paddle_two_y(5) & paddle_two_y(6))) ) ) ) # ( !paddle_two_y(1) & ( paddle_two_y(4) & ( (!paddle_two_y(2) & (!paddle_two_y(3) & (!paddle_two_y(5) & 
-- paddle_two_y(6)))) ) ) ) # ( paddle_two_y(1) & ( !paddle_two_y(4) & ( (!paddle_two_y(5) & paddle_two_y(6)) ) ) ) # ( !paddle_two_y(1) & ( !paddle_two_y(4) & ( (!paddle_two_y(5) & paddle_two_y(6)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000100000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(2),
	datab => ALT_INV_paddle_two_y(3),
	datac => ALT_INV_paddle_two_y(5),
	datad => ALT_INV_paddle_two_y(6),
	datae => ALT_INV_paddle_two_y(1),
	dataf => ALT_INV_paddle_two_y(4),
	combout => \paddle_two_y~0_combout\);

-- Location: LABCELL_X37_Y62_N12
\paddle_two_y~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~1_combout\ = ( paddle_two_y(4) & ( (!paddle_two_y(7) & (!paddle_two_y(6) & (paddle_two_y(5) & paddle_two_y(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010000000000000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(7),
	datab => ALT_INV_paddle_two_y(6),
	datac => ALT_INV_paddle_two_y(5),
	datad => ALT_INV_paddle_two_y(3),
	dataf => ALT_INV_paddle_two_y(4),
	combout => \paddle_two_y~1_combout\);

-- Location: LABCELL_X37_Y62_N0
\paddle_two_y~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~6_combout\ = ( !\KEY[0]~input_o\ & ( (((\paddle_two_y~1_combout\ & ((!paddle_two_y(1)) # (paddle_two_y(2)))))) ) ) # ( \KEY[0]~input_o\ & ( ((((\paddle_two_y~0_combout\)) # (paddle_two_y(7))) # (\KEY[1]~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000000000001111111111111111110101111101010011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(2),
	datab => \ALT_INV_KEY[1]~input_o\,
	datac => ALT_INV_paddle_two_y(7),
	datad => \ALT_INV_paddle_two_y~0_combout\,
	datae => \ALT_INV_KEY[0]~input_o\,
	dataf => \ALT_INV_paddle_two_y~1_combout\,
	datag => ALT_INV_paddle_two_y(1),
	combout => \paddle_two_y~6_combout\);

-- Location: LABCELL_X35_Y62_N0
\Add5~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~25_sumout\ = SUM(( paddle_two_y(1) ) + ( VCC ) + ( !VCC ))
-- \Add5~26\ = CARRY(( paddle_two_y(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(1),
	cin => GND,
	sumout => \Add5~25_sumout\,
	cout => \Add5~26\);

-- Location: LABCELL_X36_Y62_N0
\Add6~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add6~25_sumout\ = SUM(( paddle_two_y(1) ) + ( VCC ) + ( !VCC ))
-- \Add6~26\ = CARRY(( paddle_two_y(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(1),
	cin => GND,
	sumout => \Add6~25_sumout\,
	cout => \Add6~26\);

-- Location: LABCELL_X35_Y62_N30
\Selector14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector14~0_combout\ = ( paddle_two_y(1) & ( \KEY[0]~input_o\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ((\Add6~25_sumout\) # (\paddle_two_y~6_combout\))) ) ) ) # ( !paddle_two_y(1) & ( \KEY[0]~input_o\ & ( 
-- (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\paddle_two_y~6_combout\ & \Add6~25_sumout\)) ) ) ) # ( paddle_two_y(1) & ( !\KEY[0]~input_o\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ((\Add5~25_sumout\) # (\paddle_two_y~6_combout\))) ) ) ) # ( 
-- !paddle_two_y(1) & ( !\KEY[0]~input_o\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\paddle_two_y~6_combout\ & \Add5~25_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000101010001010100000000010001000001000101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_paddle_two_y~6_combout\,
	datac => \ALT_INV_Add5~25_sumout\,
	datad => \ALT_INV_Add6~25_sumout\,
	datae => ALT_INV_paddle_two_y(1),
	dataf => \ALT_INV_KEY[0]~input_o\,
	combout => \Selector14~0_combout\);

-- Location: FF_X35_Y62_N32
\paddle_two_y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector14~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(1));

-- Location: LABCELL_X35_Y62_N3
\Add5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add5~21_sumout\ = SUM(( !paddle_two_y(2) ) + ( VCC ) + ( \Add5~26\ ))
-- \Add5~22\ = CARRY(( !paddle_two_y(2) ) + ( VCC ) + ( \Add5~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(2),
	cin => \Add5~26\,
	sumout => \Add5~21_sumout\,
	cout => \Add5~22\);

-- Location: LABCELL_X36_Y62_N30
\Selector28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector28~0_combout\ = ( paddle_two_y(2) & ( \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ( ((!\KEY[0]~input_o\ & ((!\Add5~21_sumout\))) # (\KEY[0]~input_o\ & (!\Add6~21_sumout\))) # (\paddle_two_y~6_combout\) ) ) ) # ( !paddle_two_y(2) & ( 
-- \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ( (!\paddle_two_y~6_combout\ & ((!\KEY[0]~input_o\ & ((!\Add5~21_sumout\))) # (\KEY[0]~input_o\ & (!\Add6~21_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011100000001000001110111100101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add6~21_sumout\,
	datab => \ALT_INV_KEY[0]~input_o\,
	datac => \ALT_INV_paddle_two_y~6_combout\,
	datad => \ALT_INV_Add5~21_sumout\,
	datae => ALT_INV_paddle_two_y(2),
	dataf => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	combout => \Selector28~0_combout\);

-- Location: FF_X36_Y62_N32
\paddle_two_y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector28~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(2));

-- Location: LABCELL_X35_Y62_N36
\Selector27~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector27~0_combout\ = ( paddle_two_y(3) & ( \paddle_two_y~6_combout\ & ( \state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ ) ) ) # ( paddle_two_y(3) & ( !\paddle_two_y~6_combout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ((!\KEY[0]~input_o\ & 
-- (!\Add5~13_sumout\)) # (\KEY[0]~input_o\ & ((!\Add6~13_sumout\))))) ) ) ) # ( !paddle_two_y(3) & ( !\paddle_two_y~6_combout\ & ( (\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & ((!\KEY[0]~input_o\ & (!\Add5~13_sumout\)) # (\KEY[0]~input_o\ & 
-- ((!\Add6~13_sumout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110100001000000011010000100000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[0]~input_o\,
	datab => \ALT_INV_Add5~13_sumout\,
	datac => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datad => \ALT_INV_Add6~13_sumout\,
	datae => ALT_INV_paddle_two_y(3),
	dataf => \ALT_INV_paddle_two_y~6_combout\,
	combout => \Selector27~0_combout\);

-- Location: FF_X35_Y62_N38
\paddle_two_y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector27~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_two_y(3));

-- Location: LABCELL_X35_Y62_N45
\paddle_two_y~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~4_combout\ = ( \paddle_two_y~6_combout\ & ( !paddle_two_y(3) ) ) # ( !\paddle_two_y~6_combout\ & ( (!\KEY[0]~input_o\ & ((\Add5~13_sumout\))) # (\KEY[0]~input_o\ & (\Add6~13_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001101100011011000110110001101111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[0]~input_o\,
	datab => \ALT_INV_Add6~13_sumout\,
	datac => \ALT_INV_Add5~13_sumout\,
	datad => ALT_INV_paddle_two_y(3),
	dataf => \ALT_INV_paddle_two_y~6_combout\,
	combout => \paddle_two_y~4_combout\);

-- Location: MLABCELL_X34_Y62_N30
\Add9~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~17_sumout\ = SUM(( !\Equal7~1_combout\ $ (\puck_velocity.y\(0)) ) + ( \Equal7~1_combout\ ) + ( !VCC ))
-- \Add9~18\ = CARRY(( !\Equal7~1_combout\ $ (\puck_velocity.y\(0)) ) + ( \Equal7~1_combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000001100110000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	datad => \ALT_INV_puck_velocity.y\(0),
	cin => GND,
	sumout => \Add9~17_sumout\,
	cout => \Add9~18\);

-- Location: MLABCELL_X34_Y62_N33
\Add9~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~13_sumout\ = SUM(( GND ) + ( !\Equal7~1_combout\ $ (\puck_velocity.y\(1)) ) + ( \Add9~18\ ))
-- \Add9~14\ = CARRY(( GND ) + ( !\Equal7~1_combout\ $ (\puck_velocity.y\(1)) ) + ( \Add9~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100111100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	dataf => \ALT_INV_puck_velocity.y\(1),
	cin => \Add9~18\,
	sumout => \Add9~13_sumout\,
	cout => \Add9~14\);

-- Location: MLABCELL_X34_Y62_N0
\Add10~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~17_sumout\ = SUM(( \Equal8~1_combout\ ) + ( !\Equal8~1_combout\ $ (!\Add9~17_sumout\) ) + ( !VCC ))
-- \Add10~18\ = CARRY(( \Equal8~1_combout\ ) + ( !\Equal8~1_combout\ $ (!\Add9~17_sumout\) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	dataf => \ALT_INV_Add9~17_sumout\,
	cin => GND,
	sumout => \Add10~17_sumout\,
	cout => \Add10~18\);

-- Location: MLABCELL_X34_Y62_N3
\Add10~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~13_sumout\ = SUM(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~13_sumout\) ) + ( \Add10~18\ ))
-- \Add10~14\ = CARRY(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~13_sumout\) ) + ( \Add10~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	dataf => \ALT_INV_Add9~13_sumout\,
	cin => \Add10~18\,
	sumout => \Add10~13_sumout\,
	cout => \Add10~14\);

-- Location: LABCELL_X33_Y62_N51
\Add10~13_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~13_wirecell_combout\ = !\Add10~13_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add10~13_sumout\,
	combout => \Add10~13_wirecell_combout\);

-- Location: LABCELL_X33_Y62_N57
\Selector30~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector30~0_combout\ = ( \state.ERASE_PUCK~q\ ) # ( !\state.ERASE_PUCK~q\ & ( !\state.INIT~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~q\,
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \Selector30~0_combout\);

-- Location: FF_X33_Y62_N52
\puck_velocity.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~13_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(1));

-- Location: MLABCELL_X34_Y62_N36
\Add9~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~9_sumout\ = SUM(( !\Equal7~1_combout\ $ (\puck_velocity.y\(2)) ) + ( GND ) + ( \Add9~14\ ))
-- \Add9~10\ = CARRY(( !\Equal7~1_combout\ $ (\puck_velocity.y\(2)) ) + ( GND ) + ( \Add9~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	datad => \ALT_INV_puck_velocity.y\(2),
	cin => \Add9~14\,
	sumout => \Add9~9_sumout\,
	cout => \Add9~10\);

-- Location: MLABCELL_X34_Y62_N6
\Add10~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~9_sumout\ = SUM(( !\Equal8~1_combout\ $ (!\Add9~9_sumout\) ) + ( GND ) + ( \Add10~14\ ))
-- \Add10~10\ = CARRY(( !\Equal8~1_combout\ $ (!\Add9~9_sumout\) ) + ( GND ) + ( \Add10~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	datac => \ALT_INV_Add9~9_sumout\,
	cin => \Add10~14\,
	sumout => \Add10~9_sumout\,
	cout => \Add10~10\);

-- Location: MLABCELL_X34_Y62_N54
\Add10~9_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~9_wirecell_combout\ = ( !\Add10~9_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add10~9_sumout\,
	combout => \Add10~9_wirecell_combout\);

-- Location: FF_X34_Y62_N55
\puck_velocity.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~9_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(2));

-- Location: MLABCELL_X34_Y62_N39
\Add9~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~5_sumout\ = SUM(( !\Equal7~1_combout\ $ (\puck_velocity.y\(3)) ) + ( GND ) + ( \Add9~10\ ))
-- \Add9~6\ = CARRY(( !\Equal7~1_combout\ $ (\puck_velocity.y\(3)) ) + ( GND ) + ( \Add9~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	datac => \ALT_INV_puck_velocity.y\(3),
	cin => \Add9~10\,
	sumout => \Add9~5_sumout\,
	cout => \Add9~6\);

-- Location: MLABCELL_X34_Y62_N9
\Add10~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~5_sumout\ = SUM(( !\Equal8~1_combout\ $ (!\Add9~5_sumout\) ) + ( GND ) + ( \Add10~10\ ))
-- \Add10~6\ = CARRY(( !\Equal8~1_combout\ $ (!\Add9~5_sumout\) ) + ( GND ) + ( \Add10~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	datac => \ALT_INV_Add9~5_sumout\,
	cin => \Add10~10\,
	sumout => \Add10~5_sumout\,
	cout => \Add10~6\);

-- Location: MLABCELL_X34_Y62_N57
\Add10~5_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~5_wirecell_combout\ = ( !\Add10~5_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add10~5_sumout\,
	combout => \Add10~5_wirecell_combout\);

-- Location: FF_X34_Y62_N59
\puck_velocity.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~5_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(3));

-- Location: FF_X34_Y63_N31
\puck.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~17_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(0));

-- Location: MLABCELL_X34_Y63_N30
\Add8~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~17_sumout\ = SUM(( \puck.y\(0) ) + ( !\puck_velocity.y\(0) ) + ( !VCC ))
-- \Add8~18\ = CARRY(( \puck.y\(0) ) + ( !\puck_velocity.y\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity.y\(0),
	datad => \ALT_INV_puck.y\(0),
	cin => GND,
	sumout => \Add8~17_sumout\,
	cout => \Add8~18\);

-- Location: MLABCELL_X34_Y63_N33
\Add8~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~13_sumout\ = SUM(( \puck.y\(1) ) + ( !\puck_velocity.y\(1) ) + ( \Add8~18\ ))
-- \Add8~14\ = CARRY(( \puck.y\(1) ) + ( !\puck_velocity.y\(1) ) + ( \Add8~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity.y\(1),
	datad => \ALT_INV_puck.y\(1),
	cin => \Add8~18\,
	sumout => \Add8~13_sumout\,
	cout => \Add8~14\);

-- Location: FF_X34_Y63_N34
\puck.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~13_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(1));

-- Location: MLABCELL_X34_Y63_N36
\Add8~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~9_sumout\ = SUM(( !\puck.y\(2) ) + ( !\puck_velocity.y\(2) ) + ( \Add8~14\ ))
-- \Add8~10\ = CARRY(( !\puck.y\(2) ) + ( !\puck_velocity.y\(2) ) + ( \Add8~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck.y\(2),
	dataf => \ALT_INV_puck_velocity.y\(2),
	cin => \Add8~14\,
	sumout => \Add8~9_sumout\,
	cout => \Add8~10\);

-- Location: LABCELL_X35_Y63_N39
\Add8~9_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~9_wirecell_combout\ = ( !\Add8~9_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add8~9_sumout\,
	combout => \Add8~9_wirecell_combout\);

-- Location: FF_X35_Y63_N41
\puck.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~9_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(2));

-- Location: MLABCELL_X34_Y63_N39
\Add8~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~5_sumout\ = SUM(( !\puck_velocity.y\(3) ) + ( !\puck.y\(3) ) + ( \Add8~10\ ))
-- \Add8~6\ = CARRY(( !\puck_velocity.y\(3) ) + ( !\puck.y\(3) ) + ( \Add8~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity.y\(3),
	datac => \ALT_INV_puck.y\(3),
	cin => \Add8~10\,
	sumout => \Add8~5_sumout\,
	cout => \Add8~6\);

-- Location: LABCELL_X33_Y62_N36
\Add8~5_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~5_wirecell_combout\ = !\Add8~5_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add8~5_sumout\,
	combout => \Add8~5_wirecell_combout\);

-- Location: FF_X33_Y62_N37
\puck.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~5_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(3));

-- Location: LABCELL_X35_Y64_N57
\Add7~1_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~1_wirecell_combout\ = ( !\Add7~1_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add7~1_sumout\,
	combout => \Add7~1_wirecell_combout\);

-- Location: FF_X35_Y64_N59
\puck.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~1_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(4));

-- Location: FF_X34_Y64_N16
\puck_velocity.x[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity.x[0]~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x[0]~DUPLICATE_q\);

-- Location: LABCELL_X35_Y64_N0
\Add7~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~9_sumout\ = SUM(( \puck.x\(0) ) + ( !\puck_velocity.x[0]~DUPLICATE_q\ ) + ( !VCC ))
-- \Add7~10\ = CARRY(( \puck.x\(0) ) + ( !\puck_velocity.x[0]~DUPLICATE_q\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity.x[0]~DUPLICATE_q\,
	datad => \ALT_INV_puck.x\(0),
	cin => GND,
	sumout => \Add7~9_sumout\,
	cout => \Add7~10\);

-- Location: FF_X35_Y64_N1
\puck.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~9_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(0));

-- Location: LABCELL_X35_Y64_N3
\Add7~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~21_sumout\ = SUM(( \puck.x\(1) ) + ( \puck_velocity.x\(1) ) + ( \Add7~10\ ))
-- \Add7~22\ = CARRY(( \puck.x\(1) ) + ( \puck_velocity.x\(1) ) + ( \Add7~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity.x\(1),
	datad => \ALT_INV_puck.x\(1),
	cin => \Add7~10\,
	sumout => \Add7~21_sumout\,
	cout => \Add7~22\);

-- Location: FF_X35_Y64_N5
\puck.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~21_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(1));

-- Location: LABCELL_X35_Y64_N6
\Add7~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~25_sumout\ = SUM(( \puck.x\(2) ) + ( \puck_velocity.x\(2) ) + ( \Add7~22\ ))
-- \Add7~26\ = CARRY(( \puck.x\(2) ) + ( \puck_velocity.x\(2) ) + ( \Add7~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity.x\(2),
	datad => \ALT_INV_puck.x\(2),
	cin => \Add7~22\,
	sumout => \Add7~25_sumout\,
	cout => \Add7~26\);

-- Location: FF_X35_Y64_N7
\puck.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~25_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(2));

-- Location: LABCELL_X35_Y64_N9
\Add7~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~29_sumout\ = SUM(( \puck.x\(3) ) + ( \puck_velocity.x\(3) ) + ( \Add7~26\ ))
-- \Add7~30\ = CARRY(( \puck.x\(3) ) + ( \puck_velocity.x\(3) ) + ( \Add7~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity.x\(3),
	datad => \ALT_INV_puck.x\(3),
	cin => \Add7~26\,
	sumout => \Add7~29_sumout\,
	cout => \Add7~30\);

-- Location: FF_X35_Y64_N10
\puck.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~29_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(3));

-- Location: LABCELL_X35_Y64_N12
\Add7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~1_sumout\ = SUM(( !\puck.x\(4) ) + ( \puck_velocity.x\(4) ) + ( \Add7~30\ ))
-- \Add7~2\ = CARRY(( !\puck.x\(4) ) + ( \puck_velocity.x\(4) ) + ( \Add7~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity.x\(4),
	datad => \ALT_INV_puck.x\(4),
	cin => \Add7~30\,
	sumout => \Add7~1_sumout\,
	cout => \Add7~2\);

-- Location: LABCELL_X35_Y64_N33
\Equal9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal9~0_combout\ = ( !\Add7~25_sumout\ & ( (\Add7~21_sumout\ & !\Add7~29_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100010001000100010000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add7~21_sumout\,
	datab => \ALT_INV_Add7~29_sumout\,
	dataf => \ALT_INV_Add7~25_sumout\,
	combout => \Equal9~0_combout\);

-- Location: FF_X35_Y64_N16
\puck.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~13_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(5));

-- Location: LABCELL_X35_Y64_N15
\Add7~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~13_sumout\ = SUM(( \puck.x\(5) ) + ( \puck_velocity.x\(5) ) + ( \Add7~2\ ))
-- \Add7~14\ = CARRY(( \puck.x\(5) ) + ( \puck_velocity.x\(5) ) + ( \Add7~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity.x\(5),
	datad => \ALT_INV_puck.x\(5),
	cin => \Add7~2\,
	sumout => \Add7~13_sumout\,
	cout => \Add7~14\);

-- Location: LABCELL_X33_Y62_N42
\Add7~17_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~17_wirecell_combout\ = ( !\Add7~17_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add7~17_sumout\,
	combout => \Add7~17_wirecell_combout\);

-- Location: FF_X33_Y62_N43
\puck.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~17_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(6));

-- Location: LABCELL_X35_Y64_N18
\Add7~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~17_sumout\ = SUM(( \puck_velocity.x\(6) ) + ( !\puck.x\(6) ) + ( \Add7~14\ ))
-- \Add7~18\ = CARRY(( \puck_velocity.x\(6) ) + ( !\puck.x\(6) ) + ( \Add7~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity.x\(6),
	datac => \ALT_INV_puck.x\(6),
	cin => \Add7~14\,
	sumout => \Add7~17_sumout\,
	cout => \Add7~18\);

-- Location: LABCELL_X35_Y64_N36
\Equal9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal9~1_combout\ = ( \Add7~9_sumout\ & ( \Add7~17_sumout\ ) ) # ( !\Add7~9_sumout\ & ( \Add7~17_sumout\ ) ) # ( \Add7~9_sumout\ & ( !\Add7~17_sumout\ & ( (((!\Equal9~0_combout\) # (\Add7~13_sumout\)) # (\Add7~1_sumout\)) # (\Add7~5_sumout\) ) ) ) # ( 
-- !\Add7~9_sumout\ & ( !\Add7~17_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111101111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add7~5_sumout\,
	datab => \ALT_INV_Add7~1_sumout\,
	datac => \ALT_INV_Equal9~0_combout\,
	datad => \ALT_INV_Add7~13_sumout\,
	datae => \ALT_INV_Add7~9_sumout\,
	dataf => \ALT_INV_Add7~17_sumout\,
	combout => \Equal9~1_combout\);

-- Location: LABCELL_X33_Y67_N0
\Add2~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~125_sumout\ = SUM(( clock_counter(0) ) + ( VCC ) + ( !VCC ))
-- \Add2~126\ = CARRY(( clock_counter(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(0),
	cin => GND,
	sumout => \Add2~125_sumout\,
	cout => \Add2~126\);

-- Location: LABCELL_X33_Y66_N0
\Add2~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~73_sumout\ = SUM(( clock_counter(20) ) + ( GND ) + ( \Add2~2\ ))
-- \Add2~74\ = CARRY(( clock_counter(20) ) + ( GND ) + ( \Add2~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(20),
	cin => \Add2~2\,
	sumout => \Add2~73_sumout\,
	cout => \Add2~74\);

-- Location: LABCELL_X33_Y66_N3
\Add2~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~89_sumout\ = SUM(( clock_counter(21) ) + ( GND ) + ( \Add2~74\ ))
-- \Add2~90\ = CARRY(( clock_counter(21) ) + ( GND ) + ( \Add2~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(21),
	cin => \Add2~74\,
	sumout => \Add2~89_sumout\,
	cout => \Add2~90\);

-- Location: LABCELL_X37_Y62_N30
\Add12~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~17_sumout\ = SUM(( paddle_two_y(1) ) + ( VCC ) + ( !VCC ))
-- \Add12~18\ = CARRY(( paddle_two_y(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(1),
	cin => GND,
	sumout => \Add12~17_sumout\,
	cout => \Add12~18\);

-- Location: LABCELL_X37_Y62_N33
\Add12~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~21_sumout\ = SUM(( !paddle_two_y(2) ) + ( GND ) + ( \Add12~18\ ))
-- \Add12~22\ = CARRY(( !paddle_two_y(2) ) + ( GND ) + ( \Add12~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(2),
	cin => \Add12~18\,
	sumout => \Add12~21_sumout\,
	cout => \Add12~22\);

-- Location: LABCELL_X37_Y62_N36
\Add12~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~5_sumout\ = SUM(( !paddle_two_y(3) ) + ( VCC ) + ( \Add12~22\ ))
-- \Add12~6\ = CARRY(( !paddle_two_y(3) ) + ( VCC ) + ( \Add12~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(3),
	cin => \Add12~22\,
	sumout => \Add12~5_sumout\,
	cout => \Add12~6\);

-- Location: LABCELL_X37_Y62_N39
\Add12~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~9_sumout\ = SUM(( !paddle_two_y(4) ) + ( GND ) + ( \Add12~6\ ))
-- \Add12~10\ = CARRY(( !paddle_two_y(4) ) + ( GND ) + ( \Add12~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_two_y(4),
	cin => \Add12~6\,
	sumout => \Add12~9_sumout\,
	cout => \Add12~10\);

-- Location: LABCELL_X37_Y62_N42
\Add12~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~13_sumout\ = SUM(( !paddle_two_y(5) ) + ( GND ) + ( \Add12~10\ ))
-- \Add12~14\ = CARRY(( !paddle_two_y(5) ) + ( GND ) + ( \Add12~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(5),
	cin => \Add12~10\,
	sumout => \Add12~13_sumout\,
	cout => \Add12~14\);

-- Location: LABCELL_X37_Y62_N45
\Add12~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~25_sumout\ = SUM(( paddle_two_y(6) ) + ( GND ) + ( \Add12~14\ ))
-- \Add12~26\ = CARRY(( paddle_two_y(6) ) + ( GND ) + ( \Add12~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(6),
	cin => \Add12~14\,
	sumout => \Add12~25_sumout\,
	cout => \Add12~26\);

-- Location: LABCELL_X36_Y65_N33
\Equal6~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~3_combout\ = ( \Add12~25_sumout\ & ( !\draw.y\(6) ) ) # ( !\Add12~25_sumout\ & ( \draw.y\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(6),
	dataf => \ALT_INV_Add12~25_sumout\,
	combout => \Equal6~3_combout\);

-- Location: LABCELL_X37_Y62_N48
\Add12~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~29_sumout\ = SUM(( paddle_two_y(7) ) + ( GND ) + ( \Add12~26\ ))
-- \Add12~30\ = CARRY(( paddle_two_y(7) ) + ( GND ) + ( \Add12~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_two_y(7),
	cin => \Add12~26\,
	sumout => \Add12~29_sumout\,
	cout => \Add12~30\);

-- Location: LABCELL_X35_Y63_N48
\Selector14~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector14~2_combout\ = ( \puck.y\(1) & ( paddle_two_y(1) & ( (\draw~0_combout\ & (!\state.ERASE_PADDLE_TWO_ENTER~q\ & ((!\state.ERASE_PADDLE_ONE_ENTER~q\) # (!paddle_one_y(1))))) ) ) ) # ( !\puck.y\(1) & ( paddle_two_y(1) & ( 
-- (!\state.ERASE_PADDLE_TWO_ENTER~q\ & ((!\state.ERASE_PADDLE_ONE_ENTER~q\) # (!paddle_one_y(1)))) ) ) ) # ( \puck.y\(1) & ( !paddle_two_y(1) & ( (\draw~0_combout\ & ((!\state.ERASE_PADDLE_ONE_ENTER~q\) # (!paddle_one_y(1)))) ) ) ) # ( !\puck.y\(1) & ( 
-- !paddle_two_y(1) & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\) # (!paddle_one_y(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111110101010001100110010001011110000101000000011000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datad => ALT_INV_paddle_one_y(1),
	datae => \ALT_INV_puck.y\(1),
	dataf => ALT_INV_paddle_two_y(1),
	combout => \Selector14~2_combout\);

-- Location: LABCELL_X37_Y65_N12
\Equal6~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~4_combout\ = ( \Equal6~1_combout\ & ( (!\Add12~29_sumout\ & (!\draw.y\(7) & (!\Add12~25_sumout\ $ (\draw.y\(6))))) # (\Add12~29_sumout\ & (\draw.y\(7) & (!\Add12~25_sumout\ $ (\draw.y\(6))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000010010000011000001001000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~29_sumout\,
	datab => \ALT_INV_Add12~25_sumout\,
	datac => \ALT_INV_draw.y\(6),
	datad => \ALT_INV_draw.y\(7),
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Equal6~4_combout\);

-- Location: LABCELL_X37_Y65_N27
\Equal4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~5_combout\ = ( \Equal4~1_combout\ & ( (!\draw.y\(6) & (!\Add11~25_sumout\ & (!\Add11~29_sumout\ $ (\draw.y\(7))))) # (\draw.y\(6) & (\Add11~25_sumout\ & (!\Add11~29_sumout\ $ (\draw.y\(7))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010010000000010011001000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(6),
	datab => \ALT_INV_Add11~25_sumout\,
	datac => \ALT_INV_Add11~29_sumout\,
	datad => \ALT_INV_draw.y\(7),
	dataf => \ALT_INV_Equal4~1_combout\,
	combout => \Equal4~5_combout\);

-- Location: MLABCELL_X39_Y65_N18
\Equal4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~0_combout\ = ( \draw.y\(4) & ( \Add11~5_sumout\ & ( (\Add11~9_sumout\ & (\draw.y\(3) & (!\Add11~13_sumout\ $ (\draw.y\(5))))) ) ) ) # ( !\draw.y\(4) & ( \Add11~5_sumout\ & ( (!\Add11~9_sumout\ & (\draw.y\(3) & (!\Add11~13_sumout\ $ 
-- (\draw.y\(5))))) ) ) ) # ( \draw.y\(4) & ( !\Add11~5_sumout\ & ( (\Add11~9_sumout\ & (!\draw.y\(3) & (!\Add11~13_sumout\ $ (\draw.y\(5))))) ) ) ) # ( !\draw.y\(4) & ( !\Add11~5_sumout\ & ( (!\Add11~9_sumout\ & (!\draw.y\(3) & (!\Add11~13_sumout\ $ 
-- (\draw.y\(5))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001000000000000000010010000000000000000100100000000000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~13_sumout\,
	datab => \ALT_INV_draw.y\(5),
	datac => \ALT_INV_Add11~9_sumout\,
	datad => \ALT_INV_draw.y\(3),
	datae => \ALT_INV_draw.y\(4),
	dataf => \ALT_INV_Add11~5_sumout\,
	combout => \Equal4~0_combout\);

-- Location: MLABCELL_X34_Y65_N54
\Selector6~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~7_combout\ = ( \state.ERASE_PADDLE_ONE_LOOP~q\ & ( \Equal4~0_combout\ & ( (\Equal4~5_combout\ & ((!\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\) # ((\Equal6~4_combout\ & \Equal6~0_combout\)))) ) ) ) # ( !\state.ERASE_PADDLE_ONE_LOOP~q\ & ( 
-- \Equal4~0_combout\ & ( (!\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\) # ((\Equal6~4_combout\ & \Equal6~0_combout\)) ) ) ) # ( !\state.ERASE_PADDLE_ONE_LOOP~q\ & ( !\Equal4~0_combout\ & ( (!\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\) # ((\Equal6~4_combout\ & 
-- \Equal6~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011011101000000000000000011001100110111010000110000001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~4_combout\,
	datab => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_Equal4~5_combout\,
	datad => \ALT_INV_Equal6~0_combout\,
	datae => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \Selector6~7_combout\);

-- Location: LABCELL_X37_Y65_N24
\Equal4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~2_combout\ = ( \draw.y\(7) & ( (\Add11~29_sumout\ & (!\Add11~25_sumout\ $ (\draw.y\(6)))) ) ) # ( !\draw.y\(7) & ( (!\Add11~29_sumout\ & (!\Add11~25_sumout\ $ (\draw.y\(6)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100001100000000110000110000000000000000110000110000000011000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add11~25_sumout\,
	datac => \ALT_INV_draw.y\(6),
	datad => \ALT_INV_Add11~29_sumout\,
	dataf => \ALT_INV_draw.y\(7),
	combout => \Equal4~2_combout\);

-- Location: LABCELL_X35_Y66_N0
\Selector75~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector75~0_combout\ = ( \Equal4~0_combout\ & ( (\state.DRAW_PADDLE_ONE_LOOP~q\ & ((!\Equal4~2_combout\) # ((!\Equal4~1_combout\) # (\Add11~1_sumout\)))) ) ) # ( !\Equal4~0_combout\ & ( \state.DRAW_PADDLE_ONE_LOOP~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010001010101010101000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Equal4~2_combout\,
	datac => \ALT_INV_Add11~1_sumout\,
	datad => \ALT_INV_Equal4~1_combout\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \Selector75~0_combout\);

-- Location: LABCELL_X37_Y66_N0
\Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~13_sumout\ = SUM(( \draw.x\(0) ) + ( VCC ) + ( !VCC ))
-- \Add1~14\ = CARRY(( \draw.x\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(0),
	cin => GND,
	sumout => \Add1~13_sumout\,
	cout => \Add1~14\);

-- Location: LABCELL_X37_Y66_N3
\Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~17_sumout\ = SUM(( \draw.x\(1) ) + ( GND ) + ( \Add1~14\ ))
-- \Add1~18\ = CARRY(( \draw.x\(1) ) + ( GND ) + ( \Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(1),
	cin => \Add1~14\,
	sumout => \Add1~17_sumout\,
	cout => \Add1~18\);

-- Location: FF_X33_Y65_N8
\state.IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector71~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.IDLE~q\);

-- Location: LABCELL_X33_Y65_N45
\Selector6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~2_combout\ = ( \state.ERASE_PUCK~q\ & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (!\state.DRAW_PADDLE_ONE_ENTER~q\ & !\puck.x\(1))) ) ) # ( !\state.ERASE_PUCK~q\ & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (!\state.DRAW_PADDLE_ONE_ENTER~q\ & 
-- ((!\state.DRAW_PUCK~q\) # (!\puck.x\(1))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010000000100010001000000010001000000000001000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datab => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datac => \ALT_INV_state.DRAW_PUCK~q\,
	datad => \ALT_INV_puck.x\(1),
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \Selector6~2_combout\);

-- Location: MLABCELL_X34_Y65_N36
\WideOr16~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr16~1_combout\ = ( !\state.DRAW_PADDLE_ONE_LOOP~q\ & ( (!\state.ERASE_PADDLE_ONE_LOOP~q\ & (!\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & !\state.DRAW_PADDLE_TWO_LOOP~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	combout => \WideOr16~1_combout\);

-- Location: LABCELL_X33_Y65_N18
\Selector6~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~3_combout\ = ( \draw.x\(1) & ( (!\state.IDLE~q\ & (\Selector6~2_combout\ & \WideOr16~1_combout\)) ) ) # ( !\draw.x\(1) & ( \Selector6~2_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000011000000000000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.IDLE~q\,
	datac => \ALT_INV_Selector6~2_combout\,
	datad => \ALT_INV_WideOr16~1_combout\,
	dataf => \ALT_INV_draw.x\(1),
	combout => \Selector6~3_combout\);

-- Location: LABCELL_X33_Y65_N24
\Selector6~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~4_combout\ = ( \Add1~17_sumout\ & ( \Selector6~3_combout\ & ( (\Selector6~0_combout\ & (!\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\state.ERASE_PADDLE_TWO_ENTER~q\ & !\Selector6~1_combout\))) ) ) ) # ( !\Add1~17_sumout\ & ( 
-- \Selector6~3_combout\ & ( (!\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\state.ERASE_PADDLE_TWO_ENTER~q\ & !\Selector6~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector6~0_combout\,
	datab => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datac => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datad => \ALT_INV_Selector6~1_combout\,
	datae => \ALT_INV_Add1~17_sumout\,
	dataf => \ALT_INV_Selector6~3_combout\,
	combout => \Selector6~4_combout\);

-- Location: MLABCELL_X34_Y65_N30
\Selector6~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~5_combout\ = ( \state.ERASE_PADDLE_ONE_LOOP~q\ & ( \Equal4~3_combout\ & ( (((!\Selector6~4_combout\) # (\Selector79~0_combout\)) # (\Selector77~0_combout\)) # (\Selector75~0_combout\) ) ) ) # ( !\state.ERASE_PADDLE_ONE_LOOP~q\ & ( 
-- \Equal4~3_combout\ & ( (((!\Selector6~4_combout\) # (\Selector79~0_combout\)) # (\Selector77~0_combout\)) # (\Selector75~0_combout\) ) ) ) # ( \state.ERASE_PADDLE_ONE_LOOP~q\ & ( !\Equal4~3_combout\ ) ) # ( !\state.ERASE_PADDLE_ONE_LOOP~q\ & ( 
-- !\Equal4~3_combout\ & ( (((!\Selector6~4_combout\) # (\Selector79~0_combout\)) # (\Selector77~0_combout\)) # (\Selector75~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111101111111111111111111111111111111011111111111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector75~0_combout\,
	datab => \ALT_INV_Selector77~0_combout\,
	datac => \ALT_INV_Selector79~0_combout\,
	datad => \ALT_INV_Selector6~4_combout\,
	datae => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	dataf => \ALT_INV_Equal4~3_combout\,
	combout => \Selector6~5_combout\);

-- Location: FF_X34_Y65_N32
\draw.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector6~5_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(1));

-- Location: MLABCELL_X34_Y66_N15
\Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = ( \draw.x\(0) & ( \draw.x\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(1),
	dataf => \ALT_INV_draw.x\(0),
	combout => \Equal0~1_combout\);

-- Location: MLABCELL_X34_Y66_N36
\Selector6~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~6_combout\ = ( \Equal0~1_combout\ & ( (\Equal0~0_combout\ & \state.START~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_state.START~q\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \Selector6~6_combout\);

-- Location: MLABCELL_X34_Y66_N54
\Selector15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector15~0_combout\ = ( !\state.IDLE~DUPLICATE_q\ & ( \draw.x\(0) & ( (!\state.START~q\) # ((\draw.x\(1) & \Equal0~0_combout\)) ) ) ) # ( !\state.IDLE~DUPLICATE_q\ & ( !\draw.x\(0) & ( (!\state.START~q\ & (((!\Equal0~0_combout\) # 
-- (\WideOr16~2_combout\)) # (\draw.x\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101000101010000000000000000010101010101110110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_draw.x\(1),
	datac => \ALT_INV_WideOr16~2_combout\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_state.IDLE~DUPLICATE_q\,
	dataf => \ALT_INV_draw.x\(0),
	combout => \Selector15~0_combout\);

-- Location: MLABCELL_X34_Y65_N0
\Selector15~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector15~1_combout\ = ( \draw~0_combout\ & ( (!\Selector6~1_combout\ & ((!\draw.y\(0)) # (\Selector15~0_combout\))) ) ) # ( !\draw~0_combout\ & ( (!\Selector6~1_combout\ & (!\puck.y\(0) & ((!\draw.y\(0)) # (\Selector15~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000101000000000100010100000000010001010100010101000101010001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector6~1_combout\,
	datab => \ALT_INV_draw.y\(0),
	datac => \ALT_INV_Selector15~0_combout\,
	datad => \ALT_INV_puck.y\(0),
	dataf => \ALT_INV_draw~0_combout\,
	combout => \Selector15~1_combout\);

-- Location: LABCELL_X37_Y65_N30
\Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~25_sumout\ = SUM(( \draw.y\(0) ) + ( VCC ) + ( !VCC ))
-- \Add0~26\ = CARRY(( \draw.y\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(0),
	cin => GND,
	sumout => \Add0~25_sumout\,
	cout => \Add0~26\);

-- Location: MLABCELL_X34_Y65_N24
\Selector15~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector15~2_combout\ = ( \Selector15~1_combout\ & ( \Add0~25_sumout\ & ( ((!\Selector6~7_combout\) # ((\Selector75~0_combout\) # (\Selector6~6_combout\))) # (\Selector79~0_combout\) ) ) ) # ( !\Selector15~1_combout\ & ( \Add0~25_sumout\ ) ) # ( 
-- !\Selector15~1_combout\ & ( !\Add0~25_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111111101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector79~0_combout\,
	datab => \ALT_INV_Selector6~7_combout\,
	datac => \ALT_INV_Selector6~6_combout\,
	datad => \ALT_INV_Selector75~0_combout\,
	datae => \ALT_INV_Selector15~1_combout\,
	dataf => \ALT_INV_Add0~25_sumout\,
	combout => \Selector15~2_combout\);

-- Location: FF_X34_Y65_N25
\draw.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector15~2_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(0));

-- Location: LABCELL_X37_Y65_N33
\Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~21_sumout\ = SUM(( \draw.y\(1) ) + ( GND ) + ( \Add0~26\ ))
-- \Add0~22\ = CARRY(( \draw.y\(1) ) + ( GND ) + ( \Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(1),
	cin => \Add0~26\,
	sumout => \Add0~21_sumout\,
	cout => \Add0~22\);

-- Location: LABCELL_X35_Y66_N36
\Selector14~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector14~3_combout\ = ( \Add0~21_sumout\ & ( (!\WideOr16~0_combout\) # ((!\Selector14~2_combout\) # ((\Selector14~0_combout\) # (\Selector14~1_combout\))) ) ) # ( !\Add0~21_sumout\ & ( (!\Selector14~2_combout\) # ((\Selector14~0_combout\) # 
-- (\Selector14~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111111111110011111111111111101111111111111110111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_WideOr16~0_combout\,
	datab => \ALT_INV_Selector14~2_combout\,
	datac => \ALT_INV_Selector14~1_combout\,
	datad => \ALT_INV_Selector14~0_combout\,
	dataf => \ALT_INV_Add0~21_sumout\,
	combout => \Selector14~3_combout\);

-- Location: LABCELL_X37_Y62_N51
\Add12~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~1_sumout\ = SUM(( GND ) + ( GND ) + ( \Add12~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => \Add12~30\,
	sumout => \Add12~1_sumout\);

-- Location: LABCELL_X35_Y66_N45
\Selector65~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~0_combout\ = ( !\state.DRAW_PADDLE_ONE_LOOP~q\ & ( !\state.ERASE_PADDLE_ONE_LOOP~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	combout => \Selector65~0_combout\);

-- Location: LABCELL_X37_Y65_N15
\Equal6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~2_combout\ = ( \draw.y\(6) & ( (\Add12~25_sumout\ & (!\Add12~29_sumout\ $ (\draw.y\(7)))) ) ) # ( !\draw.y\(6) & ( (!\Add12~25_sumout\ & (!\Add12~29_sumout\ $ (\draw.y\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000001010000101000000101000000001010000001010000101000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~29_sumout\,
	datac => \ALT_INV_Add12~25_sumout\,
	datad => \ALT_INV_draw.y\(7),
	dataf => \ALT_INV_draw.y\(6),
	combout => \Equal6~2_combout\);

-- Location: LABCELL_X35_Y65_N24
\Selector65~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~1_combout\ = ( \state.DRAW_PADDLE_TWO_LOOP~q\ & ( \Equal6~1_combout\ & ( (!\Add12~1_sumout\ & (\Equal6~0_combout\ & (\Selector65~0_combout\ & \Equal6~2_combout\))) ) ) ) # ( !\state.DRAW_PADDLE_TWO_LOOP~q\ & ( \Equal6~1_combout\ & ( 
-- (\Equal6~0_combout\ & (\Selector65~0_combout\ & \Equal6~2_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000110000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~1_sumout\,
	datab => \ALT_INV_Equal6~0_combout\,
	datac => \ALT_INV_Selector65~0_combout\,
	datad => \ALT_INV_Equal6~2_combout\,
	datae => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Selector65~1_combout\);

-- Location: LABCELL_X35_Y66_N18
\Selector65~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~2_combout\ = ( \Equal4~1_combout\ & ( \Equal4~0_combout\ & ( (\Equal4~2_combout\ & (((\state.DRAW_PADDLE_ONE_LOOP~q\ & !\Add11~1_sumout\)) # (\state.ERASE_PADDLE_ONE_LOOP~q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000001001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Equal4~2_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	datad => \ALT_INV_Add11~1_sumout\,
	datae => \ALT_INV_Equal4~1_combout\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \Selector65~2_combout\);

-- Location: MLABCELL_X39_Y65_N15
\Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = ( \draw.y\(6) & ( (\draw.y\(4) & \draw.y\(5)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(4),
	datac => \ALT_INV_draw.y\(5),
	dataf => \ALT_INV_draw.y\(6),
	combout => \Equal1~0_combout\);

-- Location: LABCELL_X37_Y65_N6
\Equal1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = ( !\draw.y\(7) & ( (\draw.y\(2) & (\draw.y\(1) & (!\draw.y\(3) & \draw.y\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000001000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datab => \ALT_INV_draw.y\(1),
	datac => \ALT_INV_draw.y\(3),
	datad => \ALT_INV_draw.y\(0),
	dataf => \ALT_INV_draw.y\(7),
	combout => \Equal1~1_combout\);

-- Location: MLABCELL_X34_Y66_N9
\Equal1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = (\Equal1~0_combout\ & \Equal1~1_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~0_combout\,
	datad => \ALT_INV_Equal1~1_combout\,
	combout => \Equal1~2_combout\);

-- Location: LABCELL_X33_Y65_N12
\draw.x[3]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~11_combout\ = ( !\draw.x\(1) & ( (\Equal0~0_combout\ & (!\draw.x\(0) & \state.DRAW_TOP_LOOP~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_draw.x\(0),
	datad => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	dataf => \ALT_INV_draw.x\(1),
	combout => \draw.x[3]~11_combout\);

-- Location: FF_X33_Y65_N13
\state.DRAW_BOTTOM_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.x[3]~11_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_BOTTOM_ENTER~q\);

-- Location: LABCELL_X33_Y65_N9
\Selector70~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector70~0_combout\ = ( \state.DRAW_BOTTOM_ENTER~q\ ) # ( !\state.DRAW_BOTTOM_ENTER~q\ & ( (!\Equal2~0_combout\ & \state.DRAW_BOTTOM_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal2~0_combout\,
	datad => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	combout => \Selector70~0_combout\);

-- Location: FF_X33_Y65_N10
\state.DRAW_BOTTOM_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector70~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_BOTTOM_LOOP~q\);

-- Location: MLABCELL_X34_Y66_N48
\draw.x[5]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[5]~0_combout\ = ( \Equal0~0_combout\ & ( \draw.x\(0) & ( !\state.IDLE~DUPLICATE_q\ ) ) ) # ( !\Equal0~0_combout\ & ( \draw.x\(0) & ( !\state.IDLE~DUPLICATE_q\ ) ) ) # ( \Equal0~0_combout\ & ( !\draw.x\(0) & ( (!\state.IDLE~DUPLICATE_q\ & 
-- (((!\state.DRAW_BOTTOM_LOOP~q\ & !\state.DRAW_TOP_LOOP~q\)) # (\draw.x\(1)))) ) ) ) # ( !\Equal0~0_combout\ & ( !\draw.x\(0) & ( !\state.IDLE~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100100000001100110011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	datab => \ALT_INV_state.IDLE~DUPLICATE_q\,
	datac => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	datad => \ALT_INV_draw.x\(1),
	datae => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_draw.x\(0),
	combout => \draw.x[5]~0_combout\);

-- Location: LABCELL_X35_Y66_N57
\draw.y[4]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[4]~0_combout\ = ( \draw.x[5]~0_combout\ & ( (!\state.START~q\) # ((\Equal0~0_combout\ & \Equal0~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000111100111111000011110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_state.START~q\,
	datad => \ALT_INV_Equal0~1_combout\,
	dataf => \ALT_INV_draw.x[5]~0_combout\,
	combout => \draw.y[4]~0_combout\);

-- Location: LABCELL_X35_Y66_N30
\draw.y[4]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[4]~1_combout\ = ( \WideOr16~0_combout\ & ( \draw.y[4]~0_combout\ ) ) # ( !\WideOr16~0_combout\ & ( \draw.y[4]~0_combout\ & ( (!\state.START~q\ & (!\Selector65~1_combout\ & (!\Selector65~2_combout\))) # (\state.START~q\ & (((!\Equal1~2_combout\)))) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010110011100000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector65~1_combout\,
	datab => \ALT_INV_state.START~q\,
	datac => \ALT_INV_Selector65~2_combout\,
	datad => \ALT_INV_Equal1~2_combout\,
	datae => \ALT_INV_WideOr16~0_combout\,
	dataf => \ALT_INV_draw.y[4]~0_combout\,
	combout => \draw.y[4]~1_combout\);

-- Location: FF_X35_Y66_N38
\draw.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector14~3_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.y[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(1));

-- Location: LABCELL_X37_Y65_N9
\Equal6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~1_combout\ = ( !\draw.y\(0) & ( (!\draw.y\(2) & (!\Add12~21_sumout\ & (!\draw.y\(1) $ (\Add12~17_sumout\)))) # (\draw.y\(2) & (\Add12~21_sumout\ & (!\draw.y\(1) $ (\Add12~17_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001001000001100000100100000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datab => \ALT_INV_draw.y\(1),
	datac => \ALT_INV_Add12~17_sumout\,
	datad => \ALT_INV_Add12~21_sumout\,
	dataf => \ALT_INV_draw.y\(0),
	combout => \Equal6~1_combout\);

-- Location: MLABCELL_X34_Y65_N6
\Selector77~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector77~0_combout\ = ( \Equal6~0_combout\ & ( \Equal6~1_combout\ & ( (\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & ((!\draw.y\(7) $ (!\Add12~29_sumout\)) # (\Equal6~3_combout\))) ) ) ) # ( !\Equal6~0_combout\ & ( \Equal6~1_combout\ & ( 
-- \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ ) ) ) # ( \Equal6~0_combout\ & ( !\Equal6~1_combout\ & ( \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ ) ) ) # ( !\Equal6~0_combout\ & ( !\Equal6~1_combout\ & ( \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110001001100100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(7),
	datab => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_Equal6~3_combout\,
	datad => \ALT_INV_Add12~29_sumout\,
	datae => \ALT_INV_Equal6~0_combout\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Selector77~0_combout\);

-- Location: MLABCELL_X34_Y65_N39
\Selector77~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector77~1_combout\ = ( \state.ERASE_PADDLE_TWO_ENTER~q\ ) # ( !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( \Selector77~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Selector77~0_combout\,
	dataf => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	combout => \Selector77~1_combout\);

-- Location: FF_X34_Y65_N41
\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector77~1_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\);

-- Location: MLABCELL_X34_Y65_N15
\WideOr16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr16~0_combout\ = ( !\state.DRAW_PADDLE_ONE_LOOP~q\ & ( (!\state.DRAW_PADDLE_TWO_LOOP~q\ & (!\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & (!\state.START~q\ & !\state.ERASE_PADDLE_ONE_LOOP~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datab => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_state.START~q\,
	datad => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	combout => \WideOr16~0_combout\);

-- Location: MLABCELL_X34_Y62_N42
\Add9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~1_sumout\ = SUM(( GND ) + ( !\Equal7~1_combout\ $ (\puck_velocity.y\(4)) ) + ( \Add9~6\ ))
-- \Add9~2\ = CARRY(( GND ) + ( !\Equal7~1_combout\ $ (\puck_velocity.y\(4)) ) + ( \Add9~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100111100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	dataf => \ALT_INV_puck_velocity.y\(4),
	cin => \Add9~6\,
	sumout => \Add9~1_sumout\,
	cout => \Add9~2\);

-- Location: MLABCELL_X34_Y62_N45
\Add9~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~29_sumout\ = SUM(( !\Equal7~1_combout\ $ (\puck_velocity.y\(5)) ) + ( GND ) + ( \Add9~2\ ))
-- \Add9~30\ = CARRY(( !\Equal7~1_combout\ $ (\puck_velocity.y\(5)) ) + ( GND ) + ( \Add9~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	datac => \ALT_INV_puck_velocity.y\(5),
	cin => \Add9~2\,
	sumout => \Add9~29_sumout\,
	cout => \Add9~30\);

-- Location: MLABCELL_X34_Y62_N12
\Add10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~1_sumout\ = SUM(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~1_sumout\) ) + ( \Add10~6\ ))
-- \Add10~2\ = CARRY(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~1_sumout\) ) + ( \Add10~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	dataf => \ALT_INV_Add9~1_sumout\,
	cin => \Add10~6\,
	sumout => \Add10~1_sumout\,
	cout => \Add10~2\);

-- Location: MLABCELL_X34_Y62_N15
\Add10~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~29_sumout\ = SUM(( !\Equal8~1_combout\ $ (!\Add9~29_sumout\) ) + ( GND ) + ( \Add10~2\ ))
-- \Add10~30\ = CARRY(( !\Equal8~1_combout\ $ (!\Add9~29_sumout\) ) + ( GND ) + ( \Add10~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	datac => \ALT_INV_Add9~29_sumout\,
	cin => \Add10~2\,
	sumout => \Add10~29_sumout\,
	cout => \Add10~30\);

-- Location: LABCELL_X33_Y62_N0
\Add10~29_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~29_wirecell_combout\ = !\Add10~29_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add10~29_sumout\,
	combout => \Add10~29_wirecell_combout\);

-- Location: FF_X33_Y62_N1
\puck_velocity.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~29_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(5));

-- Location: MLABCELL_X34_Y62_N48
\Add9~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~25_sumout\ = SUM(( GND ) + ( !\Equal7~1_combout\ $ (\puck_velocity.y\(6)) ) + ( \Add9~30\ ))
-- \Add9~26\ = CARRY(( GND ) + ( !\Equal7~1_combout\ $ (\puck_velocity.y\(6)) ) + ( \Add9~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100111100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	dataf => \ALT_INV_puck_velocity.y\(6),
	cin => \Add9~30\,
	sumout => \Add9~25_sumout\,
	cout => \Add9~26\);

-- Location: MLABCELL_X34_Y62_N18
\Add10~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~25_sumout\ = SUM(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~25_sumout\) ) + ( \Add10~30\ ))
-- \Add10~26\ = CARRY(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~25_sumout\) ) + ( \Add10~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	dataf => \ALT_INV_Add9~25_sumout\,
	cin => \Add10~30\,
	sumout => \Add10~25_sumout\,
	cout => \Add10~26\);

-- Location: LABCELL_X33_Y62_N39
\Add10~25_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~25_wirecell_combout\ = ( !\Add10~25_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add10~25_sumout\,
	combout => \Add10~25_wirecell_combout\);

-- Location: FF_X33_Y62_N40
\puck_velocity.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~25_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(6));

-- Location: LABCELL_X35_Y63_N30
\Add8~1_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~1_wirecell_combout\ = ( !\Add8~1_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add8~1_sumout\,
	combout => \Add8~1_wirecell_combout\);

-- Location: FF_X35_Y63_N32
\puck.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~1_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(4));

-- Location: MLABCELL_X34_Y63_N42
\Add8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~1_sumout\ = SUM(( !\puck_velocity.y\(4) ) + ( !\puck.y\(4) ) + ( \Add8~6\ ))
-- \Add8~2\ = CARRY(( !\puck_velocity.y\(4) ) + ( !\puck.y\(4) ) + ( \Add8~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity.y\(4),
	datac => \ALT_INV_puck.y\(4),
	cin => \Add8~6\,
	sumout => \Add8~1_sumout\,
	cout => \Add8~2\);

-- Location: MLABCELL_X34_Y63_N45
\Add8~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~29_sumout\ = SUM(( !\puck_velocity.y\(5) ) + ( !\puck.y\(5) ) + ( \Add8~2\ ))
-- \Add8~30\ = CARRY(( !\puck_velocity.y\(5) ) + ( !\puck.y\(5) ) + ( \Add8~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity.y\(5),
	datac => \ALT_INV_puck.y\(5),
	cin => \Add8~2\,
	sumout => \Add8~29_sumout\,
	cout => \Add8~30\);

-- Location: LABCELL_X33_Y62_N3
\Add8~29_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~29_wirecell_combout\ = ( !\Add8~29_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add8~29_sumout\,
	combout => \Add8~29_wirecell_combout\);

-- Location: FF_X33_Y62_N5
\puck.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~29_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(5));

-- Location: MLABCELL_X34_Y63_N48
\Add8~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~25_sumout\ = SUM(( \puck.y\(6) ) + ( !\puck_velocity.y\(6) ) + ( \Add8~30\ ))
-- \Add8~26\ = CARRY(( \puck.y\(6) ) + ( !\puck_velocity.y\(6) ) + ( \Add8~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity.y\(6),
	datad => \ALT_INV_puck.y\(6),
	cin => \Add8~30\,
	sumout => \Add8~25_sumout\,
	cout => \Add8~26\);

-- Location: FF_X34_Y63_N50
\puck.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~25_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(6));

-- Location: LABCELL_X35_Y63_N45
\Selector9~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~2_combout\ = ( paddle_one_y(6) & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (!\state.DRAW_BOTTOM_LOOP~q\ & !\state.DRAW_BOTTOM_ENTER~q\)) ) ) # ( !paddle_one_y(6) & ( (!\state.DRAW_BOTTOM_LOOP~q\ & !\state.DRAW_BOTTOM_ENTER~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000010100000000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datac => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	datad => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	dataf => ALT_INV_paddle_one_y(6),
	combout => \Selector9~2_combout\);

-- Location: LABCELL_X33_Y65_N51
\Selector9~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~3_combout\ = ( paddle_two_y(6) & ( (!\state.ERASE_PADDLE_TWO_ENTER~q\ & (\Selector9~2_combout\ & ((!\puck.y\(6)) # (\draw~0_combout\)))) ) ) # ( !paddle_two_y(6) & ( (\Selector9~2_combout\ & ((!\puck.y\(6)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110011000000001111001100000000101000100000000010100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_puck.y\(6),
	datad => \ALT_INV_Selector9~2_combout\,
	dataf => ALT_INV_paddle_two_y(6),
	combout => \Selector9~3_combout\);

-- Location: LABCELL_X37_Y65_N45
\Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~5_sumout\ = SUM(( \draw.y\(5) ) + ( GND ) + ( \Add0~10\ ))
-- \Add0~6\ = CARRY(( \draw.y\(5) ) + ( GND ) + ( \Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(5),
	cin => \Add0~10\,
	sumout => \Add0~5_sumout\,
	cout => \Add0~6\);

-- Location: LABCELL_X37_Y65_N48
\Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~1_sumout\ = SUM(( \draw.y\(6) ) + ( GND ) + ( \Add0~6\ ))
-- \Add0~2\ = CARRY(( \draw.y\(6) ) + ( GND ) + ( \Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(6),
	cin => \Add0~6\,
	sumout => \Add0~1_sumout\,
	cout => \Add0~2\);

-- Location: LABCELL_X36_Y65_N30
\Selector9~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~4_combout\ = ( \Add0~1_sumout\ & ( (!\WideOr16~0_combout\) # ((!\Selector9~3_combout\) # ((\Selector9~1_combout\) # (\Selector9~0_combout\))) ) ) # ( !\Add0~1_sumout\ & ( (!\Selector9~3_combout\) # ((\Selector9~1_combout\) # 
-- (\Selector9~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111111111110011111111111111101111111111111110111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_WideOr16~0_combout\,
	datab => \ALT_INV_Selector9~3_combout\,
	datac => \ALT_INV_Selector9~0_combout\,
	datad => \ALT_INV_Selector9~1_combout\,
	dataf => \ALT_INV_Add0~1_sumout\,
	combout => \Selector9~4_combout\);

-- Location: FF_X36_Y65_N32
\draw.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector9~4_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.y[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(6));

-- Location: LABCELL_X37_Y65_N54
\Equal4~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~3_combout\ = ( \Equal4~0_combout\ & ( \draw.y\(7) & ( (\Add11~29_sumout\ & (\Equal4~1_combout\ & (!\draw.y\(6) $ (\Add11~25_sumout\)))) ) ) ) # ( \Equal4~0_combout\ & ( !\draw.y\(7) & ( (!\Add11~29_sumout\ & (\Equal4~1_combout\ & (!\draw.y\(6) $ 
-- (\Add11~25_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000010000000000000000000000001000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(6),
	datab => \ALT_INV_Add11~29_sumout\,
	datac => \ALT_INV_Equal4~1_combout\,
	datad => \ALT_INV_Add11~25_sumout\,
	datae => \ALT_INV_Equal4~0_combout\,
	dataf => \ALT_INV_draw.y\(7),
	combout => \Equal4~3_combout\);

-- Location: LABCELL_X35_Y67_N15
\Selector73~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector73~0_combout\ = ( \Equal4~3_combout\ & ( \state.ERASE_PADDLE_ONE_ENTER~q\ ) ) # ( !\Equal4~3_combout\ & ( (\state.ERASE_PADDLE_ONE_LOOP~q\) # (\state.ERASE_PADDLE_ONE_ENTER~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datad => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	dataf => \ALT_INV_Equal4~3_combout\,
	combout => \Selector73~0_combout\);

-- Location: FF_X35_Y67_N16
\state.ERASE_PADDLE_ONE_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector73~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_ONE_LOOP~q\);

-- Location: MLABCELL_X34_Y66_N18
\Selector72~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector72~2_combout\ = ( !\state.IDLE~DUPLICATE_q\ & ( \draw.x\(0) & ( (\WideOr16~2_combout\ & \WideOr16~0_combout\) ) ) ) # ( \state.IDLE~DUPLICATE_q\ & ( !\draw.x\(0) & ( (!\WideOr16~2_combout\ & (!\draw.x\(1) & \Equal0~0_combout\)) ) ) ) # ( 
-- !\state.IDLE~DUPLICATE_q\ & ( !\draw.x\(0) & ( (!\WideOr16~2_combout\ & (!\draw.x\(1) & ((\Equal0~0_combout\)))) # (\WideOr16~2_combout\ & (((\WideOr16~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110001101000000001000100000000101000001010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_WideOr16~2_combout\,
	datab => \ALT_INV_draw.x\(1),
	datac => \ALT_INV_WideOr16~0_combout\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_state.IDLE~DUPLICATE_q\,
	dataf => \ALT_INV_draw.x\(0),
	combout => \Selector72~2_combout\);

-- Location: LABCELL_X35_Y66_N42
\Selector72~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector72~3_combout\ = ( \Equal4~0_combout\ & ( (!\Selector72~2_combout\ & ((!\state.ERASE_PADDLE_ONE_LOOP~q\) # ((!\Equal4~1_combout\) # (!\Equal4~2_combout\)))) ) ) # ( !\Equal4~0_combout\ & ( !\Selector72~2_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110010001100110011001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Selector72~2_combout\,
	datac => \ALT_INV_Equal4~1_combout\,
	datad => \ALT_INV_Equal4~2_combout\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \Selector72~3_combout\);

-- Location: FF_X34_Y65_N40
\state.ERASE_PADDLE_TWO_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector77~1_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_TWO_LOOP~q\);

-- Location: LABCELL_X35_Y65_N54
\Selector72~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector72~0_combout\ = ( \state.DRAW_PADDLE_TWO_LOOP~q\ & ( \Equal6~1_combout\ & ( (\Equal6~0_combout\ & (\Equal6~2_combout\ & ((!\Add12~1_sumout\) # (\state.ERASE_PADDLE_TWO_LOOP~q\)))) ) ) ) # ( !\state.DRAW_PADDLE_TWO_LOOP~q\ & ( \Equal6~1_combout\ & 
-- ( (\Equal6~0_combout\ & (\state.ERASE_PADDLE_TWO_LOOP~q\ & \Equal6~2_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000110000000000100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~1_sumout\,
	datab => \ALT_INV_Equal6~0_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~q\,
	datad => \ALT_INV_Equal6~2_combout\,
	datae => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Selector72~0_combout\);

-- Location: LABCELL_X33_Y66_N39
\LessThan0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~6_combout\ = ( !clock_counter(24) & ( (!clock_counter(21) & (!clock_counter(23) & !clock_counter(22))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(21),
	datac => ALT_INV_clock_counter(23),
	datad => ALT_INV_clock_counter(22),
	dataf => ALT_INV_clock_counter(24),
	combout => \LessThan0~6_combout\);

-- Location: LABCELL_X33_Y66_N36
\LessThan0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~7_combout\ = ( !clock_counter(30) & ( (!clock_counter(27) & (!clock_counter(28) & !clock_counter(29))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_clock_counter(27),
	datac => ALT_INV_clock_counter(28),
	datad => ALT_INV_clock_counter(29),
	dataf => ALT_INV_clock_counter(30),
	combout => \LessThan0~7_combout\);

-- Location: LABCELL_X33_Y66_N42
\LessThan0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~8_combout\ = ( !clock_counter(25) & ( (\LessThan0~6_combout\ & (!clock_counter(26) & \LessThan0~7_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_LessThan0~6_combout\,
	datac => ALT_INV_clock_counter(26),
	datad => \ALT_INV_LessThan0~7_combout\,
	dataf => ALT_INV_clock_counter(25),
	combout => \LessThan0~8_combout\);

-- Location: LABCELL_X37_Y66_N39
\LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = (!clock_counter(13) & !clock_counter(12))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000101000001010000010100000101000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(13),
	datac => ALT_INV_clock_counter(12),
	combout => \LessThan0~0_combout\);

-- Location: MLABCELL_X34_Y67_N30
\LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = ( !clock_counter(2) & ( !clock_counter(5) & ( (!clock_counter(4) & (!clock_counter(6) & !clock_counter(3))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(4),
	datab => ALT_INV_clock_counter(6),
	datac => ALT_INV_clock_counter(3),
	datae => ALT_INV_clock_counter(2),
	dataf => ALT_INV_clock_counter(5),
	combout => \LessThan0~1_combout\);

-- Location: MLABCELL_X34_Y67_N18
\LessThan0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~4_combout\ = ( clock_counter(16) & ( clock_counter(15) & ( (clock_counter(17) & (clock_counter(14) & clock_counter(18))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(17),
	datab => ALT_INV_clock_counter(14),
	datac => ALT_INV_clock_counter(18),
	datae => ALT_INV_clock_counter(16),
	dataf => ALT_INV_clock_counter(15),
	combout => \LessThan0~4_combout\);

-- Location: MLABCELL_X34_Y67_N6
\LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = ( clock_counter(9) & ( (clock_counter(7) & (clock_counter(8) & clock_counter(10))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000010000000100000000000000000000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(7),
	datab => ALT_INV_clock_counter(8),
	datac => ALT_INV_clock_counter(10),
	datae => ALT_INV_clock_counter(9),
	combout => \LessThan0~2_combout\);

-- Location: MLABCELL_X34_Y67_N15
\LessThan0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = ( !clock_counter(11) & ( !clock_counter(13) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => ALT_INV_clock_counter(11),
	dataf => ALT_INV_clock_counter(13),
	combout => \LessThan0~3_combout\);

-- Location: MLABCELL_X34_Y67_N54
\LessThan0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~5_combout\ = ( !clock_counter(19) & ( \LessThan0~3_combout\ & ( (((!\LessThan0~4_combout\) # (!\LessThan0~2_combout\)) # (\LessThan0~1_combout\)) # (\LessThan0~0_combout\) ) ) ) # ( !clock_counter(19) & ( !\LessThan0~3_combout\ & ( 
-- (!\LessThan0~4_combout\) # (\LessThan0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101000000000000000011111111111101110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan0~0_combout\,
	datab => \ALT_INV_LessThan0~1_combout\,
	datac => \ALT_INV_LessThan0~4_combout\,
	datad => \ALT_INV_LessThan0~2_combout\,
	datae => ALT_INV_clock_counter(19),
	dataf => \ALT_INV_LessThan0~3_combout\,
	combout => \LessThan0~5_combout\);

-- Location: LABCELL_X33_Y66_N54
\Selector72~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector72~1_combout\ = ( \LessThan0~5_combout\ & ( (\state.IDLE~DUPLICATE_q\ & (!\LessThan0~8_combout\ & !clock_counter(31))) ) ) # ( !\LessThan0~5_combout\ & ( (\state.IDLE~DUPLICATE_q\ & (!clock_counter(31) & ((!\LessThan0~8_combout\) # 
-- (clock_counter(20))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001010000010000000101000001000000010000000100000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.IDLE~DUPLICATE_q\,
	datab => \ALT_INV_LessThan0~8_combout\,
	datac => ALT_INV_clock_counter(31),
	datad => ALT_INV_clock_counter(20),
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \Selector72~1_combout\);

-- Location: LABCELL_X33_Y62_N12
\Selector67~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector67~3_combout\ = ( \state.DRAW_TOP_ENTER~q\ & ( \Selector72~1_combout\ & ( (\Selector6~1_combout\ & (!\Selector72~0_combout\ & !\Selector67~1_combout\)) ) ) ) # ( !\state.DRAW_TOP_ENTER~q\ & ( \Selector72~1_combout\ & ( (\Selector6~1_combout\ & 
-- (!\Selector72~0_combout\ & !\Selector67~1_combout\)) ) ) ) # ( \state.DRAW_TOP_ENTER~q\ & ( !\Selector72~1_combout\ & ( (!\Selector72~0_combout\ & (!\Selector67~1_combout\ & ((\Selector72~3_combout\) # (\Selector6~1_combout\)))) ) ) ) # ( 
-- !\state.DRAW_TOP_ENTER~q\ & ( !\Selector72~1_combout\ & ( (\Selector6~1_combout\ & (!\Selector72~0_combout\ & !\Selector67~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000011100000000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector6~1_combout\,
	datab => \ALT_INV_Selector72~3_combout\,
	datac => \ALT_INV_Selector72~0_combout\,
	datad => \ALT_INV_Selector67~1_combout\,
	datae => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	dataf => \ALT_INV_Selector72~1_combout\,
	combout => \Selector67~3_combout\);

-- Location: FF_X33_Y62_N13
\state.DRAW_TOP_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector67~3_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_TOP_ENTER~q\);

-- Location: MLABCELL_X34_Y65_N3
\Selector68~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector68~0_combout\ = ( \state.DRAW_TOP_ENTER~q\ ) # ( !\state.DRAW_TOP_ENTER~q\ & ( (!\Equal2~0_combout\ & \state.DRAW_TOP_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Equal2~0_combout\,
	datad => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	combout => \Selector68~0_combout\);

-- Location: FF_X34_Y65_N4
\state.DRAW_TOP_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector68~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_TOP_LOOP~q\);

-- Location: MLABCELL_X34_Y66_N24
\WideOr16~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr16~2_combout\ = ( !\state.DRAW_BOTTOM_LOOP~q\ & ( !\state.DRAW_TOP_LOOP~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	combout => \WideOr16~2_combout\);

-- Location: MLABCELL_X34_Y66_N12
\Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = ( \Equal0~1_combout\ & ( (\WideOr16~2_combout\ & ((!\state.START~q\) # (\Equal0~0_combout\))) ) ) # ( !\Equal0~1_combout\ & ( (!\state.START~q\ & \WideOr16~2_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101000001010000011110000101000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datac => \ALT_INV_WideOr16~2_combout\,
	datad => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \Selector6~0_combout\);

-- Location: LABCELL_X37_Y66_N6
\Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~21_sumout\ = SUM(( \draw.x\(2) ) + ( GND ) + ( \Add1~18\ ))
-- \Add1~22\ = CARRY(( \draw.x\(2) ) + ( GND ) + ( \Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x\(2),
	cin => \Add1~18\,
	sumout => \Add1~21_sumout\,
	cout => \Add1~22\);

-- Location: LABCELL_X33_Y65_N54
\Selector5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~1_combout\ = ( \puck.x\(2) & ( (!\state.DRAW_BOTTOM_ENTER~q\ & (!\state.DRAW_TOP_ENTER~q\ & \draw~0_combout\)) ) ) # ( !\puck.x\(2) & ( (!\state.DRAW_BOTTOM_ENTER~q\ & !\state.DRAW_TOP_ENTER~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110000001100000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	datac => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datad => \ALT_INV_draw~0_combout\,
	dataf => \ALT_INV_puck.x\(2),
	combout => \Selector5~1_combout\);

-- Location: LABCELL_X33_Y65_N27
\Selector5~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~2_combout\ = ( \Add1~21_sumout\ & ( \Selector5~1_combout\ & ( (\Selector6~0_combout\ & (!\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\Selector6~1_combout\ & !\state.ERASE_PADDLE_TWO_ENTER~q\))) ) ) ) # ( !\Add1~21_sumout\ & ( 
-- \Selector5~1_combout\ & ( (!\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\ & (!\Selector6~1_combout\ & !\state.ERASE_PADDLE_TWO_ENTER~q\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000000000000100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector6~0_combout\,
	datab => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	datac => \ALT_INV_Selector6~1_combout\,
	datad => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datae => \ALT_INV_Add1~21_sumout\,
	dataf => \ALT_INV_Selector5~1_combout\,
	combout => \Selector5~2_combout\);

-- Location: LABCELL_X35_Y65_N18
\Selector5~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~3_combout\ = ( \state.DRAW_PADDLE_TWO_LOOP~q\ & ( \Equal6~1_combout\ & ( (!\Add12~1_sumout\ & (\Equal6~0_combout\ & \Equal6~2_combout\)) ) ) ) # ( !\state.DRAW_PADDLE_TWO_LOOP~q\ & ( \Equal6~1_combout\ & ( (!\state.ERASE_PADDLE_TWO_LOOP~q\) # 
-- ((\Equal6~0_combout\ & \Equal6~2_combout\)) ) ) ) # ( !\state.DRAW_PADDLE_TWO_LOOP~q\ & ( !\Equal6~1_combout\ & ( !\state.ERASE_PADDLE_TWO_LOOP~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000011110000111100110000000000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~1_sumout\,
	datab => \ALT_INV_Equal6~0_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~q\,
	datad => \ALT_INV_Equal6~2_combout\,
	datae => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Selector5~3_combout\);

-- Location: LABCELL_X35_Y65_N9
\Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = ( !\state.ERASE_PADDLE_TWO_LOOP~q\ & ( (!\state.DRAW_PADDLE_TWO_LOOP~q\ & !\state.IDLE~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000101000001010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datac => \ALT_INV_state.IDLE~q\,
	dataf => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~q\,
	combout => \Selector5~0_combout\);

-- Location: LABCELL_X35_Y65_N36
\Selector5~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~4_combout\ = ( \draw.x\(2) & ( \Selector5~0_combout\ & ( ((!\Selector5~2_combout\) # ((!\Selector5~3_combout\) # (\Selector65~3_combout\))) # (\Selector67~1_combout\) ) ) ) # ( !\draw.x\(2) & ( \Selector5~0_combout\ & ( (!\Selector5~2_combout\) 
-- # (!\Selector5~3_combout\) ) ) ) # ( \draw.x\(2) & ( !\Selector5~0_combout\ ) ) # ( !\draw.x\(2) & ( !\Selector5~0_combout\ & ( (!\Selector5~2_combout\) # (!\Selector5~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111100111111111111111111111100111111001111110111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector67~1_combout\,
	datab => \ALT_INV_Selector5~2_combout\,
	datac => \ALT_INV_Selector5~3_combout\,
	datad => \ALT_INV_Selector65~3_combout\,
	datae => \ALT_INV_draw.x\(2),
	dataf => \ALT_INV_Selector5~0_combout\,
	combout => \Selector5~4_combout\);

-- Location: FF_X35_Y65_N37
\draw.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector5~4_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(2));

-- Location: MLABCELL_X34_Y65_N12
\Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = ( !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( (!\state.DRAW_PADDLE_TWO_LOOP~q\ & (!\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & !\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datab => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	dataf => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	combout => \Selector0~0_combout\);

-- Location: LABCELL_X37_Y66_N9
\Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~25_sumout\ = SUM(( \draw.x\(3) ) + ( GND ) + ( \Add1~22\ ))
-- \Add1~26\ = CARRY(( \draw.x\(3) ) + ( GND ) + ( \Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(3),
	cin => \Add1~22\,
	sumout => \Add1~25_sumout\,
	cout => \Add1~26\);

-- Location: LABCELL_X37_Y66_N45
\Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~0_combout\ = ( \Add1~25_sumout\ & ( (!\draw.x[3]~3_combout\ & (((\puck.x\(3))) # (\draw~0_combout\))) # (\draw.x[3]~3_combout\ & (((!\Selector0~0_combout\)))) ) ) # ( !\Add1~25_sumout\ & ( (!\draw.x[3]~3_combout\ & (!\draw~0_combout\ & 
-- ((\puck.x\(3))))) # (\draw.x[3]~3_combout\ & (((!\Selector0~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000010111000001100001011100001110100111111000111010011111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_draw.x[3]~3_combout\,
	datac => \ALT_INV_Selector0~0_combout\,
	datad => \ALT_INV_puck.x\(3),
	dataf => \ALT_INV_Add1~25_sumout\,
	combout => \Selector4~0_combout\);

-- Location: MLABCELL_X34_Y66_N6
\draw.x[3]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~5_combout\ = (\Equal1~0_combout\ & (\state.START~q\ & \Equal1~1_combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~0_combout\,
	datac => \ALT_INV_state.START~q\,
	datad => \ALT_INV_Equal1~1_combout\,
	combout => \draw.x[3]~5_combout\);

-- Location: MLABCELL_X34_Y66_N27
\draw.x[3]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~8_combout\ = ( \Selector65~0_combout\ & ( (!\state.START~q\) # ((!\Equal0~1_combout\) # (!\Equal0~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111110101111111111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datac => \ALT_INV_Equal0~1_combout\,
	datad => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_Selector65~0_combout\,
	combout => \draw.x[3]~8_combout\);

-- Location: LABCELL_X35_Y66_N6
\draw.x[3]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~7_combout\ = ( \Equal4~1_combout\ & ( \Equal4~0_combout\ & ( (\Equal4~2_combout\ & (!\state.START~q\ & ((!\state.DRAW_PADDLE_ONE_LOOP~q\) # (!\Add11~1_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Equal4~2_combout\,
	datac => \ALT_INV_Add11~1_sumout\,
	datad => \ALT_INV_state.START~q\,
	datae => \ALT_INV_Equal4~1_combout\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \draw.x[3]~7_combout\);

-- Location: LABCELL_X35_Y65_N45
\draw.x[3]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~6_combout\ = ( \state.ERASE_PADDLE_TWO_LOOP~q\ & ( (!\state.DRAW_PADDLE_TWO_LOOP~q\ & (\Equal6~2_combout\ & (\Equal6~1_combout\ & \Equal6~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000100000000000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datab => \ALT_INV_Equal6~2_combout\,
	datac => \ALT_INV_Equal6~1_combout\,
	datad => \ALT_INV_Equal6~0_combout\,
	dataf => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~q\,
	combout => \draw.x[3]~6_combout\);

-- Location: LABCELL_X35_Y65_N30
\draw.x[3]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~9_combout\ = ( !\draw.x[3]~6_combout\ & ( !\draw.x[3]~4_combout\ & ( (\draw.x[5]~0_combout\ & (((!\draw.x[3]~5_combout\ & !\draw.x[3]~7_combout\)) # (\draw.x[3]~8_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010100000101000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x[5]~0_combout\,
	datab => \ALT_INV_draw.x[3]~5_combout\,
	datac => \ALT_INV_draw.x[3]~8_combout\,
	datad => \ALT_INV_draw.x[3]~7_combout\,
	datae => \ALT_INV_draw.x[3]~6_combout\,
	dataf => \ALT_INV_draw.x[3]~4_combout\,
	combout => \draw.x[3]~9_combout\);

-- Location: FF_X37_Y66_N47
\draw.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector4~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[3]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(3));

-- Location: LABCELL_X37_Y66_N12
\Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~29_sumout\ = SUM(( \draw.x\(4) ) + ( GND ) + ( \Add1~26\ ))
-- \Add1~30\ = CARRY(( \draw.x\(4) ) + ( GND ) + ( \Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x\(4),
	cin => \Add1~26\,
	sumout => \Add1~29_sumout\,
	cout => \Add1~30\);

-- Location: LABCELL_X37_Y66_N42
\Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = ( \Add1~29_sumout\ & ( (!\draw.x[3]~3_combout\ & (((!\puck.x\(4))) # (\draw~0_combout\))) # (\draw.x[3]~3_combout\ & (((!\Selector0~0_combout\)))) ) ) # ( !\Add1~29_sumout\ & ( (!\draw.x[3]~3_combout\ & (!\draw~0_combout\ & 
-- (!\puck.x\(4)))) # (\draw.x[3]~3_combout\ & (((!\Selector0~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011001110000000101100111000000011110111110001001111011111000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_draw.x[3]~3_combout\,
	datac => \ALT_INV_puck.x\(4),
	datad => \ALT_INV_Selector0~0_combout\,
	dataf => \ALT_INV_Add1~29_sumout\,
	combout => \Selector3~0_combout\);

-- Location: FF_X37_Y66_N44
\draw.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector3~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[3]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(4));

-- Location: LABCELL_X37_Y66_N15
\Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~9_sumout\ = SUM(( \draw.x\(5) ) + ( GND ) + ( \Add1~30\ ))
-- \Add1~10\ = CARRY(( \draw.x\(5) ) + ( GND ) + ( \Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(5),
	cin => \Add1~30\,
	sumout => \Add1~9_sumout\,
	cout => \Add1~10\);

-- Location: LABCELL_X37_Y66_N36
\Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = ( \Add1~9_sumout\ & ( (!\draw.x[3]~3_combout\ & ((\draw~0_combout\) # (\puck.x\(5)))) ) ) # ( !\Add1~9_sumout\ & ( (\puck.x\(5) & (!\draw~0_combout\ & !\draw.x[3]~3_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000111111000000000011111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck.x\(5),
	datac => \ALT_INV_draw~0_combout\,
	datad => \ALT_INV_draw.x[3]~3_combout\,
	dataf => \ALT_INV_Add1~9_sumout\,
	combout => \Selector2~0_combout\);

-- Location: LABCELL_X33_Y65_N57
\draw.x[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~1_combout\ = ( \Equal0~0_combout\ & ( (\WideOr16~1_combout\ & ((!\state.START~q\) # (!\Equal0~1_combout\))) ) ) # ( !\Equal0~0_combout\ & ( \WideOr16~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000010100000111100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datac => \ALT_INV_WideOr16~1_combout\,
	datad => \ALT_INV_Equal0~1_combout\,
	dataf => \ALT_INV_Equal0~0_combout\,
	combout => \draw.x[3]~1_combout\);

-- Location: LABCELL_X35_Y66_N33
\draw.x[5]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[5]~10_combout\ = ( \draw.x[3]~1_combout\ & ( \draw.x[5]~0_combout\ ) ) # ( !\draw.x[3]~1_combout\ & ( \draw.x[5]~0_combout\ & ( (!\state.START~q\ & (!\Selector65~1_combout\ & ((!\Selector65~2_combout\)))) # (\state.START~q\ & 
-- (((!\Equal1~2_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010111000001100001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector65~1_combout\,
	datab => \ALT_INV_state.START~q\,
	datac => \ALT_INV_Equal1~2_combout\,
	datad => \ALT_INV_Selector65~2_combout\,
	datae => \ALT_INV_draw.x[3]~1_combout\,
	dataf => \ALT_INV_draw.x[5]~0_combout\,
	combout => \draw.x[5]~10_combout\);

-- Location: FF_X37_Y66_N37
\draw.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector2~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[5]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(5));

-- Location: LABCELL_X37_Y66_N18
\Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~5_sumout\ = SUM(( \draw.x\(6) ) + ( GND ) + ( \Add1~10\ ))
-- \Add1~6\ = CARRY(( \draw.x\(6) ) + ( GND ) + ( \Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(6),
	cin => \Add1~10\,
	sumout => \Add1~5_sumout\,
	cout => \Add1~6\);

-- Location: LABCELL_X37_Y66_N33
\Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = ( \Add1~5_sumout\ & ( (!\draw.x[3]~3_combout\ & ((!\puck.x\(6)) # (\draw~0_combout\))) ) ) # ( !\Add1~5_sumout\ & ( (!\draw~0_combout\ & (!\draw.x[3]~3_combout\ & !\puck.x\(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000011000100110001001100010011000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_draw.x[3]~3_combout\,
	datac => \ALT_INV_puck.x\(6),
	dataf => \ALT_INV_Add1~5_sumout\,
	combout => \Selector1~0_combout\);

-- Location: FF_X37_Y66_N35
\draw.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector1~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[5]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(6));

-- Location: LABCELL_X37_Y66_N21
\Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~1_sumout\ = SUM(( \draw.x\(7) ) + ( GND ) + ( \Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(7),
	cin => \Add1~6\,
	sumout => \Add1~1_sumout\);

-- Location: FF_X35_Y64_N22
\puck.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add7~5_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.x\(7));

-- Location: LABCELL_X37_Y66_N27
\Selector0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~1_combout\ = ( \draw~0_combout\ & ( (!\draw.x[3]~3_combout\ & (\Add1~1_sumout\)) # (\draw.x[3]~3_combout\ & ((!\Selector0~0_combout\))) ) ) # ( !\draw~0_combout\ & ( (!\draw.x[3]~3_combout\ & ((\puck.x\(7)))) # (\draw.x[3]~3_combout\ & 
-- (!\Selector0~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011111100000011001111110001011100010111000101110001011100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add1~1_sumout\,
	datab => \ALT_INV_Selector0~0_combout\,
	datac => \ALT_INV_draw.x[3]~3_combout\,
	datad => \ALT_INV_puck.x\(7),
	dataf => \ALT_INV_draw~0_combout\,
	combout => \Selector0~1_combout\);

-- Location: FF_X37_Y66_N28
\draw.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector0~1_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[3]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(7));

-- Location: LABCELL_X37_Y66_N48
\Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = ( !\draw.x\(5) & ( \draw.x\(7) & ( (\draw.x\(2) & (\draw.x\(4) & (!\draw.x\(6) & \draw.x\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(2),
	datab => \ALT_INV_draw.x\(4),
	datac => \ALT_INV_draw.x\(6),
	datad => \ALT_INV_draw.x\(3),
	datae => \ALT_INV_draw.x\(5),
	dataf => \ALT_INV_draw.x\(7),
	combout => \Equal0~0_combout\);

-- Location: MLABCELL_X34_Y66_N30
\Selector66~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector66~0_combout\ = ( \Equal0~1_combout\ & ( (!\state.INIT~q\) # ((\state.START~q\ & ((!\Equal0~0_combout\) # (!\Equal1~2_combout\)))) ) ) # ( !\Equal0~1_combout\ & ( (!\state.INIT~q\) # (\state.START~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101011111111101010101111111110101010111111101010101011111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_Equal1~2_combout\,
	datad => \ALT_INV_state.START~q\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \Selector66~0_combout\);

-- Location: FF_X34_Y66_N31
\state.START\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector66~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.START~q\);

-- Location: MLABCELL_X34_Y66_N0
\Selector6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~1_combout\ = ( \Equal0~1_combout\ & ( (\state.START~q\ & (\Equal0~0_combout\ & (\Equal1~0_combout\ & \Equal1~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_Equal1~0_combout\,
	datad => \ALT_INV_Equal1~1_combout\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \Selector6~1_combout\);

-- Location: LABCELL_X33_Y62_N6
\Selector72~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector72~4_combout\ = ( \state.ERASE_PADDLE_ONE_ENTER~q\ & ( \Selector72~1_combout\ & ( (!\Selector72~0_combout\ & !\Selector67~1_combout\) ) ) ) # ( !\state.ERASE_PADDLE_ONE_ENTER~q\ & ( \Selector72~1_combout\ & ( (!\Selector72~0_combout\ & 
-- !\Selector67~1_combout\) ) ) ) # ( \state.ERASE_PADDLE_ONE_ENTER~q\ & ( !\Selector72~1_combout\ & ( (!\Selector6~1_combout\ & (\Selector72~3_combout\ & (!\Selector72~0_combout\ & !\Selector67~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector6~1_combout\,
	datab => \ALT_INV_Selector72~3_combout\,
	datac => \ALT_INV_Selector72~0_combout\,
	datad => \ALT_INV_Selector67~1_combout\,
	datae => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	dataf => \ALT_INV_Selector72~1_combout\,
	combout => \Selector72~4_combout\);

-- Location: FF_X33_Y62_N7
\state.ERASE_PADDLE_ONE_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector72~4_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_ONE_ENTER~q\);

-- Location: LABCELL_X33_Y65_N30
\draw.x[3]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~2_combout\ = ( !\state.DRAW_PADDLE_ONE_ENTER~q\ & ( (!\state.DRAW_TOP_ENTER~q\ & (!\state.DRAW_BOTTOM_ENTER~q\ & !\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100000000000100010000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datab => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	datad => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	combout => \draw.x[3]~2_combout\);

-- Location: LABCELL_X33_Y65_N36
\draw.x[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~3_combout\ = ( \draw.x[3]~1_combout\ & ( \state.ERASE_PADDLE_TWO_ENTER~q\ & ( \draw~0_combout\ ) ) ) # ( !\draw.x[3]~1_combout\ & ( \state.ERASE_PADDLE_TWO_ENTER~q\ & ( \draw~0_combout\ ) ) ) # ( \draw.x[3]~1_combout\ & ( 
-- !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( (\draw~0_combout\ & (((!\state.INIT~q\) # (!\draw.x[3]~2_combout\)) # (\state.ERASE_PADDLE_ONE_ENTER~q\))) ) ) ) # ( !\draw.x[3]~1_combout\ & ( !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( \draw~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011000100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_state.INIT~q\,
	datad => \ALT_INV_draw.x[3]~2_combout\,
	datae => \ALT_INV_draw.x[3]~1_combout\,
	dataf => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	combout => \draw.x[3]~3_combout\);

-- Location: LABCELL_X37_Y66_N30
\Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = ( \puck.x\(0) & ( (!\draw.x[3]~3_combout\ & ((!\draw~0_combout\) # (\Add1~13_sumout\))) ) ) # ( !\puck.x\(0) & ( (\draw~0_combout\ & (!\draw.x[3]~3_combout\ & \Add1~13_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010010001100100011001000110010001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_draw.x[3]~3_combout\,
	datac => \ALT_INV_Add1~13_sumout\,
	dataf => \ALT_INV_puck.x\(0),
	combout => \Selector7~0_combout\);

-- Location: FF_X37_Y66_N31
\draw.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector7~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[5]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(0));

-- Location: LABCELL_X33_Y65_N15
\Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal2~0_combout\ = ( !\draw.x\(1) & ( (!\draw.x\(0) & \Equal0~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100010001000100010001000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(0),
	datab => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_draw.x\(1),
	combout => \Equal2~0_combout\);

-- Location: LABCELL_X33_Y65_N6
\Selector71~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector71~0_combout\ = ( \LessThan0~9_combout\ & ( ((\Equal2~0_combout\ & \state.DRAW_BOTTOM_LOOP~q\)) # (\state.DRAW_PUCK~q\) ) ) # ( !\LessThan0~9_combout\ & ( (((\Equal2~0_combout\ & \state.DRAW_BOTTOM_LOOP~q\)) # (\state.IDLE~q\)) # 
-- (\state.DRAW_PUCK~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011111111111001101111111111100110111001101110011011100110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal2~0_combout\,
	datab => \ALT_INV_state.DRAW_PUCK~q\,
	datac => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	datad => \ALT_INV_state.IDLE~q\,
	dataf => \ALT_INV_LessThan0~9_combout\,
	combout => \Selector71~0_combout\);

-- Location: FF_X33_Y65_N7
\state.IDLE~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector71~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.IDLE~DUPLICATE_q\);

-- Location: LABCELL_X35_Y66_N39
\clock_counter[31]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \clock_counter[31]~0_combout\ = ( \KEY[8]~input_o\ & ( \state.IDLE~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_state.IDLE~DUPLICATE_q\,
	dataf => \ALT_INV_KEY[8]~input_o\,
	combout => \clock_counter[31]~0_combout\);

-- Location: FF_X33_Y66_N5
\clock_counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~89_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(21));

-- Location: LABCELL_X33_Y66_N6
\Add2~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~93_sumout\ = SUM(( clock_counter(22) ) + ( GND ) + ( \Add2~90\ ))
-- \Add2~94\ = CARRY(( clock_counter(22) ) + ( GND ) + ( \Add2~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(22),
	cin => \Add2~90\,
	sumout => \Add2~93_sumout\,
	cout => \Add2~94\);

-- Location: FF_X33_Y66_N7
\clock_counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~93_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(22));

-- Location: LABCELL_X33_Y66_N9
\Add2~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~97_sumout\ = SUM(( clock_counter(23) ) + ( GND ) + ( \Add2~94\ ))
-- \Add2~98\ = CARRY(( clock_counter(23) ) + ( GND ) + ( \Add2~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(23),
	cin => \Add2~94\,
	sumout => \Add2~97_sumout\,
	cout => \Add2~98\);

-- Location: FF_X33_Y66_N11
\clock_counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~97_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(23));

-- Location: LABCELL_X33_Y66_N12
\Add2~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~101_sumout\ = SUM(( clock_counter(24) ) + ( GND ) + ( \Add2~98\ ))
-- \Add2~102\ = CARRY(( clock_counter(24) ) + ( GND ) + ( \Add2~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(24),
	cin => \Add2~98\,
	sumout => \Add2~101_sumout\,
	cout => \Add2~102\);

-- Location: FF_X33_Y66_N14
\clock_counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~101_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(24));

-- Location: LABCELL_X33_Y66_N15
\Add2~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~81_sumout\ = SUM(( clock_counter(25) ) + ( GND ) + ( \Add2~102\ ))
-- \Add2~82\ = CARRY(( clock_counter(25) ) + ( GND ) + ( \Add2~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(25),
	cin => \Add2~102\,
	sumout => \Add2~81_sumout\,
	cout => \Add2~82\);

-- Location: FF_X33_Y66_N17
\clock_counter[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~81_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(25));

-- Location: LABCELL_X33_Y66_N18
\Add2~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~85_sumout\ = SUM(( clock_counter(26) ) + ( GND ) + ( \Add2~82\ ))
-- \Add2~86\ = CARRY(( clock_counter(26) ) + ( GND ) + ( \Add2~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(26),
	cin => \Add2~82\,
	sumout => \Add2~85_sumout\,
	cout => \Add2~86\);

-- Location: FF_X33_Y66_N20
\clock_counter[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~85_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(26));

-- Location: LABCELL_X33_Y66_N21
\Add2~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~105_sumout\ = SUM(( clock_counter(27) ) + ( GND ) + ( \Add2~86\ ))
-- \Add2~106\ = CARRY(( clock_counter(27) ) + ( GND ) + ( \Add2~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(27),
	cin => \Add2~86\,
	sumout => \Add2~105_sumout\,
	cout => \Add2~106\);

-- Location: FF_X33_Y66_N22
\clock_counter[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~105_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(27));

-- Location: LABCELL_X33_Y66_N24
\Add2~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~109_sumout\ = SUM(( clock_counter(28) ) + ( GND ) + ( \Add2~106\ ))
-- \Add2~110\ = CARRY(( clock_counter(28) ) + ( GND ) + ( \Add2~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(28),
	cin => \Add2~106\,
	sumout => \Add2~109_sumout\,
	cout => \Add2~110\);

-- Location: FF_X33_Y66_N26
\clock_counter[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~109_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(28));

-- Location: LABCELL_X33_Y66_N27
\Add2~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~113_sumout\ = SUM(( clock_counter(29) ) + ( GND ) + ( \Add2~110\ ))
-- \Add2~114\ = CARRY(( clock_counter(29) ) + ( GND ) + ( \Add2~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(29),
	cin => \Add2~110\,
	sumout => \Add2~113_sumout\,
	cout => \Add2~114\);

-- Location: FF_X33_Y66_N29
\clock_counter[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~113_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(29));

-- Location: LABCELL_X33_Y66_N30
\Add2~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~117_sumout\ = SUM(( clock_counter(30) ) + ( GND ) + ( \Add2~114\ ))
-- \Add2~118\ = CARRY(( clock_counter(30) ) + ( GND ) + ( \Add2~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(30),
	cin => \Add2~114\,
	sumout => \Add2~117_sumout\,
	cout => \Add2~118\);

-- Location: FF_X33_Y66_N32
\clock_counter[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~117_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(30));

-- Location: LABCELL_X33_Y66_N33
\Add2~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~77_sumout\ = SUM(( clock_counter(31) ) + ( GND ) + ( \Add2~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(31),
	cin => \Add2~118\,
	sumout => \Add2~77_sumout\);

-- Location: FF_X33_Y66_N35
\clock_counter[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~77_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(31));

-- Location: LABCELL_X33_Y66_N45
\LessThan0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~9_combout\ = ( \LessThan0~5_combout\ & ( (!clock_counter(31) & !\LessThan0~8_combout\) ) ) # ( !\LessThan0~5_combout\ & ( (!clock_counter(31) & ((!\LessThan0~8_combout\) # (clock_counter(20)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010101010101000001010101010100000101000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(31),
	datac => \ALT_INV_LessThan0~8_combout\,
	datad => ALT_INV_clock_counter(20),
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \LessThan0~9_combout\);

-- Location: FF_X33_Y67_N2
\clock_counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~125_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(0));

-- Location: LABCELL_X33_Y67_N3
\Add2~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~121_sumout\ = SUM(( clock_counter(1) ) + ( GND ) + ( \Add2~126\ ))
-- \Add2~122\ = CARRY(( clock_counter(1) ) + ( GND ) + ( \Add2~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(1),
	cin => \Add2~126\,
	sumout => \Add2~121_sumout\,
	cout => \Add2~122\);

-- Location: FF_X33_Y67_N5
\clock_counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~121_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(1));

-- Location: LABCELL_X33_Y67_N6
\Add2~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~17_sumout\ = SUM(( clock_counter(2) ) + ( GND ) + ( \Add2~122\ ))
-- \Add2~18\ = CARRY(( clock_counter(2) ) + ( GND ) + ( \Add2~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(2),
	cin => \Add2~122\,
	sumout => \Add2~17_sumout\,
	cout => \Add2~18\);

-- Location: FF_X33_Y67_N7
\clock_counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~17_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(2));

-- Location: LABCELL_X33_Y67_N9
\Add2~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~13_sumout\ = SUM(( clock_counter(3) ) + ( GND ) + ( \Add2~18\ ))
-- \Add2~14\ = CARRY(( clock_counter(3) ) + ( GND ) + ( \Add2~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(3),
	cin => \Add2~18\,
	sumout => \Add2~13_sumout\,
	cout => \Add2~14\);

-- Location: FF_X33_Y67_N10
\clock_counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~13_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(3));

-- Location: LABCELL_X33_Y67_N12
\Add2~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~21_sumout\ = SUM(( clock_counter(4) ) + ( GND ) + ( \Add2~14\ ))
-- \Add2~22\ = CARRY(( clock_counter(4) ) + ( GND ) + ( \Add2~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(4),
	cin => \Add2~14\,
	sumout => \Add2~21_sumout\,
	cout => \Add2~22\);

-- Location: FF_X33_Y67_N13
\clock_counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~21_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(4));

-- Location: LABCELL_X33_Y67_N15
\Add2~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~25_sumout\ = SUM(( clock_counter(5) ) + ( GND ) + ( \Add2~22\ ))
-- \Add2~26\ = CARRY(( clock_counter(5) ) + ( GND ) + ( \Add2~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(5),
	cin => \Add2~22\,
	sumout => \Add2~25_sumout\,
	cout => \Add2~26\);

-- Location: FF_X33_Y67_N16
\clock_counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~25_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(5));

-- Location: LABCELL_X33_Y67_N18
\Add2~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~29_sumout\ = SUM(( clock_counter(6) ) + ( GND ) + ( \Add2~26\ ))
-- \Add2~30\ = CARRY(( clock_counter(6) ) + ( GND ) + ( \Add2~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(6),
	cin => \Add2~26\,
	sumout => \Add2~29_sumout\,
	cout => \Add2~30\);

-- Location: FF_X33_Y67_N19
\clock_counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~29_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(6));

-- Location: LABCELL_X33_Y67_N21
\Add2~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~37_sumout\ = SUM(( clock_counter(7) ) + ( GND ) + ( \Add2~30\ ))
-- \Add2~38\ = CARRY(( clock_counter(7) ) + ( GND ) + ( \Add2~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(7),
	cin => \Add2~30\,
	sumout => \Add2~37_sumout\,
	cout => \Add2~38\);

-- Location: FF_X33_Y67_N22
\clock_counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~37_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(7));

-- Location: LABCELL_X33_Y67_N24
\Add2~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~41_sumout\ = SUM(( clock_counter(8) ) + ( GND ) + ( \Add2~38\ ))
-- \Add2~42\ = CARRY(( clock_counter(8) ) + ( GND ) + ( \Add2~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(8),
	cin => \Add2~38\,
	sumout => \Add2~41_sumout\,
	cout => \Add2~42\);

-- Location: FF_X33_Y67_N25
\clock_counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~41_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(8));

-- Location: LABCELL_X33_Y67_N27
\Add2~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~45_sumout\ = SUM(( clock_counter(9) ) + ( GND ) + ( \Add2~42\ ))
-- \Add2~46\ = CARRY(( clock_counter(9) ) + ( GND ) + ( \Add2~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(9),
	cin => \Add2~42\,
	sumout => \Add2~45_sumout\,
	cout => \Add2~46\);

-- Location: FF_X33_Y67_N28
\clock_counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~45_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(9));

-- Location: LABCELL_X33_Y67_N30
\Add2~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~33_sumout\ = SUM(( clock_counter(10) ) + ( GND ) + ( \Add2~46\ ))
-- \Add2~34\ = CARRY(( clock_counter(10) ) + ( GND ) + ( \Add2~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(10),
	cin => \Add2~46\,
	sumout => \Add2~33_sumout\,
	cout => \Add2~34\);

-- Location: FF_X33_Y67_N31
\clock_counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~33_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(10));

-- Location: LABCELL_X33_Y67_N33
\Add2~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~49_sumout\ = SUM(( clock_counter(11) ) + ( GND ) + ( \Add2~34\ ))
-- \Add2~50\ = CARRY(( clock_counter(11) ) + ( GND ) + ( \Add2~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(11),
	cin => \Add2~34\,
	sumout => \Add2~49_sumout\,
	cout => \Add2~50\);

-- Location: FF_X33_Y67_N34
\clock_counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~49_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(11));

-- Location: LABCELL_X33_Y67_N36
\Add2~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~9_sumout\ = SUM(( clock_counter(12) ) + ( GND ) + ( \Add2~50\ ))
-- \Add2~10\ = CARRY(( clock_counter(12) ) + ( GND ) + ( \Add2~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(12),
	cin => \Add2~50\,
	sumout => \Add2~9_sumout\,
	cout => \Add2~10\);

-- Location: FF_X33_Y67_N37
\clock_counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~9_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(12));

-- Location: LABCELL_X33_Y67_N39
\Add2~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~5_sumout\ = SUM(( clock_counter(13) ) + ( GND ) + ( \Add2~10\ ))
-- \Add2~6\ = CARRY(( clock_counter(13) ) + ( GND ) + ( \Add2~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(13),
	cin => \Add2~10\,
	sumout => \Add2~5_sumout\,
	cout => \Add2~6\);

-- Location: FF_X33_Y67_N41
\clock_counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~5_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(13));

-- Location: LABCELL_X33_Y67_N42
\Add2~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~53_sumout\ = SUM(( clock_counter(14) ) + ( GND ) + ( \Add2~6\ ))
-- \Add2~54\ = CARRY(( clock_counter(14) ) + ( GND ) + ( \Add2~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(14),
	cin => \Add2~6\,
	sumout => \Add2~53_sumout\,
	cout => \Add2~54\);

-- Location: FF_X33_Y67_N43
\clock_counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~53_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(14));

-- Location: LABCELL_X33_Y67_N45
\Add2~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~57_sumout\ = SUM(( clock_counter(15) ) + ( GND ) + ( \Add2~54\ ))
-- \Add2~58\ = CARRY(( clock_counter(15) ) + ( GND ) + ( \Add2~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(15),
	cin => \Add2~54\,
	sumout => \Add2~57_sumout\,
	cout => \Add2~58\);

-- Location: FF_X33_Y67_N46
\clock_counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~57_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(15));

-- Location: LABCELL_X33_Y67_N48
\Add2~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~61_sumout\ = SUM(( clock_counter(16) ) + ( GND ) + ( \Add2~58\ ))
-- \Add2~62\ = CARRY(( clock_counter(16) ) + ( GND ) + ( \Add2~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(16),
	cin => \Add2~58\,
	sumout => \Add2~61_sumout\,
	cout => \Add2~62\);

-- Location: FF_X33_Y67_N49
\clock_counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~61_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(16));

-- Location: LABCELL_X33_Y67_N51
\Add2~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~65_sumout\ = SUM(( clock_counter(17) ) + ( GND ) + ( \Add2~62\ ))
-- \Add2~66\ = CARRY(( clock_counter(17) ) + ( GND ) + ( \Add2~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(17),
	cin => \Add2~62\,
	sumout => \Add2~65_sumout\,
	cout => \Add2~66\);

-- Location: FF_X33_Y67_N52
\clock_counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~65_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(17));

-- Location: LABCELL_X33_Y67_N54
\Add2~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~69_sumout\ = SUM(( clock_counter(18) ) + ( GND ) + ( \Add2~66\ ))
-- \Add2~70\ = CARRY(( clock_counter(18) ) + ( GND ) + ( \Add2~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(18),
	cin => \Add2~66\,
	sumout => \Add2~69_sumout\,
	cout => \Add2~70\);

-- Location: FF_X33_Y67_N55
\clock_counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~69_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(18));

-- Location: LABCELL_X33_Y67_N57
\Add2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~1_sumout\ = SUM(( clock_counter(19) ) + ( GND ) + ( \Add2~70\ ))
-- \Add2~2\ = CARRY(( clock_counter(19) ) + ( GND ) + ( \Add2~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(19),
	cin => \Add2~70\,
	sumout => \Add2~1_sumout\,
	cout => \Add2~2\);

-- Location: FF_X33_Y67_N58
\clock_counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~1_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(19));

-- Location: FF_X33_Y66_N2
\clock_counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~73_sumout\,
	sclr => \LessThan0~9_combout\,
	ena => \clock_counter[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(20));

-- Location: MLABCELL_X34_Y66_N33
\Selector65~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~10_combout\ = ( !\state.ERASE_PUCK~q\ & ( !\state.IDLE~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.IDLE~DUPLICATE_q\,
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \Selector65~10_combout\);

-- Location: MLABCELL_X34_Y66_N42
\Selector65~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~11_combout\ = ( \Equal1~1_combout\ & ( \Selector65~10_combout\ & ( (\state.START~q\ & (\Equal0~0_combout\ & (\Equal1~0_combout\ & \Equal0~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_Equal1~0_combout\,
	datad => \ALT_INV_Equal0~1_combout\,
	datae => \ALT_INV_Equal1~1_combout\,
	dataf => \ALT_INV_Selector65~10_combout\,
	combout => \Selector65~11_combout\);

-- Location: LABCELL_X33_Y66_N48
\Selector65~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~12_combout\ = ( !\Selector65~11_combout\ & ( \LessThan0~5_combout\ & ( (!\state.IDLE~DUPLICATE_q\) # ((\LessThan0~8_combout\) # (clock_counter(31))) ) ) ) # ( !\Selector65~11_combout\ & ( !\LessThan0~5_combout\ & ( (!\state.IDLE~DUPLICATE_q\) 
-- # (((!clock_counter(20) & \LessThan0~8_combout\)) # (clock_counter(31))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111101111000000000000000011001111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(20),
	datab => \ALT_INV_state.IDLE~DUPLICATE_q\,
	datac => ALT_INV_clock_counter(31),
	datad => \ALT_INV_LessThan0~8_combout\,
	datae => \ALT_INV_Selector65~11_combout\,
	dataf => \ALT_INV_LessThan0~5_combout\,
	combout => \Selector65~12_combout\);

-- Location: LABCELL_X35_Y66_N48
\Selector65~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~7_combout\ = ( \state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & ( !\state.START~q\ & ( (!\state.IDLE~DUPLICATE_q\ & !\state.ERASE_PUCK~q\) ) ) ) # ( !\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & ( !\state.START~q\ & ( (!\state.IDLE~DUPLICATE_q\ & 
-- (!\state.ERASE_PUCK~q\ & ((\state.DRAW_PADDLE_TWO_LOOP~q\) # (\state.DRAW_PADDLE_ONE_LOOP~q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datac => \ALT_INV_state.IDLE~DUPLICATE_q\,
	datad => \ALT_INV_state.ERASE_PUCK~q\,
	datae => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	dataf => \ALT_INV_state.START~q\,
	combout => \Selector65~7_combout\);

-- Location: LABCELL_X35_Y66_N54
\Selector65~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~8_combout\ = ( \Selector65~2_combout\ & ( \Selector65~7_combout\ ) ) # ( !\Selector65~2_combout\ & ( (\Selector65~1_combout\ & \Selector65~7_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector65~1_combout\,
	datac => \ALT_INV_Selector65~7_combout\,
	dataf => \ALT_INV_Selector65~2_combout\,
	combout => \Selector65~8_combout\);

-- Location: LABCELL_X33_Y65_N33
\Selector63~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector63~0_combout\ = ( !\state.DRAW_PADDLE_ONE_ENTER~q\ & ( (!\state.DRAW_TOP_ENTER~q\ & (!\state.DRAW_BOTTOM_ENTER~q\ & (!\state.DRAW_PUCK~q\ & !\state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datab => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	datac => \ALT_INV_state.DRAW_PUCK~q\,
	datad => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~DUPLICATE_q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	combout => \Selector63~0_combout\);

-- Location: LABCELL_X33_Y65_N42
\Selector65~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~9_combout\ = ( !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (\Selector63~0_combout\ & \state.INIT~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datac => \ALT_INV_Selector63~0_combout\,
	datad => \ALT_INV_state.INIT~q\,
	dataf => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	combout => \Selector65~9_combout\);

-- Location: MLABCELL_X34_Y66_N39
\Selector65~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~4_combout\ = ( !\draw.x\(0) & ( (\Equal0~0_combout\ & (!\draw.x\(1) & ((\state.DRAW_TOP_LOOP~q\) # (\state.DRAW_BOTTOM_LOOP~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000110000000100000011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_draw.x\(1),
	datad => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	dataf => \ALT_INV_draw.x\(0),
	combout => \Selector65~4_combout\);

-- Location: LABCELL_X33_Y65_N21
\Selector65~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~5_combout\ = ( !\state.ERASE_PUCK~q\ & ( (!\state.IDLE~q\ & !\state.START~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110000000000110011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.IDLE~q\,
	datad => \ALT_INV_state.START~q\,
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \Selector65~5_combout\);

-- Location: LABCELL_X35_Y66_N12
\Selector65~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~6_combout\ = ( \Equal4~1_combout\ & ( \Equal4~0_combout\ & ( (\Selector65~5_combout\ & (((\state.ERASE_PADDLE_ONE_LOOP~q\ & \Equal4~2_combout\)) # (\Selector65~4_combout\))) ) ) ) # ( !\Equal4~1_combout\ & ( \Equal4~0_combout\ & ( 
-- (\Selector65~4_combout\ & \Selector65~5_combout\) ) ) ) # ( \Equal4~1_combout\ & ( !\Equal4~0_combout\ & ( (\Selector65~4_combout\ & \Selector65~5_combout\) ) ) ) # ( !\Equal4~1_combout\ & ( !\Equal4~0_combout\ & ( (\Selector65~4_combout\ & 
-- \Selector65~5_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000000011110000000000011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Equal4~2_combout\,
	datac => \ALT_INV_Selector65~4_combout\,
	datad => \ALT_INV_Selector65~5_combout\,
	datae => \ALT_INV_Equal4~1_combout\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \Selector65~6_combout\);

-- Location: LABCELL_X33_Y62_N18
\Selector65~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~13_combout\ = ( \Selector65~9_combout\ & ( \Selector65~6_combout\ & ( (\state.ERASE_PUCK~q\ & !\Equal9~1_combout\) ) ) ) # ( !\Selector65~9_combout\ & ( \Selector65~6_combout\ & ( (\state.ERASE_PUCK~q\ & !\Equal9~1_combout\) ) ) ) # ( 
-- \Selector65~9_combout\ & ( !\Selector65~6_combout\ & ( (!\state.ERASE_PUCK~q\ & (((\Selector65~12_combout\ & !\Selector65~8_combout\)))) # (\state.ERASE_PUCK~q\ & (!\Equal9~1_combout\)) ) ) ) # ( !\Selector65~9_combout\ & ( !\Selector65~6_combout\ & ( 
-- (\state.ERASE_PUCK~q\ & !\Equal9~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100010011100100010001000100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PUCK~q\,
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_Selector65~12_combout\,
	datad => \ALT_INV_Selector65~8_combout\,
	datae => \ALT_INV_Selector65~9_combout\,
	dataf => \ALT_INV_Selector65~6_combout\,
	combout => \Selector65~13_combout\);

-- Location: LABCELL_X37_Y62_N15
\LessThan8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan8~1_combout\ = ( !\Add12~21_sumout\ & ( \Add8~9_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add8~9_sumout\,
	dataf => \ALT_INV_Add12~21_sumout\,
	combout => \LessThan8~1_combout\);

-- Location: MLABCELL_X34_Y63_N3
\LessThan8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan8~0_combout\ = ( \Add12~17_sumout\ & ( (\Add8~13_sumout\ & (\Add8~17_sumout\ & (!\Add8~9_sumout\ $ (\Add12~21_sumout\)))) ) ) # ( !\Add12~17_sumout\ & ( (!\Add8~13_sumout\ & (\Add8~17_sumout\ & (!\Add8~9_sumout\ $ (\Add12~21_sumout\)))) # 
-- (\Add8~13_sumout\ & (!\Add8~9_sumout\ $ (((\Add12~21_sumout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100110000010011010011000001001100000100000000010000010000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~13_sumout\,
	datab => \ALT_INV_Add8~9_sumout\,
	datac => \ALT_INV_Add8~17_sumout\,
	datad => \ALT_INV_Add12~21_sumout\,
	dataf => \ALT_INV_Add12~17_sumout\,
	combout => \LessThan8~0_combout\);

-- Location: LABCELL_X37_Y62_N24
\LessThan8~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan8~2_combout\ = ( \Add8~5_sumout\ & ( \Add12~9_sumout\ & ( (!\Add8~1_sumout\) # ((\Add12~5_sumout\ & (!\LessThan8~1_combout\ & !\LessThan8~0_combout\))) ) ) ) # ( !\Add8~5_sumout\ & ( \Add12~9_sumout\ & ( ((!\Add8~1_sumout\) # 
-- ((!\LessThan8~1_combout\ & !\LessThan8~0_combout\))) # (\Add12~5_sumout\) ) ) ) # ( \Add8~5_sumout\ & ( !\Add12~9_sumout\ & ( (\Add12~5_sumout\ & (!\LessThan8~1_combout\ & (!\Add8~1_sumout\ & !\LessThan8~0_combout\))) ) ) ) # ( !\Add8~5_sumout\ & ( 
-- !\Add12~9_sumout\ & ( (!\Add8~1_sumout\ & (((!\LessThan8~1_combout\ & !\LessThan8~0_combout\)) # (\Add12~5_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101000001010000010000000000000011111101111101011111010011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~5_sumout\,
	datab => \ALT_INV_LessThan8~1_combout\,
	datac => \ALT_INV_Add8~1_sumout\,
	datad => \ALT_INV_LessThan8~0_combout\,
	datae => \ALT_INV_Add8~5_sumout\,
	dataf => \ALT_INV_Add12~9_sumout\,
	combout => \LessThan8~2_combout\);

-- Location: MLABCELL_X34_Y63_N12
\always0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~3_combout\ = ( \Add8~29_sumout\ & ( paddle_two_y(5) & ( (!paddle_two_y(7) & (!\Add8~25_sumout\ & (!\Add8~21_sumout\ & paddle_two_y(6)))) # (paddle_two_y(7) & ((!\Add8~21_sumout\) # ((!\Add8~25_sumout\ & paddle_two_y(6))))) ) ) ) # ( 
-- !\Add8~29_sumout\ & ( paddle_two_y(5) & ( (!paddle_two_y(7) & (!\Add8~25_sumout\ & (!\Add8~21_sumout\ & paddle_two_y(6)))) # (paddle_two_y(7) & ((!\Add8~21_sumout\) # ((!\Add8~25_sumout\ & paddle_two_y(6))))) ) ) ) # ( \Add8~29_sumout\ & ( 
-- !paddle_two_y(5) & ( (!paddle_two_y(7) & (!\Add8~25_sumout\ & (!\Add8~21_sumout\ & paddle_two_y(6)))) # (paddle_two_y(7) & ((!\Add8~21_sumout\) # ((!\Add8~25_sumout\ & paddle_two_y(6))))) ) ) ) # ( !\Add8~29_sumout\ & ( !paddle_two_y(5) & ( 
-- (!paddle_two_y(7) & (!\Add8~21_sumout\ & ((!\Add8~25_sumout\) # (paddle_two_y(6))))) # (paddle_two_y(7) & ((!\Add8~25_sumout\) # ((!\Add8~21_sumout\) # (paddle_two_y(6))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011001011110011001100001011001000110000101100100011000010110010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~25_sumout\,
	datab => ALT_INV_paddle_two_y(7),
	datac => \ALT_INV_Add8~21_sumout\,
	datad => ALT_INV_paddle_two_y(6),
	datae => \ALT_INV_Add8~29_sumout\,
	dataf => ALT_INV_paddle_two_y(5),
	combout => \always0~3_combout\);

-- Location: LABCELL_X35_Y62_N42
\always0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~1_combout\ = (!paddle_two_y(3) & !\Add8~5_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_two_y(3),
	datad => \ALT_INV_Add8~5_sumout\,
	combout => \always0~1_combout\);

-- Location: MLABCELL_X34_Y63_N6
\always0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~2_combout\ = ( !\Add8~29_sumout\ & ( paddle_two_y(5) & ( (!\Add8~25_sumout\ & (!paddle_two_y(6) & (!paddle_two_y(7) $ (\Add8~21_sumout\)))) # (\Add8~25_sumout\ & (paddle_two_y(6) & (!paddle_two_y(7) $ (\Add8~21_sumout\)))) ) ) ) # ( 
-- \Add8~29_sumout\ & ( !paddle_two_y(5) & ( (!\Add8~25_sumout\ & (!paddle_two_y(6) & (!paddle_two_y(7) $ (\Add8~21_sumout\)))) # (\Add8~25_sumout\ & (paddle_two_y(6) & (!paddle_two_y(7) $ (\Add8~21_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000100000100100000110000010010000010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~25_sumout\,
	datab => ALT_INV_paddle_two_y(7),
	datac => \ALT_INV_Add8~21_sumout\,
	datad => ALT_INV_paddle_two_y(6),
	datae => \ALT_INV_Add8~29_sumout\,
	dataf => ALT_INV_paddle_two_y(5),
	combout => \always0~2_combout\);

-- Location: LABCELL_X35_Y62_N48
\always0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~0_combout\ = ( \Add8~5_sumout\ & ( paddle_two_y(2) & ( (!\Add8~9_sumout\ & (paddle_two_y(1) & (!paddle_two_y(3) & !\Add8~13_sumout\))) ) ) ) # ( !\Add8~5_sumout\ & ( paddle_two_y(2) & ( (!\Add8~9_sumout\ & (paddle_two_y(1) & (paddle_two_y(3) & 
-- !\Add8~13_sumout\))) ) ) ) # ( \Add8~5_sumout\ & ( !paddle_two_y(2) & ( (!paddle_two_y(3) & ((!\Add8~9_sumout\) # ((paddle_two_y(1) & !\Add8~13_sumout\)))) ) ) ) # ( !\Add8~5_sumout\ & ( !paddle_two_y(2) & ( (paddle_two_y(3) & ((!\Add8~9_sumout\) # 
-- ((paddle_two_y(1) & !\Add8~13_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101100001010101100001010000000000010000000000010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~9_sumout\,
	datab => ALT_INV_paddle_two_y(1),
	datac => ALT_INV_paddle_two_y(3),
	datad => \ALT_INV_Add8~13_sumout\,
	datae => \ALT_INV_Add8~5_sumout\,
	dataf => ALT_INV_paddle_two_y(2),
	combout => \always0~0_combout\);

-- Location: LABCELL_X35_Y62_N54
\always0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~4_combout\ = ( \always0~2_combout\ & ( \always0~0_combout\ & ( (!\always0~3_combout\ & (\Add8~1_sumout\ & paddle_two_y(4))) ) ) ) # ( !\always0~2_combout\ & ( \always0~0_combout\ & ( !\always0~3_combout\ ) ) ) # ( \always0~2_combout\ & ( 
-- !\always0~0_combout\ & ( (!\always0~3_combout\ & ((!\always0~1_combout\ & ((paddle_two_y(4)) # (\Add8~1_sumout\))) # (\always0~1_combout\ & (\Add8~1_sumout\ & paddle_two_y(4))))) ) ) ) # ( !\always0~2_combout\ & ( !\always0~0_combout\ & ( 
-- !\always0~3_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010000010001000101010101010101010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_always0~3_combout\,
	datab => \ALT_INV_always0~1_combout\,
	datac => \ALT_INV_Add8~1_sumout\,
	datad => ALT_INV_paddle_two_y(4),
	datae => \ALT_INV_always0~2_combout\,
	dataf => \ALT_INV_always0~0_combout\,
	combout => \always0~4_combout\);

-- Location: LABCELL_X37_Y62_N54
\LessThan8~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan8~4_combout\ = ( \Add12~25_sumout\ & ( \Add12~29_sumout\ & ( (\Add8~29_sumout\ & (\Add8~25_sumout\ & (\Add8~21_sumout\ & !\Add12~13_sumout\))) ) ) ) # ( !\Add12~25_sumout\ & ( \Add12~29_sumout\ & ( (\Add8~21_sumout\ & (((\Add8~29_sumout\ & 
-- !\Add12~13_sumout\)) # (\Add8~25_sumout\))) ) ) ) # ( \Add12~25_sumout\ & ( !\Add12~29_sumout\ & ( ((\Add8~29_sumout\ & (\Add8~25_sumout\ & !\Add12~13_sumout\))) # (\Add8~21_sumout\) ) ) ) # ( !\Add12~25_sumout\ & ( !\Add12~29_sumout\ & ( 
-- (((\Add8~29_sumout\ & !\Add12~13_sumout\)) # (\Add8~21_sumout\)) # (\Add8~25_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111100111111000111110000111100000111000000110000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~29_sumout\,
	datab => \ALT_INV_Add8~25_sumout\,
	datac => \ALT_INV_Add8~21_sumout\,
	datad => \ALT_INV_Add12~13_sumout\,
	datae => \ALT_INV_Add12~25_sumout\,
	dataf => \ALT_INV_Add12~29_sumout\,
	combout => \LessThan8~4_combout\);

-- Location: MLABCELL_X39_Y65_N24
\LessThan8~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan8~3_combout\ = ( \Add12~29_sumout\ & ( \Add12~13_sumout\ & ( (\Add8~21_sumout\ & (\Add8~29_sumout\ & (!\Add8~25_sumout\ $ (\Add12~25_sumout\)))) ) ) ) # ( !\Add12~29_sumout\ & ( \Add12~13_sumout\ & ( (!\Add8~21_sumout\ & (\Add8~29_sumout\ & 
-- (!\Add8~25_sumout\ $ (\Add12~25_sumout\)))) ) ) ) # ( \Add12~29_sumout\ & ( !\Add12~13_sumout\ & ( (\Add8~21_sumout\ & (!\Add8~29_sumout\ & (!\Add8~25_sumout\ $ (\Add12~25_sumout\)))) ) ) ) # ( !\Add12~29_sumout\ & ( !\Add12~13_sumout\ & ( 
-- (!\Add8~21_sumout\ & (!\Add8~29_sumout\ & (!\Add8~25_sumout\ $ (\Add12~25_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001000000000000000010010000000000000000100100000000000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~25_sumout\,
	datab => \ALT_INV_Add12~25_sumout\,
	datac => \ALT_INV_Add8~21_sumout\,
	datad => \ALT_INV_Add8~29_sumout\,
	datae => \ALT_INV_Add12~29_sumout\,
	dataf => \ALT_INV_Add12~13_sumout\,
	combout => \LessThan8~3_combout\);

-- Location: MLABCELL_X34_Y64_N12
\always0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~5_combout\ = ( \LessThan8~3_combout\ & ( (\always0~4_combout\ & (((\LessThan8~2_combout\ & !\LessThan8~4_combout\)) # (\Add12~1_sumout\))) ) ) # ( !\LessThan8~3_combout\ & ( (\always0~4_combout\ & ((!\LessThan8~4_combout\) # (\Add12~1_sumout\))) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000011001100110000001100010011000000110001001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan8~2_combout\,
	datab => \ALT_INV_always0~4_combout\,
	datac => \ALT_INV_Add12~1_sumout\,
	datad => \ALT_INV_LessThan8~4_combout\,
	dataf => \ALT_INV_LessThan8~3_combout\,
	combout => \always0~5_combout\);

-- Location: LABCELL_X35_Y64_N30
\Equal10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal10~0_combout\ = ( \Add7~25_sumout\ & ( (!\Add7~21_sumout\ & \Add7~29_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000100010001000100010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add7~21_sumout\,
	datab => \ALT_INV_Add7~29_sumout\,
	dataf => \ALT_INV_Add7~25_sumout\,
	combout => \Equal10~0_combout\);

-- Location: LABCELL_X35_Y64_N42
\Equal10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal10~1_combout\ = ( \Add7~9_sumout\ & ( !\Add7~13_sumout\ & ( (!\Add7~17_sumout\ & (\Equal10~0_combout\ & (\Add7~5_sumout\ & \Add7~1_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000001000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add7~17_sumout\,
	datab => \ALT_INV_Equal10~0_combout\,
	datac => \ALT_INV_Add7~5_sumout\,
	datad => \ALT_INV_Add7~1_sumout\,
	datae => \ALT_INV_Add7~9_sumout\,
	dataf => \ALT_INV_Add7~13_sumout\,
	combout => \Equal10~1_combout\);

-- Location: LABCELL_X33_Y62_N24
\Selector65~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~14_combout\ = ( \Selector65~9_combout\ & ( \Selector65~6_combout\ & ( (\state.ERASE_PUCK~q\ & \Equal10~1_combout\) ) ) ) # ( !\Selector65~9_combout\ & ( \Selector65~6_combout\ & ( (\state.ERASE_PUCK~q\ & \Equal10~1_combout\) ) ) ) # ( 
-- \Selector65~9_combout\ & ( !\Selector65~6_combout\ & ( (!\state.ERASE_PUCK~q\ & (((\Selector65~12_combout\ & !\Selector65~8_combout\)))) # (\state.ERASE_PUCK~q\ & (\Equal10~1_combout\)) ) ) ) # ( !\Selector65~9_combout\ & ( !\Selector65~6_combout\ & ( 
-- (\state.ERASE_PUCK~q\ & \Equal10~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001000110110001000100010001000100010001000100010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PUCK~q\,
	datab => \ALT_INV_Equal10~1_combout\,
	datac => \ALT_INV_Selector65~12_combout\,
	datad => \ALT_INV_Selector65~8_combout\,
	datae => \ALT_INV_Selector65~9_combout\,
	dataf => \ALT_INV_Selector65~6_combout\,
	combout => \Selector65~14_combout\);

-- Location: LABCELL_X33_Y62_N30
\Selector81~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector81~0_combout\ = ( \state.DRAW_PUCK~q\ & ( \state.ERASE_PUCK~q\ & ( (!\Selector65~13_combout\ & (((!\Selector65~14_combout\) # (\always0~5_combout\)))) # (\Selector65~13_combout\ & ((!\always0~10_combout\) # ((\Selector65~14_combout\)))) ) ) ) # ( 
-- !\state.DRAW_PUCK~q\ & ( \state.ERASE_PUCK~q\ & ( (!\Selector65~13_combout\ & (((!\Selector65~14_combout\) # (\always0~5_combout\)))) # (\Selector65~13_combout\ & (!\always0~10_combout\ & ((!\Selector65~14_combout\)))) ) ) ) # ( \state.DRAW_PUCK~q\ & ( 
-- !\state.ERASE_PUCK~q\ & ( (!\Selector65~13_combout\ & (((\always0~5_combout\ & \Selector65~14_combout\)))) # (\Selector65~13_combout\ & ((!\always0~10_combout\) # ((\Selector65~14_combout\)))) ) ) ) # ( !\state.DRAW_PUCK~q\ & ( !\state.ERASE_PUCK~q\ & ( 
-- (!\Selector65~13_combout\ & (((\always0~5_combout\ & \Selector65~14_combout\)))) # (\Selector65~13_combout\ & (!\always0~10_combout\ & ((!\Selector65~14_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000001010010001000101111111101110000010101110111001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector65~13_combout\,
	datab => \ALT_INV_always0~10_combout\,
	datac => \ALT_INV_always0~5_combout\,
	datad => \ALT_INV_Selector65~14_combout\,
	datae => \ALT_INV_state.DRAW_PUCK~q\,
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \Selector81~0_combout\);

-- Location: FF_X33_Y62_N32
\state.DRAW_PUCK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector81~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PUCK~q\);

-- Location: LABCELL_X33_Y65_N3
\draw~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw~0_combout\ = ( !\state.ERASE_PUCK~q\ & ( !\state.DRAW_PUCK~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.DRAW_PUCK~q\,
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \draw~0_combout\);

-- Location: LABCELL_X35_Y63_N54
\Selector12~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector12~1_combout\ = ( \state.ERASE_PADDLE_TWO_ENTER~q\ & ( paddle_one_y(3) & ( (paddle_two_y(3) & ((\draw~0_combout\) # (\puck.y\(3)))) ) ) ) # ( !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( paddle_one_y(3) & ( (\draw~0_combout\) # (\puck.y\(3)) ) ) ) # ( 
-- \state.ERASE_PADDLE_TWO_ENTER~q\ & ( !paddle_one_y(3) & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (paddle_two_y(3) & ((\draw~0_combout\) # (\puck.y\(3))))) ) ) ) # ( !\state.ERASE_PADDLE_TWO_ENTER~q\ & ( !paddle_one_y(3) & ( 
-- (!\state.ERASE_PADDLE_ONE_ENTER~q\ & ((\draw~0_combout\) # (\puck.y\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000001110000000000000111000001110111011101110000000001110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck.y\(3),
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datad => ALT_INV_paddle_two_y(3),
	datae => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	dataf => ALT_INV_paddle_one_y(3),
	combout => \Selector12~1_combout\);

-- Location: LABCELL_X37_Y65_N36
\Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~17_sumout\ = SUM(( \draw.y\(2) ) + ( GND ) + ( \Add0~22\ ))
-- \Add0~18\ = CARRY(( \draw.y\(2) ) + ( GND ) + ( \Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(2),
	cin => \Add0~22\,
	sumout => \Add0~17_sumout\,
	cout => \Add0~18\);

-- Location: LABCELL_X37_Y65_N39
\Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~13_sumout\ = SUM(( \draw.y\(3) ) + ( GND ) + ( \Add0~18\ ))
-- \Add0~14\ = CARRY(( \draw.y\(3) ) + ( GND ) + ( \Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(3),
	cin => \Add0~18\,
	sumout => \Add0~13_sumout\,
	cout => \Add0~14\);

-- Location: LABCELL_X36_Y63_N3
\Add4~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~21_sumout\ = SUM(( !paddle_one_y(2) ) + ( GND ) + ( \Add4~26\ ))
-- \Add4~22\ = CARRY(( !paddle_one_y(2) ) + ( GND ) + ( \Add4~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(2),
	cin => \Add4~26\,
	sumout => \Add4~21_sumout\,
	cout => \Add4~22\);

-- Location: LABCELL_X36_Y63_N6
\Add4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~13_sumout\ = SUM(( !paddle_one_y(3) ) + ( GND ) + ( \Add4~22\ ))
-- \Add4~14\ = CARRY(( !paddle_one_y(3) ) + ( GND ) + ( \Add4~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_one_y(3),
	cin => \Add4~22\,
	sumout => \Add4~13_sumout\,
	cout => \Add4~14\);

-- Location: LABCELL_X36_Y65_N24
\Selector12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector12~0_combout\ = ( \Add4~13_sumout\ & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!paddle_one_y(3)) # (!\paddle_one_y~2_combout\))) ) ) ) # ( !\Add4~13_sumout\ & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & 
-- (!paddle_one_y(3) & \paddle_one_y~2_combout\)) ) ) ) # ( \Add4~13_sumout\ & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\paddle_one_y~2_combout\ & (\Add3~13_sumout\)) # (\paddle_one_y~2_combout\ & ((!paddle_one_y(3)))))) ) ) ) # ( 
-- !\Add4~13_sumout\ & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\paddle_one_y~2_combout\ & (\Add3~13_sumout\)) # (\paddle_one_y~2_combout\ & ((!paddle_one_y(3)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100110000000100010011000000000000001100000011001100110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~13_sumout\,
	datab => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datac => ALT_INV_paddle_one_y(3),
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => \ALT_INV_Add4~13_sumout\,
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector12~0_combout\);

-- Location: LABCELL_X36_Y65_N48
\Selector12~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector12~2_combout\ = ( \WideOr16~0_combout\ & ( \Selector12~0_combout\ ) ) # ( !\WideOr16~0_combout\ & ( \Selector12~0_combout\ ) ) # ( \WideOr16~0_combout\ & ( !\Selector12~0_combout\ & ( (!\Selector12~1_combout\) # ((\paddle_two_y~4_combout\ & 
-- \state.DRAW_PADDLE_TWO_ENTER~q\)) ) ) ) # ( !\WideOr16~0_combout\ & ( !\Selector12~0_combout\ & ( (!\Selector12~1_combout\) # (((\paddle_two_y~4_combout\ & \state.DRAW_PADDLE_TWO_ENTER~q\)) # (\Add0~13_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111011111110011001101110111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_paddle_two_y~4_combout\,
	datab => \ALT_INV_Selector12~1_combout\,
	datac => \ALT_INV_Add0~13_sumout\,
	datad => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\,
	datae => \ALT_INV_WideOr16~0_combout\,
	dataf => \ALT_INV_Selector12~0_combout\,
	combout => \Selector12~2_combout\);

-- Location: FF_X36_Y65_N50
\draw.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector12~2_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.y[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(3));

-- Location: LABCELL_X37_Y65_N42
\Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~9_sumout\ = SUM(( \draw.y\(4) ) + ( GND ) + ( \Add0~14\ ))
-- \Add0~10\ = CARRY(( \draw.y\(4) ) + ( GND ) + ( \Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y\(4),
	cin => \Add0~14\,
	sumout => \Add0~9_sumout\,
	cout => \Add0~10\);

-- Location: LABCELL_X33_Y65_N0
\Selector10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector10~1_combout\ = ( !\state.DRAW_BOTTOM_ENTER~q\ & ( (!\state.DRAW_BOTTOM_LOOP~q\ & ((!\state.ERASE_PADDLE_ONE_ENTER~q\) # (paddle_one_y(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000001010000111100000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(5),
	datac => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	datad => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	dataf => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	combout => \Selector10~1_combout\);

-- Location: LABCELL_X33_Y65_N48
\Selector10~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector10~2_combout\ = ( \Selector10~1_combout\ & ( (!\state.ERASE_PADDLE_TWO_ENTER~q\ & (((\puck.y\(5))) # (\draw~0_combout\))) # (\state.ERASE_PADDLE_TWO_ENTER~q\ & (paddle_two_y(5) & ((\puck.y\(5)) # (\draw~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000101010001111110010101000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_puck.y\(5),
	datad => ALT_INV_paddle_two_y(5),
	dataf => \ALT_INV_Selector10~1_combout\,
	combout => \Selector10~2_combout\);

-- Location: LABCELL_X36_Y62_N48
\paddle_two_y~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~2_combout\ = ( \Add6~5_sumout\ & ( (!\KEY[0]~input_o\ & (\Add5~5_sumout\ & ((!\paddle_two_y~6_combout\)))) # (\KEY[0]~input_o\ & (((!paddle_two_y(5)) # (!\paddle_two_y~6_combout\)))) ) ) # ( !\Add6~5_sumout\ & ( (!\KEY[0]~input_o\ & 
-- (\Add5~5_sumout\ & ((!\paddle_two_y~6_combout\)))) # (\KEY[0]~input_o\ & (((!paddle_two_y(5) & \paddle_two_y~6_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000110000010001000011000001110111001100000111011100110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add5~5_sumout\,
	datab => \ALT_INV_KEY[0]~input_o\,
	datac => ALT_INV_paddle_two_y(5),
	datad => \ALT_INV_paddle_two_y~6_combout\,
	dataf => \ALT_INV_Add6~5_sumout\,
	combout => \paddle_two_y~2_combout\);

-- Location: LABCELL_X37_Y63_N9
\Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~9_sumout\ = SUM(( !paddle_one_y(4) ) + ( VCC ) + ( \Add3~14\ ))
-- \Add3~10\ = CARRY(( !paddle_one_y(4) ) + ( VCC ) + ( \Add3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(4),
	cin => \Add3~14\,
	sumout => \Add3~9_sumout\,
	cout => \Add3~10\);

-- Location: LABCELL_X37_Y63_N12
\Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~5_sumout\ = SUM(( !paddle_one_y(5) ) + ( VCC ) + ( \Add3~10\ ))
-- \Add3~6\ = CARRY(( !paddle_one_y(5) ) + ( VCC ) + ( \Add3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_one_y(5),
	cin => \Add3~10\,
	sumout => \Add3~5_sumout\,
	cout => \Add3~6\);

-- Location: LABCELL_X36_Y65_N18
\Selector10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector10~0_combout\ = ( paddle_one_y(5) & ( \Add3~5_sumout\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & (!\paddle_one_y~2_combout\ & ((!\KEY[2]~input_o\) # (\Add4~5_sumout\)))) ) ) ) # ( !paddle_one_y(5) & ( \Add3~5_sumout\ & ( 
-- (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\KEY[2]~input_o\ & ((!\paddle_one_y~2_combout\))) # (\KEY[2]~input_o\ & ((\paddle_one_y~2_combout\) # (\Add4~5_sumout\))))) ) ) ) # ( paddle_one_y(5) & ( !\Add3~5_sumout\ & ( (\KEY[2]~input_o\ & 
-- (\state.DRAW_PADDLE_ONE_ENTER~q\ & (\Add4~5_sumout\ & !\paddle_one_y~2_combout\))) ) ) ) # ( !paddle_one_y(5) & ( !\Add3~5_sumout\ & ( (\KEY[2]~input_o\ & (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((\paddle_one_y~2_combout\) # (\Add4~5_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100010001000000010000000000100011000100010010001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[2]~input_o\,
	datab => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datac => \ALT_INV_Add4~5_sumout\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => ALT_INV_paddle_one_y(5),
	dataf => \ALT_INV_Add3~5_sumout\,
	combout => \Selector10~0_combout\);

-- Location: LABCELL_X36_Y65_N6
\Selector10~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector10~3_combout\ = ( \paddle_two_y~2_combout\ & ( \Selector10~0_combout\ ) ) # ( !\paddle_two_y~2_combout\ & ( \Selector10~0_combout\ ) ) # ( \paddle_two_y~2_combout\ & ( !\Selector10~0_combout\ & ( (!\Selector10~2_combout\) # (((\Add0~5_sumout\ & 
-- !\WideOr16~0_combout\)) # (\state.DRAW_PADDLE_TWO_ENTER~q\)) ) ) ) # ( !\paddle_two_y~2_combout\ & ( !\Selector10~0_combout\ & ( (!\Selector10~2_combout\) # ((\Add0~5_sumout\ & !\WideOr16~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101110011011100110111001111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add0~5_sumout\,
	datab => \ALT_INV_Selector10~2_combout\,
	datac => \ALT_INV_WideOr16~0_combout\,
	datad => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\,
	datae => \ALT_INV_paddle_two_y~2_combout\,
	dataf => \ALT_INV_Selector10~0_combout\,
	combout => \Selector10~3_combout\);

-- Location: FF_X36_Y65_N8
\draw.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector10~3_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.y[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(5));

-- Location: LABCELL_X37_Y62_N18
\Equal6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~0_combout\ = ( \Add12~5_sumout\ & ( \Add12~13_sumout\ & ( (\draw.y\(5) & (\draw.y\(3) & (!\draw.y\(4) $ (\Add12~9_sumout\)))) ) ) ) # ( !\Add12~5_sumout\ & ( \Add12~13_sumout\ & ( (\draw.y\(5) & (!\draw.y\(3) & (!\draw.y\(4) $ 
-- (\Add12~9_sumout\)))) ) ) ) # ( \Add12~5_sumout\ & ( !\Add12~13_sumout\ & ( (!\draw.y\(5) & (\draw.y\(3) & (!\draw.y\(4) $ (\Add12~9_sumout\)))) ) ) ) # ( !\Add12~5_sumout\ & ( !\Add12~13_sumout\ & ( (!\draw.y\(5) & (!\draw.y\(3) & (!\draw.y\(4) $ 
-- (\Add12~9_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000001000001000000000001001000000000001000001000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(5),
	datab => \ALT_INV_draw.y\(3),
	datac => \ALT_INV_draw.y\(4),
	datad => \ALT_INV_Add12~9_sumout\,
	datae => \ALT_INV_Add12~5_sumout\,
	dataf => \ALT_INV_Add12~13_sumout\,
	combout => \Equal6~0_combout\);

-- Location: LABCELL_X35_Y65_N6
\Selector79~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector79~0_combout\ = ( \Equal6~1_combout\ & ( (\state.DRAW_PADDLE_TWO_LOOP~q\ & ((!\Equal6~0_combout\) # ((!\Equal6~2_combout\) # (\Add12~1_sumout\)))) ) ) # ( !\Equal6~1_combout\ & ( \state.DRAW_PADDLE_TWO_LOOP~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010100010101010101010001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datab => \ALT_INV_Equal6~0_combout\,
	datac => \ALT_INV_Equal6~2_combout\,
	datad => \ALT_INV_Add12~1_sumout\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Selector79~0_combout\);

-- Location: LABCELL_X35_Y65_N3
\Selector79~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector79~1_combout\ = ( \state.DRAW_PADDLE_TWO_ENTER~q\ ) # ( !\state.DRAW_PADDLE_TWO_ENTER~q\ & ( \Selector79~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Selector79~0_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\,
	combout => \Selector79~1_combout\);

-- Location: FF_X35_Y65_N5
\state.DRAW_PADDLE_TWO_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector79~1_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_TWO_LOOP~q\);

-- Location: LABCELL_X35_Y65_N42
\draw.x[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[3]~4_combout\ = ( \Equal6~1_combout\ & ( (\state.DRAW_PADDLE_TWO_LOOP~q\ & (\Equal6~2_combout\ & (!\Add12~1_sumout\ & \Equal6~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	datab => \ALT_INV_Equal6~2_combout\,
	datac => \ALT_INV_Add12~1_sumout\,
	datad => \ALT_INV_Equal6~0_combout\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \draw.x[3]~4_combout\);

-- Location: FF_X35_Y65_N44
\state.ERASE_PUCK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.x[3]~4_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PUCK~q\);

-- Location: FF_X33_Y62_N49
\puck_velocity.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~21_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(7));

-- Location: MLABCELL_X34_Y62_N51
\Add9~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~21_sumout\ = SUM(( !\Equal7~1_combout\ $ (\puck_velocity.y\(7)) ) + ( GND ) + ( \Add9~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal7~1_combout\,
	datac => \ALT_INV_puck_velocity.y\(7),
	cin => \Add9~26\,
	sumout => \Add9~21_sumout\);

-- Location: MLABCELL_X34_Y62_N21
\Add10~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~21_sumout\ = SUM(( GND ) + ( !\Equal8~1_combout\ $ (!\Add9~21_sumout\) ) + ( \Add10~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010100101010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~1_combout\,
	dataf => \ALT_INV_Add9~21_sumout\,
	cin => \Add10~26\,
	sumout => \Add10~21_sumout\);

-- Location: LABCELL_X33_Y62_N48
\Add10~21_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~21_wirecell_combout\ = !\Add10~21_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add10~21_sumout\,
	combout => \Add10~21_wirecell_combout\);

-- Location: FF_X33_Y62_N50
\puck_velocity.y[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~21_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y[7]~DUPLICATE_q\);

-- Location: FF_X34_Y63_N52
\puck.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add8~21_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck.y\(7));

-- Location: MLABCELL_X34_Y63_N51
\Add8~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~21_sumout\ = SUM(( \puck.y\(7) ) + ( !\puck_velocity.y[7]~DUPLICATE_q\ ) + ( \Add8~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity.y[7]~DUPLICATE_q\,
	datad => \ALT_INV_puck.y\(7),
	cin => \Add8~26\,
	sumout => \Add8~21_sumout\);

-- Location: MLABCELL_X34_Y63_N54
\Equal7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal7~2_combout\ = ( !\Add8~13_sumout\ & ( (\Add8~17_sumout\ & \Add8~9_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add8~17_sumout\,
	datac => \ALT_INV_Add8~9_sumout\,
	dataf => \ALT_INV_Add8~13_sumout\,
	combout => \Equal7~2_combout\);

-- Location: MLABCELL_X34_Y63_N24
\Equal7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal7~1_combout\ = ( !\Add8~29_sumout\ & ( !\Add8~25_sumout\ & ( (!\Add8~21_sumout\ & (!\Add8~5_sumout\ & (\Equal7~2_combout\ & !\Add8~1_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~21_sumout\,
	datab => \ALT_INV_Add8~5_sumout\,
	datac => \ALT_INV_Equal7~2_combout\,
	datad => \ALT_INV_Add8~1_sumout\,
	datae => \ALT_INV_Add8~29_sumout\,
	dataf => \ALT_INV_Add8~25_sumout\,
	combout => \Equal7~1_combout\);

-- Location: LABCELL_X33_Y62_N54
\Add10~17_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~17_wirecell_combout\ = !\Add10~17_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add10~17_sumout\,
	combout => \Add10~17_wirecell_combout\);

-- Location: FF_X33_Y62_N55
\puck_velocity.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~17_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(0));

-- Location: MLABCELL_X34_Y63_N57
\Equal7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal7~0_combout\ = (\Add8~17_sumout\ & !\Add8~5_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000110000001100000011000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add8~17_sumout\,
	datac => \ALT_INV_Add8~5_sumout\,
	combout => \Equal7~0_combout\);

-- Location: MLABCELL_X34_Y63_N0
\Equal8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal8~0_combout\ = ( \Add8~13_sumout\ & ( !\Add8~9_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add8~9_sumout\,
	dataf => \ALT_INV_Add8~13_sumout\,
	combout => \Equal8~0_combout\);

-- Location: MLABCELL_X34_Y62_N24
\Equal8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal8~1_combout\ = ( \Add8~25_sumout\ & ( \Equal8~0_combout\ & ( (\Add8~1_sumout\ & (\Equal7~0_combout\ & (!\Add8~21_sumout\ & \Add8~29_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~1_sumout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datac => \ALT_INV_Add8~21_sumout\,
	datad => \ALT_INV_Add8~29_sumout\,
	datae => \ALT_INV_Add8~25_sumout\,
	dataf => \ALT_INV_Equal8~0_combout\,
	combout => \Equal8~1_combout\);

-- Location: LABCELL_X33_Y62_N45
\Add10~1_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~1_wirecell_combout\ = ( !\Add10~1_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add10~1_sumout\,
	combout => \Add10~1_wirecell_combout\);

-- Location: FF_X33_Y62_N46
\puck_velocity.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~1_wirecell_combout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.y\(4));

-- Location: MLABCELL_X34_Y63_N18
\always0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~7_combout\ = ( paddle_one_y(5) & ( paddle_one_y(7) & ( (\Add8~21_sumout\ & (!\Add8~29_sumout\ & (!\Add8~25_sumout\ $ (paddle_one_y(6))))) ) ) ) # ( !paddle_one_y(5) & ( paddle_one_y(7) & ( (\Add8~21_sumout\ & (\Add8~29_sumout\ & 
-- (!\Add8~25_sumout\ $ (paddle_one_y(6))))) ) ) ) # ( paddle_one_y(5) & ( !paddle_one_y(7) & ( (!\Add8~21_sumout\ & (!\Add8~29_sumout\ & (!\Add8~25_sumout\ $ (paddle_one_y(6))))) ) ) ) # ( !paddle_one_y(5) & ( !paddle_one_y(7) & ( (!\Add8~21_sumout\ & 
-- (\Add8~29_sumout\ & (!\Add8~25_sumout\ $ (paddle_one_y(6))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010010000100100000000000000000000000010010000100100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~25_sumout\,
	datab => ALT_INV_paddle_one_y(6),
	datac => \ALT_INV_Add8~21_sumout\,
	datad => \ALT_INV_Add8~29_sumout\,
	datae => ALT_INV_paddle_one_y(5),
	dataf => ALT_INV_paddle_one_y(7),
	combout => \always0~7_combout\);

-- Location: LABCELL_X35_Y63_N0
\always0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~6_combout\ = (!\Add8~9_sumout\ & ((!paddle_one_y(2)) # ((!\Add8~13_sumout\ & paddle_one_y(1))))) # (\Add8~9_sumout\ & (!\Add8~13_sumout\ & (!paddle_one_y(2) & paddle_one_y(1))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000011101000101000001110100010100000111010001010000011101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~9_sumout\,
	datab => \ALT_INV_Add8~13_sumout\,
	datac => ALT_INV_paddle_one_y(2),
	datad => ALT_INV_paddle_one_y(1),
	combout => \always0~6_combout\);

-- Location: LABCELL_X35_Y63_N12
\always0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~8_combout\ = ( \always0~6_combout\ & ( \Add8~5_sumout\ & ( (\always0~7_combout\ & ((!\Add8~1_sumout\ & ((!paddle_one_y(4)) # (!paddle_one_y(3)))) # (\Add8~1_sumout\ & (!paddle_one_y(4) & !paddle_one_y(3))))) ) ) ) # ( !\always0~6_combout\ & ( 
-- \Add8~5_sumout\ & ( (!\Add8~1_sumout\ & (\always0~7_combout\ & !paddle_one_y(4))) ) ) ) # ( \always0~6_combout\ & ( !\Add8~5_sumout\ & ( (\always0~7_combout\ & ((!\Add8~1_sumout\) # (!paddle_one_y(4)))) ) ) ) # ( !\always0~6_combout\ & ( !\Add8~5_sumout\ 
-- & ( (\always0~7_combout\ & ((!\Add8~1_sumout\ & ((!paddle_one_y(4)) # (!paddle_one_y(3)))) # (\Add8~1_sumout\ & (!paddle_one_y(4) & !paddle_one_y(3))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001000100000001100100011001000100000001000000011001000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~1_sumout\,
	datab => \ALT_INV_always0~7_combout\,
	datac => ALT_INV_paddle_one_y(4),
	datad => ALT_INV_paddle_one_y(3),
	datae => \ALT_INV_always0~6_combout\,
	dataf => \ALT_INV_Add8~5_sumout\,
	combout => \always0~8_combout\);

-- Location: MLABCELL_X39_Y64_N24
\always0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~9_combout\ = ( \Add8~25_sumout\ & ( \Add8~21_sumout\ & ( (!paddle_one_y(5) & (paddle_one_y(6) & (paddle_one_y(7) & !\Add8~29_sumout\))) ) ) ) # ( !\Add8~25_sumout\ & ( \Add8~21_sumout\ & ( (paddle_one_y(7) & (((!paddle_one_y(5) & 
-- !\Add8~29_sumout\)) # (paddle_one_y(6)))) ) ) ) # ( \Add8~25_sumout\ & ( !\Add8~21_sumout\ & ( ((!paddle_one_y(5) & (paddle_one_y(6) & !\Add8~29_sumout\))) # (paddle_one_y(7)) ) ) ) # ( !\Add8~25_sumout\ & ( !\Add8~21_sumout\ & ( (((!paddle_one_y(5) & 
-- !\Add8~29_sumout\)) # (paddle_one_y(7))) # (paddle_one_y(6)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111100111111001011110000111100001011000000110000001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(5),
	datab => ALT_INV_paddle_one_y(6),
	datac => ALT_INV_paddle_one_y(7),
	datad => \ALT_INV_Add8~29_sumout\,
	datae => \ALT_INV_Add8~25_sumout\,
	dataf => \ALT_INV_Add8~21_sumout\,
	combout => \always0~9_combout\);

-- Location: MLABCELL_X39_Y64_N54
\LessThan6~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~4_combout\ = ( \Add11~25_sumout\ & ( \Add11~13_sumout\ & ( (!\Add11~29_sumout\ & \Add8~21_sumout\) ) ) ) # ( !\Add11~25_sumout\ & ( \Add11~13_sumout\ & ( (!\Add11~29_sumout\ & ((\Add8~25_sumout\) # (\Add8~21_sumout\))) # (\Add11~29_sumout\ & 
-- (\Add8~21_sumout\ & \Add8~25_sumout\)) ) ) ) # ( \Add11~25_sumout\ & ( !\Add11~13_sumout\ & ( (!\Add11~29_sumout\ & (((\Add8~25_sumout\ & \Add8~29_sumout\)) # (\Add8~21_sumout\))) # (\Add11~29_sumout\ & (\Add8~21_sumout\ & (\Add8~25_sumout\ & 
-- \Add8~29_sumout\))) ) ) ) # ( !\Add11~25_sumout\ & ( !\Add11~13_sumout\ & ( (!\Add11~29_sumout\ & (((\Add8~29_sumout\) # (\Add8~25_sumout\)) # (\Add8~21_sumout\))) # (\Add11~29_sumout\ & (\Add8~21_sumout\ & ((\Add8~29_sumout\) # (\Add8~25_sumout\)))) ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010101110111011001000100010101100101011001010110010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~29_sumout\,
	datab => \ALT_INV_Add8~21_sumout\,
	datac => \ALT_INV_Add8~25_sumout\,
	datad => \ALT_INV_Add8~29_sumout\,
	datae => \ALT_INV_Add11~25_sumout\,
	dataf => \ALT_INV_Add11~13_sumout\,
	combout => \LessThan6~4_combout\);

-- Location: MLABCELL_X39_Y64_N18
\LessThan6~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~3_combout\ = ( \Add11~25_sumout\ & ( \Add11~13_sumout\ & ( (\Add8~25_sumout\ & (\Add8~29_sumout\ & (!\Add11~29_sumout\ $ (\Add8~21_sumout\)))) ) ) ) # ( !\Add11~25_sumout\ & ( \Add11~13_sumout\ & ( (!\Add8~25_sumout\ & (\Add8~29_sumout\ & 
-- (!\Add11~29_sumout\ $ (\Add8~21_sumout\)))) ) ) ) # ( \Add11~25_sumout\ & ( !\Add11~13_sumout\ & ( (\Add8~25_sumout\ & (!\Add8~29_sumout\ & (!\Add11~29_sumout\ $ (\Add8~21_sumout\)))) ) ) ) # ( !\Add11~25_sumout\ & ( !\Add11~13_sumout\ & ( 
-- (!\Add8~25_sumout\ & (!\Add8~29_sumout\ & (!\Add11~29_sumout\ $ (\Add8~21_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001000000000000000010010000000000000000100100000000000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~29_sumout\,
	datab => \ALT_INV_Add8~21_sumout\,
	datac => \ALT_INV_Add8~25_sumout\,
	datad => \ALT_INV_Add8~29_sumout\,
	datae => \ALT_INV_Add11~25_sumout\,
	dataf => \ALT_INV_Add11~13_sumout\,
	combout => \LessThan6~3_combout\);

-- Location: MLABCELL_X39_Y64_N15
\LessThan6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~1_combout\ = ( !\Add11~21_sumout\ & ( \Add8~9_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add8~9_sumout\,
	dataf => \ALT_INV_Add11~21_sumout\,
	combout => \LessThan6~1_combout\);

-- Location: LABCELL_X35_Y63_N3
\LessThan6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~0_combout\ = ( \Add11~17_sumout\ & ( (\Add8~13_sumout\ & (\Add8~17_sumout\ & (!\Add8~9_sumout\ $ (\Add11~21_sumout\)))) ) ) # ( !\Add11~17_sumout\ & ( (!\Add8~13_sumout\ & (\Add8~17_sumout\ & (!\Add8~9_sumout\ $ (\Add11~21_sumout\)))) # 
-- (\Add8~13_sumout\ & (!\Add8~9_sumout\ $ ((\Add11~21_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000110100101001000011010010100000000001000010000000000100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~9_sumout\,
	datab => \ALT_INV_Add8~13_sumout\,
	datac => \ALT_INV_Add11~21_sumout\,
	datad => \ALT_INV_Add8~17_sumout\,
	dataf => \ALT_INV_Add11~17_sumout\,
	combout => \LessThan6~0_combout\);

-- Location: LABCELL_X35_Y63_N6
\LessThan6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~2_combout\ = ( \Add11~9_sumout\ & ( \Add8~5_sumout\ & ( (!\Add8~1_sumout\) # ((!\LessThan6~1_combout\ & (!\LessThan6~0_combout\ & \Add11~5_sumout\))) ) ) ) # ( !\Add11~9_sumout\ & ( \Add8~5_sumout\ & ( (!\Add8~1_sumout\ & 
-- (!\LessThan6~1_combout\ & (!\LessThan6~0_combout\ & \Add11~5_sumout\))) ) ) ) # ( \Add11~9_sumout\ & ( !\Add8~5_sumout\ & ( (!\Add8~1_sumout\) # (((!\LessThan6~1_combout\ & !\LessThan6~0_combout\)) # (\Add11~5_sumout\)) ) ) ) # ( !\Add11~9_sumout\ & ( 
-- !\Add8~5_sumout\ & ( (!\Add8~1_sumout\ & (((!\LessThan6~1_combout\ & !\LessThan6~0_combout\)) # (\Add11~5_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010101010111010101111111100000000100000001010101011101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~1_sumout\,
	datab => \ALT_INV_LessThan6~1_combout\,
	datac => \ALT_INV_LessThan6~0_combout\,
	datad => \ALT_INV_Add11~5_sumout\,
	datae => \ALT_INV_Add11~9_sumout\,
	dataf => \ALT_INV_Add8~5_sumout\,
	combout => \LessThan6~2_combout\);

-- Location: MLABCELL_X34_Y64_N18
\always0~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~10_combout\ = ( \LessThan6~3_combout\ & ( \LessThan6~2_combout\ & ( (((!\Add11~1_sumout\ & \LessThan6~4_combout\)) # (\always0~9_combout\)) # (\always0~8_combout\) ) ) ) # ( !\LessThan6~3_combout\ & ( \LessThan6~2_combout\ & ( 
-- (((!\Add11~1_sumout\ & \LessThan6~4_combout\)) # (\always0~9_combout\)) # (\always0~8_combout\) ) ) ) # ( \LessThan6~3_combout\ & ( !\LessThan6~2_combout\ & ( (!\Add11~1_sumout\) # ((\always0~9_combout\) # (\always0~8_combout\)) ) ) ) # ( 
-- !\LessThan6~3_combout\ & ( !\LessThan6~2_combout\ & ( (((!\Add11~1_sumout\ & \LessThan6~4_combout\)) # (\always0~9_combout\)) # (\always0~8_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111110111111101111111011111100111111101111110011111110111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~1_sumout\,
	datab => \ALT_INV_always0~8_combout\,
	datac => \ALT_INV_always0~9_combout\,
	datad => \ALT_INV_LessThan6~4_combout\,
	datae => \ALT_INV_LessThan6~3_combout\,
	dataf => \ALT_INV_LessThan6~2_combout\,
	combout => \always0~10_combout\);

-- Location: MLABCELL_X34_Y64_N24
\Selector46~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector46~1_combout\ = ( \always0~4_combout\ & ( \LessThan8~2_combout\ & ( (\Equal10~1_combout\ & ((!\LessThan8~4_combout\) # (\Add12~1_sumout\))) ) ) ) # ( \always0~4_combout\ & ( !\LessThan8~2_combout\ & ( (\Equal10~1_combout\ & 
-- (((!\LessThan8~4_combout\ & !\LessThan8~3_combout\)) # (\Add12~1_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100010001000100000000000000000011000100110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add12~1_sumout\,
	datab => \ALT_INV_Equal10~1_combout\,
	datac => \ALT_INV_LessThan8~4_combout\,
	datad => \ALT_INV_LessThan8~3_combout\,
	datae => \ALT_INV_always0~4_combout\,
	dataf => \ALT_INV_LessThan8~2_combout\,
	combout => \Selector46~1_combout\);

-- Location: MLABCELL_X34_Y64_N30
\Add13~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~9_sumout\ = SUM(( \puck_velocity.x\(0) ) + ( VCC ) + ( !VCC ))
-- \Add13~10\ = CARRY(( \puck_velocity.x\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity.x\(0),
	cin => GND,
	sumout => \Add13~9_sumout\,
	cout => \Add13~10\);

-- Location: MLABCELL_X34_Y64_N0
\Selector53~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector53~0_combout\ = ( !\state.ERASE_PUCK~q\ & ( (((!\state.INIT~q\) # ((!\puck_velocity.x\(0))))) ) ) # ( \state.ERASE_PUCK~q\ & ( (!\Equal9~1_combout\ & ((!\always0~10_combout\ & (((\Add13~9_sumout\)))) # (\always0~10_combout\ & 
-- (((!\puck_velocity.x\(0))))))) # (\Equal9~1_combout\ & (((!\Selector46~1_combout\ & (!\puck_velocity.x\(0))) # (\Selector46~1_combout\ & ((\Add13~9_sumout\)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "1111111111110000011101000000000011111111111100001111111110001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_always0~10_combout\,
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_Selector46~1_combout\,
	datad => \ALT_INV_puck_velocity.x\(0),
	datae => \ALT_INV_state.ERASE_PUCK~q\,
	dataf => \ALT_INV_Add13~9_sumout\,
	datag => \ALT_INV_state.INIT~q\,
	combout => \Selector53~0_combout\);

-- Location: MLABCELL_X34_Y64_N15
\puck_velocity.x[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity.x[0]~0_combout\ = !\Selector53~0_combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Selector53~0_combout\,
	combout => \puck_velocity.x[0]~0_combout\);

-- Location: FF_X34_Y64_N17
\puck_velocity.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity.x[0]~0_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(0));

-- Location: MLABCELL_X34_Y64_N33
\Add13~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~21_sumout\ = SUM(( !\puck_velocity.x\(1) ) + ( GND ) + ( \Add13~10\ ))
-- \Add13~22\ = CARRY(( !\puck_velocity.x\(1) ) + ( GND ) + ( \Add13~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(1),
	cin => \Add13~10\,
	sumout => \Add13~21_sumout\,
	cout => \Add13~22\);

-- Location: MLABCELL_X34_Y64_N54
\Selector46~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector46~0_combout\ = ( \state.ERASE_PUCK~q\ & ( \always0~5_combout\ & ( (!\Equal9~1_combout\ & ((!\always0~10_combout\))) # (\Equal9~1_combout\ & (\Equal10~1_combout\)) ) ) ) # ( !\state.ERASE_PUCK~q\ & ( \always0~5_combout\ & ( !\state.INIT~q\ ) ) ) 
-- # ( \state.ERASE_PUCK~q\ & ( !\always0~5_combout\ & ( (!\always0~10_combout\ & !\Equal9~1_combout\) ) ) ) # ( !\state.ERASE_PUCK~q\ & ( !\always0~5_combout\ & ( !\state.INIT~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010111100000000000010101010101010101111000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~q\,
	datab => \ALT_INV_Equal10~1_combout\,
	datac => \ALT_INV_always0~10_combout\,
	datad => \ALT_INV_Equal9~1_combout\,
	datae => \ALT_INV_state.ERASE_PUCK~q\,
	dataf => \ALT_INV_always0~5_combout\,
	combout => \Selector46~0_combout\);

-- Location: FF_X34_Y64_N34
\puck_velocity.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~21_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(1));

-- Location: MLABCELL_X34_Y64_N36
\Add13~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~25_sumout\ = SUM(( !\puck_velocity.x\(2) ) + ( GND ) + ( \Add13~22\ ))
-- \Add13~26\ = CARRY(( !\puck_velocity.x\(2) ) + ( GND ) + ( \Add13~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(2),
	cin => \Add13~22\,
	sumout => \Add13~25_sumout\,
	cout => \Add13~26\);

-- Location: FF_X34_Y64_N37
\puck_velocity.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~25_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(2));

-- Location: MLABCELL_X34_Y64_N39
\Add13~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~29_sumout\ = SUM(( !\puck_velocity.x\(3) ) + ( GND ) + ( \Add13~26\ ))
-- \Add13~30\ = CARRY(( !\puck_velocity.x\(3) ) + ( GND ) + ( \Add13~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(3),
	cin => \Add13~26\,
	sumout => \Add13~29_sumout\,
	cout => \Add13~30\);

-- Location: FF_X34_Y64_N40
\puck_velocity.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~29_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(3));

-- Location: MLABCELL_X34_Y64_N42
\Add13~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~1_sumout\ = SUM(( !\puck_velocity.x\(4) ) + ( GND ) + ( \Add13~30\ ))
-- \Add13~2\ = CARRY(( !\puck_velocity.x\(4) ) + ( GND ) + ( \Add13~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(4),
	cin => \Add13~30\,
	sumout => \Add13~1_sumout\,
	cout => \Add13~2\);

-- Location: FF_X34_Y64_N43
\puck_velocity.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~1_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(4));

-- Location: MLABCELL_X34_Y64_N45
\Add13~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~13_sumout\ = SUM(( !\puck_velocity.x\(5) ) + ( GND ) + ( \Add13~2\ ))
-- \Add13~14\ = CARRY(( !\puck_velocity.x\(5) ) + ( GND ) + ( \Add13~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(5),
	cin => \Add13~2\,
	sumout => \Add13~13_sumout\,
	cout => \Add13~14\);

-- Location: FF_X34_Y64_N46
\puck_velocity.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~13_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(5));

-- Location: MLABCELL_X34_Y64_N48
\Add13~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~17_sumout\ = SUM(( !\puck_velocity.x\(6) ) + ( GND ) + ( \Add13~14\ ))
-- \Add13~18\ = CARRY(( !\puck_velocity.x\(6) ) + ( GND ) + ( \Add13~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(6),
	cin => \Add13~14\,
	sumout => \Add13~17_sumout\,
	cout => \Add13~18\);

-- Location: FF_X34_Y64_N49
\puck_velocity.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~17_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(6));

-- Location: MLABCELL_X34_Y64_N51
\Add13~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~5_sumout\ = SUM(( !\puck_velocity.x\(7) ) + ( GND ) + ( \Add13~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity.x\(7),
	cin => \Add13~18\,
	sumout => \Add13~5_sumout\);

-- Location: FF_X34_Y64_N52
\puck_velocity.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add13~5_sumout\,
	clrn => \KEY[8]~input_o\,
	sclr => \ALT_INV_state.ERASE_PUCK~q\,
	ena => \Selector46~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity.x\(7));

-- Location: LABCELL_X35_Y64_N21
\Add7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~5_sumout\ = SUM(( \puck.x\(7) ) + ( \puck_velocity.x\(7) ) + ( \Add7~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity.x\(7),
	datad => \ALT_INV_puck.x\(7),
	cin => \Add7~18\,
	sumout => \Add7~5_sumout\);

-- Location: LABCELL_X35_Y64_N54
\Equal9~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal9~2_combout\ = ( !\Add7~13_sumout\ & ( \Add7~9_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add7~9_sumout\,
	dataf => \ALT_INV_Add7~13_sumout\,
	combout => \Equal9~2_combout\);

-- Location: LABCELL_X35_Y64_N24
\Selector65~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~18_combout\ = ( \Equal9~2_combout\ & ( \Add7~17_sumout\ & ( \state.ERASE_PUCK~q\ ) ) ) # ( !\Equal9~2_combout\ & ( \Add7~17_sumout\ & ( \state.ERASE_PUCK~q\ ) ) ) # ( \Equal9~2_combout\ & ( !\Add7~17_sumout\ & ( (\state.ERASE_PUCK~q\ & 
-- (((!\Equal9~0_combout\) # (\Add7~1_sumout\)) # (\Add7~5_sumout\))) ) ) ) # ( !\Equal9~2_combout\ & ( !\Add7~17_sumout\ & ( \state.ERASE_PUCK~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111011100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add7~5_sumout\,
	datab => \ALT_INV_Add7~1_sumout\,
	datac => \ALT_INV_Equal9~0_combout\,
	datad => \ALT_INV_state.ERASE_PUCK~q\,
	datae => \ALT_INV_Equal9~2_combout\,
	dataf => \ALT_INV_Add7~17_sumout\,
	combout => \Selector65~18_combout\);

-- Location: LABCELL_X35_Y64_N48
\Selector65~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~15_combout\ = ( \Equal9~2_combout\ & ( \state.ERASE_PUCK~q\ & ( ((!\Equal10~0_combout\) # ((!\Add7~5_sumout\) # (!\Add7~1_sumout\))) # (\Add7~17_sumout\) ) ) ) # ( !\Equal9~2_combout\ & ( \state.ERASE_PUCK~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add7~17_sumout\,
	datab => \ALT_INV_Equal10~0_combout\,
	datac => \ALT_INV_Add7~5_sumout\,
	datad => \ALT_INV_Add7~1_sumout\,
	datae => \ALT_INV_Equal9~2_combout\,
	dataf => \ALT_INV_state.ERASE_PUCK~q\,
	combout => \Selector65~15_combout\);

-- Location: LABCELL_X33_Y66_N57
\Selector65~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~16_combout\ = ( \Selector65~9_combout\ & ( (!\Selector65~11_combout\ & !\Selector72~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Selector65~11_combout\,
	datad => \ALT_INV_Selector72~1_combout\,
	dataf => \ALT_INV_Selector65~9_combout\,
	combout => \Selector65~16_combout\);

-- Location: LABCELL_X35_Y66_N24
\Selector65~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~17_combout\ = ( \Selector65~6_combout\ & ( \Selector65~1_combout\ & ( !\state.ERASE_PUCK~q\ ) ) ) # ( !\Selector65~6_combout\ & ( \Selector65~1_combout\ & ( (!\state.ERASE_PUCK~q\ & ((!\Selector65~16_combout\) # (\Selector65~7_combout\))) ) ) 
-- ) # ( \Selector65~6_combout\ & ( !\Selector65~1_combout\ & ( !\state.ERASE_PUCK~q\ ) ) ) # ( !\Selector65~6_combout\ & ( !\Selector65~1_combout\ & ( (!\state.ERASE_PUCK~q\ & ((!\Selector65~16_combout\) # ((\Selector65~2_combout\ & 
-- \Selector65~7_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110100000000111111110000000011001111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector65~2_combout\,
	datab => \ALT_INV_Selector65~16_combout\,
	datac => \ALT_INV_Selector65~7_combout\,
	datad => \ALT_INV_state.ERASE_PUCK~q\,
	datae => \ALT_INV_Selector65~6_combout\,
	dataf => \ALT_INV_Selector65~1_combout\,
	combout => \Selector65~17_combout\);

-- Location: MLABCELL_X34_Y64_N6
\Selector65~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~19_combout\ = ( \state.INIT~q\ & ( \Selector65~17_combout\ ) ) # ( !\state.INIT~q\ & ( \Selector65~17_combout\ ) ) # ( \state.INIT~q\ & ( !\Selector65~17_combout\ & ( (!\Selector65~18_combout\ & (((!\always0~10_combout\) # 
-- (!\Selector65~15_combout\)))) # (\Selector65~18_combout\ & (((\Selector65~15_combout\)) # (\always0~5_combout\))) ) ) ) # ( !\state.INIT~q\ & ( !\Selector65~17_combout\ & ( (!\Selector65~18_combout\ & (((!\always0~10_combout\ & \Selector65~15_combout\)))) 
-- # (\Selector65~18_combout\ & (((\Selector65~15_combout\)) # (\always0~5_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000111110101101110111111010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector65~18_combout\,
	datab => \ALT_INV_always0~5_combout\,
	datac => \ALT_INV_always0~10_combout\,
	datad => \ALT_INV_Selector65~15_combout\,
	datae => \ALT_INV_state.INIT~q\,
	dataf => \ALT_INV_Selector65~17_combout\,
	combout => \Selector65~19_combout\);

-- Location: FF_X34_Y64_N7
\state.INIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector65~19_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.INIT~q\);

-- Location: LABCELL_X36_Y65_N45
\Selector16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector16~0_combout\ = ( \state.DRAW_PADDLE_ONE_ENTER~q\ ) # ( !\state.DRAW_PADDLE_ONE_ENTER~q\ & ( !\state.INIT~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_state.INIT~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	combout => \Selector16~0_combout\);

-- Location: FF_X36_Y63_N38
\paddle_one_y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector14~1_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(1));

-- Location: LABCELL_X37_Y63_N3
\Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~21_sumout\ = SUM(( !paddle_one_y(2) ) + ( VCC ) + ( \Add3~26\ ))
-- \Add3~22\ = CARRY(( !paddle_one_y(2) ) + ( VCC ) + ( \Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(2),
	cin => \Add3~26\,
	sumout => \Add3~21_sumout\,
	cout => \Add3~22\);

-- Location: LABCELL_X37_Y63_N6
\Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~13_sumout\ = SUM(( !paddle_one_y(3) ) + ( VCC ) + ( \Add3~22\ ))
-- \Add3~14\ = CARRY(( !paddle_one_y(3) ) + ( VCC ) + ( \Add3~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(3),
	cin => \Add3~22\,
	sumout => \Add3~13_sumout\,
	cout => \Add3~14\);

-- Location: LABCELL_X36_Y65_N36
\Selector20~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector20~0_combout\ = ( paddle_one_y(3) & ( \state.DRAW_PADDLE_ONE_ENTER~q\ & ( ((!\KEY[2]~input_o\ & (!\Add3~13_sumout\)) # (\KEY[2]~input_o\ & ((!\Add4~13_sumout\)))) # (\paddle_one_y~2_combout\) ) ) ) # ( !paddle_one_y(3) & ( 
-- \state.DRAW_PADDLE_ONE_ENTER~q\ & ( (!\paddle_one_y~2_combout\ & ((!\KEY[2]~input_o\ & (!\Add3~13_sumout\)) # (\KEY[2]~input_o\ & ((!\Add4~13_sumout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010001100100000001011111110110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~13_sumout\,
	datab => \ALT_INV_paddle_one_y~2_combout\,
	datac => \ALT_INV_KEY[2]~input_o\,
	datad => \ALT_INV_Add4~13_sumout\,
	datae => ALT_INV_paddle_one_y(3),
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	combout => \Selector20~0_combout\);

-- Location: FF_X36_Y65_N38
\paddle_one_y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector20~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(3));

-- Location: LABCELL_X36_Y63_N9
\Add4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~9_sumout\ = SUM(( !paddle_one_y(4) ) + ( GND ) + ( \Add4~14\ ))
-- \Add4~10\ = CARRY(( !paddle_one_y(4) ) + ( GND ) + ( \Add4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(4),
	cin => \Add4~14\,
	sumout => \Add4~9_sumout\,
	cout => \Add4~10\);

-- Location: LABCELL_X36_Y63_N42
\Selector19~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector19~0_combout\ = ( paddle_one_y(4) & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\Add4~9_sumout\) # (\paddle_one_y~2_combout\))) ) ) ) # ( !paddle_one_y(4) & ( \KEY[2]~input_o\ & ( (!\Add4~9_sumout\ & 
-- (\state.DRAW_PADDLE_ONE_ENTER~q\ & !\paddle_one_y~2_combout\)) ) ) ) # ( paddle_one_y(4) & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\Add3~9_sumout\) # (\paddle_one_y~2_combout\))) ) ) ) # ( !paddle_one_y(4) & ( !\KEY[2]~input_o\ & ( 
-- (!\Add3~9_sumout\ & (\state.DRAW_PADDLE_ONE_ENTER~q\ & !\paddle_one_y~2_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000000000000010100000111100001100000000000000110000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~9_sumout\,
	datab => \ALT_INV_Add4~9_sumout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => ALT_INV_paddle_one_y(4),
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector19~0_combout\);

-- Location: FF_X36_Y63_N44
\paddle_one_y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector19~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(4));

-- Location: LABCELL_X36_Y63_N12
\Add4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~5_sumout\ = SUM(( !paddle_one_y(5) ) + ( GND ) + ( \Add4~10\ ))
-- \Add4~6\ = CARRY(( !paddle_one_y(5) ) + ( GND ) + ( \Add4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_one_y(5),
	cin => \Add4~10\,
	sumout => \Add4~5_sumout\,
	cout => \Add4~6\);

-- Location: LABCELL_X36_Y65_N0
\Selector18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector18~0_combout\ = ( paddle_one_y(5) & ( \state.DRAW_PADDLE_ONE_ENTER~q\ & ( ((!\KEY[2]~input_o\ & ((!\Add3~5_sumout\))) # (\KEY[2]~input_o\ & (!\Add4~5_sumout\))) # (\paddle_one_y~2_combout\) ) ) ) # ( !paddle_one_y(5) & ( 
-- \state.DRAW_PADDLE_ONE_ENTER~q\ & ( (!\KEY[2]~input_o\ & (((!\Add3~5_sumout\) # (\paddle_one_y~2_combout\)))) # (\KEY[2]~input_o\ & (!\Add4~5_sumout\ & ((!\paddle_one_y~2_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011100100101010101110010011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[2]~input_o\,
	datab => \ALT_INV_Add4~5_sumout\,
	datac => \ALT_INV_Add3~5_sumout\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => ALT_INV_paddle_one_y(5),
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	combout => \Selector18~0_combout\);

-- Location: FF_X36_Y65_N2
\paddle_one_y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector18~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(5));

-- Location: LABCELL_X37_Y63_N15
\Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~1_sumout\ = SUM(( paddle_one_y(6) ) + ( VCC ) + ( \Add3~6\ ))
-- \Add3~2\ = CARRY(( paddle_one_y(6) ) + ( VCC ) + ( \Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(6),
	cin => \Add3~6\,
	sumout => \Add3~1_sumout\,
	cout => \Add3~2\);

-- Location: LABCELL_X36_Y63_N15
\Add4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~1_sumout\ = SUM(( paddle_one_y(6) ) + ( GND ) + ( \Add4~6\ ))
-- \Add4~2\ = CARRY(( paddle_one_y(6) ) + ( GND ) + ( \Add4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(6),
	cin => \Add4~6\,
	sumout => \Add4~1_sumout\,
	cout => \Add4~2\);

-- Location: LABCELL_X36_Y63_N24
\Selector9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~1_combout\ = ( paddle_one_y(6) & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((\paddle_one_y~2_combout\) # (\Add4~1_sumout\))) ) ) ) # ( !paddle_one_y(6) & ( \KEY[2]~input_o\ & ( (\Add4~1_sumout\ & (\state.DRAW_PADDLE_ONE_ENTER~q\ 
-- & !\paddle_one_y~2_combout\)) ) ) ) # ( paddle_one_y(6) & ( !\KEY[2]~input_o\ & ( (\Add3~1_sumout\ & (\state.DRAW_PADDLE_ONE_ENTER~q\ & !\paddle_one_y~2_combout\)) ) ) ) # ( !paddle_one_y(6) & ( !\KEY[2]~input_o\ & ( (\Add3~1_sumout\ & 
-- (\state.DRAW_PADDLE_ONE_ENTER~q\ & !\paddle_one_y~2_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000000011000000000000001100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~1_sumout\,
	datab => \ALT_INV_Add4~1_sumout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => ALT_INV_paddle_one_y(6),
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector9~1_combout\);

-- Location: FF_X36_Y63_N26
\paddle_one_y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector9~1_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(6));

-- Location: LABCELL_X37_Y63_N18
\Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~17_sumout\ = SUM(( paddle_one_y(7) ) + ( VCC ) + ( \Add3~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_one_y(7),
	cin => \Add3~2\,
	sumout => \Add3~17_sumout\);

-- Location: LABCELL_X36_Y63_N18
\Add4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add4~17_sumout\ = SUM(( paddle_one_y(7) ) + ( GND ) + ( \Add4~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_one_y(7),
	cin => \Add4~2\,
	sumout => \Add4~17_sumout\);

-- Location: LABCELL_X36_Y65_N54
\Selector8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~1_combout\ = ( paddle_one_y(7) & ( \state.DRAW_PADDLE_ONE_ENTER~q\ & ( ((\Add3~17_sumout\ & !\paddle_one_y~2_combout\)) # (\KEY[2]~input_o\) ) ) ) # ( !paddle_one_y(7) & ( \state.DRAW_PADDLE_ONE_ENTER~q\ & ( (!\paddle_one_y~2_combout\ & 
-- ((!\KEY[2]~input_o\ & (\Add3~17_sumout\)) # (\KEY[2]~input_o\ & ((\Add4~17_sumout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010011000000000101111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~17_sumout\,
	datab => \ALT_INV_Add4~17_sumout\,
	datac => \ALT_INV_KEY[2]~input_o\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => ALT_INV_paddle_one_y(7),
	dataf => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	combout => \Selector8~1_combout\);

-- Location: FF_X36_Y65_N56
\paddle_one_y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector8~1_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(7));

-- Location: MLABCELL_X39_Y64_N6
\paddle_one_y~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_one_y~0_combout\ = ( paddle_one_y(1) & ( paddle_one_y(4) & ( (!paddle_one_y(3) & (paddle_one_y(6) & !paddle_one_y(5))) ) ) ) # ( !paddle_one_y(1) & ( paddle_one_y(4) & ( (!paddle_one_y(3) & (paddle_one_y(6) & (!paddle_one_y(5) & 
-- !paddle_one_y(2)))) ) ) ) # ( paddle_one_y(1) & ( !paddle_one_y(4) & ( (paddle_one_y(6) & !paddle_one_y(5)) ) ) ) # ( !paddle_one_y(1) & ( !paddle_one_y(4) & ( (paddle_one_y(6) & !paddle_one_y(5)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000100000000000000010000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(3),
	datab => ALT_INV_paddle_one_y(6),
	datac => ALT_INV_paddle_one_y(5),
	datad => ALT_INV_paddle_one_y(2),
	datae => ALT_INV_paddle_one_y(1),
	dataf => ALT_INV_paddle_one_y(4),
	combout => \paddle_one_y~0_combout\);

-- Location: MLABCELL_X39_Y64_N12
\paddle_one_y~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_one_y~1_combout\ = ( paddle_one_y(4) & ( (paddle_one_y(5) & (!paddle_one_y(6) & (paddle_one_y(3) & !paddle_one_y(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000100000000000000010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_one_y(5),
	datab => ALT_INV_paddle_one_y(6),
	datac => ALT_INV_paddle_one_y(3),
	datad => ALT_INV_paddle_one_y(7),
	dataf => ALT_INV_paddle_one_y(4),
	combout => \paddle_one_y~1_combout\);

-- Location: MLABCELL_X39_Y64_N0
\paddle_one_y~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_one_y~2_combout\ = ( !\KEY[2]~input_o\ & ( (((\paddle_one_y~1_combout\ & ((!paddle_one_y(1)) # (paddle_one_y(2)))))) ) ) # ( \KEY[2]~input_o\ & ( ((((\paddle_one_y~0_combout\)) # (paddle_one_y(7)))) # (\KEY[3]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000000000010111111111111111110011111100110101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[3]~input_o\,
	datab => ALT_INV_paddle_one_y(2),
	datac => ALT_INV_paddle_one_y(7),
	datad => \ALT_INV_paddle_one_y~0_combout\,
	datae => \ALT_INV_KEY[2]~input_o\,
	dataf => \ALT_INV_paddle_one_y~1_combout\,
	datag => ALT_INV_paddle_one_y(1),
	combout => \paddle_one_y~2_combout\);

-- Location: LABCELL_X36_Y63_N30
\Selector21~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector21~0_combout\ = ( paddle_one_y(2) & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\Add4~21_sumout\) # (\paddle_one_y~2_combout\))) ) ) ) # ( !paddle_one_y(2) & ( \KEY[2]~input_o\ & ( (!\paddle_one_y~2_combout\ & (!\Add4~21_sumout\ 
-- & \state.DRAW_PADDLE_ONE_ENTER~q\)) ) ) ) # ( paddle_one_y(2) & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & ((!\Add3~21_sumout\) # (\paddle_one_y~2_combout\))) ) ) ) # ( !paddle_one_y(2) & ( !\KEY[2]~input_o\ & ( (!\Add3~21_sumout\ & 
-- (!\paddle_one_y~2_combout\ & \state.DRAW_PADDLE_ONE_ENTER~q\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010001000000000001011101100000000110000000000000011110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~21_sumout\,
	datab => \ALT_INV_paddle_one_y~2_combout\,
	datac => \ALT_INV_Add4~21_sumout\,
	datad => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datae => ALT_INV_paddle_one_y(2),
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector21~0_combout\);

-- Location: FF_X36_Y63_N32
\paddle_one_y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector21~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \Selector16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_one_y(2));

-- Location: LABCELL_X36_Y63_N54
\Selector13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~0_combout\ = ( \state.DRAW_PADDLE_ONE_ENTER~q\ & ( \KEY[2]~input_o\ & ( (!\paddle_one_y~2_combout\ & ((\Add4~21_sumout\))) # (\paddle_one_y~2_combout\ & (!paddle_one_y(2))) ) ) ) # ( \state.DRAW_PADDLE_ONE_ENTER~q\ & ( !\KEY[2]~input_o\ & ( 
-- (!\paddle_one_y~2_combout\ & (\Add3~21_sumout\)) # (\paddle_one_y~2_combout\ & ((!paddle_one_y(2)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101011100110000000000000000000000111111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~21_sumout\,
	datab => ALT_INV_paddle_one_y(2),
	datac => \ALT_INV_Add4~21_sumout\,
	datad => \ALT_INV_paddle_one_y~2_combout\,
	datae => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector13~0_combout\);

-- Location: LABCELL_X35_Y63_N36
\Selector13~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~4_combout\ = ( paddle_one_y(2) & ( (!\state.DRAW_TOP_ENTER~q\ & !\state.DRAW_BOTTOM_ENTER~q\) ) ) # ( !paddle_one_y(2) & ( (!\state.DRAW_TOP_ENTER~q\ & (!\state.ERASE_PADDLE_ONE_ENTER~q\ & !\state.DRAW_BOTTOM_ENTER~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000010101010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datac => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datad => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	dataf => ALT_INV_paddle_one_y(2),
	combout => \Selector13~4_combout\);

-- Location: LABCELL_X35_Y63_N21
\Selector13~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~5_combout\ = ( paddle_two_y(2) & ( (\Selector13~4_combout\ & ((\puck.y\(2)) # (\draw~0_combout\))) ) ) # ( !paddle_two_y(2) & ( (!\state.ERASE_PADDLE_TWO_ENTER~q\ & (\Selector13~4_combout\ & ((\puck.y\(2)) # (\draw~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000101010000000000010101000000000001111110000000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_puck.y\(2),
	datad => \ALT_INV_Selector13~4_combout\,
	dataf => ALT_INV_paddle_two_y(2),
	combout => \Selector13~5_combout\);

-- Location: MLABCELL_X34_Y65_N18
\Selector13~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~6_combout\ = ( \WideOr16~2_combout\ & ( \Add0~17_sumout\ & ( (!\Selector6~6_combout\ & \Selector13~5_combout\) ) ) ) # ( !\WideOr16~2_combout\ & ( \Add0~17_sumout\ & ( (!\Selector6~6_combout\ & (\Equal2~0_combout\ & \Selector13~5_combout\)) ) 
-- ) ) # ( \WideOr16~2_combout\ & ( !\Add0~17_sumout\ & ( (\Selector13~5_combout\ & ((!\Selector6~6_combout\) # (!\Equal1~2_combout\))) ) ) ) # ( !\WideOr16~2_combout\ & ( !\Add0~17_sumout\ & ( (\Equal2~0_combout\ & (\Selector13~5_combout\ & 
-- ((!\Selector6~6_combout\) # (!\Equal1~2_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110010000000001111101000000000001000100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector6~6_combout\,
	datab => \ALT_INV_Equal2~0_combout\,
	datac => \ALT_INV_Equal1~2_combout\,
	datad => \ALT_INV_Selector13~5_combout\,
	datae => \ALT_INV_WideOr16~2_combout\,
	dataf => \ALT_INV_Add0~17_sumout\,
	combout => \Selector13~6_combout\);

-- Location: LABCELL_X36_Y62_N54
\paddle_two_y~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~5_combout\ = ( paddle_two_y(2) & ( (!\paddle_two_y~6_combout\ & ((!\KEY[0]~input_o\ & (\Add5~21_sumout\)) # (\KEY[0]~input_o\ & ((\Add6~21_sumout\))))) ) ) # ( !paddle_two_y(2) & ( ((!\KEY[0]~input_o\ & (\Add5~21_sumout\)) # 
-- (\KEY[0]~input_o\ & ((\Add6~21_sumout\)))) # (\paddle_two_y~6_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101110101111111010111010111111100001000001010100000100000101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_paddle_two_y~6_combout\,
	datab => \ALT_INV_KEY[0]~input_o\,
	datac => \ALT_INV_Add5~21_sumout\,
	datad => \ALT_INV_Add6~21_sumout\,
	dataf => ALT_INV_paddle_two_y(2),
	combout => \paddle_two_y~5_combout\);

-- Location: MLABCELL_X34_Y65_N42
\Selector13~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~1_combout\ = ( \Selector79~0_combout\ & ( \Add0~17_sumout\ ) ) # ( !\Selector79~0_combout\ & ( \Add0~17_sumout\ & ( (((\state.ERASE_PADDLE_ONE_LOOP~q\ & !\Equal4~3_combout\)) # (\Selector75~0_combout\)) # (\Selector77~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001111111001111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Selector77~0_combout\,
	datac => \ALT_INV_Selector75~0_combout\,
	datad => \ALT_INV_Equal4~3_combout\,
	datae => \ALT_INV_Selector79~0_combout\,
	dataf => \ALT_INV_Add0~17_sumout\,
	combout => \Selector13~1_combout\);

-- Location: MLABCELL_X34_Y66_N3
\Selector13~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~2_combout\ = ( \WideOr16~2_combout\ & ( (!\state.IDLE~DUPLICATE_q\ & ((!\state.START~q\) # ((\Equal0~0_combout\ & \Equal0~1_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010100000101100001010000010110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_state.IDLE~DUPLICATE_q\,
	datad => \ALT_INV_Equal0~1_combout\,
	dataf => \ALT_INV_WideOr16~2_combout\,
	combout => \Selector13~2_combout\);

-- Location: LABCELL_X35_Y65_N12
\Selector13~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~3_combout\ = ( \Selector65~3_combout\ & ( \draw.y\(2) ) ) # ( !\Selector65~3_combout\ & ( (\draw.y\(2) & ((!\Selector13~2_combout\) # ((\Selector72~0_combout\) # (\Selector67~1_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010101010101010001010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datab => \ALT_INV_Selector13~2_combout\,
	datac => \ALT_INV_Selector67~1_combout\,
	datad => \ALT_INV_Selector72~0_combout\,
	dataf => \ALT_INV_Selector65~3_combout\,
	combout => \Selector13~3_combout\);

-- Location: LABCELL_X35_Y65_N48
\Selector13~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~7_combout\ = ( \state.DRAW_PADDLE_TWO_ENTER~q\ & ( \Selector13~3_combout\ ) ) # ( !\state.DRAW_PADDLE_TWO_ENTER~q\ & ( \Selector13~3_combout\ ) ) # ( \state.DRAW_PADDLE_TWO_ENTER~q\ & ( !\Selector13~3_combout\ & ( ((!\Selector13~6_combout\) # 
-- ((\Selector13~1_combout\) # (\paddle_two_y~5_combout\))) # (\Selector13~0_combout\) ) ) ) # ( !\state.DRAW_PADDLE_TWO_ENTER~q\ & ( !\Selector13~3_combout\ & ( ((!\Selector13~6_combout\) # (\Selector13~1_combout\)) # (\Selector13~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101110111111111110111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector13~0_combout\,
	datab => \ALT_INV_Selector13~6_combout\,
	datac => \ALT_INV_paddle_two_y~5_combout\,
	datad => \ALT_INV_Selector13~1_combout\,
	datae => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\,
	dataf => \ALT_INV_Selector13~3_combout\,
	combout => \Selector13~7_combout\);

-- Location: FF_X35_Y65_N50
\draw.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector13~7_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(2));

-- Location: LABCELL_X37_Y65_N18
\Equal4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~1_combout\ = ( !\draw.y\(0) & ( (!\draw.y\(2) & (!\Add11~21_sumout\ & (!\draw.y\(1) $ (\Add11~17_sumout\)))) # (\draw.y\(2) & (\Add11~21_sumout\ & (!\draw.y\(1) $ (\Add11~17_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001001000001100000100100000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datab => \ALT_INV_draw.y\(1),
	datac => \ALT_INV_Add11~17_sumout\,
	datad => \ALT_INV_Add11~21_sumout\,
	dataf => \ALT_INV_draw.y\(0),
	combout => \Equal4~1_combout\);

-- Location: LABCELL_X37_Y65_N21
\Equal4~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~4_combout\ = ( \draw.y\(6) & ( !\Add11~25_sumout\ ) ) # ( !\draw.y\(6) & ( \Add11~25_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add11~25_sumout\,
	dataf => \ALT_INV_draw.y\(6),
	combout => \Equal4~4_combout\);

-- Location: LABCELL_X37_Y65_N0
\Selector65~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector65~3_combout\ = ( \Equal4~0_combout\ & ( \draw.y\(7) & ( (\Equal4~1_combout\ & (\state.ERASE_PADDLE_ONE_LOOP~q\ & (!\Equal4~4_combout\ & \Add11~29_sumout\))) ) ) ) # ( \Equal4~0_combout\ & ( !\draw.y\(7) & ( (\Equal4~1_combout\ & 
-- (\state.ERASE_PADDLE_ONE_LOOP~q\ & (!\Equal4~4_combout\ & !\Add11~29_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000100000000000000000000000000000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~1_combout\,
	datab => \ALT_INV_state.ERASE_PADDLE_ONE_LOOP~q\,
	datac => \ALT_INV_Equal4~4_combout\,
	datad => \ALT_INV_Add11~29_sumout\,
	datae => \ALT_INV_Equal4~0_combout\,
	dataf => \ALT_INV_draw.y\(7),
	combout => \Selector65~3_combout\);

-- Location: FF_X37_Y65_N2
\state.DRAW_PADDLE_ONE_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector65~3_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_ONE_ENTER~q\);

-- Location: LABCELL_X35_Y67_N9
\Selector75~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector75~1_combout\ = ( \Selector75~0_combout\ ) # ( !\Selector75~0_combout\ & ( \state.DRAW_PADDLE_ONE_ENTER~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	dataf => \ALT_INV_Selector75~0_combout\,
	combout => \Selector75~1_combout\);

-- Location: FF_X35_Y67_N11
\state.DRAW_PADDLE_ONE_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector75~1_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_ONE_LOOP~q\);

-- Location: LABCELL_X35_Y66_N3
\Selector67~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector67~1_combout\ = ( \Equal4~0_combout\ & ( (\state.DRAW_PADDLE_ONE_LOOP~q\ & (\Equal4~2_combout\ & (!\Add11~1_sumout\ & \Equal4~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	datab => \ALT_INV_Equal4~2_combout\,
	datac => \ALT_INV_Add11~1_sumout\,
	datad => \ALT_INV_Equal4~1_combout\,
	dataf => \ALT_INV_Equal4~0_combout\,
	combout => \Selector67~1_combout\);

-- Location: FF_X35_Y66_N5
\state.ERASE_PADDLE_TWO_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector67~1_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_TWO_ENTER~q\);

-- Location: LABCELL_X35_Y63_N24
\Selector8~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~2_combout\ = ( !\state.ERASE_PADDLE_ONE_ENTER~q\ & ( paddle_one_y(7) & ( (!\state.ERASE_PADDLE_TWO_ENTER~q\ & (((!\puck.y\(7))) # (\draw~0_combout\))) # (\state.ERASE_PADDLE_TWO_ENTER~q\ & (!paddle_two_y(7) & ((!\puck.y\(7)) # 
-- (\draw~0_combout\)))) ) ) ) # ( \state.ERASE_PADDLE_ONE_ENTER~q\ & ( !paddle_one_y(7) & ( (!\state.ERASE_PADDLE_TWO_ENTER~q\ & (((!\puck.y\(7))) # (\draw~0_combout\))) # (\state.ERASE_PADDLE_TWO_ENTER~q\ & (!paddle_two_y(7) & ((!\puck.y\(7)) # 
-- (\draw~0_combout\)))) ) ) ) # ( !\state.ERASE_PADDLE_ONE_ENTER~q\ & ( !paddle_one_y(7) & ( (!\state.ERASE_PADDLE_TWO_ENTER~q\ & (((!\puck.y\(7))) # (\draw~0_combout\))) # (\state.ERASE_PADDLE_TWO_ENTER~q\ & (!paddle_two_y(7) & ((!\puck.y\(7)) # 
-- (\draw~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111001110100010111100111010001011110011101000100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_puck.y\(7),
	datad => ALT_INV_paddle_two_y(7),
	datae => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	dataf => ALT_INV_paddle_one_y(7),
	combout => \Selector8~2_combout\);

-- Location: LABCELL_X37_Y65_N51
\Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~29_sumout\ = SUM(( \draw.y\(7) ) + ( GND ) + ( \Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(7),
	cin => \Add0~2\,
	sumout => \Add0~29_sumout\);

-- Location: LABCELL_X36_Y65_N42
\Selector8~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~3_combout\ = ( \Add0~29_sumout\ & ( (!\Selector8~2_combout\) # (((!\WideOr16~0_combout\) # (\Selector8~1_combout\)) # (\Selector8~0_combout\)) ) ) # ( !\Add0~29_sumout\ & ( (!\Selector8~2_combout\) # ((\Selector8~1_combout\) # 
-- (\Selector8~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111110111111101111111011111111111111101111111111111110111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector8~2_combout\,
	datab => \ALT_INV_Selector8~0_combout\,
	datac => \ALT_INV_Selector8~1_combout\,
	datad => \ALT_INV_WideOr16~0_combout\,
	dataf => \ALT_INV_Add0~29_sumout\,
	combout => \Selector8~3_combout\);

-- Location: FF_X36_Y65_N44
\draw.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector8~3_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.y[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(7));

-- Location: MLABCELL_X34_Y65_N48
\Selector67~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector67~2_combout\ = ( \Equal6~0_combout\ & ( \Equal6~1_combout\ & ( (\state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\ & (!\Equal6~3_combout\ & (!\draw.y\(7) $ (\Add12~29_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000010000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(7),
	datab => \ALT_INV_state.ERASE_PADDLE_TWO_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_Equal6~3_combout\,
	datad => \ALT_INV_Add12~29_sumout\,
	datae => \ALT_INV_Equal6~0_combout\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Selector67~2_combout\);

-- Location: FF_X34_Y65_N49
\state.DRAW_PADDLE_TWO_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector67~2_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_TWO_ENTER~q\);

-- Location: LABCELL_X35_Y63_N33
\Selector11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector11~1_combout\ = ( paddle_one_y(4) & ( (!\state.DRAW_BOTTOM_LOOP~q\ & !\state.DRAW_BOTTOM_ENTER~q\) ) ) # ( !paddle_one_y(4) & ( (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (!\state.DRAW_BOTTOM_LOOP~q\ & !\state.DRAW_BOTTOM_ENTER~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datac => \ALT_INV_state.DRAW_BOTTOM_LOOP~q\,
	datad => \ALT_INV_state.DRAW_BOTTOM_ENTER~q\,
	dataf => ALT_INV_paddle_one_y(4),
	combout => \Selector11~1_combout\);

-- Location: LABCELL_X35_Y63_N18
\Selector11~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector11~2_combout\ = ( \puck.y\(4) & ( (\Selector11~1_combout\ & ((!\state.ERASE_PADDLE_TWO_ENTER~q\) # (paddle_two_y(4)))) ) ) # ( !\puck.y\(4) & ( (\draw~0_combout\ & (\Selector11~1_combout\ & ((!\state.ERASE_PADDLE_TWO_ENTER~q\) # 
-- (paddle_two_y(4))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000011000000100000001100001010000011110000101000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_Selector11~1_combout\,
	datad => ALT_INV_paddle_two_y(4),
	dataf => \ALT_INV_puck.y\(4),
	combout => \Selector11~2_combout\);

-- Location: LABCELL_X36_Y63_N48
\Selector11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector11~0_combout\ = ( \paddle_one_y~2_combout\ & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & !paddle_one_y(4)) ) ) ) # ( !\paddle_one_y~2_combout\ & ( \KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & \Add4~9_sumout\) ) ) ) # ( 
-- \paddle_one_y~2_combout\ & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & !paddle_one_y(4)) ) ) ) # ( !\paddle_one_y~2_combout\ & ( !\KEY[2]~input_o\ & ( (\state.DRAW_PADDLE_ONE_ENTER~q\ & \Add3~9_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101010101010000000000010001000100010101010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ONE_ENTER~q\,
	datab => \ALT_INV_Add4~9_sumout\,
	datac => \ALT_INV_Add3~9_sumout\,
	datad => ALT_INV_paddle_one_y(4),
	datae => \ALT_INV_paddle_one_y~2_combout\,
	dataf => \ALT_INV_KEY[2]~input_o\,
	combout => \Selector11~0_combout\);

-- Location: LABCELL_X36_Y62_N57
\paddle_two_y~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_two_y~3_combout\ = ( \Add5~9_sumout\ & ( (!\paddle_two_y~6_combout\ & ((!\KEY[0]~input_o\) # ((\Add6~9_sumout\)))) # (\paddle_two_y~6_combout\ & (((!paddle_two_y(4))))) ) ) # ( !\Add5~9_sumout\ & ( (!\paddle_two_y~6_combout\ & (\KEY[0]~input_o\ & 
-- (\Add6~9_sumout\))) # (\paddle_two_y~6_combout\ & (((!paddle_two_y(4))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011100000010010101110000001011011111100010101101111110001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_paddle_two_y~6_combout\,
	datab => \ALT_INV_KEY[0]~input_o\,
	datac => \ALT_INV_Add6~9_sumout\,
	datad => ALT_INV_paddle_two_y(4),
	dataf => \ALT_INV_Add5~9_sumout\,
	combout => \paddle_two_y~3_combout\);

-- Location: LABCELL_X36_Y65_N12
\Selector11~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector11~3_combout\ = ( \Add0~9_sumout\ & ( \paddle_two_y~3_combout\ & ( ((!\Selector11~2_combout\) # ((!\WideOr16~0_combout\) # (\Selector11~0_combout\))) # (\state.DRAW_PADDLE_TWO_ENTER~q\) ) ) ) # ( !\Add0~9_sumout\ & ( \paddle_two_y~3_combout\ & ( 
-- ((!\Selector11~2_combout\) # (\Selector11~0_combout\)) # (\state.DRAW_PADDLE_TWO_ENTER~q\) ) ) ) # ( \Add0~9_sumout\ & ( !\paddle_two_y~3_combout\ & ( (!\Selector11~2_combout\) # ((!\WideOr16~0_combout\) # (\Selector11~0_combout\)) ) ) ) # ( 
-- !\Add0~9_sumout\ & ( !\paddle_two_y~3_combout\ & ( (!\Selector11~2_combout\) # (\Selector11~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011111111111111001111111111011101111111111111110111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_TWO_ENTER~q\,
	datab => \ALT_INV_Selector11~2_combout\,
	datac => \ALT_INV_WideOr16~0_combout\,
	datad => \ALT_INV_Selector11~0_combout\,
	datae => \ALT_INV_Add0~9_sumout\,
	dataf => \ALT_INV_paddle_two_y~3_combout\,
	combout => \Selector11~3_combout\);

-- Location: FF_X36_Y65_N14
\draw.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector11~3_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.y[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(4));

-- Location: MLABCELL_X39_Y65_N3
\vga_u0|LessThan3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|LessThan3~0_combout\ = ( \draw.y\(6) & ( (\draw.y\(4) & (\draw.y\(3) & \draw.y\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(4),
	datac => \ALT_INV_draw.y\(3),
	datad => \ALT_INV_draw.y\(5),
	dataf => \ALT_INV_draw.y\(6),
	combout => \vga_u0|LessThan3~0_combout\);

-- Location: LABCELL_X35_Y63_N42
\Selector64~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector64~1_combout\ = (!\state.ERASE_PADDLE_ONE_ENTER~q\ & (\draw~0_combout\ & (!\state.ERASE_PADDLE_TWO_ENTER~q\ & \state.INIT~q\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000001000000000000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PADDLE_ONE_ENTER~q\,
	datab => \ALT_INV_draw~0_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_TWO_ENTER~q\,
	datad => \ALT_INV_state.INIT~q\,
	combout => \Selector64~1_combout\);

-- Location: LABCELL_X35_Y65_N15
\Selector64~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector64~0_combout\ = ( !\state.IDLE~q\ & ( (!\state.DRAW_PADDLE_ONE_LOOP~q\ & !\state.DRAW_PADDLE_TWO_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.DRAW_PADDLE_ONE_LOOP~q\,
	datad => \ALT_INV_state.DRAW_PADDLE_TWO_LOOP~q\,
	dataf => \ALT_INV_state.IDLE~q\,
	combout => \Selector64~0_combout\);

-- Location: LABCELL_X35_Y65_N0
\Selector64~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector64~2_combout\ = ( \Selector64~0_combout\ & ( (\Selector64~1_combout\ & \plot~q\) ) ) # ( !\Selector64~0_combout\ & ( (\Selector64~1_combout\ & (((!\Selector75~0_combout\ & !\Selector79~0_combout\)) # (\plot~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001111000010000000111100000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector75~0_combout\,
	datab => \ALT_INV_Selector79~0_combout\,
	datac => \ALT_INV_Selector64~1_combout\,
	datad => \ALT_INV_plot~q\,
	dataf => \ALT_INV_Selector64~0_combout\,
	combout => \Selector64~2_combout\);

-- Location: FF_X35_Y65_N2
plot : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector64~2_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \plot~q\);

-- Location: FF_X37_Y66_N34
\draw.x[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector1~0_combout\,
	clrn => \KEY[8]~input_o\,
	ena => \draw.x[5]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x[6]~DUPLICATE_q\);

-- Location: MLABCELL_X39_Y65_N6
\vga_u0|writeEn~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|writeEn~0_combout\ = ( \draw.x\(5) & ( (!\plot~q\ & !\draw.x\(7)) ) ) # ( !\draw.x\(5) & ( (!\plot~q\ & ((!\draw.x\(7)) # (!\draw.x[6]~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011000000110011001100000011000000110000001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_plot~q\,
	datac => \ALT_INV_draw.x\(7),
	datad => \ALT_INV_draw.x[6]~DUPLICATE_q\,
	dataf => \ALT_INV_draw.x\(5),
	combout => \vga_u0|writeEn~0_combout\);

-- Location: MLABCELL_X39_Y65_N30
\vga_u0|user_input_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~9_sumout\ = SUM(( !\draw.y\(0) $ (!\draw.x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~10\ = CARRY(( !\draw.y\(0) $ (!\draw.x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~11\ = SHARE((\draw.y\(0) & \draw.x\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y\(0),
	datac => \ALT_INV_draw.x\(5),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|user_input_translator|Add1~9_sumout\,
	cout => \vga_u0|user_input_translator|Add1~10\,
	shareout => \vga_u0|user_input_translator|Add1~11\);

-- Location: MLABCELL_X39_Y65_N33
\vga_u0|user_input_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~13_sumout\ = SUM(( !\draw.y\(1) $ (!\draw.x[6]~DUPLICATE_q\) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~14\ = CARRY(( !\draw.y\(1) $ (!\draw.x[6]~DUPLICATE_q\) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~15\ = SHARE((\draw.y\(1) & \draw.x[6]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(1),
	datac => \ALT_INV_draw.x[6]~DUPLICATE_q\,
	cin => \vga_u0|user_input_translator|Add1~10\,
	sharein => \vga_u0|user_input_translator|Add1~11\,
	sumout => \vga_u0|user_input_translator|Add1~13_sumout\,
	cout => \vga_u0|user_input_translator|Add1~14\,
	shareout => \vga_u0|user_input_translator|Add1~15\);

-- Location: MLABCELL_X39_Y65_N36
\vga_u0|user_input_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~17_sumout\ = SUM(( !\draw.y\(0) $ (!\draw.y\(2) $ (\draw.x\(7))) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~18\ = CARRY(( !\draw.y\(0) $ (!\draw.y\(2) $ (\draw.x\(7))) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~19\ = SHARE((!\draw.y\(0) & (\draw.y\(2) & \draw.x\(7))) # (\draw.y\(0) & ((\draw.x\(7)) # (\draw.y\(2)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110011111100000000000000000011110011000011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y\(0),
	datac => \ALT_INV_draw.y\(2),
	datad => \ALT_INV_draw.x\(7),
	cin => \vga_u0|user_input_translator|Add1~14\,
	sharein => \vga_u0|user_input_translator|Add1~15\,
	sumout => \vga_u0|user_input_translator|Add1~17_sumout\,
	cout => \vga_u0|user_input_translator|Add1~18\,
	shareout => \vga_u0|user_input_translator|Add1~19\);

-- Location: MLABCELL_X39_Y65_N39
\vga_u0|user_input_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~21_sumout\ = SUM(( !\draw.y\(1) $ (!\draw.y\(3)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~22\ = CARRY(( !\draw.y\(1) $ (!\draw.y\(3)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~23\ = SHARE((\draw.y\(1) & \draw.y\(3)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(1),
	datac => \ALT_INV_draw.y\(3),
	cin => \vga_u0|user_input_translator|Add1~18\,
	sharein => \vga_u0|user_input_translator|Add1~19\,
	sumout => \vga_u0|user_input_translator|Add1~21_sumout\,
	cout => \vga_u0|user_input_translator|Add1~22\,
	shareout => \vga_u0|user_input_translator|Add1~23\);

-- Location: MLABCELL_X39_Y65_N42
\vga_u0|user_input_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~25_sumout\ = SUM(( !\draw.y\(2) $ (!\draw.y\(4)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~26\ = CARRY(( !\draw.y\(2) $ (!\draw.y\(4)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~27\ = SHARE((\draw.y\(2) & \draw.y\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datac => \ALT_INV_draw.y\(4),
	cin => \vga_u0|user_input_translator|Add1~22\,
	sharein => \vga_u0|user_input_translator|Add1~23\,
	sumout => \vga_u0|user_input_translator|Add1~25_sumout\,
	cout => \vga_u0|user_input_translator|Add1~26\,
	shareout => \vga_u0|user_input_translator|Add1~27\);

-- Location: MLABCELL_X39_Y65_N45
\vga_u0|user_input_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~29_sumout\ = SUM(( !\draw.y\(3) $ (!\draw.y\(5)) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~30\ = CARRY(( !\draw.y\(3) $ (!\draw.y\(5)) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~31\ = SHARE((\draw.y\(3) & \draw.y\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y\(3),
	datac => \ALT_INV_draw.y\(5),
	cin => \vga_u0|user_input_translator|Add1~26\,
	sharein => \vga_u0|user_input_translator|Add1~27\,
	sumout => \vga_u0|user_input_translator|Add1~29_sumout\,
	cout => \vga_u0|user_input_translator|Add1~30\,
	shareout => \vga_u0|user_input_translator|Add1~31\);

-- Location: MLABCELL_X39_Y65_N48
\vga_u0|user_input_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~33_sumout\ = SUM(( !\draw.y\(4) $ (!\draw.y\(6)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~34\ = CARRY(( !\draw.y\(4) $ (!\draw.y\(6)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~35\ = SHARE((\draw.y\(4) & \draw.y\(6)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(4),
	datac => \ALT_INV_draw.y\(6),
	cin => \vga_u0|user_input_translator|Add1~30\,
	sharein => \vga_u0|user_input_translator|Add1~31\,
	sumout => \vga_u0|user_input_translator|Add1~33_sumout\,
	cout => \vga_u0|user_input_translator|Add1~34\,
	shareout => \vga_u0|user_input_translator|Add1~35\);

-- Location: MLABCELL_X39_Y65_N51
\vga_u0|user_input_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~37_sumout\ = SUM(( \draw.y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~38\ = CARRY(( \draw.y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(5),
	cin => \vga_u0|user_input_translator|Add1~34\,
	sharein => \vga_u0|user_input_translator|Add1~35\,
	sumout => \vga_u0|user_input_translator|Add1~37_sumout\,
	cout => \vga_u0|user_input_translator|Add1~38\,
	shareout => \vga_u0|user_input_translator|Add1~39\);

-- Location: MLABCELL_X39_Y65_N54
\vga_u0|user_input_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~5_sumout\ = SUM(( \draw.y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~6\ = CARRY(( \draw.y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~7\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(6),
	cin => \vga_u0|user_input_translator|Add1~38\,
	sharein => \vga_u0|user_input_translator|Add1~39\,
	sumout => \vga_u0|user_input_translator|Add1~5_sumout\,
	cout => \vga_u0|user_input_translator|Add1~6\,
	shareout => \vga_u0|user_input_translator|Add1~7\);

-- Location: MLABCELL_X39_Y65_N57
\vga_u0|user_input_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~1_sumout\ = SUM(( GND ) + ( \vga_u0|user_input_translator|Add1~7\ ) + ( \vga_u0|user_input_translator|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|user_input_translator|Add1~6\,
	sharein => \vga_u0|user_input_translator|Add1~7\,
	sumout => \vga_u0|user_input_translator|Add1~1_sumout\);

-- Location: MLABCELL_X39_Y65_N9
\vga_u0|VideoMemory|auto_generated|decode2|w_anode118w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2) = ( !\vga_u0|user_input_translator|Add1~1_sumout\ & ( (!\vga_u0|LessThan3~0_combout\ & (\vga_u0|writeEn~0_combout\ & \vga_u0|user_input_translator|Add1~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|ALT_INV_LessThan3~0_combout\,
	datac => \vga_u0|ALT_INV_writeEn~0_combout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2));

-- Location: LABCELL_X37_Y67_N27
\vga_u0|controller|controller_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~5_sumout\ = SUM(( GND ) + ( \vga_u0|controller|controller_translator|Add1~3\ ) + ( \vga_u0|controller|controller_translator|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|controller|controller_translator|Add1~2\,
	sharein => \vga_u0|controller|controller_translator|Add1~3\,
	sumout => \vga_u0|controller|controller_translator|Add1~5_sumout\);

-- Location: LABCELL_X37_Y67_N51
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2) = ( \vga_u0|controller|controller_translator|Add1~1_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2));

-- Location: LABCELL_X35_Y67_N18
\Selector63~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector63~1_combout\ = ( colour(1) & ( \WideOr16~2_combout\ & ( (!\WideOr16~0_combout\) # ((!\Selector63~0_combout\) # (\state.IDLE~DUPLICATE_q\)) ) ) ) # ( !colour(1) & ( \WideOr16~2_combout\ & ( !\Selector63~0_combout\ ) ) ) # ( colour(1) & ( 
-- !\WideOr16~2_combout\ ) ) # ( !colour(1) & ( !\WideOr16~2_combout\ & ( !\Selector63~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100111111111111111111001100110011001110111111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_WideOr16~0_combout\,
	datab => \ALT_INV_Selector63~0_combout\,
	datac => \ALT_INV_state.IDLE~DUPLICATE_q\,
	datae => ALT_INV_colour(1),
	dataf => \ALT_INV_WideOr16~2_combout\,
	combout => \Selector63~1_combout\);

-- Location: FF_X35_Y67_N20
\colour[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector63~1_combout\,
	clrn => \KEY[8]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => colour(1));

-- Location: M10K_X26_Y67_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a5\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\);

-- Location: FF_X37_Y67_N29
\vga_u0|VideoMemory|auto_generated|address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~5_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1));

-- Location: FF_X37_Y67_N32
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1));

-- Location: MLABCELL_X39_Y65_N0
\vga_u0|VideoMemory|auto_generated|decode2|w_anode105w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2) = ( !\vga_u0|user_input_translator|Add1~1_sumout\ & ( (\vga_u0|writeEn~0_combout\ & (!\vga_u0|LessThan3~0_combout\ & !\vga_u0|user_input_translator|Add1~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|ALT_INV_writeEn~0_combout\,
	datac => \vga_u0|ALT_INV_LessThan3~0_combout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2));

-- Location: LABCELL_X37_Y67_N48
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2) = ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2));

-- Location: M10K_X26_Y66_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a2\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\);

-- Location: MLABCELL_X39_Y65_N12
\vga_u0|VideoMemory|auto_generated|decode2|w_anode126w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2) = ( \vga_u0|user_input_translator|Add1~1_sumout\ & ( (\vga_u0|writeEn~0_combout\ & (!\vga_u0|LessThan3~0_combout\ & !\vga_u0|user_input_translator|Add1~5_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|ALT_INV_writeEn~0_combout\,
	datac => \vga_u0|ALT_INV_LessThan3~0_combout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2));

-- Location: LABCELL_X37_Y66_N24
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2) = ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ & ( \vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	dataf => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2));

-- Location: M10K_X41_Y66_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a7\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\);

-- Location: LABCELL_X37_Y67_N42
\vga_u0|controller|VGA_R[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_R[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & ( \vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)) # ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\)))) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & ( \vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|controller|on_screen~1_combout\ & (((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & 
-- \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a8\ & 
-- ( (\vga_u0|controller|on_screen~1_combout\ & (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\)))) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ & ( !\vga_u0|VideoMemory|auto_generated|ram_block1a8\ & ( (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & (\vga_u0|controller|on_screen~1_combout\ & 
-- (\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000000001000110000000000000001001100110010001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datab => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\,
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\,
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\,
	combout => \vga_u0|controller|VGA_R[0]~0_combout\);

-- Location: M10K_X26_Y68_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a1\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\);

-- Location: M10K_X41_Y67_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a4\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\);

-- Location: LABCELL_X37_Y67_N30
\vga_u0|controller|VGA_G[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_G[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & 
-- \vga_u0|controller|on_screen~1_combout\) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & \vga_u0|controller|on_screen~1_combout\) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ & (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & \vga_u0|controller|on_screen~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000101010100000000001111110000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\,
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datad => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\,
	combout => \vga_u0|controller|VGA_G[0]~0_combout\);

-- Location: MLABCELL_X39_Y67_N30
\~GND\ : cyclonev_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: M10K_X38_Y67_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a3\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y66_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a6\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y68_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a0\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LABCELL_X37_Y67_N36
\vga_u0|controller|VGA_B[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_B[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & 
-- ((\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\)) # 
-- (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & (\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ & \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000100010000001100110011000000110001000100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\,
	datab => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\,
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	combout => \vga_u0|controller|VGA_B[0]~0_combout\);

-- Location: LABCELL_X36_Y67_N42
\vga_u0|controller|VGA_HS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (((\vga_u0|controller|xCounter\(0) & \vga_u0|controller|xCounter\(1))) # (\vga_u0|controller|xCounter\(3))) # (\vga_u0|controller|xCounter\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110111111111110011011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(0),
	datab => \vga_u0|controller|ALT_INV_xCounter\(2),
	datac => \vga_u0|controller|ALT_INV_xCounter\(1),
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|VGA_HS1~0_combout\);

-- Location: LABCELL_X36_Y67_N54
\vga_u0|controller|VGA_HS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~1_combout\ = ( \vga_u0|controller|xCounter\(7) & ( \vga_u0|controller|xCounter\(6) & ( ((!\vga_u0|controller|xCounter\(9)) # ((\vga_u0|controller|VGA_HS1~0_combout\ & \vga_u0|controller|xCounter\(5)))) # 
-- (\vga_u0|controller|xCounter\(8)) ) ) ) # ( !\vga_u0|controller|xCounter\(7) & ( \vga_u0|controller|xCounter\(6) ) ) # ( \vga_u0|controller|xCounter\(7) & ( !\vga_u0|controller|xCounter\(6) & ( ((!\vga_u0|controller|xCounter\(9)) # 
-- ((!\vga_u0|controller|VGA_HS1~0_combout\ & !\vga_u0|controller|xCounter\(5)))) # (\vga_u0|controller|xCounter\(8)) ) ) ) # ( !\vga_u0|controller|xCounter\(7) & ( !\vga_u0|controller|xCounter\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111011101110111111111111111111101110111011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	datae => \vga_u0|controller|ALT_INV_xCounter\(7),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(6),
	combout => \vga_u0|controller|VGA_HS1~1_combout\);

-- Location: FF_X36_Y67_N55
\vga_u0|controller|VGA_HS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_HS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS1~q\);

-- Location: FF_X36_Y69_N31
\vga_u0|controller|VGA_HS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_HS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS~q\);

-- Location: LABCELL_X35_Y67_N24
\vga_u0|controller|VGA_VS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~0_combout\ = ( !\vga_u0|controller|yCounter\(4) & ( \vga_u0|controller|yCounter[2]~DUPLICATE_q\ & ( (!\vga_u0|controller|yCounter\(9) & (\vga_u0|controller|yCounter\(3) & (!\vga_u0|controller|yCounter\(1) $ 
-- (!\vga_u0|controller|yCounter\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010010000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(1),
	datab => \vga_u0|controller|ALT_INV_yCounter\(9),
	datac => \vga_u0|controller|ALT_INV_yCounter\(0),
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	datae => \vga_u0|controller|ALT_INV_yCounter\(4),
	dataf => \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\,
	combout => \vga_u0|controller|VGA_VS1~0_combout\);

-- Location: LABCELL_X36_Y69_N6
\vga_u0|controller|VGA_VS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~1_combout\ = ( \vga_u0|controller|yCounter\(8) & ( (!\vga_u0|controller|yCounter\(5)) # ((!\vga_u0|controller|yCounter\(7)) # ((!\vga_u0|controller|VGA_VS1~0_combout\) # (!\vga_u0|controller|yCounter[6]~DUPLICATE_q\))) ) ) # ( 
-- !\vga_u0|controller|yCounter\(8) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111011111111111111111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(5),
	datab => \vga_u0|controller|ALT_INV_yCounter\(7),
	datac => \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datae => \vga_u0|controller|ALT_INV_yCounter\(8),
	combout => \vga_u0|controller|VGA_VS1~1_combout\);

-- Location: FF_X36_Y69_N7
\vga_u0|controller|VGA_VS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_VS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS1~q\);

-- Location: FF_X36_Y69_N11
\vga_u0|controller|VGA_VS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_VS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS~q\);

-- Location: LABCELL_X36_Y67_N48
\vga_u0|controller|VGA_BLANK1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_BLANK1~0_combout\ = ( \vga_u0|controller|LessThan7~0_combout\ & ( (!\vga_u0|controller|xCounter\(9)) # ((!\vga_u0|controller|xCounter\(8) & !\vga_u0|controller|xCounter[7]~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011101100111011001110110011101100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\,
	dataf => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	combout => \vga_u0|controller|VGA_BLANK1~0_combout\);

-- Location: FF_X36_Y67_N49
\vga_u0|controller|VGA_BLANK1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_BLANK1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK1~q\);

-- Location: FF_X42_Y78_N1
\vga_u0|controller|VGA_BLANK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_BLANK1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK~q\);

-- Location: IOIBUF_X89_Y23_N38
\KEY[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(4),
	o => \KEY[4]~input_o\);

-- Location: IOIBUF_X8_Y81_N18
\KEY[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(5),
	o => \KEY[5]~input_o\);

-- Location: IOIBUF_X56_Y0_N18
\KEY[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(6),
	o => \KEY[6]~input_o\);

-- Location: IOIBUF_X58_Y0_N58
\KEY[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(7),
	o => \KEY[7]~input_o\);


pll_reconfig_inst_tasks : altera_pll_reconfig_tasks
-- pragma translate_off
GENERIC MAP (
		number_of_fplls => 1);
-- pragma translate_on
END structure;


