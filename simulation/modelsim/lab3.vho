-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.1 Build 190 01/19/2015 SJ Full Version"

-- DATE "10/11/2016 19:09:27"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	lab3_task4 IS
    PORT (
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(3 DOWNTO 0);
	VGA_R : OUT std_logic_vector(9 DOWNTO 0);
	VGA_G : OUT std_logic_vector(9 DOWNTO 0);
	VGA_B : OUT std_logic_vector(9 DOWNTO 0);
	VGA_HS : OUT std_logic;
	VGA_VS : OUT std_logic;
	VGA_BLANK : OUT std_logic;
	VGA_SYNC : OUT std_logic;
	VGA_CLK : OUT std_logic
	);
END lab3_task4;

-- Design Ports Information
-- KEY[2]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[2]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[3]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[5]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[6]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[7]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[8]	=>  Location: PIN_AD30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_R[9]	=>  Location: PIN_AC30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[0]	=>  Location: PIN_J9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[1]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[3]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[4]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[5]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[6]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[7]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[8]	=>  Location: PIN_H7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_G[9]	=>  Location: PIN_C4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[0]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[1]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[2]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[3]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[4]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[5]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[7]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[8]	=>  Location: PIN_C5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_B[9]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_HS	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_VS	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_BLANK	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_SYNC	=>  Location: PIN_AK3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- VGA_CLK	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF lab3_task4 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_VGA_R : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_G : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_B : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL ww_VGA_BLANK : std_logic;
SIGNAL ww_VGA_SYNC : std_logic;
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \Add2~125_sumout\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \KEY[3]~inputCLKENA0_outclk\ : std_logic;
SIGNAL \WideOr5~0_combout\ : std_logic;
SIGNAL \paddle_width~0_combout\ : std_logic;
SIGNAL \Selector108~0_combout\ : std_logic;
SIGNAL \paddle_width~1_combout\ : std_logic;
SIGNAL \Selector107~0_combout\ : std_logic;
SIGNAL \paddle_width[3]~DUPLICATE_q\ : std_logic;
SIGNAL \Add13~14\ : std_logic;
SIGNAL \Add13~18\ : std_logic;
SIGNAL \Add13~1_sumout\ : std_logic;
SIGNAL \Add7~22\ : std_logic;
SIGNAL \Add7~25_sumout\ : std_logic;
SIGNAL \Add8~18\ : std_logic;
SIGNAL \Add8~22\ : std_logic;
SIGNAL \Add8~25_sumout\ : std_logic;
SIGNAL \paddle_x~4_combout\ : std_logic;
SIGNAL \Selector19~0_combout\ : std_logic;
SIGNAL \Add8~26\ : std_logic;
SIGNAL \Add8~9_sumout\ : std_logic;
SIGNAL \Add7~26\ : std_logic;
SIGNAL \Add7~9_sumout\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \Selector2~4_combout\ : std_logic;
SIGNAL \Add13~2\ : std_logic;
SIGNAL \Add13~6\ : std_logic;
SIGNAL \Add13~9_sumout\ : std_logic;
SIGNAL \Add13~10\ : std_logic;
SIGNAL \Add13~21_sumout\ : std_logic;
SIGNAL \Add18~17_sumout\ : std_logic;
SIGNAL \Add12~29_sumout\ : std_logic;
SIGNAL \Selector46~0_combout\ : std_logic;
SIGNAL \Add12~30\ : std_logic;
SIGNAL \Add12~13_sumout\ : std_logic;
SIGNAL \Selector45~0_combout\ : std_logic;
SIGNAL \Selector29~0_combout\ : std_logic;
SIGNAL \puck_velocity_one.x[0]~DUPLICATE_q\ : std_logic;
SIGNAL \Add9~29_sumout\ : std_logic;
SIGNAL \Selector30~0_combout\ : std_logic;
SIGNAL \Add9~30\ : std_logic;
SIGNAL \Add9~13_sumout\ : std_logic;
SIGNAL \Selector44~0_combout\ : std_logic;
SIGNAL \Add12~14\ : std_logic;
SIGNAL \Add12~9_sumout\ : std_logic;
SIGNAL \puck_velocity_one.x[2]~DUPLICATE_q\ : std_logic;
SIGNAL \Selector28~0_combout\ : std_logic;
SIGNAL \Add9~14\ : std_logic;
SIGNAL \Add9~9_sumout\ : std_logic;
SIGNAL \Equal7~1_combout\ : std_logic;
SIGNAL \Selector40~0_combout\ : std_logic;
SIGNAL \Add12~10\ : std_logic;
SIGNAL \Add12~6\ : std_logic;
SIGNAL \Add12~1_sumout\ : std_logic;
SIGNAL \Selector42~0_combout\ : std_logic;
SIGNAL \Add12~2\ : std_logic;
SIGNAL \Add12~25_sumout\ : std_logic;
SIGNAL \Selector41~0_combout\ : std_logic;
SIGNAL \Add12~26\ : std_logic;
SIGNAL \Add12~21_sumout\ : std_logic;
SIGNAL \puck_velocity_one.x[6]~DUPLICATE_q\ : std_logic;
SIGNAL \Selector24~0_combout\ : std_logic;
SIGNAL \puck_velocity_one.x[5]~DUPLICATE_q\ : std_logic;
SIGNAL \Add9~5_wirecell_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \WideOr6~0_combout\ : std_logic;
SIGNAL \Add9~10\ : std_logic;
SIGNAL \Add9~6\ : std_logic;
SIGNAL \Add9~1_sumout\ : std_logic;
SIGNAL \Selector26~0_combout\ : std_logic;
SIGNAL \Add9~2\ : std_logic;
SIGNAL \Add9~25_sumout\ : std_logic;
SIGNAL \Add9~25_wirecell_combout\ : std_logic;
SIGNAL \Add9~26\ : std_logic;
SIGNAL \Add9~21_sumout\ : std_logic;
SIGNAL \Equal7~0_combout\ : std_logic;
SIGNAL \Add12~22\ : std_logic;
SIGNAL \Add12~17_sumout\ : std_logic;
SIGNAL \Selector39~0_combout\ : std_logic;
SIGNAL \Selector23~0_combout\ : std_logic;
SIGNAL \Add9~22\ : std_logic;
SIGNAL \Add9~17_sumout\ : std_logic;
SIGNAL \Equal6~1_combout\ : std_logic;
SIGNAL \Equal6~0_combout\ : std_logic;
SIGNAL \Add12~5_sumout\ : std_logic;
SIGNAL \Selector43~0_combout\ : std_logic;
SIGNAL \Add9~5_sumout\ : std_logic;
SIGNAL \always0~0_combout\ : std_logic;
SIGNAL \always0~1_combout\ : std_logic;
SIGNAL \always0~2_combout\ : std_logic;
SIGNAL \Add13~17_sumout\ : std_logic;
SIGNAL \Add13~5_sumout\ : std_logic;
SIGNAL \always0~5_combout\ : std_logic;
SIGNAL \always0~3_combout\ : std_logic;
SIGNAL \LessThan5~0_combout\ : std_logic;
SIGNAL \always0~4_combout\ : std_logic;
SIGNAL \always0~6_combout\ : std_logic;
SIGNAL \always0~7_combout\ : std_logic;
SIGNAL \LessThan4~0_combout\ : std_logic;
SIGNAL \always0~8_combout\ : std_logic;
SIGNAL \Add10~13_wirecell_combout\ : std_logic;
SIGNAL \Add11~1_sumout\ : std_logic;
SIGNAL \Add14~1_sumout\ : std_logic;
SIGNAL \Selector54~0_combout\ : std_logic;
SIGNAL \Add11~2\ : std_logic;
SIGNAL \Add11~6\ : std_logic;
SIGNAL \Add11~9_sumout\ : std_logic;
SIGNAL \Add14~2\ : std_logic;
SIGNAL \Add14~6\ : std_logic;
SIGNAL \Add14~9_sumout\ : std_logic;
SIGNAL \puck_velocity_one~2_combout\ : std_logic;
SIGNAL \Selector52~0_combout\ : std_logic;
SIGNAL \Add11~10\ : std_logic;
SIGNAL \Add11~13_sumout\ : std_logic;
SIGNAL \Add14~10\ : std_logic;
SIGNAL \Add14~13_sumout\ : std_logic;
SIGNAL \puck_velocity_one~3_combout\ : std_logic;
SIGNAL \Selector51~0_combout\ : std_logic;
SIGNAL \Selector37~0_combout\ : std_logic;
SIGNAL \Add10~1_sumout\ : std_logic;
SIGNAL \Selector38~0_combout\ : std_logic;
SIGNAL \Add10~2\ : std_logic;
SIGNAL \Add10~6\ : std_logic;
SIGNAL \Add10~9_sumout\ : std_logic;
SIGNAL \Add10~9_wirecell_combout\ : std_logic;
SIGNAL \Add10~10\ : std_logic;
SIGNAL \Add10~13_sumout\ : std_logic;
SIGNAL \Selector48~0_combout\ : std_logic;
SIGNAL \Add11~14\ : std_logic;
SIGNAL \Add11~21_sumout\ : std_logic;
SIGNAL \Add14~14\ : std_logic;
SIGNAL \Add14~21_sumout\ : std_logic;
SIGNAL \puck_velocity_one~5_combout\ : std_logic;
SIGNAL \Selector50~0_combout\ : std_logic;
SIGNAL \Add11~22\ : std_logic;
SIGNAL \Add11~26\ : std_logic;
SIGNAL \Add11~29_sumout\ : std_logic;
SIGNAL \Add11~25_sumout\ : std_logic;
SIGNAL \Add14~22\ : std_logic;
SIGNAL \Add14~26\ : std_logic;
SIGNAL \Add14~29_sumout\ : std_logic;
SIGNAL \puck_velocity_one~7_combout\ : std_logic;
SIGNAL \puck_velocity_one.y[6]~DUPLICATE_q\ : std_logic;
SIGNAL \Selector32~0_combout\ : std_logic;
SIGNAL \Add10~25_wirecell_combout\ : std_logic;
SIGNAL \Add10~14\ : std_logic;
SIGNAL \Add10~21_sumout\ : std_logic;
SIGNAL \Add10~21_wirecell_combout\ : std_logic;
SIGNAL \Add10~22\ : std_logic;
SIGNAL \Add10~26\ : std_logic;
SIGNAL \Add10~29_sumout\ : std_logic;
SIGNAL \Add11~30\ : std_logic;
SIGNAL \Add11~17_sumout\ : std_logic;
SIGNAL \Add14~30\ : std_logic;
SIGNAL \Add14~17_sumout\ : std_logic;
SIGNAL \puck_velocity_one~4_combout\ : std_logic;
SIGNAL \Selector47~0_combout\ : std_logic;
SIGNAL \Selector31~0_combout\ : std_logic;
SIGNAL \Add10~30\ : std_logic;
SIGNAL \Add10~17_sumout\ : std_logic;
SIGNAL \Equal5~1_combout\ : std_logic;
SIGNAL \Equal5~2_combout\ : std_logic;
SIGNAL \Add11~5_sumout\ : std_logic;
SIGNAL \Add14~5_sumout\ : std_logic;
SIGNAL \puck_velocity_one~1_combout\ : std_logic;
SIGNAL \Selector53~0_combout\ : std_logic;
SIGNAL \Add10~5_sumout\ : std_logic;
SIGNAL \Equal5~0_combout\ : std_logic;
SIGNAL \puck_velocity_one~0_combout\ : std_logic;
SIGNAL \Add14~25_sumout\ : std_logic;
SIGNAL \puck_velocity_one~6_combout\ : std_logic;
SIGNAL \Selector49~0_combout\ : std_logic;
SIGNAL \Add10~25_sumout\ : std_logic;
SIGNAL \Equal8~0_combout\ : std_logic;
SIGNAL \Selector90~0_combout\ : std_logic;
SIGNAL \state.DRAW_PUCK_ONE~q\ : std_logic;
SIGNAL \state.ERASE_PUCK_TWO~q\ : std_logic;
SIGNAL \Selector78~0_combout\ : std_logic;
SIGNAL \Add18~18\ : std_logic;
SIGNAL \Add18~13_sumout\ : std_logic;
SIGNAL \Selector77~0_combout\ : std_logic;
SIGNAL \Add18~14\ : std_logic;
SIGNAL \Add18~9_sumout\ : std_logic;
SIGNAL \Selector76~0_combout\ : std_logic;
SIGNAL \Add18~10\ : std_logic;
SIGNAL \Add18~5_sumout\ : std_logic;
SIGNAL \Selector75~0_combout\ : std_logic;
SIGNAL \Add18~6\ : std_logic;
SIGNAL \Add18~1_sumout\ : std_logic;
SIGNAL \Selector74~0_combout\ : std_logic;
SIGNAL \Add15~1_wirecell_combout\ : std_logic;
SIGNAL \state.INIT~q\ : std_logic;
SIGNAL \WideOr7~0_combout\ : std_logic;
SIGNAL \puck_two.x[4]~DUPLICATE_q\ : std_logic;
SIGNAL \Add15~17_sumout\ : std_logic;
SIGNAL \Selector62~0_combout\ : std_logic;
SIGNAL \Add15~18\ : std_logic;
SIGNAL \Add15~13_sumout\ : std_logic;
SIGNAL \Selector61~0_combout\ : std_logic;
SIGNAL \Add15~14\ : std_logic;
SIGNAL \Add15~9_sumout\ : std_logic;
SIGNAL \Selector60~0_combout\ : std_logic;
SIGNAL \Add15~10\ : std_logic;
SIGNAL \Add15~5_sumout\ : std_logic;
SIGNAL \Add15~5_wirecell_combout\ : std_logic;
SIGNAL \Add15~6\ : std_logic;
SIGNAL \Add15~1_sumout\ : std_logic;
SIGNAL \Add15~29_wirecell_combout\ : std_logic;
SIGNAL \Add18~2\ : std_logic;
SIGNAL \Add18~29_sumout\ : std_logic;
SIGNAL \Selector73~0_combout\ : std_logic;
SIGNAL \Add15~2\ : std_logic;
SIGNAL \Add15~29_sumout\ : std_logic;
SIGNAL \Equal11~1_combout\ : std_logic;
SIGNAL \Equal11~0_combout\ : std_logic;
SIGNAL \Add18~30\ : std_logic;
SIGNAL \Add18~26\ : std_logic;
SIGNAL \Add18~21_sumout\ : std_logic;
SIGNAL \Selector71~0_combout\ : std_logic;
SIGNAL \Selector55~0_combout\ : std_logic;
SIGNAL \Add15~30\ : std_logic;
SIGNAL \Add15~26\ : std_logic;
SIGNAL \Add15~21_sumout\ : std_logic;
SIGNAL \Equal10~1_combout\ : std_logic;
SIGNAL \Equal10~0_combout\ : std_logic;
SIGNAL \Add18~25_sumout\ : std_logic;
SIGNAL \Selector72~0_combout\ : std_logic;
SIGNAL \Add15~25_sumout\ : std_logic;
SIGNAL \Add15~25_wirecell_combout\ : std_logic;
SIGNAL \state.DRAW_PUCK_TWO~q\ : std_logic;
SIGNAL \draw.y[3]~0_combout\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \Add8~13_sumout\ : std_logic;
SIGNAL \Add7~13_sumout\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \Add1~13_sumout\ : std_logic;
SIGNAL \state.DRAW_RIGHT_LOOP~q\ : std_logic;
SIGNAL \vga_u0|writeEn~1_combout\ : std_logic;
SIGNAL \Equal2~0_combout\ : std_logic;
SIGNAL \draw.x[4]~0_combout\ : std_logic;
SIGNAL \state.DRAW_RIGHT_ENTER~q\ : std_logic;
SIGNAL \Selector95~0_combout\ : std_logic;
SIGNAL \state.DRAW_RIGHT_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \draw.y[3]~1_combout\ : std_logic;
SIGNAL \state.DRAW_TOP_LOOP~q\ : std_logic;
SIGNAL \Selector9~0_combout\ : std_logic;
SIGNAL \LessThan6~2_combout\ : std_logic;
SIGNAL \LessThan6~1_combout\ : std_logic;
SIGNAL \LessThan6~3_combout\ : std_logic;
SIGNAL \LessThan6~0_combout\ : std_logic;
SIGNAL \Add16~5_wirecell_combout\ : std_logic;
SIGNAL \puck_two.y[4]~DUPLICATE_q\ : std_logic;
SIGNAL \Add17~22\ : std_logic;
SIGNAL \Add17~25_sumout\ : std_logic;
SIGNAL \Add19~22\ : std_logic;
SIGNAL \Add19~25_sumout\ : std_logic;
SIGNAL \Add19~25_wirecell_combout\ : std_logic;
SIGNAL \Add17~26\ : std_logic;
SIGNAL \Add17~29_sumout\ : std_logic;
SIGNAL \Add19~26\ : std_logic;
SIGNAL \Add19~29_sumout\ : std_logic;
SIGNAL \Add19~29_wirecell_combout\ : std_logic;
SIGNAL \Add17~30\ : std_logic;
SIGNAL \Add17~13_sumout\ : std_logic;
SIGNAL \Add19~30\ : std_logic;
SIGNAL \Add19~13_sumout\ : std_logic;
SIGNAL \Add19~13_wirecell_combout\ : std_logic;
SIGNAL \Add17~14\ : std_logic;
SIGNAL \Add17~5_sumout\ : std_logic;
SIGNAL \Add19~14\ : std_logic;
SIGNAL \Add19~5_sumout\ : std_logic;
SIGNAL \Add19~5_wirecell_combout\ : std_logic;
SIGNAL \Selector70~0_combout\ : std_logic;
SIGNAL \Add16~22\ : std_logic;
SIGNAL \Add16~25_sumout\ : std_logic;
SIGNAL \Selector69~0_combout\ : std_logic;
SIGNAL \Add16~26\ : std_logic;
SIGNAL \Add16~29_sumout\ : std_logic;
SIGNAL \Add16~29_wirecell_combout\ : std_logic;
SIGNAL \Add16~30\ : std_logic;
SIGNAL \Add16~13_sumout\ : std_logic;
SIGNAL \Add16~13_wirecell_combout\ : std_logic;
SIGNAL \Add16~14\ : std_logic;
SIGNAL \Add16~5_sumout\ : std_logic;
SIGNAL \Add17~6\ : std_logic;
SIGNAL \Add17~10\ : std_logic;
SIGNAL \Add17~1_sumout\ : std_logic;
SIGNAL \Add17~9_sumout\ : std_logic;
SIGNAL \Add19~6\ : std_logic;
SIGNAL \Add19~10\ : std_logic;
SIGNAL \Add19~1_sumout\ : std_logic;
SIGNAL \Add19~1_wirecell_combout\ : std_logic;
SIGNAL \puck_velocity_two.y[6]~DUPLICATE_q\ : std_logic;
SIGNAL \Selector64~0_combout\ : std_logic;
SIGNAL \Add16~6\ : std_logic;
SIGNAL \Add16~10\ : std_logic;
SIGNAL \Add16~1_sumout\ : std_logic;
SIGNAL \Add17~2\ : std_logic;
SIGNAL \Add17~17_sumout\ : std_logic;
SIGNAL \Add19~2\ : std_logic;
SIGNAL \Add19~17_sumout\ : std_logic;
SIGNAL \Add19~17_wirecell_combout\ : std_logic;
SIGNAL \Selector63~0_combout\ : std_logic;
SIGNAL \Add16~2\ : std_logic;
SIGNAL \Add16~17_sumout\ : std_logic;
SIGNAL \Equal9~1_combout\ : std_logic;
SIGNAL \Add17~21_sumout\ : std_logic;
SIGNAL \Add19~21_sumout\ : std_logic;
SIGNAL \Add19~21_wirecell_combout\ : std_logic;
SIGNAL \Add16~21_sumout\ : std_logic;
SIGNAL \Equal9~0_combout\ : std_logic;
SIGNAL \Equal12~0_combout\ : std_logic;
SIGNAL \Add19~9_sumout\ : std_logic;
SIGNAL \Add19~9_wirecell_combout\ : std_logic;
SIGNAL \Add16~9_sumout\ : std_logic;
SIGNAL \Add16~9_wirecell_combout\ : std_logic;
SIGNAL \draw.y[6]~3_combout\ : std_logic;
SIGNAL \draw.y[6]~4_combout\ : std_logic;
SIGNAL \Add0~25_sumout\ : std_logic;
SIGNAL \Selector15~0_combout\ : std_logic;
SIGNAL \Equal4~2_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_ENTER~q\ : std_logic;
SIGNAL \Selector102~0_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_LOOP~q\ : std_logic;
SIGNAL \Selector100~0_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_LOOP~q\ : std_logic;
SIGNAL \draw.x[4]~2_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \draw.x[4]~1_combout\ : std_logic;
SIGNAL \draw.y[6]~5_combout\ : std_logic;
SIGNAL \draw.y[3]~7_combout\ : std_logic;
SIGNAL \Add0~26\ : std_logic;
SIGNAL \Add0~21_sumout\ : std_logic;
SIGNAL \Selector14~0_combout\ : std_logic;
SIGNAL \draw.y[6]~6_combout\ : std_logic;
SIGNAL \Add0~22\ : std_logic;
SIGNAL \Add0~18\ : std_logic;
SIGNAL \Add0~13_sumout\ : std_logic;
SIGNAL \Selector12~0_combout\ : std_logic;
SIGNAL \Add0~14\ : std_logic;
SIGNAL \Add0~9_sumout\ : std_logic;
SIGNAL \Selector11~0_combout\ : std_logic;
SIGNAL \Add0~10\ : std_logic;
SIGNAL \Add0~5_sumout\ : std_logic;
SIGNAL \Selector10~0_combout\ : std_logic;
SIGNAL \Add0~6\ : std_logic;
SIGNAL \Add0~1_sumout\ : std_logic;
SIGNAL \Selector9~1_combout\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \draw.x[4]~8_combout\ : std_logic;
SIGNAL \state.DRAW_LEFT_ENTER~q\ : std_logic;
SIGNAL \WideOr1~0_combout\ : std_logic;
SIGNAL \Selector7~1_combout\ : std_logic;
SIGNAL \Selector7~2_combout\ : std_logic;
SIGNAL \Selector7~3_combout\ : std_logic;
SIGNAL \Selector7~4_combout\ : std_logic;
SIGNAL \draw.x[4]~7_combout\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \Selector7~5_combout\ : std_logic;
SIGNAL \Add1~14\ : std_logic;
SIGNAL \Add1~17_sumout\ : std_logic;
SIGNAL \Selector6~2_combout\ : std_logic;
SIGNAL \Selector6~1_combout\ : std_logic;
SIGNAL \Selector6~3_combout\ : std_logic;
SIGNAL \Selector6~4_combout\ : std_logic;
SIGNAL \draw.x[4]~5_combout\ : std_logic;
SIGNAL \draw.x[4]~6_combout\ : std_logic;
SIGNAL \Add1~18\ : std_logic;
SIGNAL \Add1~22\ : std_logic;
SIGNAL \Add1~25_sumout\ : std_logic;
SIGNAL \Selector4~1_combout\ : std_logic;
SIGNAL \Selector4~2_combout\ : std_logic;
SIGNAL \Selector4~3_combout\ : std_logic;
SIGNAL \Selector4~4_combout\ : std_logic;
SIGNAL \Add1~26\ : std_logic;
SIGNAL \Add1~29_sumout\ : std_logic;
SIGNAL \Selector3~1_combout\ : std_logic;
SIGNAL \Selector3~2_combout\ : std_logic;
SIGNAL \Selector3~3_combout\ : std_logic;
SIGNAL \Add1~30\ : std_logic;
SIGNAL \Add1~10\ : std_logic;
SIGNAL \Add1~5_sumout\ : std_logic;
SIGNAL \Selector1~1_combout\ : std_logic;
SIGNAL \Add8~10\ : std_logic;
SIGNAL \Add8~5_sumout\ : std_logic;
SIGNAL \Add7~10\ : std_logic;
SIGNAL \Add7~5_sumout\ : std_logic;
SIGNAL \paddle_x~3_combout\ : std_logic;
SIGNAL \Selector1~2_combout\ : std_logic;
SIGNAL \Equal4~4_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \draw.y[6]~9_combout\ : std_logic;
SIGNAL \state.ERASE_PUCK_ONE~q\ : std_logic;
SIGNAL \draw~0_combout\ : std_logic;
SIGNAL \Selector2~1_combout\ : std_logic;
SIGNAL \Add1~9_sumout\ : std_logic;
SIGNAL \Selector2~2_combout\ : std_logic;
SIGNAL \Selector2~3_combout\ : std_logic;
SIGNAL \Equal4~0_combout\ : std_logic;
SIGNAL \Add1~21_sumout\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \Selector5~2_combout\ : std_logic;
SIGNAL \Selector5~3_combout\ : std_logic;
SIGNAL \Selector5~7_combout\ : std_logic;
SIGNAL \Selector5~6_combout\ : std_logic;
SIGNAL \Selector5~5_combout\ : std_logic;
SIGNAL \Selector5~8_combout\ : std_logic;
SIGNAL \Selector5~4_combout\ : std_logic;
SIGNAL \Selector5~9_combout\ : std_logic;
SIGNAL \Selector5~10_combout\ : std_logic;
SIGNAL \draw.x[4]~4_combout\ : std_logic;
SIGNAL \state.DRAW_TOP_ENTER~q\ : std_logic;
SIGNAL \Selector93~0_combout\ : std_logic;
SIGNAL \Selector93~1_combout\ : std_logic;
SIGNAL \state.DRAW_TOP_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \draw.y[6]~2_combout\ : std_logic;
SIGNAL \draw.x[4]~3_combout\ : std_logic;
SIGNAL \Selector0~1_combout\ : std_logic;
SIGNAL \Add1~6\ : std_logic;
SIGNAL \Add1~1_sumout\ : std_logic;
SIGNAL \Selector0~2_combout\ : std_logic;
SIGNAL \Selector0~3_combout\ : std_logic;
SIGNAL \Selector0~4_combout\ : std_logic;
SIGNAL \draw.y[6]~8_combout\ : std_logic;
SIGNAL \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ : std_logic;
SIGNAL \Selector6~5_combout\ : std_logic;
SIGNAL \Add8~14\ : std_logic;
SIGNAL \Add8~17_sumout\ : std_logic;
SIGNAL \Add7~14\ : std_logic;
SIGNAL \Add7~17_sumout\ : std_logic;
SIGNAL \Selector5~1_combout\ : std_logic;
SIGNAL \Selector5~11_combout\ : std_logic;
SIGNAL \Add7~18\ : std_logic;
SIGNAL \Add7~21_sumout\ : std_logic;
SIGNAL \Add8~21_sumout\ : std_logic;
SIGNAL \Selector4~0_combout\ : std_logic;
SIGNAL \Selector4~5_combout\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \paddle_x~0_combout\ : std_logic;
SIGNAL \paddle_x~1_combout\ : std_logic;
SIGNAL \paddle_x~2_combout\ : std_logic;
SIGNAL \paddle_x~5_combout\ : std_logic;
SIGNAL \Selector17~0_combout\ : std_logic;
SIGNAL \Add8~6\ : std_logic;
SIGNAL \Add8~1_sumout\ : std_logic;
SIGNAL \Add7~6\ : std_logic;
SIGNAL \Add7~1_sumout\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \Selector0~5_combout\ : std_logic;
SIGNAL \Add13~22\ : std_logic;
SIGNAL \Add13~25_sumout\ : std_logic;
SIGNAL \LessThan7~3_combout\ : std_logic;
SIGNAL \LessThan7~1_combout\ : std_logic;
SIGNAL \LessThan7~4_combout\ : std_logic;
SIGNAL \LessThan7~2_combout\ : std_logic;
SIGNAL \LessThan7~0_combout\ : std_logic;
SIGNAL \LessThan7~5_combout\ : std_logic;
SIGNAL \state~53_combout\ : std_logic;
SIGNAL \Selector90~1_combout\ : std_logic;
SIGNAL \state.INIT~DUPLICATE_q\ : std_logic;
SIGNAL \Selector91~0_combout\ : std_logic;
SIGNAL \state.START~q\ : std_logic;
SIGNAL \Add0~17_sumout\ : std_logic;
SIGNAL \Selector13~0_combout\ : std_logic;
SIGNAL \Selector13~2_combout\ : std_logic;
SIGNAL \Selector13~1_combout\ : std_logic;
SIGNAL \Selector13~3_combout\ : std_logic;
SIGNAL \Selector13~4_combout\ : std_logic;
SIGNAL \draw.y[3]~DUPLICATE_q\ : std_logic;
SIGNAL \Add0~2\ : std_logic;
SIGNAL \Add0~29_sumout\ : std_logic;
SIGNAL \Selector8~0_combout\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \Selector97~0_combout\ : std_logic;
SIGNAL \state.DRAW_LEFT_LOOP~q\ : std_logic;
SIGNAL \Selector98~1_combout\ : std_logic;
SIGNAL \state.IDLE~q\ : std_logic;
SIGNAL \Add2~2\ : std_logic;
SIGNAL \Add2~73_sumout\ : std_logic;
SIGNAL \clock_counter[5]~0_combout\ : std_logic;
SIGNAL \Add2~74\ : std_logic;
SIGNAL \Add2~101_sumout\ : std_logic;
SIGNAL \Add2~102\ : std_logic;
SIGNAL \Add2~105_sumout\ : std_logic;
SIGNAL \Add2~106\ : std_logic;
SIGNAL \Add2~109_sumout\ : std_logic;
SIGNAL \Add2~110\ : std_logic;
SIGNAL \Add2~81_sumout\ : std_logic;
SIGNAL \Add2~82\ : std_logic;
SIGNAL \Add2~85_sumout\ : std_logic;
SIGNAL \Add2~86\ : std_logic;
SIGNAL \Add2~89_sumout\ : std_logic;
SIGNAL \Add2~90\ : std_logic;
SIGNAL \Add2~93_sumout\ : std_logic;
SIGNAL \Add2~94\ : std_logic;
SIGNAL \Add2~97_sumout\ : std_logic;
SIGNAL \LessThan0~4_combout\ : std_logic;
SIGNAL \LessThan0~5_combout\ : std_logic;
SIGNAL \Add2~98\ : std_logic;
SIGNAL \Add2~77_sumout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \Selector98~0_combout\ : std_logic;
SIGNAL \Add2~126\ : std_logic;
SIGNAL \Add2~121_sumout\ : std_logic;
SIGNAL \Add2~122\ : std_logic;
SIGNAL \Add2~117_sumout\ : std_logic;
SIGNAL \Add2~118\ : std_logic;
SIGNAL \Add2~113_sumout\ : std_logic;
SIGNAL \Add2~114\ : std_logic;
SIGNAL \Add2~33_sumout\ : std_logic;
SIGNAL \Add2~34\ : std_logic;
SIGNAL \Add2~29_sumout\ : std_logic;
SIGNAL \Add2~30\ : std_logic;
SIGNAL \Add2~17_sumout\ : std_logic;
SIGNAL \Add2~18\ : std_logic;
SIGNAL \Add2~25_sumout\ : std_logic;
SIGNAL \Add2~26\ : std_logic;
SIGNAL \Add2~21_sumout\ : std_logic;
SIGNAL \Add2~22\ : std_logic;
SIGNAL \Add2~49_sumout\ : std_logic;
SIGNAL \Add2~50\ : std_logic;
SIGNAL \Add2~45_sumout\ : std_logic;
SIGNAL \Add2~46\ : std_logic;
SIGNAL \Add2~41_sumout\ : std_logic;
SIGNAL \Add2~42\ : std_logic;
SIGNAL \Add2~37_sumout\ : std_logic;
SIGNAL \Add2~38\ : std_logic;
SIGNAL \Add2~13_sumout\ : std_logic;
SIGNAL \Add2~14\ : std_logic;
SIGNAL \Add2~9_sumout\ : std_logic;
SIGNAL \Add2~10\ : std_logic;
SIGNAL \Add2~5_sumout\ : std_logic;
SIGNAL \Add2~6\ : std_logic;
SIGNAL \Add2~69_sumout\ : std_logic;
SIGNAL \Add2~70\ : std_logic;
SIGNAL \Add2~65_sumout\ : std_logic;
SIGNAL \Add2~66\ : std_logic;
SIGNAL \Add2~61_sumout\ : std_logic;
SIGNAL \Add2~62\ : std_logic;
SIGNAL \Add2~57_sumout\ : std_logic;
SIGNAL \Add2~58\ : std_logic;
SIGNAL \Add2~53_sumout\ : std_logic;
SIGNAL \Add2~54\ : std_logic;
SIGNAL \Add2~1_sumout\ : std_logic;
SIGNAL \Selector94~0_combout\ : std_logic;
SIGNAL \state.ERASE_PADDLE_ENTER~q\ : std_logic;
SIGNAL \Add3~73_sumout\ : std_logic;
SIGNAL \paddle_counter[10]~0_combout\ : std_logic;
SIGNAL \Add3~74\ : std_logic;
SIGNAL \Add3~29_sumout\ : std_logic;
SIGNAL \Add3~30\ : std_logic;
SIGNAL \Add3~33_sumout\ : std_logic;
SIGNAL \Add3~34\ : std_logic;
SIGNAL \Add3~5_sumout\ : std_logic;
SIGNAL \Add3~6\ : std_logic;
SIGNAL \Add3~9_sumout\ : std_logic;
SIGNAL \Add3~10\ : std_logic;
SIGNAL \Add3~121_sumout\ : std_logic;
SIGNAL \Add3~122\ : std_logic;
SIGNAL \Add3~125_sumout\ : std_logic;
SIGNAL \Add3~126\ : std_logic;
SIGNAL \Add3~61_sumout\ : std_logic;
SIGNAL \Add3~62\ : std_logic;
SIGNAL \Add3~69_sumout\ : std_logic;
SIGNAL \Add3~70\ : std_logic;
SIGNAL \Add3~37_sumout\ : std_logic;
SIGNAL \Add3~38\ : std_logic;
SIGNAL \Add3~77_sumout\ : std_logic;
SIGNAL \Add3~78\ : std_logic;
SIGNAL \Add3~81_sumout\ : std_logic;
SIGNAL \Add3~82\ : std_logic;
SIGNAL \Add3~85_sumout\ : std_logic;
SIGNAL \Add3~86\ : std_logic;
SIGNAL \Add3~65_sumout\ : std_logic;
SIGNAL \Add3~66\ : std_logic;
SIGNAL \Add3~41_sumout\ : std_logic;
SIGNAL \Add3~42\ : std_logic;
SIGNAL \Add3~25_sumout\ : std_logic;
SIGNAL \Add3~26\ : std_logic;
SIGNAL \Add3~45_sumout\ : std_logic;
SIGNAL \Add3~46\ : std_logic;
SIGNAL \Add3~49_sumout\ : std_logic;
SIGNAL \Add3~50\ : std_logic;
SIGNAL \Add3~53_sumout\ : std_logic;
SIGNAL \Equal3~1_combout\ : std_logic;
SIGNAL \Add3~54\ : std_logic;
SIGNAL \Add3~1_sumout\ : std_logic;
SIGNAL \Add3~2\ : std_logic;
SIGNAL \Add3~13_sumout\ : std_logic;
SIGNAL \Add3~14\ : std_logic;
SIGNAL \Add3~17_sumout\ : std_logic;
SIGNAL \Add3~18\ : std_logic;
SIGNAL \Add3~21_sumout\ : std_logic;
SIGNAL \Add3~22\ : std_logic;
SIGNAL \Add3~89_sumout\ : std_logic;
SIGNAL \Add3~90\ : std_logic;
SIGNAL \Add3~93_sumout\ : std_logic;
SIGNAL \Add3~94\ : std_logic;
SIGNAL \Add3~97_sumout\ : std_logic;
SIGNAL \Add3~98\ : std_logic;
SIGNAL \Add3~101_sumout\ : std_logic;
SIGNAL \Add3~102\ : std_logic;
SIGNAL \Add3~117_sumout\ : std_logic;
SIGNAL \Add3~118\ : std_logic;
SIGNAL \Add3~105_sumout\ : std_logic;
SIGNAL \Add3~106\ : std_logic;
SIGNAL \Add3~57_sumout\ : std_logic;
SIGNAL \Add3~58\ : std_logic;
SIGNAL \Add3~109_sumout\ : std_logic;
SIGNAL \Add3~110\ : std_logic;
SIGNAL \Add3~113_sumout\ : std_logic;
SIGNAL \Equal3~4_combout\ : std_logic;
SIGNAL \Equal3~3_combout\ : std_logic;
SIGNAL \Equal3~2_combout\ : std_logic;
SIGNAL \Equal3~5_combout\ : std_logic;
SIGNAL \Equal3~0_combout\ : std_logic;
SIGNAL \Equal3~6_combout\ : std_logic;
SIGNAL \pflag~0_combout\ : std_logic;
SIGNAL \pflag~q\ : std_logic;
SIGNAL \paddle_width~2_combout\ : std_logic;
SIGNAL \Selector110~0_combout\ : std_logic;
SIGNAL \Selector109~0_combout\ : std_logic;
SIGNAL \Add13~13_sumout\ : std_logic;
SIGNAL \Equal4~1_combout\ : std_logic;
SIGNAL \Equal4~3_combout\ : std_logic;
SIGNAL \Selector89~0_combout\ : std_logic;
SIGNAL \Selector89~1_combout\ : std_logic;
SIGNAL \plot~q\ : std_logic;
SIGNAL \vga_u0|writeEn~0_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~6\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~7\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|LessThan3~0_combout\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ : std_logic;
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\ : std_logic;
SIGNAL \vga_u0|controller|Add0~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~38\ : std_logic;
SIGNAL \vga_u0|controller|Add0~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~14\ : std_logic;
SIGNAL \vga_u0|controller|Add0~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~30\ : std_logic;
SIGNAL \vga_u0|controller|Add0~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~34\ : std_logic;
SIGNAL \vga_u0|controller|Add0~18\ : std_logic;
SIGNAL \vga_u0|controller|Add0~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~22\ : std_logic;
SIGNAL \vga_u0|controller|Add0~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~26\ : std_logic;
SIGNAL \vga_u0|controller|Add0~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~10\ : std_logic;
SIGNAL \vga_u0|controller|Add0~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~6\ : std_logic;
SIGNAL \vga_u0|controller|Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|Equal0~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add0~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[4]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|Add1~6\ : std_logic;
SIGNAL \vga_u0|controller|Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|always1~2_combout\ : std_logic;
SIGNAL \vga_u0|controller|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[5]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[7]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~10\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~11\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~14\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~15\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~18\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~19\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~22\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~23\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~26\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~27\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~30\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~31\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~34\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~35\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~38\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~39\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~2\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~3\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~1_sumout\ : std_logic;
SIGNAL \Selector88~0_combout\ : std_logic;
SIGNAL \Selector88~1_combout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|controller|xCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~9_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~13_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~17_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~21_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~25_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~29_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~33_sumout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \vga_u0|user_input_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|Add1~37_sumout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|LessThan7~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|on_screen~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_R[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_G[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_B[0]~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_HS~q\ : std_logic;
SIGNAL \vga_u0|controller|yCounter[1]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_VS~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK1~q\ : std_logic;
SIGNAL \vga_u0|controller|VGA_BLANK~q\ : std_logic;
SIGNAL \puck_one.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|controller|yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \puck_velocity_two.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|controller|xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \puck_two.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL clock_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL \puck_one.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \puck_two.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL paddle_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL \puck_velocity_one.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \puck_velocity_one.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \puck_velocity_two.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \draw.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \draw.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL colour : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\ : std_logic_vector(2 DOWNTO 0);
SIGNAL paddle_width : std_logic_vector(3 DOWNTO 0);
SIGNAL paddle_x : std_logic_vector(7 DOWNTO 0);
SIGNAL \vga_u0|mypll|altpll_component|auto_generated|clk\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \ALT_INV_puck_velocity_one.x[0]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_paddle_width[3]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_state.INIT~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_draw.y[3]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_one.x[5]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_one.x[6]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_one.x[2]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_one.y[6]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_two.y[6]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_two.x[4]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_puck_two.y[4]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[1]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[4]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter[8]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_KEY[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_Equal11~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal10~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal7~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal6~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal9~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal11~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal10~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal7~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal6~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal5~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal5~1_combout\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_one~0_combout\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_two.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_puck_velocity_one.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_puck_velocity_one.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Equal3~6_combout\ : std_logic;
SIGNAL \ALT_INV_Equal3~5_combout\ : std_logic;
SIGNAL \ALT_INV_Equal3~4_combout\ : std_logic;
SIGNAL \ALT_INV_Equal3~3_combout\ : std_logic;
SIGNAL \ALT_INV_Equal3~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal3~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal3~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector98~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector13~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_width~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector93~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~4_combout\ : std_logic;
SIGNAL \ALT_INV_state~53_combout\ : std_logic;
SIGNAL \ALT_INV_Equal12~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal9~0_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan6~0_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan7~5_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan7~4_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan7~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan7~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan7~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan7~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector90~0_combout\ : std_logic;
SIGNAL \ALT_INV_always0~8_combout\ : std_logic;
SIGNAL \ALT_INV_always0~7_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan4~0_combout\ : std_logic;
SIGNAL \ALT_INV_always0~6_combout\ : std_logic;
SIGNAL \ALT_INV_always0~5_combout\ : std_logic;
SIGNAL \ALT_INV_always0~4_combout\ : std_logic;
SIGNAL \ALT_INV_always0~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan5~0_combout\ : std_logic;
SIGNAL \ALT_INV_always0~2_combout\ : std_logic;
SIGNAL \ALT_INV_always0~1_combout\ : std_logic;
SIGNAL \ALT_INV_always0~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal8~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal5~0_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~5_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~4_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~3_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~2_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~1_combout\ : std_logic;
SIGNAL \ALT_INV_LessThan0~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector3~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector3~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector3~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_x~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector4~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector4~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector4~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector4~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~9_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~8_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~7_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~6_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~5_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector5~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector6~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector7~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector7~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector7~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector7~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector7~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~7_combout\ : std_logic;
SIGNAL \ALT_INV_Selector88~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector89~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector2~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector2~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector2~0_combout\ : std_logic;
SIGNAL \ALT_INV_Selector1~1_combout\ : std_logic;
SIGNAL \ALT_INV_Selector1~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_x~3_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~5_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~4_combout\ : std_logic;
SIGNAL \ALT_INV_Selector0~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector0~2_combout\ : std_logic;
SIGNAL \ALT_INV_Selector0~1_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~3_combout\ : std_logic;
SIGNAL \ALT_INV_Selector0~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_x~2_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_x~1_combout\ : std_logic;
SIGNAL ALT_INV_paddle_x : std_logic_vector(7 DOWNTO 1);
SIGNAL \ALT_INV_paddle_width~1_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_width~0_combout\ : std_logic;
SIGNAL \ALT_INV_pflag~q\ : std_logic;
SIGNAL ALT_INV_paddle_width : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_paddle_x~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~2_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y[6]~5_combout\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~1_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_RIGHT_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.IDLE~q\ : std_logic;
SIGNAL \ALT_INV_draw.x[4]~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal1~1_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Equal1~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_LEFT_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.START~q\ : std_logic;
SIGNAL \ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \ALT_INV_Equal2~0_combout\ : std_logic;
SIGNAL \vga_u0|ALT_INV_writeEn~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~2_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal4~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y[6]~4_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y[6]~3_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y[6]~2_combout\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_WideOr6~0_combout\ : std_logic;
SIGNAL \ALT_INV_draw.y[3]~1_combout\ : std_logic;
SIGNAL \ALT_INV_draw~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PUCK_ONE~q\ : std_logic;
SIGNAL \ALT_INV_draw.y[3]~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PUCK_TWO~q\ : std_logic;
SIGNAL \ALT_INV_Selector9~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_TOP_LOOP~q\ : std_logic;
SIGNAL \ALT_INV_WideOr1~0_combout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_LEFT_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_TOP_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_RIGHT_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PADDLE_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_state.INIT~q\ : std_logic;
SIGNAL \ALT_INV_state.ERASE_PADDLE_ENTER~q\ : std_logic;
SIGNAL \ALT_INV_draw.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL ALT_INV_colour : std_logic_vector(1 DOWNTO 1);
SIGNAL \vga_u0|ALT_INV_writeEn~0_combout\ : std_logic;
SIGNAL \ALT_INV_plot~q\ : std_logic;
SIGNAL \vga_u0|ALT_INV_LessThan3~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_always1~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_on_screen~1_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_on_screen~0_combout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_LessThan7~0_combout\ : std_logic;
SIGNAL \ALT_INV_paddle_x~5_combout\ : std_logic;
SIGNAL ALT_INV_clock_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_Add17~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add17~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add18~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add12~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add14~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add11~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add19~1_sumout\ : std_logic;
SIGNAL \ALT_INV_puck_velocity_two.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL ALT_INV_paddle_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL \ALT_INV_Add0~29_sumout\ : std_logic;
SIGNAL \ALT_INV_puck_two.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_puck_one.y\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add15~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add9~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add10~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add16~1_sumout\ : std_logic;
SIGNAL \ALT_INV_puck_one.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_puck_two.x\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_Add1~29_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add7~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add8~1_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~25_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~21_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~17_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~13_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~9_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~5_sumout\ : std_logic;
SIGNAL \ALT_INV_Add13~1_sumout\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PUCK_ONE~q\ : std_logic;
SIGNAL \ALT_INV_state.DRAW_PUCK_TWO~q\ : std_logic;
SIGNAL \ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \vga_u0|controller|ALT_INV_yCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|controller|ALT_INV_xCounter\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ : std_logic;
SIGNAL \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ : std_logic;

BEGIN

ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
VGA_R <= ww_VGA_R;
VGA_G <= ww_VGA_G;
VGA_B <= ww_VGA_B;
VGA_HS <= ww_VGA_HS;
VGA_VS <= ww_VGA_VS;
VGA_BLANK <= ww_VGA_BLANK;
VGA_SYNC <= ww_VGA_SYNC;
VGA_CLK <= ww_VGA_CLK;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\ <= (colour(1) & colour(1));

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & \draw.x\(3) & \draw.x\(2) & \draw.x\(1) & 
\draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\);

\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(0);
\vga_u0|VideoMemory|auto_generated|ram_block1a8\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\(0) <= colour(1);

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\ <= (gnd & \~GND~combout\);

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & \vga_u0|user_input_translator|Add1~25_sumout\ & 
\vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & \draw.x\(3) & \draw.x\(2) & \draw.x\(1) & 
\draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & \vga_u0|controller|controller_translator|Add1~25_sumout\ & 
\vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & \vga_u0|controller|controller_translator|Add1~9_sumout\ & 
\vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\);

\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\(0);

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\(0) <= \~GND~combout\;

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\vga_u0|user_input_translator|Add1~37_sumout\ & \vga_u0|user_input_translator|Add1~33_sumout\ & \vga_u0|user_input_translator|Add1~29_sumout\ & 
\vga_u0|user_input_translator|Add1~25_sumout\ & \vga_u0|user_input_translator|Add1~21_sumout\ & \vga_u0|user_input_translator|Add1~17_sumout\ & \vga_u0|user_input_translator|Add1~13_sumout\ & \vga_u0|user_input_translator|Add1~9_sumout\ & \draw.x\(4) & 
\draw.x\(3) & \draw.x\(2) & \draw.x\(1) & \draw.x\(0));

\vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\vga_u0|controller|controller_translator|Add1~37_sumout\ & \vga_u0|controller|controller_translator|Add1~33_sumout\ & \vga_u0|controller|controller_translator|Add1~29_sumout\ & 
\vga_u0|controller|controller_translator|Add1~25_sumout\ & \vga_u0|controller|controller_translator|Add1~21_sumout\ & \vga_u0|controller|controller_translator|Add1~17_sumout\ & \vga_u0|controller|controller_translator|Add1~13_sumout\ & 
\vga_u0|controller|controller_translator|Add1~9_sumout\ & \vga_u0|controller|xCounter\(6) & \vga_u0|controller|xCounter\(5) & \vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|xCounter\(3) & \vga_u0|controller|xCounter[2]~DUPLICATE_q\
);

\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ <= \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(0);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(1);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(2);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(3);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(4);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(5);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(6);
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\(7);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\ <= (gnd & gnd & gnd & \CLOCK_50~input_o\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI7\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI6\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI4\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI3\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI2\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_MHI0\);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\ <= \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\(6);

\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\ <= (\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH7\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH6\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH5\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH4\
& \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH3\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH2\ & 
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH1\ & \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_VCOPH0\);
\ALT_INV_puck_velocity_one.x[0]~DUPLICATE_q\ <= NOT \puck_velocity_one.x[0]~DUPLICATE_q\;
\ALT_INV_paddle_width[3]~DUPLICATE_q\ <= NOT \paddle_width[3]~DUPLICATE_q\;
\ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\ <= NOT \state.DRAW_RIGHT_LOOP~DUPLICATE_q\;
\ALT_INV_state.DRAW_PADDLE_LOOP~DUPLICATE_q\ <= NOT \state.DRAW_PADDLE_LOOP~DUPLICATE_q\;
\ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\ <= NOT \state.DRAW_TOP_LOOP~DUPLICATE_q\;
\ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\ <= NOT \state.DRAW_PADDLE_ENTER~DUPLICATE_q\;
\ALT_INV_state.INIT~DUPLICATE_q\ <= NOT \state.INIT~DUPLICATE_q\;
\ALT_INV_draw.y[3]~DUPLICATE_q\ <= NOT \draw.y[3]~DUPLICATE_q\;
\ALT_INV_puck_velocity_one.x[5]~DUPLICATE_q\ <= NOT \puck_velocity_one.x[5]~DUPLICATE_q\;
\ALT_INV_puck_velocity_one.x[6]~DUPLICATE_q\ <= NOT \puck_velocity_one.x[6]~DUPLICATE_q\;
\ALT_INV_puck_velocity_one.x[2]~DUPLICATE_q\ <= NOT \puck_velocity_one.x[2]~DUPLICATE_q\;
\ALT_INV_puck_velocity_one.y[6]~DUPLICATE_q\ <= NOT \puck_velocity_one.y[6]~DUPLICATE_q\;
\ALT_INV_puck_velocity_two.y[6]~DUPLICATE_q\ <= NOT \puck_velocity_two.y[6]~DUPLICATE_q\;
\ALT_INV_puck_two.x[4]~DUPLICATE_q\ <= NOT \puck_two.x[4]~DUPLICATE_q\;
\ALT_INV_puck_two.y[4]~DUPLICATE_q\ <= NOT \puck_two.y[4]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[1]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[1]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[2]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[4]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[4]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[7]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\ <= NOT \vga_u0|controller|xCounter[8]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[5]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[6]~DUPLICATE_q\;
\vga_u0|controller|ALT_INV_yCounter[8]~DUPLICATE_q\ <= NOT \vga_u0|controller|yCounter[8]~DUPLICATE_q\;
\ALT_INV_KEY[1]~input_o\ <= NOT \KEY[1]~input_o\;
\ALT_INV_KEY[0]~input_o\ <= NOT \KEY[0]~input_o\;
\ALT_INV_KEY[3]~input_o\ <= NOT \KEY[3]~input_o\;
\ALT_INV_Equal11~1_combout\ <= NOT \Equal11~1_combout\;
\ALT_INV_Equal10~1_combout\ <= NOT \Equal10~1_combout\;
\ALT_INV_Equal7~1_combout\ <= NOT \Equal7~1_combout\;
\ALT_INV_Equal6~1_combout\ <= NOT \Equal6~1_combout\;
\ALT_INV_LessThan6~3_combout\ <= NOT \LessThan6~3_combout\;
\ALT_INV_LessThan6~2_combout\ <= NOT \LessThan6~2_combout\;
\ALT_INV_LessThan6~1_combout\ <= NOT \LessThan6~1_combout\;
\ALT_INV_Equal9~1_combout\ <= NOT \Equal9~1_combout\;
\ALT_INV_Equal11~0_combout\ <= NOT \Equal11~0_combout\;
\ALT_INV_Equal10~0_combout\ <= NOT \Equal10~0_combout\;
\ALT_INV_Equal7~0_combout\ <= NOT \Equal7~0_combout\;
\ALT_INV_Equal6~0_combout\ <= NOT \Equal6~0_combout\;
\ALT_INV_Equal5~2_combout\ <= NOT \Equal5~2_combout\;
\ALT_INV_Equal5~1_combout\ <= NOT \Equal5~1_combout\;
\ALT_INV_puck_velocity_one~0_combout\ <= NOT \puck_velocity_one~0_combout\;
\ALT_INV_puck_velocity_two.x\(0) <= NOT \puck_velocity_two.x\(0);
\ALT_INV_puck_velocity_one.x\(0) <= NOT \puck_velocity_one.x\(0);
\ALT_INV_puck_velocity_one.y\(0) <= NOT \puck_velocity_one.y\(0);
\ALT_INV_Equal3~6_combout\ <= NOT \Equal3~6_combout\;
\ALT_INV_Equal3~5_combout\ <= NOT \Equal3~5_combout\;
\ALT_INV_Equal3~4_combout\ <= NOT \Equal3~4_combout\;
\ALT_INV_Equal3~3_combout\ <= NOT \Equal3~3_combout\;
\ALT_INV_Equal3~2_combout\ <= NOT \Equal3~2_combout\;
\ALT_INV_Equal3~1_combout\ <= NOT \Equal3~1_combout\;
\ALT_INV_Equal3~0_combout\ <= NOT \Equal3~0_combout\;
\ALT_INV_Selector98~0_combout\ <= NOT \Selector98~0_combout\;
\ALT_INV_Selector13~3_combout\ <= NOT \Selector13~3_combout\;
\ALT_INV_Selector13~2_combout\ <= NOT \Selector13~2_combout\;
\ALT_INV_Selector13~1_combout\ <= NOT \Selector13~1_combout\;
\ALT_INV_Selector13~0_combout\ <= NOT \Selector13~0_combout\;
\ALT_INV_paddle_width~2_combout\ <= NOT \paddle_width~2_combout\;
\ALT_INV_Selector93~0_combout\ <= NOT \Selector93~0_combout\;
\ALT_INV_Equal4~4_combout\ <= NOT \Equal4~4_combout\;
\ALT_INV_state~53_combout\ <= NOT \state~53_combout\;
\ALT_INV_Equal12~0_combout\ <= NOT \Equal12~0_combout\;
\ALT_INV_Equal9~0_combout\ <= NOT \Equal9~0_combout\;
\ALT_INV_LessThan6~0_combout\ <= NOT \LessThan6~0_combout\;
\ALT_INV_LessThan7~5_combout\ <= NOT \LessThan7~5_combout\;
\ALT_INV_LessThan7~4_combout\ <= NOT \LessThan7~4_combout\;
\ALT_INV_LessThan7~3_combout\ <= NOT \LessThan7~3_combout\;
\ALT_INV_LessThan7~2_combout\ <= NOT \LessThan7~2_combout\;
\ALT_INV_LessThan7~1_combout\ <= NOT \LessThan7~1_combout\;
\ALT_INV_LessThan7~0_combout\ <= NOT \LessThan7~0_combout\;
\ALT_INV_Selector90~0_combout\ <= NOT \Selector90~0_combout\;
\ALT_INV_always0~8_combout\ <= NOT \always0~8_combout\;
\ALT_INV_always0~7_combout\ <= NOT \always0~7_combout\;
\ALT_INV_LessThan4~0_combout\ <= NOT \LessThan4~0_combout\;
\ALT_INV_always0~6_combout\ <= NOT \always0~6_combout\;
\ALT_INV_always0~5_combout\ <= NOT \always0~5_combout\;
\ALT_INV_always0~4_combout\ <= NOT \always0~4_combout\;
\ALT_INV_always0~3_combout\ <= NOT \always0~3_combout\;
\ALT_INV_LessThan5~0_combout\ <= NOT \LessThan5~0_combout\;
\ALT_INV_always0~2_combout\ <= NOT \always0~2_combout\;
\ALT_INV_always0~1_combout\ <= NOT \always0~1_combout\;
\ALT_INV_always0~0_combout\ <= NOT \always0~0_combout\;
\ALT_INV_Equal8~0_combout\ <= NOT \Equal8~0_combout\;
\ALT_INV_Equal5~0_combout\ <= NOT \Equal5~0_combout\;
\ALT_INV_LessThan0~5_combout\ <= NOT \LessThan0~5_combout\;
\ALT_INV_LessThan0~4_combout\ <= NOT \LessThan0~4_combout\;
\ALT_INV_LessThan0~3_combout\ <= NOT \LessThan0~3_combout\;
\ALT_INV_LessThan0~2_combout\ <= NOT \LessThan0~2_combout\;
\ALT_INV_LessThan0~1_combout\ <= NOT \LessThan0~1_combout\;
\ALT_INV_LessThan0~0_combout\ <= NOT \LessThan0~0_combout\;
\vga_u0|controller|ALT_INV_VGA_VS1~0_combout\ <= NOT \vga_u0|controller|VGA_VS1~0_combout\;
\vga_u0|controller|ALT_INV_VGA_HS1~0_combout\ <= NOT \vga_u0|controller|VGA_HS1~0_combout\;
\ALT_INV_Selector3~2_combout\ <= NOT \Selector3~2_combout\;
\ALT_INV_Selector3~1_combout\ <= NOT \Selector3~1_combout\;
\ALT_INV_Selector3~0_combout\ <= NOT \Selector3~0_combout\;
\ALT_INV_paddle_x~4_combout\ <= NOT \paddle_x~4_combout\;
\ALT_INV_Selector4~3_combout\ <= NOT \Selector4~3_combout\;
\ALT_INV_Selector4~2_combout\ <= NOT \Selector4~2_combout\;
\ALT_INV_Selector4~1_combout\ <= NOT \Selector4~1_combout\;
\ALT_INV_Selector4~0_combout\ <= NOT \Selector4~0_combout\;
\ALT_INV_Selector5~9_combout\ <= NOT \Selector5~9_combout\;
\ALT_INV_Selector5~8_combout\ <= NOT \Selector5~8_combout\;
\ALT_INV_Selector5~7_combout\ <= NOT \Selector5~7_combout\;
\ALT_INV_Selector5~6_combout\ <= NOT \Selector5~6_combout\;
\ALT_INV_Selector5~5_combout\ <= NOT \Selector5~5_combout\;
\ALT_INV_Selector5~4_combout\ <= NOT \Selector5~4_combout\;
\ALT_INV_Selector5~3_combout\ <= NOT \Selector5~3_combout\;
\ALT_INV_Selector5~2_combout\ <= NOT \Selector5~2_combout\;
\ALT_INV_Selector5~1_combout\ <= NOT \Selector5~1_combout\;
\ALT_INV_Selector5~0_combout\ <= NOT \Selector5~0_combout\;
\ALT_INV_Selector6~3_combout\ <= NOT \Selector6~3_combout\;
\ALT_INV_Selector6~2_combout\ <= NOT \Selector6~2_combout\;
\ALT_INV_Selector6~1_combout\ <= NOT \Selector6~1_combout\;
\ALT_INV_Selector6~0_combout\ <= NOT \Selector6~0_combout\;
\ALT_INV_Selector7~4_combout\ <= NOT \Selector7~4_combout\;
\ALT_INV_Selector7~3_combout\ <= NOT \Selector7~3_combout\;
\ALT_INV_Selector7~2_combout\ <= NOT \Selector7~2_combout\;
\ALT_INV_Selector7~1_combout\ <= NOT \Selector7~1_combout\;
\ALT_INV_Selector7~0_combout\ <= NOT \Selector7~0_combout\;
\ALT_INV_draw.x[4]~7_combout\ <= NOT \draw.x[4]~7_combout\;
\ALT_INV_Selector88~0_combout\ <= NOT \Selector88~0_combout\;
\ALT_INV_Selector89~0_combout\ <= NOT \Selector89~0_combout\;
\ALT_INV_Equal4~3_combout\ <= NOT \Equal4~3_combout\;
\ALT_INV_Selector2~2_combout\ <= NOT \Selector2~2_combout\;
\ALT_INV_Selector2~1_combout\ <= NOT \Selector2~1_combout\;
\ALT_INV_Selector2~0_combout\ <= NOT \Selector2~0_combout\;
\ALT_INV_Selector1~1_combout\ <= NOT \Selector1~1_combout\;
\ALT_INV_Selector1~0_combout\ <= NOT \Selector1~0_combout\;
\ALT_INV_paddle_x~3_combout\ <= NOT \paddle_x~3_combout\;
\ALT_INV_draw.x[4]~5_combout\ <= NOT \draw.x[4]~5_combout\;
\ALT_INV_draw.x[4]~4_combout\ <= NOT \draw.x[4]~4_combout\;
\ALT_INV_Selector0~3_combout\ <= NOT \Selector0~3_combout\;
\ALT_INV_Selector0~2_combout\ <= NOT \Selector0~2_combout\;
\ALT_INV_Selector0~1_combout\ <= NOT \Selector0~1_combout\;
\ALT_INV_draw.x[4]~3_combout\ <= NOT \draw.x[4]~3_combout\;
\ALT_INV_Selector0~0_combout\ <= NOT \Selector0~0_combout\;
\ALT_INV_paddle_x~2_combout\ <= NOT \paddle_x~2_combout\;
\ALT_INV_paddle_x~1_combout\ <= NOT \paddle_x~1_combout\;
ALT_INV_paddle_x(1) <= NOT paddle_x(1);
\ALT_INV_paddle_width~1_combout\ <= NOT \paddle_width~1_combout\;
\ALT_INV_paddle_width~0_combout\ <= NOT \paddle_width~0_combout\;
\ALT_INV_pflag~q\ <= NOT \pflag~q\;
ALT_INV_paddle_width(3) <= NOT paddle_width(3);
ALT_INV_paddle_width(2) <= NOT paddle_width(2);
ALT_INV_paddle_width(1) <= NOT paddle_width(1);
ALT_INV_paddle_x(2) <= NOT paddle_x(2);
ALT_INV_paddle_x(4) <= NOT paddle_x(4);
ALT_INV_paddle_x(3) <= NOT paddle_x(3);
\ALT_INV_paddle_x~0_combout\ <= NOT \paddle_x~0_combout\;
ALT_INV_paddle_x(6) <= NOT paddle_x(6);
ALT_INV_paddle_x(5) <= NOT paddle_x(5);
ALT_INV_paddle_x(7) <= NOT paddle_x(7);
\ALT_INV_draw.x[4]~2_combout\ <= NOT \draw.x[4]~2_combout\;
\ALT_INV_draw.y[6]~5_combout\ <= NOT \draw.y[6]~5_combout\;
\ALT_INV_draw.x[4]~1_combout\ <= NOT \draw.x[4]~1_combout\;
\ALT_INV_state.DRAW_RIGHT_LOOP~q\ <= NOT \state.DRAW_RIGHT_LOOP~q\;
\ALT_INV_state.IDLE~q\ <= NOT \state.IDLE~q\;
\ALT_INV_draw.x[4]~0_combout\ <= NOT \draw.x[4]~0_combout\;
\ALT_INV_Equal1~2_combout\ <= NOT \Equal1~2_combout\;
\ALT_INV_Equal1~1_combout\ <= NOT \Equal1~1_combout\;
\ALT_INV_draw.y\(7) <= NOT \draw.y\(7);
\ALT_INV_draw.y\(0) <= NOT \draw.y\(0);
\ALT_INV_draw.y\(1) <= NOT \draw.y\(1);
\ALT_INV_draw.y\(2) <= NOT \draw.y\(2);
\ALT_INV_Equal1~0_combout\ <= NOT \Equal1~0_combout\;
\ALT_INV_state.DRAW_LEFT_LOOP~q\ <= NOT \state.DRAW_LEFT_LOOP~q\;
\ALT_INV_state.START~q\ <= NOT \state.START~q\;
\ALT_INV_Equal0~0_combout\ <= NOT \Equal0~0_combout\;
\ALT_INV_Equal2~0_combout\ <= NOT \Equal2~0_combout\;
\vga_u0|ALT_INV_writeEn~1_combout\ <= NOT \vga_u0|writeEn~1_combout\;
\ALT_INV_Equal4~2_combout\ <= NOT \Equal4~2_combout\;
\ALT_INV_Equal4~1_combout\ <= NOT \Equal4~1_combout\;
ALT_INV_paddle_width(0) <= NOT paddle_width(0);
\ALT_INV_Equal4~0_combout\ <= NOT \Equal4~0_combout\;
\ALT_INV_draw.y[6]~4_combout\ <= NOT \draw.y[6]~4_combout\;
\ALT_INV_draw.y[6]~3_combout\ <= NOT \draw.y[6]~3_combout\;
\ALT_INV_draw.y[6]~2_combout\ <= NOT \draw.y[6]~2_combout\;
\ALT_INV_state.ERASE_PADDLE_LOOP~q\ <= NOT \state.ERASE_PADDLE_LOOP~q\;
\ALT_INV_state.DRAW_PADDLE_LOOP~q\ <= NOT \state.DRAW_PADDLE_LOOP~q\;
\ALT_INV_WideOr6~0_combout\ <= NOT \WideOr6~0_combout\;
\ALT_INV_draw.y[3]~1_combout\ <= NOT \draw.y[3]~1_combout\;
\ALT_INV_draw~0_combout\ <= NOT \draw~0_combout\;
\ALT_INV_state.ERASE_PUCK_ONE~q\ <= NOT \state.ERASE_PUCK_ONE~q\;
\ALT_INV_draw.y[3]~0_combout\ <= NOT \draw.y[3]~0_combout\;
\ALT_INV_state.ERASE_PUCK_TWO~q\ <= NOT \state.ERASE_PUCK_TWO~q\;
\ALT_INV_Selector9~0_combout\ <= NOT \Selector9~0_combout\;
\ALT_INV_state.DRAW_TOP_LOOP~q\ <= NOT \state.DRAW_TOP_LOOP~q\;
\ALT_INV_WideOr1~0_combout\ <= NOT \WideOr1~0_combout\;
\ALT_INV_state.DRAW_LEFT_ENTER~q\ <= NOT \state.DRAW_LEFT_ENTER~q\;
\ALT_INV_state.DRAW_TOP_ENTER~q\ <= NOT \state.DRAW_TOP_ENTER~q\;
\ALT_INV_state.DRAW_RIGHT_ENTER~q\ <= NOT \state.DRAW_RIGHT_ENTER~q\;
\ALT_INV_state.DRAW_PADDLE_ENTER~q\ <= NOT \state.DRAW_PADDLE_ENTER~q\;
\ALT_INV_state.INIT~q\ <= NOT \state.INIT~q\;
\ALT_INV_state.ERASE_PADDLE_ENTER~q\ <= NOT \state.ERASE_PADDLE_ENTER~q\;
\ALT_INV_draw.x\(4) <= NOT \draw.x\(4);
\ALT_INV_draw.x\(3) <= NOT \draw.x\(3);
\ALT_INV_draw.x\(2) <= NOT \draw.x\(2);
\ALT_INV_draw.x\(1) <= NOT \draw.x\(1);
\ALT_INV_draw.x\(0) <= NOT \draw.x\(0);
ALT_INV_colour(1) <= NOT colour(1);
\vga_u0|ALT_INV_writeEn~0_combout\ <= NOT \vga_u0|writeEn~0_combout\;
\ALT_INV_plot~q\ <= NOT \plot~q\;
\ALT_INV_draw.x\(5) <= NOT \draw.x\(5);
\ALT_INV_draw.x\(6) <= NOT \draw.x\(6);
\ALT_INV_draw.x\(7) <= NOT \draw.x\(7);
\vga_u0|ALT_INV_LessThan3~0_combout\ <= NOT \vga_u0|LessThan3~0_combout\;
\ALT_INV_draw.y\(3) <= NOT \draw.y\(3);
\ALT_INV_draw.y\(4) <= NOT \draw.y\(4);
\ALT_INV_draw.y\(5) <= NOT \draw.y\(5);
\ALT_INV_draw.y\(6) <= NOT \draw.y\(6);
\vga_u0|controller|ALT_INV_always1~1_combout\ <= NOT \vga_u0|controller|always1~1_combout\;
\vga_u0|controller|ALT_INV_always1~0_combout\ <= NOT \vga_u0|controller|always1~0_combout\;
\vga_u0|controller|ALT_INV_Equal0~1_combout\ <= NOT \vga_u0|controller|Equal0~1_combout\;
\vga_u0|controller|ALT_INV_Equal0~0_combout\ <= NOT \vga_u0|controller|Equal0~0_combout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1) <= NOT \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1);
\vga_u0|controller|ALT_INV_on_screen~1_combout\ <= NOT \vga_u0|controller|on_screen~1_combout\;
\vga_u0|controller|ALT_INV_on_screen~0_combout\ <= NOT \vga_u0|controller|on_screen~0_combout\;
\vga_u0|controller|ALT_INV_LessThan7~0_combout\ <= NOT \vga_u0|controller|LessThan7~0_combout\;
\ALT_INV_paddle_x~5_combout\ <= NOT \paddle_x~5_combout\;
ALT_INV_clock_counter(0) <= NOT clock_counter(0);
ALT_INV_clock_counter(1) <= NOT clock_counter(1);
ALT_INV_clock_counter(2) <= NOT clock_counter(2);
\ALT_INV_Add17~29_sumout\ <= NOT \Add17~29_sumout\;
\ALT_INV_Add17~25_sumout\ <= NOT \Add17~25_sumout\;
\ALT_INV_Add17~21_sumout\ <= NOT \Add17~21_sumout\;
\ALT_INV_Add17~17_sumout\ <= NOT \Add17~17_sumout\;
\ALT_INV_Add17~13_sumout\ <= NOT \Add17~13_sumout\;
\ALT_INV_Add17~9_sumout\ <= NOT \Add17~9_sumout\;
\ALT_INV_Add17~5_sumout\ <= NOT \Add17~5_sumout\;
ALT_INV_clock_counter(3) <= NOT clock_counter(3);
\ALT_INV_Add17~1_sumout\ <= NOT \Add17~1_sumout\;
\ALT_INV_Add19~29_sumout\ <= NOT \Add19~29_sumout\;
\ALT_INV_Add19~25_sumout\ <= NOT \Add19~25_sumout\;
\ALT_INV_Add19~21_sumout\ <= NOT \Add19~21_sumout\;
\ALT_INV_Add19~17_sumout\ <= NOT \Add19~17_sumout\;
\ALT_INV_Add19~13_sumout\ <= NOT \Add19~13_sumout\;
\ALT_INV_Add19~9_sumout\ <= NOT \Add19~9_sumout\;
\ALT_INV_Add19~5_sumout\ <= NOT \Add19~5_sumout\;
\ALT_INV_Add18~17_sumout\ <= NOT \Add18~17_sumout\;
\ALT_INV_Add12~29_sumout\ <= NOT \Add12~29_sumout\;
\ALT_INV_Add14~29_sumout\ <= NOT \Add14~29_sumout\;
\ALT_INV_Add11~29_sumout\ <= NOT \Add11~29_sumout\;
\ALT_INV_Add14~25_sumout\ <= NOT \Add14~25_sumout\;
\ALT_INV_Add11~25_sumout\ <= NOT \Add11~25_sumout\;
\ALT_INV_Add14~21_sumout\ <= NOT \Add14~21_sumout\;
\ALT_INV_Add11~21_sumout\ <= NOT \Add11~21_sumout\;
\ALT_INV_Add14~17_sumout\ <= NOT \Add14~17_sumout\;
\ALT_INV_Add11~17_sumout\ <= NOT \Add11~17_sumout\;
\ALT_INV_Add14~13_sumout\ <= NOT \Add14~13_sumout\;
\ALT_INV_Add11~13_sumout\ <= NOT \Add11~13_sumout\;
\ALT_INV_Add14~9_sumout\ <= NOT \Add14~9_sumout\;
\ALT_INV_Add11~9_sumout\ <= NOT \Add11~9_sumout\;
\ALT_INV_Add14~5_sumout\ <= NOT \Add14~5_sumout\;
\ALT_INV_Add11~5_sumout\ <= NOT \Add11~5_sumout\;
\ALT_INV_Add14~1_sumout\ <= NOT \Add14~1_sumout\;
\ALT_INV_Add11~1_sumout\ <= NOT \Add11~1_sumout\;
\ALT_INV_Add19~1_sumout\ <= NOT \Add19~1_sumout\;
\ALT_INV_puck_velocity_two.y\(2) <= NOT \puck_velocity_two.y\(2);
\ALT_INV_puck_velocity_two.y\(1) <= NOT \puck_velocity_two.y\(1);
\ALT_INV_puck_velocity_two.y\(0) <= NOT \puck_velocity_two.y\(0);
\ALT_INV_puck_velocity_two.y\(7) <= NOT \puck_velocity_two.y\(7);
\ALT_INV_puck_velocity_two.y\(3) <= NOT \puck_velocity_two.y\(3);
\ALT_INV_puck_velocity_two.y\(5) <= NOT \puck_velocity_two.y\(5);
\ALT_INV_puck_velocity_two.y\(4) <= NOT \puck_velocity_two.y\(4);
\ALT_INV_puck_velocity_two.x\(5) <= NOT \puck_velocity_two.x\(5);
\ALT_INV_puck_velocity_two.x\(6) <= NOT \puck_velocity_two.x\(6);
\ALT_INV_puck_velocity_two.x\(7) <= NOT \puck_velocity_two.x\(7);
\ALT_INV_puck_velocity_two.x\(1) <= NOT \puck_velocity_two.x\(1);
\ALT_INV_puck_velocity_two.x\(2) <= NOT \puck_velocity_two.x\(2);
\ALT_INV_puck_velocity_two.x\(3) <= NOT \puck_velocity_two.x\(3);
\ALT_INV_puck_velocity_two.x\(4) <= NOT \puck_velocity_two.x\(4);
\ALT_INV_puck_velocity_one.x\(5) <= NOT \puck_velocity_one.x\(5);
\ALT_INV_puck_velocity_one.x\(6) <= NOT \puck_velocity_one.x\(6);
\ALT_INV_puck_velocity_one.x\(7) <= NOT \puck_velocity_one.x\(7);
\ALT_INV_puck_velocity_one.x\(1) <= NOT \puck_velocity_one.x\(1);
\ALT_INV_puck_velocity_one.x\(2) <= NOT \puck_velocity_one.x\(2);
\ALT_INV_puck_velocity_one.x\(3) <= NOT \puck_velocity_one.x\(3);
\ALT_INV_puck_velocity_one.x\(4) <= NOT \puck_velocity_one.x\(4);
\ALT_INV_puck_velocity_one.y\(6) <= NOT \puck_velocity_one.y\(6);
\ALT_INV_puck_velocity_one.y\(5) <= NOT \puck_velocity_one.y\(5);
\ALT_INV_puck_velocity_one.y\(4) <= NOT \puck_velocity_one.y\(4);
\ALT_INV_puck_velocity_one.y\(7) <= NOT \puck_velocity_one.y\(7);
\ALT_INV_puck_velocity_one.y\(3) <= NOT \puck_velocity_one.y\(3);
\ALT_INV_puck_velocity_one.y\(2) <= NOT \puck_velocity_one.y\(2);
\ALT_INV_puck_velocity_one.y\(1) <= NOT \puck_velocity_one.y\(1);
\ALT_INV_puck_velocity_two.y\(6) <= NOT \puck_velocity_two.y\(6);
ALT_INV_paddle_counter(6) <= NOT paddle_counter(6);
ALT_INV_paddle_counter(5) <= NOT paddle_counter(5);
ALT_INV_paddle_counter(27) <= NOT paddle_counter(27);
ALT_INV_paddle_counter(31) <= NOT paddle_counter(31);
ALT_INV_paddle_counter(30) <= NOT paddle_counter(30);
ALT_INV_paddle_counter(28) <= NOT paddle_counter(28);
ALT_INV_paddle_counter(26) <= NOT paddle_counter(26);
ALT_INV_paddle_counter(25) <= NOT paddle_counter(25);
ALT_INV_paddle_counter(24) <= NOT paddle_counter(24);
ALT_INV_paddle_counter(23) <= NOT paddle_counter(23);
ALT_INV_paddle_counter(12) <= NOT paddle_counter(12);
ALT_INV_paddle_counter(11) <= NOT paddle_counter(11);
ALT_INV_paddle_counter(10) <= NOT paddle_counter(10);
ALT_INV_paddle_counter(0) <= NOT paddle_counter(0);
ALT_INV_paddle_counter(8) <= NOT paddle_counter(8);
ALT_INV_paddle_counter(13) <= NOT paddle_counter(13);
ALT_INV_paddle_counter(7) <= NOT paddle_counter(7);
ALT_INV_paddle_counter(29) <= NOT paddle_counter(29);
ALT_INV_paddle_counter(18) <= NOT paddle_counter(18);
ALT_INV_paddle_counter(17) <= NOT paddle_counter(17);
ALT_INV_paddle_counter(16) <= NOT paddle_counter(16);
ALT_INV_paddle_counter(14) <= NOT paddle_counter(14);
ALT_INV_paddle_counter(9) <= NOT paddle_counter(9);
ALT_INV_paddle_counter(2) <= NOT paddle_counter(2);
ALT_INV_paddle_counter(1) <= NOT paddle_counter(1);
ALT_INV_paddle_counter(15) <= NOT paddle_counter(15);
ALT_INV_paddle_counter(22) <= NOT paddle_counter(22);
ALT_INV_paddle_counter(21) <= NOT paddle_counter(21);
ALT_INV_paddle_counter(20) <= NOT paddle_counter(20);
ALT_INV_paddle_counter(4) <= NOT paddle_counter(4);
ALT_INV_paddle_counter(3) <= NOT paddle_counter(3);
ALT_INV_paddle_counter(19) <= NOT paddle_counter(19);
\ALT_INV_Add0~29_sumout\ <= NOT \Add0~29_sumout\;
\ALT_INV_puck_two.y\(7) <= NOT \puck_two.y\(7);
\ALT_INV_puck_one.y\(7) <= NOT \puck_one.y\(7);
\ALT_INV_Add0~25_sumout\ <= NOT \Add0~25_sumout\;
\ALT_INV_puck_two.y\(0) <= NOT \puck_two.y\(0);
\ALT_INV_puck_one.y\(0) <= NOT \puck_one.y\(0);
\ALT_INV_puck_one.y\(1) <= NOT \puck_one.y\(1);
\ALT_INV_puck_two.y\(1) <= NOT \puck_two.y\(1);
\ALT_INV_Add0~21_sumout\ <= NOT \Add0~21_sumout\;
\ALT_INV_puck_one.y\(2) <= NOT \puck_one.y\(2);
\ALT_INV_puck_two.y\(2) <= NOT \puck_two.y\(2);
\ALT_INV_Add0~17_sumout\ <= NOT \Add0~17_sumout\;
\ALT_INV_Add16~29_sumout\ <= NOT \Add16~29_sumout\;
\ALT_INV_Add16~25_sumout\ <= NOT \Add16~25_sumout\;
\ALT_INV_Add16~21_sumout\ <= NOT \Add16~21_sumout\;
\ALT_INV_Add16~17_sumout\ <= NOT \Add16~17_sumout\;
\ALT_INV_Add16~13_sumout\ <= NOT \Add16~13_sumout\;
\ALT_INV_Add16~9_sumout\ <= NOT \Add16~9_sumout\;
\ALT_INV_Add16~5_sumout\ <= NOT \Add16~5_sumout\;
\ALT_INV_Add15~29_sumout\ <= NOT \Add15~29_sumout\;
\ALT_INV_Add15~25_sumout\ <= NOT \Add15~25_sumout\;
\ALT_INV_Add15~21_sumout\ <= NOT \Add15~21_sumout\;
\ALT_INV_Add15~17_sumout\ <= NOT \Add15~17_sumout\;
\ALT_INV_Add15~13_sumout\ <= NOT \Add15~13_sumout\;
\ALT_INV_Add15~9_sumout\ <= NOT \Add15~9_sumout\;
\ALT_INV_Add15~5_sumout\ <= NOT \Add15~5_sumout\;
\ALT_INV_Add15~1_sumout\ <= NOT \Add15~1_sumout\;
\ALT_INV_Add9~29_sumout\ <= NOT \Add9~29_sumout\;
\ALT_INV_Add9~25_sumout\ <= NOT \Add9~25_sumout\;
\ALT_INV_Add9~21_sumout\ <= NOT \Add9~21_sumout\;
\ALT_INV_Add9~17_sumout\ <= NOT \Add9~17_sumout\;
\ALT_INV_Add9~13_sumout\ <= NOT \Add9~13_sumout\;
\ALT_INV_Add9~9_sumout\ <= NOT \Add9~9_sumout\;
\ALT_INV_Add9~5_sumout\ <= NOT \Add9~5_sumout\;
\ALT_INV_Add9~1_sumout\ <= NOT \Add9~1_sumout\;
\ALT_INV_Add10~29_sumout\ <= NOT \Add10~29_sumout\;
\ALT_INV_Add10~25_sumout\ <= NOT \Add10~25_sumout\;
\ALT_INV_Add10~21_sumout\ <= NOT \Add10~21_sumout\;
\ALT_INV_Add10~17_sumout\ <= NOT \Add10~17_sumout\;
\ALT_INV_Add10~13_sumout\ <= NOT \Add10~13_sumout\;
\ALT_INV_Add10~9_sumout\ <= NOT \Add10~9_sumout\;
\ALT_INV_Add10~5_sumout\ <= NOT \Add10~5_sumout\;
\ALT_INV_Add10~1_sumout\ <= NOT \Add10~1_sumout\;
ALT_INV_clock_counter(25) <= NOT clock_counter(25);
ALT_INV_clock_counter(24) <= NOT clock_counter(24);
ALT_INV_clock_counter(23) <= NOT clock_counter(23);
ALT_INV_clock_counter(30) <= NOT clock_counter(30);
ALT_INV_clock_counter(29) <= NOT clock_counter(29);
ALT_INV_clock_counter(28) <= NOT clock_counter(28);
ALT_INV_clock_counter(27) <= NOT clock_counter(27);
ALT_INV_clock_counter(26) <= NOT clock_counter(26);
ALT_INV_clock_counter(31) <= NOT clock_counter(31);
ALT_INV_clock_counter(22) <= NOT clock_counter(22);
ALT_INV_clock_counter(16) <= NOT clock_counter(16);
ALT_INV_clock_counter(17) <= NOT clock_counter(17);
ALT_INV_clock_counter(18) <= NOT clock_counter(18);
ALT_INV_clock_counter(19) <= NOT clock_counter(19);
ALT_INV_clock_counter(20) <= NOT clock_counter(20);
ALT_INV_clock_counter(9) <= NOT clock_counter(9);
ALT_INV_clock_counter(10) <= NOT clock_counter(10);
ALT_INV_clock_counter(11) <= NOT clock_counter(11);
ALT_INV_clock_counter(12) <= NOT clock_counter(12);
ALT_INV_clock_counter(4) <= NOT clock_counter(4);
ALT_INV_clock_counter(5) <= NOT clock_counter(5);
ALT_INV_clock_counter(7) <= NOT clock_counter(7);
ALT_INV_clock_counter(8) <= NOT clock_counter(8);
ALT_INV_clock_counter(6) <= NOT clock_counter(6);
ALT_INV_clock_counter(13) <= NOT clock_counter(13);
ALT_INV_clock_counter(14) <= NOT clock_counter(14);
ALT_INV_clock_counter(15) <= NOT clock_counter(15);
ALT_INV_clock_counter(21) <= NOT clock_counter(21);
\ALT_INV_Add16~1_sumout\ <= NOT \Add16~1_sumout\;
\ALT_INV_puck_one.x\(4) <= NOT \puck_one.x\(4);
\ALT_INV_puck_two.x\(4) <= NOT \puck_two.x\(4);
\ALT_INV_Add1~29_sumout\ <= NOT \Add1~29_sumout\;
\ALT_INV_Add7~25_sumout\ <= NOT \Add7~25_sumout\;
\ALT_INV_Add8~25_sumout\ <= NOT \Add8~25_sumout\;
\ALT_INV_puck_one.x\(3) <= NOT \puck_one.x\(3);
\ALT_INV_puck_two.x\(3) <= NOT \puck_two.x\(3);
\ALT_INV_Add1~25_sumout\ <= NOT \Add1~25_sumout\;
\ALT_INV_Add7~21_sumout\ <= NOT \Add7~21_sumout\;
\ALT_INV_Add8~21_sumout\ <= NOT \Add8~21_sumout\;
\ALT_INV_puck_one.x\(2) <= NOT \puck_one.x\(2);
\ALT_INV_puck_two.x\(2) <= NOT \puck_two.x\(2);
\ALT_INV_Add7~17_sumout\ <= NOT \Add7~17_sumout\;
\ALT_INV_Add8~17_sumout\ <= NOT \Add8~17_sumout\;
\ALT_INV_Add1~21_sumout\ <= NOT \Add1~21_sumout\;
\ALT_INV_puck_one.x\(1) <= NOT \puck_one.x\(1);
\ALT_INV_puck_two.x\(1) <= NOT \puck_two.x\(1);
\ALT_INV_Add1~17_sumout\ <= NOT \Add1~17_sumout\;
\ALT_INV_Add7~13_sumout\ <= NOT \Add7~13_sumout\;
\ALT_INV_Add8~13_sumout\ <= NOT \Add8~13_sumout\;
\ALT_INV_puck_two.x\(0) <= NOT \puck_two.x\(0);
\ALT_INV_puck_one.x\(0) <= NOT \puck_one.x\(0);
\ALT_INV_Add1~13_sumout\ <= NOT \Add1~13_sumout\;
\ALT_INV_puck_two.x\(5) <= NOT \puck_two.x\(5);
\ALT_INV_Add1~9_sumout\ <= NOT \Add1~9_sumout\;
\ALT_INV_puck_one.x\(5) <= NOT \puck_one.x\(5);
\ALT_INV_Add7~9_sumout\ <= NOT \Add7~9_sumout\;
\ALT_INV_Add8~9_sumout\ <= NOT \Add8~9_sumout\;
\ALT_INV_puck_two.x\(6) <= NOT \puck_two.x\(6);
\ALT_INV_Add1~5_sumout\ <= NOT \Add1~5_sumout\;
\ALT_INV_puck_one.x\(6) <= NOT \puck_one.x\(6);
\ALT_INV_Add7~5_sumout\ <= NOT \Add7~5_sumout\;
\ALT_INV_Add8~5_sumout\ <= NOT \Add8~5_sumout\;
\ALT_INV_puck_one.x\(7) <= NOT \puck_one.x\(7);
\ALT_INV_puck_two.x\(7) <= NOT \puck_two.x\(7);
\ALT_INV_Add1~1_sumout\ <= NOT \Add1~1_sumout\;
\ALT_INV_Add7~1_sumout\ <= NOT \Add7~1_sumout\;
\ALT_INV_Add8~1_sumout\ <= NOT \Add8~1_sumout\;
\ALT_INV_puck_one.y\(3) <= NOT \puck_one.y\(3);
\ALT_INV_puck_two.y\(3) <= NOT \puck_two.y\(3);
\ALT_INV_Add0~13_sumout\ <= NOT \Add0~13_sumout\;
\ALT_INV_puck_one.y\(4) <= NOT \puck_one.y\(4);
\ALT_INV_puck_two.y\(4) <= NOT \puck_two.y\(4);
\ALT_INV_Add0~9_sumout\ <= NOT \Add0~9_sumout\;
\ALT_INV_puck_one.y\(5) <= NOT \puck_one.y\(5);
\ALT_INV_puck_two.y\(5) <= NOT \puck_two.y\(5);
\ALT_INV_Add0~5_sumout\ <= NOT \Add0~5_sumout\;
\ALT_INV_Add13~25_sumout\ <= NOT \Add13~25_sumout\;
\ALT_INV_Add13~21_sumout\ <= NOT \Add13~21_sumout\;
\ALT_INV_Add13~17_sumout\ <= NOT \Add13~17_sumout\;
\ALT_INV_Add13~13_sumout\ <= NOT \Add13~13_sumout\;
\ALT_INV_Add13~9_sumout\ <= NOT \Add13~9_sumout\;
\ALT_INV_Add13~5_sumout\ <= NOT \Add13~5_sumout\;
\ALT_INV_Add13~1_sumout\ <= NOT \Add13~1_sumout\;
\ALT_INV_state.DRAW_PUCK_ONE~q\ <= NOT \state.DRAW_PUCK_ONE~q\;
\ALT_INV_state.DRAW_PUCK_TWO~q\ <= NOT \state.DRAW_PUCK_TWO~q\;
\ALT_INV_puck_one.y\(6) <= NOT \puck_one.y\(6);
\ALT_INV_puck_two.y\(6) <= NOT \puck_two.y\(6);
\ALT_INV_Add0~1_sumout\ <= NOT \Add0~1_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~5_sumout\;
\vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|controller|controller_translator|Add1~1_sumout\;
\vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\ <= NOT \vga_u0|user_input_translator|Add1~5_sumout\;
\vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\ <= NOT \vga_u0|user_input_translator|Add1~1_sumout\;
\vga_u0|controller|ALT_INV_yCounter\(0) <= NOT \vga_u0|controller|yCounter\(0);
\vga_u0|controller|ALT_INV_yCounter\(1) <= NOT \vga_u0|controller|yCounter\(1);
\vga_u0|controller|ALT_INV_yCounter\(2) <= NOT \vga_u0|controller|yCounter\(2);
\vga_u0|controller|ALT_INV_yCounter\(3) <= NOT \vga_u0|controller|yCounter\(3);
\vga_u0|controller|ALT_INV_yCounter\(4) <= NOT \vga_u0|controller|yCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(0) <= NOT \vga_u0|controller|xCounter\(0);
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a8\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\;
\vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\ <= NOT \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\;
\vga_u0|controller|ALT_INV_xCounter\(3) <= NOT \vga_u0|controller|xCounter\(3);
\vga_u0|controller|ALT_INV_xCounter\(2) <= NOT \vga_u0|controller|xCounter\(2);
\vga_u0|controller|ALT_INV_xCounter\(6) <= NOT \vga_u0|controller|xCounter\(6);
\vga_u0|controller|ALT_INV_xCounter\(5) <= NOT \vga_u0|controller|xCounter\(5);
\vga_u0|controller|ALT_INV_xCounter\(4) <= NOT \vga_u0|controller|xCounter\(4);
\vga_u0|controller|ALT_INV_xCounter\(1) <= NOT \vga_u0|controller|xCounter\(1);
\vga_u0|controller|ALT_INV_xCounter\(7) <= NOT \vga_u0|controller|xCounter\(7);
\vga_u0|controller|ALT_INV_xCounter\(8) <= NOT \vga_u0|controller|xCounter\(8);
\vga_u0|controller|ALT_INV_xCounter\(9) <= NOT \vga_u0|controller|xCounter\(9);
\vga_u0|controller|ALT_INV_yCounter\(5) <= NOT \vga_u0|controller|yCounter\(5);
\vga_u0|controller|ALT_INV_yCounter\(6) <= NOT \vga_u0|controller|yCounter\(6);
\vga_u0|controller|ALT_INV_yCounter\(7) <= NOT \vga_u0|controller|yCounter\(7);
\vga_u0|controller|ALT_INV_yCounter\(8) <= NOT \vga_u0|controller|yCounter\(8);
\vga_u0|controller|ALT_INV_yCounter\(9) <= NOT \vga_u0|controller|yCounter\(9);

-- Location: IOOBUF_X40_Y81_N53
\VGA_R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(0));

-- Location: IOOBUF_X38_Y81_N2
\VGA_R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(1));

-- Location: IOOBUF_X26_Y81_N59
\VGA_R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(2));

-- Location: IOOBUF_X38_Y81_N19
\VGA_R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(3));

-- Location: IOOBUF_X36_Y81_N36
\VGA_R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(4));

-- Location: IOOBUF_X22_Y81_N19
\VGA_R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(5));

-- Location: IOOBUF_X22_Y81_N2
\VGA_R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(6));

-- Location: IOOBUF_X26_Y81_N42
\VGA_R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(7));

-- Location: IOOBUF_X89_Y25_N39
\VGA_R[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(8));

-- Location: IOOBUF_X89_Y25_N56
\VGA_R[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_R[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_R(9));

-- Location: IOOBUF_X4_Y81_N19
\VGA_G[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(0));

-- Location: IOOBUF_X4_Y81_N2
\VGA_G[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(1));

-- Location: IOOBUF_X20_Y81_N19
\VGA_G[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(2));

-- Location: IOOBUF_X6_Y81_N2
\VGA_G[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(3));

-- Location: IOOBUF_X10_Y81_N59
\VGA_G[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(4));

-- Location: IOOBUF_X10_Y81_N42
\VGA_G[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(5));

-- Location: IOOBUF_X18_Y81_N42
\VGA_G[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(6));

-- Location: IOOBUF_X18_Y81_N59
\VGA_G[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(7));

-- Location: IOOBUF_X16_Y81_N19
\VGA_G[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(8));

-- Location: IOOBUF_X20_Y81_N53
\VGA_G[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_G[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_G(9));

-- Location: IOOBUF_X40_Y81_N36
\VGA_B[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(0));

-- Location: IOOBUF_X28_Y81_N19
\VGA_B[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(1));

-- Location: IOOBUF_X20_Y81_N2
\VGA_B[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(2));

-- Location: IOOBUF_X36_Y81_N19
\VGA_B[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(3));

-- Location: IOOBUF_X28_Y81_N2
\VGA_B[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(4));

-- Location: IOOBUF_X36_Y81_N2
\VGA_B[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(5));

-- Location: IOOBUF_X40_Y81_N19
\VGA_B[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(6));

-- Location: IOOBUF_X32_Y81_N19
\VGA_B[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(7));

-- Location: IOOBUF_X22_Y81_N53
\VGA_B[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(8));

-- Location: IOOBUF_X40_Y81_N2
\VGA_B[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_B[0]~0_combout\,
	devoe => ww_devoe,
	o => ww_VGA_B(9));

-- Location: IOOBUF_X36_Y81_N53
\VGA_HS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_HS~q\,
	devoe => ww_devoe,
	o => ww_VGA_HS);

-- Location: IOOBUF_X34_Y81_N42
\VGA_VS~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_VS~q\,
	devoe => ww_devoe,
	o => ww_VGA_VS);

-- Location: IOOBUF_X28_Y81_N53
\VGA_BLANK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|controller|VGA_BLANK~q\,
	devoe => ww_devoe,
	o => ww_VGA_BLANK);

-- Location: IOOBUF_X20_Y0_N53
\VGA_SYNC~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_VGA_SYNC);

-- Location: IOOBUF_X38_Y81_N36
\VGA_CLK~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	devoe => ww_devoe,
	o => ww_VGA_CLK);

-- Location: IOIBUF_X32_Y0_N1
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: CLKCTRL_G5
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: LABCELL_X35_Y70_N0
\Add2~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~125_sumout\ = SUM(( clock_counter(0) ) + ( VCC ) + ( !VCC ))
-- \Add2~126\ = CARRY(( clock_counter(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(0),
	cin => GND,
	sumout => \Add2~125_sumout\,
	cout => \Add2~126\);

-- Location: IOIBUF_X36_Y0_N1
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X40_Y0_N18
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: CLKCTRL_G7
\KEY[3]~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \KEY[3]~input_o\,
	outclk => \KEY[3]~inputCLKENA0_outclk\);

-- Location: LABCELL_X37_Y72_N33
\WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr5~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ ) ) # ( !\state.INIT~DUPLICATE_q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \WideOr5~0_combout\);

-- Location: FF_X36_Y69_N10
\paddle_width[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector107~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_width(3));

-- Location: LABCELL_X36_Y69_N12
\paddle_width~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_width~0_combout\ = ( \pflag~q\ & ( !paddle_width(2) $ (((!paddle_width(1)) # ((paddle_width(0)) # (paddle_width(3))))) ) ) # ( !\pflag~q\ & ( paddle_width(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001101100011001100110110001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_width(1),
	datab => ALT_INV_paddle_width(2),
	datac => ALT_INV_paddle_width(3),
	datad => ALT_INV_paddle_width(0),
	dataf => \ALT_INV_pflag~q\,
	combout => \paddle_width~0_combout\);

-- Location: LABCELL_X36_Y69_N6
\Selector108~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector108~0_combout\ = ( \paddle_width~0_combout\ & ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	dataf => \ALT_INV_paddle_width~0_combout\,
	combout => \Selector108~0_combout\);

-- Location: FF_X36_Y69_N8
\paddle_width[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector108~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_width(2));

-- Location: LABCELL_X36_Y69_N15
\paddle_width~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_width~1_combout\ = ( \pflag~q\ & ( (!\paddle_width[3]~DUPLICATE_q\ & ((!paddle_width(1)) # ((paddle_width(0)) # (paddle_width(2))))) ) ) # ( !\pflag~q\ & ( !\paddle_width[3]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000010110000111100001011000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_width(1),
	datab => ALT_INV_paddle_width(2),
	datac => \ALT_INV_paddle_width[3]~DUPLICATE_q\,
	datad => ALT_INV_paddle_width(0),
	dataf => \ALT_INV_pflag~q\,
	combout => \paddle_width~1_combout\);

-- Location: LABCELL_X36_Y69_N9
\Selector107~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector107~0_combout\ = (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & !\paddle_width~1_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000110000001100000011000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datac => \ALT_INV_paddle_width~1_combout\,
	combout => \Selector107~0_combout\);

-- Location: FF_X36_Y69_N11
\paddle_width[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector107~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \paddle_width[3]~DUPLICATE_q\);

-- Location: LABCELL_X36_Y69_N30
\Add13~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~13_sumout\ = SUM(( paddle_x(1) ) + ( !paddle_width(1) ) + ( !VCC ))
-- \Add13~14\ = CARRY(( paddle_x(1) ) + ( !paddle_width(1) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_width(1),
	datad => ALT_INV_paddle_x(1),
	cin => GND,
	sumout => \Add13~13_sumout\,
	cout => \Add13~14\);

-- Location: LABCELL_X36_Y69_N33
\Add13~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~17_sumout\ = SUM(( paddle_width(2) ) + ( paddle_x(2) ) + ( \Add13~14\ ))
-- \Add13~18\ = CARRY(( paddle_width(2) ) + ( paddle_x(2) ) + ( \Add13~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(2),
	datac => ALT_INV_paddle_width(2),
	cin => \Add13~14\,
	sumout => \Add13~17_sumout\,
	cout => \Add13~18\);

-- Location: LABCELL_X36_Y69_N36
\Add13~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~1_sumout\ = SUM(( paddle_x(3) ) + ( !\paddle_width[3]~DUPLICATE_q\ ) + ( \Add13~18\ ))
-- \Add13~2\ = CARRY(( paddle_x(3) ) + ( !\paddle_width[3]~DUPLICATE_q\ ) + ( \Add13~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_paddle_width[3]~DUPLICATE_q\,
	datad => ALT_INV_paddle_x(3),
	cin => \Add13~18\,
	sumout => \Add13~1_sumout\,
	cout => \Add13~2\);

-- Location: LABCELL_X35_Y72_N6
\Add7~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~21_sumout\ = SUM(( paddle_x(3) ) + ( GND ) + ( \Add7~18\ ))
-- \Add7~22\ = CARRY(( paddle_x(3) ) + ( GND ) + ( \Add7~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(3),
	cin => \Add7~18\,
	sumout => \Add7~21_sumout\,
	cout => \Add7~22\);

-- Location: LABCELL_X35_Y72_N9
\Add7~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~25_sumout\ = SUM(( !paddle_x(4) ) + ( GND ) + ( \Add7~22\ ))
-- \Add7~26\ = CARRY(( !paddle_x(4) ) + ( GND ) + ( \Add7~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(4),
	cin => \Add7~22\,
	sumout => \Add7~25_sumout\,
	cout => \Add7~26\);

-- Location: MLABCELL_X34_Y72_N3
\Add8~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~17_sumout\ = SUM(( paddle_x(2) ) + ( VCC ) + ( \Add8~14\ ))
-- \Add8~18\ = CARRY(( paddle_x(2) ) + ( VCC ) + ( \Add8~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(2),
	cin => \Add8~14\,
	sumout => \Add8~17_sumout\,
	cout => \Add8~18\);

-- Location: MLABCELL_X34_Y72_N6
\Add8~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~21_sumout\ = SUM(( paddle_x(3) ) + ( VCC ) + ( \Add8~18\ ))
-- \Add8~22\ = CARRY(( paddle_x(3) ) + ( VCC ) + ( \Add8~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(3),
	cin => \Add8~18\,
	sumout => \Add8~21_sumout\,
	cout => \Add8~22\);

-- Location: MLABCELL_X34_Y72_N9
\Add8~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~25_sumout\ = SUM(( !paddle_x(4) ) + ( VCC ) + ( \Add8~22\ ))
-- \Add8~26\ = CARRY(( !paddle_x(4) ) + ( VCC ) + ( \Add8~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(4),
	cin => \Add8~22\,
	sumout => \Add8~25_sumout\,
	cout => \Add8~26\);

-- Location: MLABCELL_X34_Y72_N51
\paddle_x~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_x~4_combout\ = ( \Add8~25_sumout\ & ( (\Add7~25_sumout\) # (\KEY[0]~input_o\) ) ) # ( !\Add8~25_sumout\ & ( (!\KEY[0]~input_o\ & \Add7~25_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_KEY[0]~input_o\,
	datad => \ALT_INV_Add7~25_sumout\,
	dataf => \ALT_INV_Add8~25_sumout\,
	combout => \paddle_x~4_combout\);

-- Location: LABCELL_X37_Y72_N24
\Selector19~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector19~0_combout\ = ( \paddle_x~4_combout\ & ( (\paddle_x~5_combout\ & (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & paddle_x(4))) ) ) # ( !\paddle_x~4_combout\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((!\paddle_x~5_combout\) # (paddle_x(4)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001111000011000000111100000000000000110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_paddle_x~5_combout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datad => ALT_INV_paddle_x(4),
	dataf => \ALT_INV_paddle_x~4_combout\,
	combout => \Selector19~0_combout\);

-- Location: FF_X37_Y72_N26
\paddle_x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector19~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(4));

-- Location: MLABCELL_X34_Y72_N12
\Add8~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~9_sumout\ = SUM(( paddle_x(5) ) + ( VCC ) + ( \Add8~26\ ))
-- \Add8~10\ = CARRY(( paddle_x(5) ) + ( VCC ) + ( \Add8~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(5),
	cin => \Add8~26\,
	sumout => \Add8~9_sumout\,
	cout => \Add8~10\);

-- Location: LABCELL_X35_Y72_N12
\Add7~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~9_sumout\ = SUM(( paddle_x(5) ) + ( GND ) + ( \Add7~26\ ))
-- \Add7~10\ = CARRY(( paddle_x(5) ) + ( GND ) + ( \Add7~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(5),
	cin => \Add7~26\,
	sumout => \Add7~9_sumout\,
	cout => \Add7~10\);

-- Location: LABCELL_X35_Y72_N45
\Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = ( \KEY[0]~input_o\ & ( \Add8~9_sumout\ ) ) # ( !\KEY[0]~input_o\ & ( \Add7~9_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add8~9_sumout\,
	datac => \ALT_INV_Add7~9_sumout\,
	dataf => \ALT_INV_KEY[0]~input_o\,
	combout => \Selector2~0_combout\);

-- Location: LABCELL_X37_Y72_N42
\Selector2~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~4_combout\ = ( \paddle_x~5_combout\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & paddle_x(5)) ) ) # ( !\paddle_x~5_combout\ & ( (\Selector2~0_combout\ & \state.DRAW_PADDLE_ENTER~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Selector2~0_combout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datad => ALT_INV_paddle_x(5),
	dataf => \ALT_INV_paddle_x~5_combout\,
	combout => \Selector2~4_combout\);

-- Location: FF_X37_Y72_N44
\paddle_x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector2~4_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(5));

-- Location: LABCELL_X36_Y69_N39
\Add13~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~5_sumout\ = SUM(( !paddle_x(4) ) + ( GND ) + ( \Add13~2\ ))
-- \Add13~6\ = CARRY(( !paddle_x(4) ) + ( GND ) + ( \Add13~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(4),
	cin => \Add13~2\,
	sumout => \Add13~5_sumout\,
	cout => \Add13~6\);

-- Location: LABCELL_X36_Y69_N42
\Add13~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~9_sumout\ = SUM(( paddle_x(5) ) + ( GND ) + ( \Add13~6\ ))
-- \Add13~10\ = CARRY(( paddle_x(5) ) + ( GND ) + ( \Add13~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(5),
	cin => \Add13~6\,
	sumout => \Add13~9_sumout\,
	cout => \Add13~10\);

-- Location: LABCELL_X36_Y69_N45
\Add13~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~21_sumout\ = SUM(( !paddle_x(6) ) + ( GND ) + ( \Add13~10\ ))
-- \Add13~22\ = CARRY(( !paddle_x(6) ) + ( GND ) + ( \Add13~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(6),
	cin => \Add13~10\,
	sumout => \Add13~21_sumout\,
	cout => \Add13~22\);

-- Location: MLABCELL_X34_Y69_N30
\Add18~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~17_sumout\ = SUM(( !\puck_velocity_two.x\(0) $ (((\Equal11~0_combout\) # (\Equal10~0_combout\))) ) + ( (\Equal11~0_combout\) # (\Equal10~0_combout\) ) + ( !VCC ))
-- \Add18~18\ = CARRY(( !\puck_velocity_two.x\(0) $ (((\Equal11~0_combout\) # (\Equal10~0_combout\))) ) + ( (\Equal11~0_combout\) # (\Equal10~0_combout\) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000100010001000100000000000000000001000011110000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal10~0_combout\,
	datab => \ALT_INV_Equal11~0_combout\,
	datac => \ALT_INV_puck_velocity_two.x\(0),
	cin => GND,
	sumout => \Add18~17_sumout\,
	cout => \Add18~18\);

-- Location: LABCELL_X33_Y70_N30
\Add12~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~29_sumout\ = SUM(( !\puck_velocity_one.x\(0) $ (((\Equal7~0_combout\) # (\Equal6~0_combout\))) ) + ( (\Equal7~0_combout\) # (\Equal6~0_combout\) ) + ( !VCC ))
-- \Add12~30\ = CARRY(( !\puck_velocity_one.x\(0) $ (((\Equal7~0_combout\) # (\Equal6~0_combout\))) ) + ( (\Equal7~0_combout\) # (\Equal6~0_combout\) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000100010001000100000000000000000001000011110000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datac => \ALT_INV_puck_velocity_one.x\(0),
	cin => GND,
	sumout => \Add12~29_sumout\,
	cout => \Add12~30\);

-- Location: LABCELL_X33_Y70_N3
\Selector46~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector46~0_combout\ = ( \state.ERASE_PUCK_ONE~q\ & ( !\Add12~29_sumout\ ) ) # ( !\state.ERASE_PUCK_ONE~q\ & ( (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_Add12~29_sumout\,
	datad => \ALT_INV_puck_velocity_one.x\(0),
	dataf => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	combout => \Selector46~0_combout\);

-- Location: FF_X33_Y70_N5
\puck_velocity_one.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector46~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(0));

-- Location: LABCELL_X33_Y70_N33
\Add12~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~13_sumout\ = SUM(( !\puck_velocity_one.x\(1) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~30\ ))
-- \Add12~14\ = CARRY(( !\puck_velocity_one.x\(1) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111100001111000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datac => \ALT_INV_puck_velocity_one.x\(1),
	cin => \Add12~30\,
	sumout => \Add12~13_sumout\,
	cout => \Add12~14\);

-- Location: LABCELL_X33_Y70_N21
\Selector45~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector45~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(1))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_velocity_one.x\(1),
	combout => \Selector45~0_combout\);

-- Location: FF_X33_Y70_N34
\puck_velocity_one.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~13_sumout\,
	asdata => \Selector45~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(1));

-- Location: LABCELL_X33_Y72_N45
\Selector29~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector29~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_one.x\(1))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_one.x\(1),
	combout => \Selector29~0_combout\);

-- Location: FF_X34_Y70_N5
\puck_one.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~13_sumout\,
	asdata => \Selector29~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(1));

-- Location: FF_X33_Y70_N4
\puck_velocity_one.x[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector46~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x[0]~DUPLICATE_q\);

-- Location: MLABCELL_X34_Y70_N0
\Add9~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~29_sumout\ = SUM(( \puck_one.x\(0) ) + ( !\puck_velocity_one.x[0]~DUPLICATE_q\ ) + ( !VCC ))
-- \Add9~30\ = CARRY(( \puck_one.x\(0) ) + ( !\puck_velocity_one.x[0]~DUPLICATE_q\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_one.x[0]~DUPLICATE_q\,
	datad => \ALT_INV_puck_one.x\(0),
	cin => GND,
	sumout => \Add9~29_sumout\,
	cout => \Add9~30\);

-- Location: LABCELL_X33_Y72_N42
\Selector30~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector30~0_combout\ = ( \puck_one.x\(0) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_one.x\(0),
	combout => \Selector30~0_combout\);

-- Location: FF_X34_Y70_N2
\puck_one.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~29_sumout\,
	asdata => \Selector30~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(0));

-- Location: MLABCELL_X34_Y70_N3
\Add9~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~13_sumout\ = SUM(( \puck_one.x\(1) ) + ( \puck_velocity_one.x\(1) ) + ( \Add9~30\ ))
-- \Add9~14\ = CARRY(( \puck_one.x\(1) ) + ( \puck_velocity_one.x\(1) ) + ( \Add9~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.x\(1),
	datad => \ALT_INV_puck_one.x\(1),
	cin => \Add9~30\,
	sumout => \Add9~13_sumout\,
	cout => \Add9~14\);

-- Location: LABCELL_X33_Y70_N9
\Selector44~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector44~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(2))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_one.x\(2),
	combout => \Selector44~0_combout\);

-- Location: FF_X33_Y70_N38
\puck_velocity_one.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~9_sumout\,
	asdata => \Selector44~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(2));

-- Location: LABCELL_X33_Y70_N36
\Add12~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~9_sumout\ = SUM(( !\puck_velocity_one.x\(2) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~14\ ))
-- \Add12~10\ = CARRY(( !\puck_velocity_one.x\(2) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111100001111000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datac => \ALT_INV_puck_velocity_one.x\(2),
	cin => \Add12~14\,
	sumout => \Add12~9_sumout\,
	cout => \Add12~10\);

-- Location: FF_X33_Y70_N37
\puck_velocity_one.x[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~9_sumout\,
	asdata => \Selector44~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x[2]~DUPLICATE_q\);

-- Location: LABCELL_X33_Y72_N21
\Selector28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector28~0_combout\ = ( \puck_one.x\(2) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \ALT_INV_puck_one.x\(2),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector28~0_combout\);

-- Location: FF_X34_Y70_N8
\puck_one.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~9_sumout\,
	asdata => \Selector28~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(2));

-- Location: MLABCELL_X34_Y70_N6
\Add9~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~9_sumout\ = SUM(( \puck_one.x\(2) ) + ( \puck_velocity_one.x[2]~DUPLICATE_q\ ) + ( \Add9~14\ ))
-- \Add9~10\ = CARRY(( \puck_one.x\(2) ) + ( \puck_velocity_one.x[2]~DUPLICATE_q\ ) + ( \Add9~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.x[2]~DUPLICATE_q\,
	datad => \ALT_INV_puck_one.x\(2),
	cin => \Add9~14\,
	sumout => \Add9~9_sumout\,
	cout => \Add9~10\);

-- Location: MLABCELL_X34_Y70_N27
\Equal7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal7~1_combout\ = (\Add9~13_sumout\ & (!\Add9~9_sumout\ & !\Add9~29_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000000000010001000000000001000100000000000100010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~13_sumout\,
	datab => \ALT_INV_Add9~9_sumout\,
	datad => \ALT_INV_Add9~29_sumout\,
	combout => \Equal7~1_combout\);

-- Location: LABCELL_X33_Y70_N0
\Selector40~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector40~0_combout\ = ( \puck_velocity_one.x\(6) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_velocity_one.x\(6),
	combout => \Selector40~0_combout\);

-- Location: FF_X33_Y70_N50
\puck_velocity_one.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~21_sumout\,
	asdata => \Selector40~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(6));

-- Location: LABCELL_X33_Y70_N39
\Add12~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~5_sumout\ = SUM(( !\puck_velocity_one.x\(3) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~10\ ))
-- \Add12~6\ = CARRY(( !\puck_velocity_one.x\(3) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111100001111000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datac => \ALT_INV_puck_velocity_one.x\(3),
	cin => \Add12~10\,
	sumout => \Add12~5_sumout\,
	cout => \Add12~6\);

-- Location: LABCELL_X33_Y70_N42
\Add12~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~1_sumout\ = SUM(( !\puck_velocity_one.x\(4) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~6\ ))
-- \Add12~2\ = CARRY(( !\puck_velocity_one.x\(4) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datad => \ALT_INV_puck_velocity_one.x\(4),
	cin => \Add12~6\,
	sumout => \Add12~1_sumout\,
	cout => \Add12~2\);

-- Location: LABCELL_X33_Y70_N6
\Selector42~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector42~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(4))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_one.x\(4),
	combout => \Selector42~0_combout\);

-- Location: FF_X33_Y70_N43
\puck_velocity_one.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~1_sumout\,
	asdata => \Selector42~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(4));

-- Location: LABCELL_X33_Y70_N45
\Add12~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~25_sumout\ = SUM(( !\puck_velocity_one.x\(5) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~2\ ))
-- \Add12~26\ = CARRY(( !\puck_velocity_one.x\(5) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datad => \ALT_INV_puck_velocity_one.x\(5),
	cin => \Add12~2\,
	sumout => \Add12~25_sumout\,
	cout => \Add12~26\);

-- Location: LABCELL_X33_Y70_N18
\Selector41~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector41~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(5))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_one.x\(5),
	combout => \Selector41~0_combout\);

-- Location: FF_X33_Y70_N47
\puck_velocity_one.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~25_sumout\,
	asdata => \Selector41~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(5));

-- Location: LABCELL_X33_Y70_N48
\Add12~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~21_sumout\ = SUM(( !\puck_velocity_one.x\(6) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~26\ ))
-- \Add12~22\ = CARRY(( !\puck_velocity_one.x\(6) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datad => \ALT_INV_puck_velocity_one.x\(6),
	cin => \Add12~26\,
	sumout => \Add12~21_sumout\,
	cout => \Add12~22\);

-- Location: FF_X33_Y70_N49
\puck_velocity_one.x[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~21_sumout\,
	asdata => \Selector40~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x[6]~DUPLICATE_q\);

-- Location: LABCELL_X35_Y72_N42
\Selector24~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector24~0_combout\ = ( \puck_one.x\(6) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_one.x\(6),
	combout => \Selector24~0_combout\);

-- Location: FF_X34_Y70_N20
\puck_one.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~21_sumout\,
	asdata => \Selector24~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(6));

-- Location: FF_X33_Y70_N46
\puck_velocity_one.x[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~25_sumout\,
	asdata => \Selector41~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x[5]~DUPLICATE_q\);

-- Location: MLABCELL_X34_Y71_N48
\Add9~5_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~5_wirecell_combout\ = !\Add9~5_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add9~5_sumout\,
	combout => \Add9~5_wirecell_combout\);

-- Location: LABCELL_X27_Y71_N30
\~GND\ : cyclonev_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: MLABCELL_X34_Y71_N51
\WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr6~0_combout\ = (!\state.INIT~DUPLICATE_q\) # (\state.ERASE_PUCK_ONE~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101111101011111010111110101111101011111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	datac => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \WideOr6~0_combout\);

-- Location: FF_X34_Y71_N49
\puck_one.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~5_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	ena => \WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(3));

-- Location: MLABCELL_X34_Y70_N9
\Add9~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~5_sumout\ = SUM(( \puck_velocity_one.x\(3) ) + ( !\puck_one.x\(3) ) + ( \Add9~10\ ))
-- \Add9~6\ = CARRY(( \puck_velocity_one.x\(3) ) + ( !\puck_one.x\(3) ) + ( \Add9~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_one.x\(3),
	datac => \ALT_INV_puck_one.x\(3),
	cin => \Add9~10\,
	sumout => \Add9~5_sumout\,
	cout => \Add9~6\);

-- Location: MLABCELL_X34_Y70_N12
\Add9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~1_sumout\ = SUM(( \puck_one.x\(4) ) + ( \puck_velocity_one.x\(4) ) + ( \Add9~6\ ))
-- \Add9~2\ = CARRY(( \puck_one.x\(4) ) + ( \puck_velocity_one.x\(4) ) + ( \Add9~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_one.x\(4),
	datad => \ALT_INV_puck_one.x\(4),
	cin => \Add9~6\,
	sumout => \Add9~1_sumout\,
	cout => \Add9~2\);

-- Location: LABCELL_X33_Y72_N54
\Selector26~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector26~0_combout\ = ( \puck_one.x\(4) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \ALT_INV_puck_one.x\(4),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector26~0_combout\);

-- Location: FF_X34_Y70_N14
\puck_one.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~1_sumout\,
	asdata => \Selector26~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(4));

-- Location: MLABCELL_X34_Y70_N15
\Add9~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~25_sumout\ = SUM(( \puck_velocity_one.x[5]~DUPLICATE_q\ ) + ( !\puck_one.x\(5) ) + ( \Add9~2\ ))
-- \Add9~26\ = CARRY(( \puck_velocity_one.x[5]~DUPLICATE_q\ ) + ( !\puck_one.x\(5) ) + ( \Add9~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_one.x[5]~DUPLICATE_q\,
	datac => \ALT_INV_puck_one.x\(5),
	cin => \Add9~2\,
	sumout => \Add9~25_sumout\,
	cout => \Add9~26\);

-- Location: MLABCELL_X34_Y71_N15
\Add9~25_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~25_wirecell_combout\ = !\Add9~25_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~25_sumout\,
	combout => \Add9~25_wirecell_combout\);

-- Location: FF_X34_Y71_N16
\puck_one.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~25_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	ena => \WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(5));

-- Location: MLABCELL_X34_Y70_N18
\Add9~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~21_sumout\ = SUM(( \puck_one.x\(6) ) + ( \puck_velocity_one.x[6]~DUPLICATE_q\ ) + ( \Add9~26\ ))
-- \Add9~22\ = CARRY(( \puck_one.x\(6) ) + ( \puck_velocity_one.x[6]~DUPLICATE_q\ ) + ( \Add9~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_one.x[6]~DUPLICATE_q\,
	datad => \ALT_INV_puck_one.x\(6),
	cin => \Add9~26\,
	sumout => \Add9~21_sumout\,
	cout => \Add9~22\);

-- Location: LABCELL_X33_Y70_N54
\Equal7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal7~0_combout\ = ( !\Add9~25_sumout\ & ( \Add9~5_sumout\ & ( (\Add9~17_sumout\ & (\Equal7~1_combout\ & (!\Add9~21_sumout\ & \Add9~1_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~17_sumout\,
	datab => \ALT_INV_Equal7~1_combout\,
	datac => \ALT_INV_Add9~21_sumout\,
	datad => \ALT_INV_Add9~1_sumout\,
	datae => \ALT_INV_Add9~25_sumout\,
	dataf => \ALT_INV_Add9~5_sumout\,
	combout => \Equal7~0_combout\);

-- Location: LABCELL_X33_Y70_N51
\Add12~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add12~17_sumout\ = SUM(( !\puck_velocity_one.x\(7) $ (((!\Equal6~0_combout\ & !\Equal7~0_combout\))) ) + ( GND ) + ( \Add12~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal6~0_combout\,
	datab => \ALT_INV_Equal7~0_combout\,
	datad => \ALT_INV_puck_velocity_one.x\(7),
	cin => \Add12~22\,
	sumout => \Add12~17_sumout\);

-- Location: LABCELL_X33_Y70_N15
\Selector39~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector39~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(7))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_velocity_one.x\(7),
	combout => \Selector39~0_combout\);

-- Location: FF_X33_Y70_N52
\puck_velocity_one.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~17_sumout\,
	asdata => \Selector39~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(7));

-- Location: LABCELL_X33_Y72_N6
\Selector23~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector23~0_combout\ = ( \puck_one.x\(7) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_one.x\(7),
	combout => \Selector23~0_combout\);

-- Location: FF_X34_Y70_N23
\puck_one.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add9~17_sumout\,
	asdata => \Selector23~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.x\(7));

-- Location: MLABCELL_X34_Y70_N21
\Add9~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add9~17_sumout\ = SUM(( \puck_one.x\(7) ) + ( \puck_velocity_one.x\(7) ) + ( \Add9~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_one.x\(7),
	datad => \ALT_INV_puck_one.x\(7),
	cin => \Add9~22\,
	sumout => \Add9~17_sumout\);

-- Location: MLABCELL_X34_Y70_N24
\Equal6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~1_combout\ = (\Add9~13_sumout\ & (\Add9~9_sumout\ & !\Add9~29_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010000000100000001000000010000000100000001000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~13_sumout\,
	datab => \ALT_INV_Add9~9_sumout\,
	datac => \ALT_INV_Add9~29_sumout\,
	combout => \Equal6~1_combout\);

-- Location: LABCELL_X33_Y70_N24
\Equal6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal6~0_combout\ = ( !\Add9~25_sumout\ & ( \Equal6~1_combout\ & ( (!\Add9~17_sumout\ & (!\Add9~5_sumout\ & (!\Add9~21_sumout\ & !\Add9~1_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~17_sumout\,
	datab => \ALT_INV_Add9~5_sumout\,
	datac => \ALT_INV_Add9~21_sumout\,
	datad => \ALT_INV_Add9~1_sumout\,
	datae => \ALT_INV_Add9~25_sumout\,
	dataf => \ALT_INV_Equal6~1_combout\,
	combout => \Equal6~0_combout\);

-- Location: LABCELL_X33_Y70_N12
\Selector43~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector43~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.x\(3))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_one.x\(3),
	combout => \Selector43~0_combout\);

-- Location: FF_X33_Y70_N40
\puck_velocity_one.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add12~5_sumout\,
	asdata => \Selector43~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.x\(3));

-- Location: MLABCELL_X34_Y70_N57
\always0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~0_combout\ = ( paddle_x(1) & ( (!paddle_x(2) & (!\Add9~9_sumout\ & !\Add9~13_sumout\)) # (paddle_x(2) & ((!\Add9~9_sumout\) # (!\Add9~13_sumout\))) ) ) # ( !paddle_x(1) & ( (paddle_x(2) & !\Add9~9_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000010100000101000011110101010100001111010101010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(2),
	datac => \ALT_INV_Add9~9_sumout\,
	datad => \ALT_INV_Add9~13_sumout\,
	dataf => ALT_INV_paddle_x(1),
	combout => \always0~0_combout\);

-- Location: LABCELL_X35_Y72_N24
\always0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~1_combout\ = ( paddle_x(4) & ( (!\Add9~1_sumout\ & ((!\Add9~5_sumout\ & ((\always0~0_combout\) # (paddle_x(3)))) # (\Add9~5_sumout\ & (paddle_x(3) & \always0~0_combout\)))) ) ) # ( !paddle_x(4) & ( (!\Add9~1_sumout\) # ((!\Add9~5_sumout\ & 
-- ((\always0~0_combout\) # (paddle_x(3)))) # (\Add9~5_sumout\ & (paddle_x(3) & \always0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100101011111111110010101100101011000000000010101100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~5_sumout\,
	datab => ALT_INV_paddle_x(3),
	datac => \ALT_INV_always0~0_combout\,
	datad => \ALT_INV_Add9~1_sumout\,
	dataf => ALT_INV_paddle_x(4),
	combout => \always0~1_combout\);

-- Location: MLABCELL_X34_Y72_N24
\always0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~2_combout\ = ( paddle_x(7) & ( paddle_x(6) & ( (\Add9~17_sumout\ & (!\Add9~21_sumout\ & (!\Add9~25_sumout\ $ (paddle_x(5))))) ) ) ) # ( !paddle_x(7) & ( paddle_x(6) & ( (!\Add9~17_sumout\ & (!\Add9~21_sumout\ & (!\Add9~25_sumout\ $ 
-- (paddle_x(5))))) ) ) ) # ( paddle_x(7) & ( !paddle_x(6) & ( (\Add9~17_sumout\ & (\Add9~21_sumout\ & (!\Add9~25_sumout\ $ (paddle_x(5))))) ) ) ) # ( !paddle_x(7) & ( !paddle_x(6) & ( (!\Add9~17_sumout\ & (\Add9~21_sumout\ & (!\Add9~25_sumout\ $ 
-- (paddle_x(5))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000100000000100000000110000000010000000010000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~25_sumout\,
	datab => \ALT_INV_Add9~17_sumout\,
	datac => \ALT_INV_Add9~21_sumout\,
	datad => ALT_INV_paddle_x(5),
	datae => ALT_INV_paddle_x(7),
	dataf => ALT_INV_paddle_x(6),
	combout => \always0~2_combout\);

-- Location: MLABCELL_X34_Y70_N42
\always0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~5_combout\ = ( \Add13~5_sumout\ & ( \Add13~1_sumout\ & ( (\Add9~5_sumout\ & (\Add9~9_sumout\ & (!\Add13~17_sumout\ & \Add9~1_sumout\))) ) ) ) # ( !\Add13~5_sumout\ & ( \Add13~1_sumout\ & ( ((\Add9~5_sumout\ & (\Add9~9_sumout\ & 
-- !\Add13~17_sumout\))) # (\Add9~1_sumout\) ) ) ) # ( \Add13~5_sumout\ & ( !\Add13~1_sumout\ & ( (\Add9~1_sumout\ & (((\Add9~9_sumout\ & !\Add13~17_sumout\)) # (\Add9~5_sumout\))) ) ) ) # ( !\Add13~5_sumout\ & ( !\Add13~1_sumout\ & ( (((\Add9~9_sumout\ & 
-- !\Add13~17_sumout\)) # (\Add9~1_sumout\)) # (\Add9~5_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111010111111111000000000111010100010000111111110000000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~5_sumout\,
	datab => \ALT_INV_Add9~9_sumout\,
	datac => \ALT_INV_Add13~17_sumout\,
	datad => \ALT_INV_Add9~1_sumout\,
	datae => \ALT_INV_Add13~5_sumout\,
	dataf => \ALT_INV_Add13~1_sumout\,
	combout => \always0~5_combout\);

-- Location: MLABCELL_X34_Y70_N54
\always0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~3_combout\ = ( \Add13~1_sumout\ & ( (\Add9~5_sumout\ & (!\Add9~1_sumout\ $ (\Add13~5_sumout\))) ) ) # ( !\Add13~1_sumout\ & ( (!\Add9~5_sumout\ & (!\Add9~1_sumout\ $ (\Add13~5_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000110000110000000011000000001100000000110000110000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add9~1_sumout\,
	datac => \ALT_INV_Add9~5_sumout\,
	datad => \ALT_INV_Add13~5_sumout\,
	dataf => \ALT_INV_Add13~1_sumout\,
	combout => \always0~3_combout\);

-- Location: MLABCELL_X34_Y70_N30
\LessThan5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan5~0_combout\ = ( \Add13~21_sumout\ & ( (\Add9~21_sumout\ & (!\Add9~17_sumout\ $ (\Add13~25_sumout\))) ) ) # ( !\Add13~21_sumout\ & ( (!\Add9~21_sumout\ & (!\Add9~17_sumout\ $ (\Add13~25_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100000100010100010000010001001000100000100010100010000010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~21_sumout\,
	datab => \ALT_INV_Add9~17_sumout\,
	datad => \ALT_INV_Add13~25_sumout\,
	dataf => \ALT_INV_Add13~21_sumout\,
	combout => \LessThan5~0_combout\);

-- Location: MLABCELL_X34_Y70_N36
\always0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~4_combout\ = ( \Add9~13_sumout\ & ( \Add13~17_sumout\ & ( (\Add9~9_sumout\ & ((!\Add13~13_sumout\) # ((\Add9~29_sumout\ & !paddle_width(0))))) ) ) ) # ( !\Add9~13_sumout\ & ( \Add13~17_sumout\ & ( (!\Add13~13_sumout\ & (\Add9~9_sumout\ & 
-- (\Add9~29_sumout\ & !paddle_width(0)))) ) ) ) # ( \Add9~13_sumout\ & ( !\Add13~17_sumout\ & ( (!\Add9~9_sumout\ & ((!\Add13~13_sumout\) # ((\Add9~29_sumout\ & !paddle_width(0))))) ) ) ) # ( !\Add9~13_sumout\ & ( !\Add13~17_sumout\ & ( (!\Add13~13_sumout\ 
-- & (!\Add9~9_sumout\ & (\Add9~29_sumout\ & !paddle_width(0)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000000100011001000100000000010000000000010001100100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~13_sumout\,
	datab => \ALT_INV_Add9~9_sumout\,
	datac => \ALT_INV_Add9~29_sumout\,
	datad => ALT_INV_paddle_width(0),
	datae => \ALT_INV_Add9~13_sumout\,
	dataf => \ALT_INV_Add13~17_sumout\,
	combout => \always0~4_combout\);

-- Location: MLABCELL_X34_Y70_N48
\always0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~6_combout\ = ( \always0~4_combout\ & ( \Add9~25_sumout\ & ( (\Add13~9_sumout\ & (\LessThan5~0_combout\ & ((\always0~3_combout\) # (\always0~5_combout\)))) ) ) ) # ( !\always0~4_combout\ & ( \Add9~25_sumout\ & ( (\Add13~9_sumout\ & 
-- (\always0~5_combout\ & \LessThan5~0_combout\)) ) ) ) # ( \always0~4_combout\ & ( !\Add9~25_sumout\ & ( (!\Add13~9_sumout\ & (\LessThan5~0_combout\ & ((\always0~3_combout\) # (\always0~5_combout\)))) ) ) ) # ( !\always0~4_combout\ & ( !\Add9~25_sumout\ & ( 
-- (!\Add13~9_sumout\ & (\always0~5_combout\ & \LessThan5~0_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100010000000000010101000000000000100010000000000010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~9_sumout\,
	datab => \ALT_INV_always0~5_combout\,
	datac => \ALT_INV_always0~3_combout\,
	datad => \ALT_INV_LessThan5~0_combout\,
	datae => \ALT_INV_always0~4_combout\,
	dataf => \ALT_INV_Add9~25_sumout\,
	combout => \always0~6_combout\);

-- Location: LABCELL_X36_Y69_N54
\always0~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~7_combout\ = ( paddle_x(6) & ( paddle_x(7) & ( (\Add13~25_sumout\ & (\Add9~17_sumout\ & ((!\Add9~21_sumout\) # (\Add13~21_sumout\)))) ) ) ) # ( !paddle_x(6) & ( paddle_x(7) & ( (\Add13~25_sumout\ & (\Add9~17_sumout\ & (\Add9~21_sumout\ & 
-- \Add13~21_sumout\))) ) ) ) # ( paddle_x(6) & ( !paddle_x(7) & ( (!\Add13~25_sumout\ & (!\Add9~17_sumout\ & ((!\Add9~21_sumout\) # (\Add13~21_sumout\)))) # (\Add13~25_sumout\ & ((!\Add9~17_sumout\) # ((!\Add9~21_sumout\) # (\Add13~21_sumout\)))) ) ) ) # ( 
-- !paddle_x(6) & ( !paddle_x(7) & ( (!\Add9~17_sumout\ & (\Add9~21_sumout\ & ((\Add13~21_sumout\) # (\Add13~25_sumout\)))) # (\Add9~17_sumout\ & (\Add13~25_sumout\ & ((!\Add9~21_sumout\) # (\Add13~21_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010000011101110101001101110100000000000000010001000000010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~25_sumout\,
	datab => \ALT_INV_Add9~17_sumout\,
	datac => \ALT_INV_Add9~21_sumout\,
	datad => \ALT_INV_Add13~21_sumout\,
	datae => ALT_INV_paddle_x(6),
	dataf => ALT_INV_paddle_x(7),
	combout => \always0~7_combout\);

-- Location: MLABCELL_X34_Y70_N33
\LessThan4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan4~0_combout\ = ( paddle_x(6) & ( (!\Add9~21_sumout\ & (!\Add9~17_sumout\ $ (paddle_x(7)))) ) ) # ( !paddle_x(6) & ( (\Add9~21_sumout\ & (!\Add9~17_sumout\ $ (paddle_x(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000101000001010000010100000110000010100000101000001010000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add9~21_sumout\,
	datab => \ALT_INV_Add9~17_sumout\,
	datac => ALT_INV_paddle_x(7),
	dataf => ALT_INV_paddle_x(6),
	combout => \LessThan4~0_combout\);

-- Location: LABCELL_X36_Y69_N24
\always0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \always0~8_combout\ = ( paddle_x(5) & ( \LessThan5~0_combout\ & ( (\always0~7_combout\ & ((!\Add9~25_sumout\ & ((!\LessThan4~0_combout\))) # (\Add9~25_sumout\ & (\Add13~9_sumout\)))) ) ) ) # ( !paddle_x(5) & ( \LessThan5~0_combout\ & ( 
-- (\always0~7_combout\ & ((!\Add9~25_sumout\) # (\Add13~9_sumout\))) ) ) ) # ( paddle_x(5) & ( !\LessThan5~0_combout\ & ( (\always0~7_combout\ & ((!\LessThan4~0_combout\) # (\Add9~25_sumout\))) ) ) ) # ( !paddle_x(5) & ( !\LessThan5~0_combout\ & ( 
-- \always0~7_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010000010101010001010100010101000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_always0~7_combout\,
	datab => \ALT_INV_Add13~9_sumout\,
	datac => \ALT_INV_Add9~25_sumout\,
	datad => \ALT_INV_LessThan4~0_combout\,
	datae => ALT_INV_paddle_x(5),
	dataf => \ALT_INV_LessThan5~0_combout\,
	combout => \always0~8_combout\);

-- Location: MLABCELL_X34_Y71_N9
\Add10~13_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~13_wirecell_combout\ = !\Add10~13_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add10~13_sumout\,
	combout => \Add10~13_wirecell_combout\);

-- Location: FF_X34_Y71_N10
\puck_one.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~13_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	ena => \WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(3));

-- Location: MLABCELL_X39_Y69_N0
\Add11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~1_sumout\ = SUM(( !\Equal5~2_combout\ $ (\puck_velocity_one.y\(0)) ) + ( \Equal5~2_combout\ ) + ( !VCC ))
-- \Add11~2\ = CARRY(( !\Equal5~2_combout\ $ (\puck_velocity_one.y\(0)) ) + ( \Equal5~2_combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000001100110000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	datad => \ALT_INV_puck_velocity_one.y\(0),
	cin => GND,
	sumout => \Add11~1_sumout\,
	cout => \Add11~2\);

-- Location: MLABCELL_X39_Y69_N30
\Add14~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~1_sumout\ = SUM(( !\Add11~1_sumout\ ) + ( VCC ) + ( !VCC ))
-- \Add14~2\ = CARRY(( !\Add11~1_sumout\ ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add11~1_sumout\,
	cin => GND,
	sumout => \Add14~1_sumout\,
	cout => \Add14~2\);

-- Location: MLABCELL_X39_Y70_N54
\Selector54~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector54~0_combout\ = ( \puck_velocity_one.y\(0) & ( \puck_velocity_one~0_combout\ & ( (!\state.ERASE_PUCK_ONE~q\ & ((\state.INIT~DUPLICATE_q\))) # (\state.ERASE_PUCK_ONE~q\ & (!\Add14~1_sumout\)) ) ) ) # ( !\puck_velocity_one.y\(0) & ( 
-- \puck_velocity_one~0_combout\ & ( (\state.ERASE_PUCK_ONE~q\ & !\Add14~1_sumout\) ) ) ) # ( \puck_velocity_one.y\(0) & ( !\puck_velocity_one~0_combout\ & ( (!\state.ERASE_PUCK_ONE~q\ & (\state.INIT~DUPLICATE_q\)) # (\state.ERASE_PUCK_ONE~q\ & 
-- ((!\Add11~1_sumout\))) ) ) ) # ( !\puck_velocity_one.y\(0) & ( !\puck_velocity_one~0_combout\ & ( (\state.ERASE_PUCK_ONE~q\ & !\Add11~1_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000010111110000101001000100010001000100111001001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	datab => \ALT_INV_Add14~1_sumout\,
	datac => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_Add11~1_sumout\,
	datae => \ALT_INV_puck_velocity_one.y\(0),
	dataf => \ALT_INV_puck_velocity_one~0_combout\,
	combout => \Selector54~0_combout\);

-- Location: FF_X39_Y70_N55
\puck_velocity_one.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector54~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(0));

-- Location: MLABCELL_X39_Y69_N3
\Add11~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~5_sumout\ = SUM(( GND ) + ( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(1)) ) + ( \Add11~2\ ))
-- \Add11~6\ = CARRY(( GND ) + ( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(1)) ) + ( \Add11~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011000011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	dataf => \ALT_INV_puck_velocity_one.y\(1),
	cin => \Add11~2\,
	sumout => \Add11~5_sumout\,
	cout => \Add11~6\);

-- Location: MLABCELL_X39_Y69_N6
\Add11~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~9_sumout\ = SUM(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(2)) ) + ( GND ) + ( \Add11~6\ ))
-- \Add11~10\ = CARRY(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(2)) ) + ( GND ) + ( \Add11~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	datad => \ALT_INV_puck_velocity_one.y\(2),
	cin => \Add11~6\,
	sumout => \Add11~9_sumout\,
	cout => \Add11~10\);

-- Location: MLABCELL_X39_Y69_N33
\Add14~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~5_sumout\ = SUM(( !\Add11~5_sumout\ ) + ( GND ) + ( \Add14~2\ ))
-- \Add14~6\ = CARRY(( !\Add11~5_sumout\ ) + ( GND ) + ( \Add14~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_Add11~5_sumout\,
	cin => \Add14~2\,
	sumout => \Add14~5_sumout\,
	cout => \Add14~6\);

-- Location: MLABCELL_X39_Y69_N36
\Add14~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~9_sumout\ = SUM(( !\Add11~9_sumout\ ) + ( GND ) + ( \Add14~6\ ))
-- \Add14~10\ = CARRY(( !\Add11~9_sumout\ ) + ( GND ) + ( \Add14~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add11~9_sumout\,
	cin => \Add14~6\,
	sumout => \Add14~9_sumout\,
	cout => \Add14~10\);

-- Location: MLABCELL_X39_Y70_N6
\puck_velocity_one~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~2_combout\ = ( \puck_velocity_one~0_combout\ & ( \Add14~9_sumout\ ) ) # ( !\puck_velocity_one~0_combout\ & ( \Add11~9_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add14~9_sumout\,
	datac => \ALT_INV_Add11~9_sumout\,
	dataf => \ALT_INV_puck_velocity_one~0_combout\,
	combout => \puck_velocity_one~2_combout\);

-- Location: MLABCELL_X39_Y70_N9
\Selector52~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector52~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.y\(2))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_one.y\(2),
	combout => \Selector52~0_combout\);

-- Location: FF_X39_Y70_N7
\puck_velocity_one.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~2_combout\,
	asdata => \Selector52~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(2));

-- Location: MLABCELL_X39_Y69_N9
\Add11~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~13_sumout\ = SUM(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(3)) ) + ( GND ) + ( \Add11~10\ ))
-- \Add11~14\ = CARRY(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(3)) ) + ( GND ) + ( \Add11~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	datad => \ALT_INV_puck_velocity_one.y\(3),
	cin => \Add11~10\,
	sumout => \Add11~13_sumout\,
	cout => \Add11~14\);

-- Location: MLABCELL_X39_Y69_N39
\Add14~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~13_sumout\ = SUM(( !\Add11~13_sumout\ ) + ( GND ) + ( \Add14~10\ ))
-- \Add14~14\ = CARRY(( !\Add11~13_sumout\ ) + ( GND ) + ( \Add14~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add11~13_sumout\,
	cin => \Add14~10\,
	sumout => \Add14~13_sumout\,
	cout => \Add14~14\);

-- Location: MLABCELL_X39_Y70_N42
\puck_velocity_one~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~3_combout\ = ( \puck_velocity_one~0_combout\ & ( \Add14~13_sumout\ ) ) # ( !\puck_velocity_one~0_combout\ & ( \Add11~13_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add11~13_sumout\,
	datac => \ALT_INV_Add14~13_sumout\,
	dataf => \ALT_INV_puck_velocity_one~0_combout\,
	combout => \puck_velocity_one~3_combout\);

-- Location: MLABCELL_X39_Y70_N45
\Selector51~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector51~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_one.y\(3))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_velocity_one.y\(3),
	combout => \Selector51~0_combout\);

-- Location: FF_X39_Y70_N43
\puck_velocity_one.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~3_combout\,
	asdata => \Selector51~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(3));

-- Location: LABCELL_X40_Y71_N45
\Selector37~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector37~0_combout\ = ( \puck_one.y\(1) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_one.y\(1),
	combout => \Selector37~0_combout\);

-- Location: FF_X40_Y71_N5
\puck_one.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~5_sumout\,
	asdata => \Selector37~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(1));

-- Location: LABCELL_X40_Y71_N0
\Add10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~1_sumout\ = SUM(( \puck_one.y\(0) ) + ( !\puck_velocity_one.y\(0) ) + ( !VCC ))
-- \Add10~2\ = CARRY(( \puck_one.y\(0) ) + ( !\puck_velocity_one.y\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.y\(0),
	datad => \ALT_INV_puck_one.y\(0),
	cin => GND,
	sumout => \Add10~1_sumout\,
	cout => \Add10~2\);

-- Location: LABCELL_X31_Y70_N30
\Selector38~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector38~0_combout\ = ( \puck_one.y\(0) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_one.y\(0),
	combout => \Selector38~0_combout\);

-- Location: FF_X40_Y71_N2
\puck_one.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~1_sumout\,
	asdata => \Selector38~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(0));

-- Location: LABCELL_X40_Y71_N3
\Add10~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~5_sumout\ = SUM(( \puck_one.y\(1) ) + ( \puck_velocity_one.y\(1) ) + ( \Add10~2\ ))
-- \Add10~6\ = CARRY(( \puck_one.y\(1) ) + ( \puck_velocity_one.y\(1) ) + ( \Add10~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.y\(1),
	datad => \ALT_INV_puck_one.y\(1),
	cin => \Add10~2\,
	sumout => \Add10~5_sumout\,
	cout => \Add10~6\);

-- Location: LABCELL_X40_Y71_N6
\Add10~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~9_sumout\ = SUM(( \puck_velocity_one.y\(2) ) + ( !\puck_one.y\(2) ) + ( \Add10~6\ ))
-- \Add10~10\ = CARRY(( \puck_velocity_one.y\(2) ) + ( !\puck_one.y\(2) ) + ( \Add10~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_one.y\(2),
	datac => \ALT_INV_puck_velocity_one.y\(2),
	cin => \Add10~6\,
	sumout => \Add10~9_sumout\,
	cout => \Add10~10\);

-- Location: MLABCELL_X34_Y71_N12
\Add10~9_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~9_wirecell_combout\ = !\Add10~9_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add10~9_sumout\,
	combout => \Add10~9_wirecell_combout\);

-- Location: FF_X34_Y71_N13
\puck_one.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~9_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	ena => \WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(2));

-- Location: LABCELL_X40_Y71_N9
\Add10~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~13_sumout\ = SUM(( \puck_velocity_one.y\(3) ) + ( !\puck_one.y\(3) ) + ( \Add10~10\ ))
-- \Add10~14\ = CARRY(( \puck_velocity_one.y\(3) ) + ( !\puck_one.y\(3) ) + ( \Add10~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_one.y\(3),
	datad => \ALT_INV_puck_velocity_one.y\(3),
	cin => \Add10~10\,
	sumout => \Add10~13_sumout\,
	cout => \Add10~14\);

-- Location: LABCELL_X40_Y71_N48
\Selector48~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector48~0_combout\ = ( \puck_velocity_one.y[6]~DUPLICATE_q\ & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_velocity_one.y[6]~DUPLICATE_q\,
	combout => \Selector48~0_combout\);

-- Location: FF_X40_Y71_N34
\puck_velocity_one.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~7_combout\,
	asdata => \Selector48~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(6));

-- Location: MLABCELL_X39_Y69_N12
\Add11~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~21_sumout\ = SUM(( GND ) + ( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(4)) ) + ( \Add11~14\ ))
-- \Add11~22\ = CARRY(( GND ) + ( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(4)) ) + ( \Add11~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011000011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	dataf => \ALT_INV_puck_velocity_one.y\(4),
	cin => \Add11~14\,
	sumout => \Add11~21_sumout\,
	cout => \Add11~22\);

-- Location: MLABCELL_X39_Y69_N42
\Add14~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~21_sumout\ = SUM(( !\Add11~21_sumout\ ) + ( GND ) + ( \Add14~14\ ))
-- \Add14~22\ = CARRY(( !\Add11~21_sumout\ ) + ( GND ) + ( \Add14~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add11~21_sumout\,
	cin => \Add14~14\,
	sumout => \Add14~21_sumout\,
	cout => \Add14~22\);

-- Location: MLABCELL_X39_Y69_N24
\puck_velocity_one~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~5_combout\ = ( \Add11~21_sumout\ & ( (!\puck_velocity_one~0_combout\) # (\Add14~21_sumout\) ) ) # ( !\Add11~21_sumout\ & ( (\Add14~21_sumout\ & \puck_velocity_one~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001111110011111100111111001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add14~21_sumout\,
	datac => \ALT_INV_puck_velocity_one~0_combout\,
	dataf => \ALT_INV_Add11~21_sumout\,
	combout => \puck_velocity_one~5_combout\);

-- Location: LABCELL_X37_Y69_N45
\Selector50~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector50~0_combout\ = ( \puck_velocity_one.y\(4) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_velocity_one.y\(4),
	combout => \Selector50~0_combout\);

-- Location: FF_X39_Y69_N25
\puck_velocity_one.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~5_combout\,
	asdata => \Selector50~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(4));

-- Location: MLABCELL_X39_Y69_N15
\Add11~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~25_sumout\ = SUM(( GND ) + ( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(5)) ) + ( \Add11~22\ ))
-- \Add11~26\ = CARRY(( GND ) + ( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(5)) ) + ( \Add11~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011000011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	dataf => \ALT_INV_puck_velocity_one.y\(5),
	cin => \Add11~22\,
	sumout => \Add11~25_sumout\,
	cout => \Add11~26\);

-- Location: MLABCELL_X39_Y69_N18
\Add11~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~29_sumout\ = SUM(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(6)) ) + ( GND ) + ( \Add11~26\ ))
-- \Add11~30\ = CARRY(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(6)) ) + ( GND ) + ( \Add11~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011110000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	datac => \ALT_INV_puck_velocity_one.y\(6),
	cin => \Add11~26\,
	sumout => \Add11~29_sumout\,
	cout => \Add11~30\);

-- Location: MLABCELL_X39_Y69_N45
\Add14~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~25_sumout\ = SUM(( !\Add11~25_sumout\ ) + ( GND ) + ( \Add14~22\ ))
-- \Add14~26\ = CARRY(( !\Add11~25_sumout\ ) + ( GND ) + ( \Add14~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add11~25_sumout\,
	cin => \Add14~22\,
	sumout => \Add14~25_sumout\,
	cout => \Add14~26\);

-- Location: MLABCELL_X39_Y69_N48
\Add14~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~29_sumout\ = SUM(( !\Add11~29_sumout\ ) + ( GND ) + ( \Add14~26\ ))
-- \Add14~30\ = CARRY(( !\Add11~29_sumout\ ) + ( GND ) + ( \Add14~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add11~29_sumout\,
	cin => \Add14~26\,
	sumout => \Add14~29_sumout\,
	cout => \Add14~30\);

-- Location: LABCELL_X40_Y71_N33
\puck_velocity_one~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~7_combout\ = (!\puck_velocity_one~0_combout\ & (\Add11~29_sumout\)) # (\puck_velocity_one~0_combout\ & ((\Add14~29_sumout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100001111010101010000111101010101000011110101010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~29_sumout\,
	datac => \ALT_INV_Add14~29_sumout\,
	datad => \ALT_INV_puck_velocity_one~0_combout\,
	combout => \puck_velocity_one~7_combout\);

-- Location: FF_X40_Y71_N35
\puck_velocity_one.y[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~7_combout\,
	asdata => \Selector48~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y[6]~DUPLICATE_q\);

-- Location: LABCELL_X40_Y71_N51
\Selector32~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector32~0_combout\ = ( \puck_one.y\(6) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_one.y\(6),
	combout => \Selector32~0_combout\);

-- Location: FF_X40_Y71_N20
\puck_one.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~29_sumout\,
	asdata => \Selector32~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(6));

-- Location: MLABCELL_X34_Y71_N0
\Add10~25_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~25_wirecell_combout\ = ( !\Add10~25_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add10~25_sumout\,
	combout => \Add10~25_wirecell_combout\);

-- Location: FF_X34_Y71_N1
\puck_one.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~25_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	ena => \WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(5));

-- Location: LABCELL_X40_Y71_N12
\Add10~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~21_sumout\ = SUM(( \puck_velocity_one.y\(4) ) + ( !\puck_one.y\(4) ) + ( \Add10~14\ ))
-- \Add10~22\ = CARRY(( \puck_velocity_one.y\(4) ) + ( !\puck_one.y\(4) ) + ( \Add10~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_one.y\(4),
	datad => \ALT_INV_puck_velocity_one.y\(4),
	cin => \Add10~14\,
	sumout => \Add10~21_sumout\,
	cout => \Add10~22\);

-- Location: MLABCELL_X34_Y71_N6
\Add10~21_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~21_wirecell_combout\ = !\Add10~21_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add10~21_sumout\,
	combout => \Add10~21_wirecell_combout\);

-- Location: FF_X34_Y71_N7
\puck_one.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~21_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	ena => \WideOr6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(4));

-- Location: LABCELL_X40_Y71_N15
\Add10~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~25_sumout\ = SUM(( \puck_velocity_one.y\(5) ) + ( !\puck_one.y\(5) ) + ( \Add10~22\ ))
-- \Add10~26\ = CARRY(( \puck_velocity_one.y\(5) ) + ( !\puck_one.y\(5) ) + ( \Add10~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_one.y\(5),
	datac => \ALT_INV_puck_one.y\(5),
	cin => \Add10~22\,
	sumout => \Add10~25_sumout\,
	cout => \Add10~26\);

-- Location: LABCELL_X40_Y71_N18
\Add10~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~29_sumout\ = SUM(( \puck_one.y\(6) ) + ( \puck_velocity_one.y[6]~DUPLICATE_q\ ) + ( \Add10~26\ ))
-- \Add10~30\ = CARRY(( \puck_one.y\(6) ) + ( \puck_velocity_one.y[6]~DUPLICATE_q\ ) + ( \Add10~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.y[6]~DUPLICATE_q\,
	datad => \ALT_INV_puck_one.y\(6),
	cin => \Add10~26\,
	sumout => \Add10~29_sumout\,
	cout => \Add10~30\);

-- Location: MLABCELL_X39_Y69_N21
\Add11~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add11~17_sumout\ = SUM(( !\Equal5~2_combout\ $ (!\puck_velocity_one.y\(7)) ) + ( GND ) + ( \Add11~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011110000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal5~2_combout\,
	datac => \ALT_INV_puck_velocity_one.y\(7),
	cin => \Add11~30\,
	sumout => \Add11~17_sumout\);

-- Location: MLABCELL_X39_Y69_N51
\Add14~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add14~17_sumout\ = SUM(( !\Add11~17_sumout\ ) + ( GND ) + ( \Add14~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~17_sumout\,
	cin => \Add14~30\,
	sumout => \Add14~17_sumout\);

-- Location: MLABCELL_X39_Y69_N54
\puck_velocity_one~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~4_combout\ = ( \Add14~17_sumout\ & ( (\Add11~17_sumout\) # (\puck_velocity_one~0_combout\) ) ) # ( !\Add14~17_sumout\ & ( (!\puck_velocity_one~0_combout\ & \Add11~17_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101001011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_one~0_combout\,
	datac => \ALT_INV_Add11~17_sumout\,
	dataf => \ALT_INV_Add14~17_sumout\,
	combout => \puck_velocity_one~4_combout\);

-- Location: MLABCELL_X39_Y69_N27
\Selector47~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector47~0_combout\ = (\puck_velocity_one.y\(7) & \state.INIT~DUPLICATE_q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.y\(7),
	datad => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector47~0_combout\);

-- Location: FF_X39_Y69_N55
\puck_velocity_one.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~4_combout\,
	asdata => \Selector47~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(7));

-- Location: LABCELL_X40_Y71_N24
\Selector31~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector31~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_one.y\(7))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_one.y\(7),
	combout => \Selector31~0_combout\);

-- Location: FF_X40_Y71_N23
\puck_one.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add10~17_sumout\,
	asdata => \Selector31~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_one.y\(7));

-- Location: LABCELL_X40_Y71_N21
\Add10~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add10~17_sumout\ = SUM(( \puck_one.y\(7) ) + ( \puck_velocity_one.y\(7) ) + ( \Add10~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_one.y\(7),
	datad => \ALT_INV_puck_one.y\(7),
	cin => \Add10~30\,
	sumout => \Add10~17_sumout\);

-- Location: LABCELL_X40_Y71_N36
\Equal5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal5~1_combout\ = ( \Add10~9_sumout\ & ( (!\Add10~5_sumout\ & \Add10~1_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add10~5_sumout\,
	datad => \ALT_INV_Add10~1_sumout\,
	dataf => \ALT_INV_Add10~9_sumout\,
	combout => \Equal5~1_combout\);

-- Location: LABCELL_X40_Y71_N54
\Equal5~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal5~2_combout\ = ( \Equal5~1_combout\ & ( !\Add10~21_sumout\ & ( (!\Add10~13_sumout\ & (!\Add10~25_sumout\ & (!\Add10~29_sumout\ & !\Add10~17_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add10~13_sumout\,
	datab => \ALT_INV_Add10~25_sumout\,
	datac => \ALT_INV_Add10~29_sumout\,
	datad => \ALT_INV_Add10~17_sumout\,
	datae => \ALT_INV_Equal5~1_combout\,
	dataf => \ALT_INV_Add10~21_sumout\,
	combout => \Equal5~2_combout\);

-- Location: LABCELL_X40_Y71_N27
\puck_velocity_one~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~1_combout\ = ( \Add14~5_sumout\ & ( (\puck_velocity_one~0_combout\) # (\Add11~5_sumout\) ) ) # ( !\Add14~5_sumout\ & ( (\Add11~5_sumout\ & !\puck_velocity_one~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000010101010000000001010101111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add11~5_sumout\,
	datad => \ALT_INV_puck_velocity_one~0_combout\,
	dataf => \ALT_INV_Add14~5_sumout\,
	combout => \puck_velocity_one~1_combout\);

-- Location: LABCELL_X40_Y71_N30
\Selector53~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector53~0_combout\ = (\puck_velocity_one.y\(1) & \state.INIT~DUPLICATE_q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_one.y\(1),
	datad => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector53~0_combout\);

-- Location: FF_X40_Y71_N28
\puck_velocity_one.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~1_combout\,
	asdata => \Selector53~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(1));

-- Location: LABCELL_X40_Y71_N39
\Equal5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal5~0_combout\ = ( !\Add10~17_sumout\ & ( (!\Add10~5_sumout\ & (\Add10~9_sumout\ & (!\Add10~13_sumout\ & \Add10~1_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100000000000000010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add10~5_sumout\,
	datab => \ALT_INV_Add10~9_sumout\,
	datac => \ALT_INV_Add10~13_sumout\,
	datad => \ALT_INV_Add10~1_sumout\,
	dataf => \ALT_INV_Add10~17_sumout\,
	combout => \Equal5~0_combout\);

-- Location: MLABCELL_X39_Y70_N30
\puck_velocity_one~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~0_combout\ = ( !\always0~6_combout\ & ( \Equal5~0_combout\ & ( (\Equal8~0_combout\ & (\always0~8_combout\ & ((!\always0~2_combout\) # (!\always0~1_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010101000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal8~0_combout\,
	datab => \ALT_INV_always0~2_combout\,
	datac => \ALT_INV_always0~1_combout\,
	datad => \ALT_INV_always0~8_combout\,
	datae => \ALT_INV_always0~6_combout\,
	dataf => \ALT_INV_Equal5~0_combout\,
	combout => \puck_velocity_one~0_combout\);

-- Location: MLABCELL_X39_Y69_N57
\puck_velocity_one~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \puck_velocity_one~6_combout\ = ( \Add11~25_sumout\ & ( (!\puck_velocity_one~0_combout\) # (\Add14~25_sumout\) ) ) # ( !\Add11~25_sumout\ & ( (\puck_velocity_one~0_combout\ & \Add14~25_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010110101111101011111010111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_one~0_combout\,
	datac => \ALT_INV_Add14~25_sumout\,
	dataf => \ALT_INV_Add11~25_sumout\,
	combout => \puck_velocity_one~6_combout\);

-- Location: LABCELL_X36_Y70_N45
\Selector49~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector49~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \puck_velocity_one.y\(5) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_velocity_one.y\(5),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector49~0_combout\);

-- Location: FF_X39_Y69_N58
\puck_velocity_one.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \puck_velocity_one~6_combout\,
	asdata => \Selector49~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_one.y\(5));

-- Location: LABCELL_X40_Y71_N42
\Equal8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal8~0_combout\ = ( \Add10~21_sumout\ & ( (\Add10~25_sumout\ & \Add10~29_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add10~25_sumout\,
	datac => \ALT_INV_Add10~29_sumout\,
	dataf => \ALT_INV_Add10~21_sumout\,
	combout => \Equal8~0_combout\);

-- Location: MLABCELL_X39_Y70_N0
\Selector90~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector90~0_combout\ = ( \Equal8~0_combout\ & ( \Equal5~0_combout\ & ( (!\always0~6_combout\ & (\always0~8_combout\ & ((!\always0~1_combout\) # (!\always0~2_combout\)))) ) ) ) # ( !\Equal8~0_combout\ & ( \Equal5~0_combout\ ) ) # ( \Equal8~0_combout\ & ( 
-- !\Equal5~0_combout\ ) ) # ( !\Equal8~0_combout\ & ( !\Equal5~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111110000000011100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_always0~1_combout\,
	datab => \ALT_INV_always0~2_combout\,
	datac => \ALT_INV_always0~6_combout\,
	datad => \ALT_INV_always0~8_combout\,
	datae => \ALT_INV_Equal8~0_combout\,
	dataf => \ALT_INV_Equal5~0_combout\,
	combout => \Selector90~0_combout\);

-- Location: FF_X39_Y70_N2
\state.DRAW_PUCK_ONE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector90~0_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PUCK_ONE~q\);

-- Location: FF_X34_Y71_N41
\state.ERASE_PUCK_TWO\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \state.DRAW_PUCK_ONE~q\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PUCK_TWO~q\);

-- Location: MLABCELL_X34_Y69_N3
\Selector78~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector78~0_combout\ = ( \state.ERASE_PUCK_TWO~q\ & ( !\Add18~17_sumout\ ) ) # ( !\state.ERASE_PUCK_TWO~q\ & ( (\state.INIT~DUPLICATE_q\ & \puck_velocity_two.x\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_Add18~17_sumout\,
	datad => \ALT_INV_puck_velocity_two.x\(0),
	dataf => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	combout => \Selector78~0_combout\);

-- Location: FF_X34_Y69_N5
\puck_velocity_two.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector78~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(0));

-- Location: MLABCELL_X34_Y69_N33
\Add18~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~13_sumout\ = SUM(( !\puck_velocity_two.x\(1) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~18\ ))
-- \Add18~14\ = CARRY(( !\puck_velocity_two.x\(1) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111100001111000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal10~0_combout\,
	datab => \ALT_INV_Equal11~0_combout\,
	datac => \ALT_INV_puck_velocity_two.x\(1),
	cin => \Add18~18\,
	sumout => \Add18~13_sumout\,
	cout => \Add18~14\);

-- Location: MLABCELL_X34_Y69_N0
\Selector77~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector77~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_two.x\(1))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_two.x\(1),
	combout => \Selector77~0_combout\);

-- Location: FF_X34_Y69_N34
\puck_velocity_two.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~13_sumout\,
	asdata => \Selector77~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(1));

-- Location: MLABCELL_X34_Y69_N36
\Add18~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~9_sumout\ = SUM(( !\puck_velocity_two.x\(2) $ (((!\Equal11~0_combout\ & !\Equal10~0_combout\))) ) + ( GND ) + ( \Add18~14\ ))
-- \Add18~10\ = CARRY(( !\puck_velocity_two.x\(2) $ (((!\Equal11~0_combout\ & !\Equal10~0_combout\))) ) + ( GND ) + ( \Add18~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011110011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal11~0_combout\,
	datac => \ALT_INV_puck_velocity_two.x\(2),
	datad => \ALT_INV_Equal10~0_combout\,
	cin => \Add18~14\,
	sumout => \Add18~9_sumout\,
	cout => \Add18~10\);

-- Location: MLABCELL_X34_Y69_N18
\Selector76~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector76~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_two.x\(2))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_velocity_two.x\(2),
	combout => \Selector76~0_combout\);

-- Location: FF_X34_Y69_N38
\puck_velocity_two.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~9_sumout\,
	asdata => \Selector76~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(2));

-- Location: MLABCELL_X34_Y69_N39
\Add18~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~5_sumout\ = SUM(( !\puck_velocity_two.x\(3) $ (((!\Equal11~0_combout\ & !\Equal10~0_combout\))) ) + ( GND ) + ( \Add18~10\ ))
-- \Add18~6\ = CARRY(( !\puck_velocity_two.x\(3) $ (((!\Equal11~0_combout\ & !\Equal10~0_combout\))) ) + ( GND ) + ( \Add18~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011110011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal11~0_combout\,
	datac => \ALT_INV_puck_velocity_two.x\(3),
	datad => \ALT_INV_Equal10~0_combout\,
	cin => \Add18~10\,
	sumout => \Add18~5_sumout\,
	cout => \Add18~6\);

-- Location: MLABCELL_X34_Y69_N15
\Selector75~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector75~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_two.x\(3))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_velocity_two.x\(3),
	combout => \Selector75~0_combout\);

-- Location: FF_X34_Y69_N40
\puck_velocity_two.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~5_sumout\,
	asdata => \Selector75~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(3));

-- Location: MLABCELL_X34_Y69_N42
\Add18~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~1_sumout\ = SUM(( !\puck_velocity_two.x\(4) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~6\ ))
-- \Add18~2\ = CARRY(( !\puck_velocity_two.x\(4) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal10~0_combout\,
	datab => \ALT_INV_Equal11~0_combout\,
	datad => \ALT_INV_puck_velocity_two.x\(4),
	cin => \Add18~6\,
	sumout => \Add18~1_sumout\,
	cout => \Add18~2\);

-- Location: MLABCELL_X34_Y69_N9
\Selector74~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector74~0_combout\ = ( \puck_velocity_two.x\(4) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_velocity_two.x\(4),
	combout => \Selector74~0_combout\);

-- Location: FF_X34_Y69_N44
\puck_velocity_two.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~1_sumout\,
	asdata => \Selector74~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(4));

-- Location: LABCELL_X33_Y71_N51
\Add15~1_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~1_wirecell_combout\ = !\Add15~1_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~1_sumout\,
	combout => \Add15~1_wirecell_combout\);

-- Location: FF_X33_Y71_N35
\state.INIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector90~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.INIT~q\);

-- Location: LABCELL_X33_Y71_N54
\WideOr7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr7~0_combout\ = ( \state.INIT~q\ & ( \state.ERASE_PUCK_TWO~q\ ) ) # ( !\state.INIT~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	dataf => \ALT_INV_state.INIT~q\,
	combout => \WideOr7~0_combout\);

-- Location: FF_X33_Y71_N52
\puck_two.x[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~1_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x[4]~DUPLICATE_q\);

-- Location: LABCELL_X33_Y69_N0
\Add15~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~17_sumout\ = SUM(( \puck_two.x\(0) ) + ( !\puck_velocity_two.x\(0) ) + ( !VCC ))
-- \Add15~18\ = CARRY(( \puck_two.x\(0) ) + ( !\puck_velocity_two.x\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_two.x\(0),
	datad => \ALT_INV_puck_two.x\(0),
	cin => GND,
	sumout => \Add15~17_sumout\,
	cout => \Add15~18\);

-- Location: LABCELL_X37_Y72_N27
\Selector62~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector62~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \puck_two.x\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_two.x\(0),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector62~0_combout\);

-- Location: FF_X33_Y69_N2
\puck_two.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~17_sumout\,
	asdata => \Selector62~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(0));

-- Location: LABCELL_X33_Y69_N3
\Add15~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~13_sumout\ = SUM(( \puck_two.x\(1) ) + ( \puck_velocity_two.x\(1) ) + ( \Add15~18\ ))
-- \Add15~14\ = CARRY(( \puck_two.x\(1) ) + ( \puck_velocity_two.x\(1) ) + ( \Add15~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_two.x\(1),
	datad => \ALT_INV_puck_two.x\(1),
	cin => \Add15~18\,
	sumout => \Add15~13_sumout\,
	cout => \Add15~14\);

-- Location: LABCELL_X37_Y72_N15
\Selector61~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector61~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \puck_two.x\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_two.x\(1),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector61~0_combout\);

-- Location: FF_X33_Y69_N5
\puck_two.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~13_sumout\,
	asdata => \Selector61~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(1));

-- Location: LABCELL_X33_Y69_N6
\Add15~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~9_sumout\ = SUM(( \puck_two.x\(2) ) + ( \puck_velocity_two.x\(2) ) + ( \Add15~14\ ))
-- \Add15~10\ = CARRY(( \puck_two.x\(2) ) + ( \puck_velocity_two.x\(2) ) + ( \Add15~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_two.x\(2),
	datad => \ALT_INV_puck_two.x\(2),
	cin => \Add15~14\,
	sumout => \Add15~9_sumout\,
	cout => \Add15~10\);

-- Location: MLABCELL_X34_Y71_N21
\Selector60~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector60~0_combout\ = ( \puck_two.x\(2) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_two.x\(2),
	combout => \Selector60~0_combout\);

-- Location: FF_X33_Y69_N8
\puck_two.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~9_sumout\,
	asdata => \Selector60~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(2));

-- Location: LABCELL_X33_Y69_N9
\Add15~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~5_sumout\ = SUM(( \puck_velocity_two.x\(3) ) + ( !\puck_two.x\(3) ) + ( \Add15~10\ ))
-- \Add15~6\ = CARRY(( \puck_velocity_two.x\(3) ) + ( !\puck_two.x\(3) ) + ( \Add15~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_two.x\(3),
	datad => \ALT_INV_puck_velocity_two.x\(3),
	cin => \Add15~10\,
	sumout => \Add15~5_sumout\,
	cout => \Add15~6\);

-- Location: LABCELL_X33_Y71_N48
\Add15~5_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~5_wirecell_combout\ = !\Add15~5_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add15~5_sumout\,
	combout => \Add15~5_wirecell_combout\);

-- Location: FF_X33_Y71_N49
\puck_two.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~5_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(3));

-- Location: LABCELL_X33_Y69_N12
\Add15~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~1_sumout\ = SUM(( \puck_velocity_two.x\(4) ) + ( !\puck_two.x[4]~DUPLICATE_q\ ) + ( \Add15~6\ ))
-- \Add15~2\ = CARRY(( \puck_velocity_two.x\(4) ) + ( !\puck_two.x[4]~DUPLICATE_q\ ) + ( \Add15~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_two.x\(4),
	datac => \ALT_INV_puck_two.x[4]~DUPLICATE_q\,
	cin => \Add15~6\,
	sumout => \Add15~1_sumout\,
	cout => \Add15~2\);

-- Location: LABCELL_X33_Y71_N15
\Add15~29_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~29_wirecell_combout\ = ( !\Add15~29_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add15~29_sumout\,
	combout => \Add15~29_wirecell_combout\);

-- Location: FF_X33_Y71_N17
\puck_two.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~29_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(5));

-- Location: MLABCELL_X34_Y69_N45
\Add18~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~29_sumout\ = SUM(( !\puck_velocity_two.x\(5) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~2\ ))
-- \Add18~30\ = CARRY(( !\puck_velocity_two.x\(5) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal10~0_combout\,
	datab => \ALT_INV_Equal11~0_combout\,
	datad => \ALT_INV_puck_velocity_two.x\(5),
	cin => \Add18~2\,
	sumout => \Add18~29_sumout\,
	cout => \Add18~30\);

-- Location: MLABCELL_X34_Y69_N12
\Selector73~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector73~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_velocity_two.x\(5))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_puck_velocity_two.x\(5),
	combout => \Selector73~0_combout\);

-- Location: FF_X34_Y69_N46
\puck_velocity_two.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~29_sumout\,
	asdata => \Selector73~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(5));

-- Location: LABCELL_X33_Y69_N15
\Add15~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~29_sumout\ = SUM(( \puck_velocity_two.x\(5) ) + ( !\puck_two.x\(5) ) + ( \Add15~2\ ))
-- \Add15~30\ = CARRY(( \puck_velocity_two.x\(5) ) + ( !\puck_two.x\(5) ) + ( \Add15~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_two.x\(5),
	datac => \ALT_INV_puck_velocity_two.x\(5),
	cin => \Add15~2\,
	sumout => \Add15~29_sumout\,
	cout => \Add15~30\);

-- Location: LABCELL_X33_Y69_N27
\Equal11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal11~1_combout\ = (\Add15~13_sumout\ & (!\Add15~9_sumout\ & !\Add15~17_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000000000010001000000000001000100000000000100010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~13_sumout\,
	datab => \ALT_INV_Add15~9_sumout\,
	datad => \ALT_INV_Add15~17_sumout\,
	combout => \Equal11~1_combout\);

-- Location: MLABCELL_X34_Y69_N24
\Equal11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal11~0_combout\ = ( \Add15~21_sumout\ & ( \Equal11~1_combout\ & ( (\Add15~1_sumout\ & (!\Add15~29_sumout\ & (\Add15~5_sumout\ & !\Add15~25_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000010000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~1_sumout\,
	datab => \ALT_INV_Add15~29_sumout\,
	datac => \ALT_INV_Add15~5_sumout\,
	datad => \ALT_INV_Add15~25_sumout\,
	datae => \ALT_INV_Add15~21_sumout\,
	dataf => \ALT_INV_Equal11~1_combout\,
	combout => \Equal11~0_combout\);

-- Location: MLABCELL_X34_Y69_N48
\Add18~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~25_sumout\ = SUM(( !\puck_velocity_two.x\(6) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~30\ ))
-- \Add18~26\ = CARRY(( !\puck_velocity_two.x\(6) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal10~0_combout\,
	datab => \ALT_INV_Equal11~0_combout\,
	datad => \ALT_INV_puck_velocity_two.x\(6),
	cin => \Add18~30\,
	sumout => \Add18~25_sumout\,
	cout => \Add18~26\);

-- Location: MLABCELL_X34_Y69_N51
\Add18~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add18~21_sumout\ = SUM(( !\puck_velocity_two.x\(7) $ (((!\Equal10~0_combout\ & !\Equal11~0_combout\))) ) + ( GND ) + ( \Add18~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000111011110001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal10~0_combout\,
	datab => \ALT_INV_Equal11~0_combout\,
	datad => \ALT_INV_puck_velocity_two.x\(7),
	cin => \Add18~26\,
	sumout => \Add18~21_sumout\);

-- Location: MLABCELL_X34_Y69_N6
\Selector71~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector71~0_combout\ = ( \puck_velocity_two.x\(7) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_velocity_two.x\(7),
	combout => \Selector71~0_combout\);

-- Location: FF_X34_Y69_N53
\puck_velocity_two.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~21_sumout\,
	asdata => \Selector71~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(7));

-- Location: LABCELL_X33_Y72_N0
\Selector55~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector55~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \puck_two.x\(7) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_two.x\(7),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector55~0_combout\);

-- Location: FF_X33_Y69_N23
\puck_two.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~21_sumout\,
	asdata => \Selector55~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(7));

-- Location: LABCELL_X33_Y69_N18
\Add15~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~25_sumout\ = SUM(( \puck_velocity_two.x\(6) ) + ( !\puck_two.x\(6) ) + ( \Add15~30\ ))
-- \Add15~26\ = CARRY(( \puck_velocity_two.x\(6) ) + ( !\puck_two.x\(6) ) + ( \Add15~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_two.x\(6),
	datac => \ALT_INV_puck_two.x\(6),
	cin => \Add15~30\,
	sumout => \Add15~25_sumout\,
	cout => \Add15~26\);

-- Location: LABCELL_X33_Y69_N21
\Add15~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~21_sumout\ = SUM(( \puck_two.x\(7) ) + ( \puck_velocity_two.x\(7) ) + ( \Add15~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_two.x\(7),
	datad => \ALT_INV_puck_two.x\(7),
	cin => \Add15~26\,
	sumout => \Add15~21_sumout\);

-- Location: LABCELL_X33_Y69_N24
\Equal10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal10~1_combout\ = (\Add15~13_sumout\ & (\Add15~9_sumout\ & !\Add15~17_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010000000100000001000000010000000100000001000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~13_sumout\,
	datab => \ALT_INV_Add15~9_sumout\,
	datac => \ALT_INV_Add15~17_sumout\,
	combout => \Equal10~1_combout\);

-- Location: MLABCELL_X34_Y69_N54
\Equal10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal10~0_combout\ = ( !\Add15~5_sumout\ & ( \Equal10~1_combout\ & ( (!\Add15~21_sumout\ & (!\Add15~25_sumout\ & (!\Add15~1_sumout\ & !\Add15~29_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~21_sumout\,
	datab => \ALT_INV_Add15~25_sumout\,
	datac => \ALT_INV_Add15~1_sumout\,
	datad => \ALT_INV_Add15~29_sumout\,
	datae => \ALT_INV_Add15~5_sumout\,
	dataf => \ALT_INV_Equal10~1_combout\,
	combout => \Equal10~0_combout\);

-- Location: MLABCELL_X34_Y69_N21
\Selector72~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector72~0_combout\ = ( \puck_velocity_two.x\(6) & ( \state.INIT~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	dataf => \ALT_INV_puck_velocity_two.x\(6),
	combout => \Selector72~0_combout\);

-- Location: FF_X34_Y69_N50
\puck_velocity_two.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add18~25_sumout\,
	asdata => \Selector72~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.x\(6));

-- Location: LABCELL_X33_Y71_N12
\Add15~25_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add15~25_wirecell_combout\ = ( !\Add15~25_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add15~25_sumout\,
	combout => \Add15~25_wirecell_combout\);

-- Location: FF_X33_Y71_N14
\puck_two.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~25_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(6));

-- Location: FF_X33_Y71_N32
\state.DRAW_PUCK_TWO\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \state~53_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PUCK_TWO~q\);

-- Location: LABCELL_X35_Y69_N45
\draw.y[3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[3]~0_combout\ = ( !\state.ERASE_PUCK_TWO~q\ & ( !\state.DRAW_PUCK_TWO~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.DRAW_PUCK_TWO~q\,
	dataf => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	combout => \draw.y[3]~0_combout\);

-- Location: LABCELL_X33_Y71_N45
\Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = ( paddle_x(6) & ( (!\puck_two.x\(6) & !\draw.y[3]~0_combout\) ) ) # ( !paddle_x(6) & ( ((!\puck_two.x\(6) & !\draw.y[3]~0_combout\)) # (\state.ERASE_PADDLE_ENTER~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111001100110011111100110011001111110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	datac => \ALT_INV_puck_two.x\(6),
	datad => \ALT_INV_draw.y[3]~0_combout\,
	dataf => ALT_INV_paddle_x(6),
	combout => \Selector1~0_combout\);

-- Location: FF_X33_Y71_N53
\puck_two.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add15~1_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.x\(4));

-- Location: LABCELL_X33_Y71_N36
\Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = ( \state.ERASE_PADDLE_ENTER~q\ & ( (!paddle_x(4)) # ((!\draw.y[3]~0_combout\ & !\puck_two.x\(4))) ) ) # ( !\state.ERASE_PADDLE_ENTER~q\ & ( (!\draw.y[3]~0_combout\ & !\puck_two.x\(4)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110000001100000011111111110000001111111111000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y[3]~0_combout\,
	datac => \ALT_INV_puck_two.x\(4),
	datad => ALT_INV_paddle_x(4),
	dataf => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	combout => \Selector3~0_combout\);

-- Location: MLABCELL_X34_Y72_N0
\Add8~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~13_sumout\ = SUM(( paddle_x(1) ) + ( VCC ) + ( !VCC ))
-- \Add8~14\ = CARRY(( paddle_x(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(1),
	cin => GND,
	sumout => \Add8~13_sumout\,
	cout => \Add8~14\);

-- Location: LABCELL_X35_Y72_N0
\Add7~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~13_sumout\ = SUM(( paddle_x(1) ) + ( VCC ) + ( !VCC ))
-- \Add7~14\ = CARRY(( paddle_x(1) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(1),
	cin => GND,
	sumout => \Add7~13_sumout\,
	cout => \Add7~14\);

-- Location: MLABCELL_X34_Y72_N45
\Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = ( \Add7~13_sumout\ & ( (!\KEY[0]~input_o\) # (\Add8~13_sumout\) ) ) # ( !\Add7~13_sumout\ & ( (\KEY[0]~input_o\ & \Add8~13_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111110000111111111111000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_KEY[0]~input_o\,
	datad => \ALT_INV_Add8~13_sumout\,
	dataf => \ALT_INV_Add7~13_sumout\,
	combout => \Selector6~0_combout\);

-- Location: LABCELL_X36_Y72_N0
\Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~13_sumout\ = SUM(( \draw.x\(0) ) + ( VCC ) + ( !VCC ))
-- \Add1~14\ = CARRY(( \draw.x\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x\(0),
	cin => GND,
	sumout => \Add1~13_sumout\,
	cout => \Add1~14\);

-- Location: FF_X35_Y71_N4
\state.DRAW_RIGHT_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector95~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_RIGHT_LOOP~q\);

-- Location: MLABCELL_X39_Y72_N51
\vga_u0|writeEn~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|writeEn~1_combout\ = ( !\draw.x\(5) & ( !\draw.x\(6) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_draw.x\(6),
	dataf => \ALT_INV_draw.x\(5),
	combout => \vga_u0|writeEn~1_combout\);

-- Location: LABCELL_X36_Y72_N42
\Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal2~0_combout\ = ( \draw.x\(4) & ( \draw.x\(3) & ( (\draw.x\(1) & (\draw.x\(0) & \draw.x\(7))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(1),
	datab => \ALT_INV_draw.x\(0),
	datac => \ALT_INV_draw.x\(7),
	datae => \ALT_INV_draw.x\(4),
	dataf => \ALT_INV_draw.x\(3),
	combout => \Equal2~0_combout\);

-- Location: LABCELL_X36_Y71_N39
\draw.x[4]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~0_combout\ = ( \Equal2~0_combout\ & ( (!\draw.x\(2) & (\vga_u0|writeEn~1_combout\ & \state.DRAW_TOP_LOOP~DUPLICATE_q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000010000000100000001000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(2),
	datab => \vga_u0|ALT_INV_writeEn~1_combout\,
	datac => \ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\,
	dataf => \ALT_INV_Equal2~0_combout\,
	combout => \draw.x[4]~0_combout\);

-- Location: FF_X36_Y71_N41
\state.DRAW_RIGHT_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.x[4]~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_RIGHT_ENTER~q\);

-- Location: LABCELL_X35_Y71_N3
\Selector95~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector95~0_combout\ = ( \state.DRAW_RIGHT_ENTER~q\ ) # ( !\state.DRAW_RIGHT_ENTER~q\ & ( (!\Equal1~2_combout\ & \state.DRAW_RIGHT_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Equal1~2_combout\,
	datad => \ALT_INV_state.DRAW_RIGHT_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_RIGHT_ENTER~q\,
	combout => \Selector95~0_combout\);

-- Location: FF_X35_Y71_N5
\state.DRAW_RIGHT_LOOP~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector95~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_RIGHT_LOOP~DUPLICATE_q\);

-- Location: MLABCELL_X39_Y71_N51
\draw.y[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[3]~1_combout\ = ( \draw.y[3]~0_combout\ & ( \draw~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw~0_combout\,
	dataf => \ALT_INV_draw.y[3]~0_combout\,
	combout => \draw.y[3]~1_combout\);

-- Location: FF_X36_Y71_N46
\state.DRAW_TOP_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector93~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_TOP_LOOP~q\);

-- Location: MLABCELL_X34_Y71_N3
\Selector9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~0_combout\ = ( !\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\state.ERASE_PADDLE_ENTER~q\ & (((!\state.INIT~DUPLICATE_q\) # (!\WideOr1~0_combout\)) # (\state.DRAW_TOP_LOOP~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011010000111100001101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	datad => \ALT_INV_WideOr1~0_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector9~0_combout\);

-- Location: MLABCELL_X34_Y72_N33
\LessThan6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~2_combout\ = ( \Add15~25_sumout\ & ( (!paddle_x(7) & (paddle_x(5) & (!paddle_x(6) & !\Add15~21_sumout\))) # (paddle_x(7) & ((!\Add15~21_sumout\) # ((paddle_x(5) & !paddle_x(6))))) ) ) # ( !\Add15~25_sumout\ & ( (!paddle_x(7) & 
-- (!\Add15~21_sumout\ & ((!paddle_x(6)) # (paddle_x(5))))) # (paddle_x(7) & (((!paddle_x(6)) # (!\Add15~21_sumout\)) # (paddle_x(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101111100001101110111110000110101001111000001000100111100000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(5),
	datab => ALT_INV_paddle_x(6),
	datac => ALT_INV_paddle_x(7),
	datad => \ALT_INV_Add15~21_sumout\,
	dataf => \ALT_INV_Add15~25_sumout\,
	combout => \LessThan6~2_combout\);

-- Location: LABCELL_X33_Y69_N48
\LessThan6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~1_combout\ = ( \Add15~13_sumout\ & ( paddle_x(1) & ( (!paddle_x(3) & (!\Add15~9_sumout\ & (paddle_x(2) & !\Add15~5_sumout\))) # (paddle_x(3) & ((!\Add15~5_sumout\) # ((!\Add15~9_sumout\ & paddle_x(2))))) ) ) ) # ( !\Add15~13_sumout\ & ( 
-- paddle_x(1) & ( (!paddle_x(3) & (!\Add15~5_sumout\ & ((!\Add15~9_sumout\) # (paddle_x(2))))) # (paddle_x(3) & ((!\Add15~9_sumout\) # ((!\Add15~5_sumout\) # (paddle_x(2))))) ) ) ) # ( \Add15~13_sumout\ & ( !paddle_x(1) & ( (!paddle_x(3) & 
-- (!\Add15~9_sumout\ & (paddle_x(2) & !\Add15~5_sumout\))) # (paddle_x(3) & ((!\Add15~5_sumout\) # ((!\Add15~9_sumout\ & paddle_x(2))))) ) ) ) # ( !\Add15~13_sumout\ & ( !paddle_x(1) & ( (!paddle_x(3) & (!\Add15~9_sumout\ & (paddle_x(2) & 
-- !\Add15~5_sumout\))) # (paddle_x(3) & ((!\Add15~5_sumout\) # ((!\Add15~9_sumout\ & paddle_x(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101110100000100010111010000010011011111010001010101110100000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(3),
	datab => \ALT_INV_Add15~9_sumout\,
	datac => ALT_INV_paddle_x(2),
	datad => \ALT_INV_Add15~5_sumout\,
	datae => \ALT_INV_Add15~13_sumout\,
	dataf => ALT_INV_paddle_x(1),
	combout => \LessThan6~1_combout\);

-- Location: MLABCELL_X34_Y72_N42
\LessThan6~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~3_combout\ = ( paddle_x(6) & ( (!paddle_x(7) & (!\Add15~29_sumout\ & (!\Add15~25_sumout\ & !\Add15~21_sumout\))) # (paddle_x(7) & ((!\Add15~21_sumout\) # ((!\Add15~29_sumout\ & !\Add15~25_sumout\)))) ) ) # ( !paddle_x(6) & ( (!paddle_x(7) & 
-- (!\Add15~21_sumout\ & ((!\Add15~29_sumout\) # (!\Add15~25_sumout\)))) # (paddle_x(7) & ((!\Add15~29_sumout\) # ((!\Add15~25_sumout\) # (!\Add15~21_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111101100110010111110110011001010110011001000001011001100100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~29_sumout\,
	datab => ALT_INV_paddle_x(7),
	datac => \ALT_INV_Add15~25_sumout\,
	datad => \ALT_INV_Add15~21_sumout\,
	dataf => ALT_INV_paddle_x(6),
	combout => \LessThan6~3_combout\);

-- Location: MLABCELL_X34_Y72_N48
\LessThan6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan6~0_combout\ = ( \LessThan6~3_combout\ & ( ((!\Add15~1_sumout\ & ((!paddle_x(4)) # (\LessThan6~1_combout\))) # (\Add15~1_sumout\ & (\LessThan6~1_combout\ & !paddle_x(4)))) # (\LessThan6~2_combout\) ) ) # ( !\LessThan6~3_combout\ & ( 
-- (\LessThan6~2_combout\ & ((!\Add15~1_sumout\ & ((!paddle_x(4)) # (\LessThan6~1_combout\))) # (\Add15~1_sumout\ & (\LessThan6~1_combout\ & !paddle_x(4))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010100000100010001010000010011011111010111011101111101011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~2_combout\,
	datab => \ALT_INV_Add15~1_sumout\,
	datac => \ALT_INV_LessThan6~1_combout\,
	datad => ALT_INV_paddle_x(4),
	dataf => \ALT_INV_LessThan6~3_combout\,
	combout => \LessThan6~0_combout\);

-- Location: LABCELL_X31_Y71_N54
\Add16~5_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~5_wirecell_combout\ = ( !\Add16~5_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add16~5_sumout\,
	combout => \Add16~5_wirecell_combout\);

-- Location: FF_X31_Y71_N56
\puck_two.y[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~5_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y[4]~DUPLICATE_q\);

-- Location: LABCELL_X30_Y71_N30
\Add17~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~21_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(0)) ) + ( \Equal9~1_combout\ ) + ( !VCC ))
-- \Add17~22\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(0)) ) + ( \Equal9~1_combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_puck_velocity_two.y\(0),
	cin => GND,
	sumout => \Add17~21_sumout\,
	cout => \Add17~22\);

-- Location: LABCELL_X30_Y71_N33
\Add17~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~25_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(1)) ) + ( GND ) + ( \Add17~22\ ))
-- \Add17~26\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(1)) ) + ( GND ) + ( \Add17~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datad => \ALT_INV_puck_velocity_two.y\(1),
	cin => \Add17~22\,
	sumout => \Add17~25_sumout\,
	cout => \Add17~26\);

-- Location: LABCELL_X30_Y71_N0
\Add19~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~21_sumout\ = SUM(( (!\LessThan6~0_combout\ & (\Equal12~0_combout\ & !\LessThan7~5_combout\)) ) + ( !\Add17~21_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( !VCC ))
-- \Add19~22\ = CARRY(( (!\LessThan6~0_combout\ & (\Equal12~0_combout\ & !\LessThan7~5_combout\)) ) + ( !\Add17~21_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111110010000000000000000000000010000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	dataf => \ALT_INV_Add17~21_sumout\,
	cin => GND,
	sumout => \Add19~21_sumout\,
	cout => \Add19~22\);

-- Location: LABCELL_X30_Y71_N3
\Add19~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~25_sumout\ = SUM(( GND ) + ( !\Add17~25_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~22\ ))
-- \Add19~26\ = CARRY(( GND ) + ( !\Add17~25_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111110010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	dataf => \ALT_INV_Add17~25_sumout\,
	cin => \Add19~22\,
	sumout => \Add19~25_sumout\,
	cout => \Add19~26\);

-- Location: LABCELL_X33_Y71_N27
\Add19~25_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~25_wirecell_combout\ = !\Add19~25_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add19~25_sumout\,
	combout => \Add19~25_wirecell_combout\);

-- Location: FF_X33_Y71_N28
\puck_velocity_two.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~25_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(1));

-- Location: LABCELL_X30_Y71_N36
\Add17~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~29_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(2)) ) + ( GND ) + ( \Add17~26\ ))
-- \Add17~30\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(2)) ) + ( GND ) + ( \Add17~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_puck_velocity_two.y\(2),
	cin => \Add17~26\,
	sumout => \Add17~29_sumout\,
	cout => \Add17~30\);

-- Location: LABCELL_X30_Y71_N6
\Add19~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~29_sumout\ = SUM(( !\Add17~29_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( GND ) + ( \Add19~26\ ))
-- \Add19~30\ = CARRY(( !\Add17~29_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( GND ) + ( \Add19~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010000011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	datad => \ALT_INV_Add17~29_sumout\,
	cin => \Add19~26\,
	sumout => \Add19~29_sumout\,
	cout => \Add19~30\);

-- Location: LABCELL_X33_Y71_N3
\Add19~29_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~29_wirecell_combout\ = ( !\Add19~29_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add19~29_sumout\,
	combout => \Add19~29_wirecell_combout\);

-- Location: FF_X33_Y71_N4
\puck_velocity_two.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~29_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(2));

-- Location: LABCELL_X30_Y71_N39
\Add17~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~13_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(3)) ) + ( GND ) + ( \Add17~30\ ))
-- \Add17~14\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(3)) ) + ( GND ) + ( \Add17~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datad => \ALT_INV_puck_velocity_two.y\(3),
	cin => \Add17~30\,
	sumout => \Add17~13_sumout\,
	cout => \Add17~14\);

-- Location: LABCELL_X30_Y71_N9
\Add19~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~13_sumout\ = SUM(( GND ) + ( !\Add17~13_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~30\ ))
-- \Add19~14\ = CARRY(( GND ) + ( !\Add17~13_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111110010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	dataf => \ALT_INV_Add17~13_sumout\,
	cin => \Add19~30\,
	sumout => \Add19~13_sumout\,
	cout => \Add19~14\);

-- Location: LABCELL_X33_Y71_N0
\Add19~13_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~13_wirecell_combout\ = ( !\Add19~13_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add19~13_sumout\,
	combout => \Add19~13_wirecell_combout\);

-- Location: FF_X33_Y71_N1
\puck_velocity_two.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~13_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(3));

-- Location: LABCELL_X30_Y71_N42
\Add17~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~5_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(4)) ) + ( GND ) + ( \Add17~14\ ))
-- \Add17~6\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(4)) ) + ( GND ) + ( \Add17~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_puck_velocity_two.y\(4),
	cin => \Add17~14\,
	sumout => \Add17~5_sumout\,
	cout => \Add17~6\);

-- Location: LABCELL_X30_Y71_N12
\Add19~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~5_sumout\ = SUM(( GND ) + ( !\Add17~5_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~14\ ))
-- \Add19~6\ = CARRY(( GND ) + ( !\Add17~5_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111110010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	dataf => \ALT_INV_Add17~5_sumout\,
	cin => \Add19~14\,
	sumout => \Add19~5_sumout\,
	cout => \Add19~6\);

-- Location: LABCELL_X33_Y71_N24
\Add19~5_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~5_wirecell_combout\ = ( !\Add19~5_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add19~5_sumout\,
	combout => \Add19~5_wirecell_combout\);

-- Location: FF_X33_Y71_N25
\puck_velocity_two.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~5_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(4));

-- Location: LABCELL_X31_Y71_N45
\Selector70~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector70~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \puck_two.y\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_puck_two.y\(0),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector70~0_combout\);

-- Location: FF_X31_Y71_N2
\puck_two.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~21_sumout\,
	asdata => \Selector70~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(0));

-- Location: LABCELL_X31_Y71_N0
\Add16~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~21_sumout\ = SUM(( \puck_two.y\(0) ) + ( !\puck_velocity_two.y\(0) ) + ( !VCC ))
-- \Add16~22\ = CARRY(( \puck_two.y\(0) ) + ( !\puck_velocity_two.y\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_two.y\(0),
	datad => \ALT_INV_puck_two.y\(0),
	cin => GND,
	sumout => \Add16~21_sumout\,
	cout => \Add16~22\);

-- Location: LABCELL_X31_Y71_N3
\Add16~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~25_sumout\ = SUM(( \puck_two.y\(1) ) + ( !\puck_velocity_two.y\(1) ) + ( \Add16~22\ ))
-- \Add16~26\ = CARRY(( \puck_two.y\(1) ) + ( !\puck_velocity_two.y\(1) ) + ( \Add16~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_two.y\(1),
	datad => \ALT_INV_puck_two.y\(1),
	cin => \Add16~22\,
	sumout => \Add16~25_sumout\,
	cout => \Add16~26\);

-- Location: LABCELL_X31_Y71_N57
\Selector69~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector69~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( \puck_two.y\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_two.y\(1),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector69~0_combout\);

-- Location: FF_X31_Y71_N5
\puck_two.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~25_sumout\,
	asdata => \Selector69~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(1));

-- Location: LABCELL_X31_Y71_N6
\Add16~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~29_sumout\ = SUM(( !\puck_velocity_two.y\(2) ) + ( !\puck_two.y\(2) ) + ( \Add16~26\ ))
-- \Add16~30\ = CARRY(( !\puck_velocity_two.y\(2) ) + ( !\puck_two.y\(2) ) + ( \Add16~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_velocity_two.y\(2),
	datac => \ALT_INV_puck_two.y\(2),
	cin => \Add16~26\,
	sumout => \Add16~29_sumout\,
	cout => \Add16~30\);

-- Location: LABCELL_X31_Y71_N33
\Add16~29_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~29_wirecell_combout\ = !\Add16~29_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add16~29_sumout\,
	combout => \Add16~29_wirecell_combout\);

-- Location: FF_X31_Y71_N35
\puck_two.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~29_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(2));

-- Location: LABCELL_X31_Y71_N9
\Add16~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~13_sumout\ = SUM(( !\puck_velocity_two.y\(3) ) + ( !\puck_two.y\(3) ) + ( \Add16~30\ ))
-- \Add16~14\ = CARRY(( !\puck_velocity_two.y\(3) ) + ( !\puck_two.y\(3) ) + ( \Add16~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_two.y\(3),
	datac => \ALT_INV_puck_two.y\(3),
	cin => \Add16~30\,
	sumout => \Add16~13_sumout\,
	cout => \Add16~14\);

-- Location: LABCELL_X31_Y71_N30
\Add16~13_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~13_wirecell_combout\ = !\Add16~13_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Add16~13_sumout\,
	combout => \Add16~13_wirecell_combout\);

-- Location: FF_X31_Y71_N32
\puck_two.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~13_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(3));

-- Location: LABCELL_X31_Y71_N12
\Add16~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~5_sumout\ = SUM(( !\puck_velocity_two.y\(4) ) + ( !\puck_two.y[4]~DUPLICATE_q\ ) + ( \Add16~14\ ))
-- \Add16~6\ = CARRY(( !\puck_velocity_two.y\(4) ) + ( !\puck_two.y[4]~DUPLICATE_q\ ) + ( \Add16~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_two.y[4]~DUPLICATE_q\,
	datac => \ALT_INV_puck_velocity_two.y\(4),
	cin => \Add16~14\,
	sumout => \Add16~5_sumout\,
	cout => \Add16~6\);

-- Location: FF_X30_Y71_N26
\puck_velocity_two.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~1_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(6));

-- Location: LABCELL_X30_Y71_N45
\Add17~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~9_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(5)) ) + ( GND ) + ( \Add17~6\ ))
-- \Add17~10\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(5)) ) + ( GND ) + ( \Add17~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_puck_velocity_two.y\(5),
	cin => \Add17~6\,
	sumout => \Add17~9_sumout\,
	cout => \Add17~10\);

-- Location: LABCELL_X30_Y71_N48
\Add17~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~1_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(6)) ) + ( GND ) + ( \Add17~10\ ))
-- \Add17~2\ = CARRY(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(6)) ) + ( GND ) + ( \Add17~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_puck_velocity_two.y\(6),
	cin => \Add17~10\,
	sumout => \Add17~1_sumout\,
	cout => \Add17~2\);

-- Location: LABCELL_X30_Y71_N15
\Add19~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~9_sumout\ = SUM(( !\Add17~9_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( GND ) + ( \Add19~6\ ))
-- \Add19~10\ = CARRY(( !\Add17~9_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( GND ) + ( \Add19~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010000011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	datad => \ALT_INV_Add17~9_sumout\,
	cin => \Add19~6\,
	sumout => \Add19~9_sumout\,
	cout => \Add19~10\);

-- Location: LABCELL_X30_Y71_N18
\Add19~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~1_sumout\ = SUM(( GND ) + ( !\Add17~1_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~10\ ))
-- \Add19~2\ = CARRY(( GND ) + ( !\Add17~1_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111110010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	dataf => \ALT_INV_Add17~1_sumout\,
	cin => \Add19~10\,
	sumout => \Add19~1_sumout\,
	cout => \Add19~2\);

-- Location: LABCELL_X30_Y71_N24
\Add19~1_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~1_wirecell_combout\ = ( !\Add19~1_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add19~1_sumout\,
	combout => \Add19~1_wirecell_combout\);

-- Location: FF_X30_Y71_N25
\puck_velocity_two.y[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~1_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y[6]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y71_N39
\Selector64~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector64~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_two.y\(6))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_two.y\(6),
	combout => \Selector64~0_combout\);

-- Location: FF_X31_Y71_N20
\puck_two.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~1_sumout\,
	asdata => \Selector64~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(6));

-- Location: LABCELL_X31_Y71_N15
\Add16~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~9_sumout\ = SUM(( !\puck_two.y\(5) ) + ( !\puck_velocity_two.y\(5) ) + ( \Add16~6\ ))
-- \Add16~10\ = CARRY(( !\puck_two.y\(5) ) + ( !\puck_velocity_two.y\(5) ) + ( \Add16~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_two.y\(5),
	datad => \ALT_INV_puck_two.y\(5),
	cin => \Add16~6\,
	sumout => \Add16~9_sumout\,
	cout => \Add16~10\);

-- Location: LABCELL_X31_Y71_N18
\Add16~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~1_sumout\ = SUM(( \puck_two.y\(6) ) + ( !\puck_velocity_two.y[6]~DUPLICATE_q\ ) + ( \Add16~10\ ))
-- \Add16~2\ = CARRY(( \puck_two.y\(6) ) + ( !\puck_velocity_two.y[6]~DUPLICATE_q\ ) + ( \Add16~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001100110011001100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_puck_velocity_two.y[6]~DUPLICATE_q\,
	datad => \ALT_INV_puck_two.y\(6),
	cin => \Add16~10\,
	sumout => \Add16~1_sumout\,
	cout => \Add16~2\);

-- Location: LABCELL_X30_Y71_N51
\Add17~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add17~17_sumout\ = SUM(( !\Equal9~1_combout\ $ (\puck_velocity_two.y\(7)) ) + ( GND ) + ( \Add17~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100001111000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal9~1_combout\,
	datac => \ALT_INV_puck_velocity_two.y\(7),
	cin => \Add17~2\,
	sumout => \Add17~17_sumout\);

-- Location: LABCELL_X30_Y71_N21
\Add19~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~17_sumout\ = SUM(( GND ) + ( !\Add17~17_sumout\ $ ((((!\Equal12~0_combout\) # (\LessThan7~5_combout\)) # (\LessThan6~0_combout\))) ) + ( \Add19~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110111110010000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan6~0_combout\,
	datab => \ALT_INV_Equal12~0_combout\,
	datac => \ALT_INV_LessThan7~5_combout\,
	dataf => \ALT_INV_Add17~17_sumout\,
	cin => \Add19~2\,
	sumout => \Add19~17_sumout\);

-- Location: LABCELL_X30_Y71_N57
\Add19~17_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~17_wirecell_combout\ = ( !\Add19~17_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add19~17_sumout\,
	combout => \Add19~17_wirecell_combout\);

-- Location: FF_X30_Y71_N59
\puck_velocity_two.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~17_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(7));

-- Location: LABCELL_X31_Y71_N36
\Selector63~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector63~0_combout\ = (\state.INIT~DUPLICATE_q\ & \puck_two.y\(7))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.INIT~DUPLICATE_q\,
	datac => \ALT_INV_puck_two.y\(7),
	combout => \Selector63~0_combout\);

-- Location: FF_X31_Y71_N23
\puck_two.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~17_sumout\,
	asdata => \Selector63~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(7));

-- Location: LABCELL_X31_Y71_N21
\Add16~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~17_sumout\ = SUM(( \puck_two.y\(7) ) + ( !\puck_velocity_two.y\(7) ) + ( \Add16~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_velocity_two.y\(7),
	datad => \ALT_INV_puck_two.y\(7),
	cin => \Add16~2\,
	sumout => \Add16~17_sumout\);

-- Location: LABCELL_X31_Y71_N24
\Equal9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal9~1_combout\ = ( !\Add16~17_sumout\ & ( !\Add16~9_sumout\ & ( (\Equal9~0_combout\ & (!\Add16~5_sumout\ & (!\Add16~1_sumout\ & !\Add16~13_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal9~0_combout\,
	datab => \ALT_INV_Add16~5_sumout\,
	datac => \ALT_INV_Add16~1_sumout\,
	datad => \ALT_INV_Add16~13_sumout\,
	datae => \ALT_INV_Add16~17_sumout\,
	dataf => \ALT_INV_Add16~9_sumout\,
	combout => \Equal9~1_combout\);

-- Location: LABCELL_X33_Y71_N6
\Add19~21_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~21_wirecell_combout\ = !\Add19~21_sumout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add19~21_sumout\,
	combout => \Add19~21_wirecell_combout\);

-- Location: FF_X33_Y71_N7
\puck_velocity_two.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~21_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(0));

-- Location: LABCELL_X31_Y71_N42
\Equal9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal9~0_combout\ = ( \Add16~29_sumout\ & ( (\Add16~21_sumout\ & !\Add16~25_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010000010100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add16~21_sumout\,
	datac => \ALT_INV_Add16~25_sumout\,
	dataf => \ALT_INV_Add16~29_sumout\,
	combout => \Equal9~0_combout\);

-- Location: LABCELL_X31_Y71_N48
\Equal12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal12~0_combout\ = ( !\Add16~17_sumout\ & ( \Add16~9_sumout\ & ( (\Equal9~0_combout\ & (\Add16~5_sumout\ & (\Add16~1_sumout\ & !\Add16~13_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000001000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal9~0_combout\,
	datab => \ALT_INV_Add16~5_sumout\,
	datac => \ALT_INV_Add16~1_sumout\,
	datad => \ALT_INV_Add16~13_sumout\,
	datae => \ALT_INV_Add16~17_sumout\,
	dataf => \ALT_INV_Add16~9_sumout\,
	combout => \Equal12~0_combout\);

-- Location: LABCELL_X30_Y71_N27
\Add19~9_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add19~9_wirecell_combout\ = ( !\Add19~9_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add19~9_sumout\,
	combout => \Add19~9_wirecell_combout\);

-- Location: FF_X30_Y71_N28
\puck_velocity_two.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add19~9_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_velocity_two.y\(5));

-- Location: LABCELL_X33_Y71_N9
\Add16~9_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add16~9_wirecell_combout\ = ( !\Add16~9_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_Add16~9_sumout\,
	combout => \Add16~9_wirecell_combout\);

-- Location: FF_X33_Y71_N10
\puck_two.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~9_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(5));

-- Location: MLABCELL_X34_Y71_N45
\draw.y[6]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~3_combout\ = ( !\state.DRAW_PUCK_ONE~q\ & ( (!\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & \WideOr1~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datac => \ALT_INV_WideOr1~0_combout\,
	dataf => \ALT_INV_state.DRAW_PUCK_ONE~q\,
	combout => \draw.y[6]~3_combout\);

-- Location: LABCELL_X33_Y71_N39
\draw.y[6]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~4_combout\ = ( \draw.y[6]~2_combout\ & ( (\draw.y[3]~0_combout\ & ((!\draw.y[6]~3_combout\) # ((\state.ERASE_PADDLE_ENTER~q\) # (\WideOr6~0_combout\)))) ) ) # ( !\draw.y[6]~2_combout\ & ( \draw.y[3]~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100100011001100110010001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y[6]~3_combout\,
	datab => \ALT_INV_draw.y[3]~0_combout\,
	datac => \ALT_INV_WideOr6~0_combout\,
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => \ALT_INV_draw.y[6]~2_combout\,
	combout => \draw.y[6]~4_combout\);

-- Location: FF_X31_Y71_N55
\puck_two.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add16~5_wirecell_combout\,
	asdata => \~GND~combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sload => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	ena => \WideOr7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \puck_two.y\(4));

-- Location: MLABCELL_X39_Y71_N0
\Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~25_sumout\ = SUM(( \draw.y\(0) ) + ( VCC ) + ( !VCC ))
-- \Add0~26\ = CARRY(( \draw.y\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(0),
	cin => GND,
	sumout => \Add0~25_sumout\,
	cout => \Add0~26\);

-- Location: MLABCELL_X39_Y71_N48
\Selector15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector15~0_combout\ = ( \puck_two.y\(0) & ( (!\draw.y[6]~4_combout\ & (((!\draw.y[3]~1_combout\)) # (\Add0~25_sumout\))) # (\draw.y[6]~4_combout\ & (((!\draw.y[3]~1_combout\ & \puck_one.y\(0))))) ) ) # ( !\puck_two.y\(0) & ( (!\draw.y[6]~4_combout\ & 
-- (\Add0~25_sumout\ & (\draw.y[3]~1_combout\))) # (\draw.y[6]~4_combout\ & (((!\draw.y[3]~1_combout\ & \puck_one.y\(0))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000110100000001000011010011000100111101001100010011110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add0~25_sumout\,
	datab => \ALT_INV_draw.y[6]~4_combout\,
	datac => \ALT_INV_draw.y[3]~1_combout\,
	datad => \ALT_INV_puck_one.y\(0),
	dataf => \ALT_INV_puck_two.y\(0),
	combout => \Selector15~0_combout\);

-- Location: LABCELL_X37_Y71_N24
\Equal4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~2_combout\ = (!\draw.x\(7) & (!\Add13~25_sumout\ & (!\Add13~21_sumout\ $ (\draw.x\(6))))) # (\draw.x\(7) & (\Add13~25_sumout\ & (!\Add13~21_sumout\ $ (\draw.x\(6)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001000000001001100100000000100110010000000010011001000000001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(7),
	datab => \ALT_INV_Add13~25_sumout\,
	datac => \ALT_INV_Add13~21_sumout\,
	datad => \ALT_INV_draw.x\(6),
	combout => \Equal4~2_combout\);

-- Location: FF_X37_Y71_N38
\state.DRAW_PADDLE_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.y[6]~8_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_ENTER~q\);

-- Location: LABCELL_X37_Y71_N12
\Selector102~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector102~0_combout\ = ( \state.DRAW_PADDLE_ENTER~q\ ) # ( !\state.DRAW_PADDLE_ENTER~q\ & ( (!\Equal4~3_combout\ & \state.DRAW_PADDLE_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~3_combout\,
	datad => \ALT_INV_state.DRAW_PADDLE_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~q\,
	combout => \Selector102~0_combout\);

-- Location: FF_X37_Y71_N14
\state.DRAW_PADDLE_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector102~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_LOOP~q\);

-- Location: LABCELL_X37_Y71_N0
\Selector100~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector100~0_combout\ = ((!\Equal4~3_combout\ & \state.ERASE_PADDLE_LOOP~q\)) # (\state.ERASE_PADDLE_ENTER~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111110101111000011111010111100001111101011110000111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~3_combout\,
	datac => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	datad => \ALT_INV_state.ERASE_PADDLE_LOOP~q\,
	combout => \Selector100~0_combout\);

-- Location: FF_X37_Y71_N1
\state.ERASE_PADDLE_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector100~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_LOOP~q\);

-- Location: LABCELL_X37_Y71_N9
\draw.x[4]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~2_combout\ = ( !\state.DRAW_TOP_LOOP~q\ & ( (\state.ERASE_PADDLE_LOOP~q\) # (\state.DRAW_PADDLE_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.DRAW_PADDLE_LOOP~q\,
	datad => \ALT_INV_state.ERASE_PADDLE_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_TOP_LOOP~q\,
	combout => \draw.x[4]~2_combout\);

-- Location: LABCELL_X35_Y71_N12
\Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = ( \draw.x\(2) & ( (\Equal2~0_combout\ & \vga_u0|writeEn~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal2~0_combout\,
	datad => \vga_u0|ALT_INV_writeEn~1_combout\,
	dataf => \ALT_INV_draw.x\(2),
	combout => \Equal0~0_combout\);

-- Location: LABCELL_X35_Y71_N24
\draw.x[4]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~1_combout\ = ( \Equal1~0_combout\ & ( (!\state.IDLE~q\ & ((!\Equal1~1_combout\) # (!\state.DRAW_RIGHT_LOOP~DUPLICATE_q\))) ) ) # ( !\Equal1~0_combout\ & ( !\state.IDLE~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111010000000001111101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~1_combout\,
	datac => \ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\,
	datad => \ALT_INV_state.IDLE~q\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \draw.x[4]~1_combout\);

-- Location: LABCELL_X35_Y71_N36
\draw.y[6]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~5_combout\ = ( \Equal1~2_combout\ & ( !\draw.x[4]~0_combout\ & ( (!\state.START~q\ & (\draw.x[4]~1_combout\ & !\state.DRAW_LEFT_LOOP~q\)) ) ) ) # ( !\Equal1~2_combout\ & ( !\draw.x[4]~0_combout\ & ( (\draw.x[4]~1_combout\ & ((!\state.START~q\) 
-- # (\Equal0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101100001011000010100000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_draw.x[4]~1_combout\,
	datad => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	datae => \ALT_INV_Equal1~2_combout\,
	dataf => \ALT_INV_draw.x[4]~0_combout\,
	combout => \draw.y[6]~5_combout\);

-- Location: LABCELL_X37_Y71_N45
\draw.y[3]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[3]~7_combout\ = ( \draw.y[6]~5_combout\ & ( (!\Equal4~1_combout\) # ((!\Equal4~2_combout\) # ((!\draw.x[4]~2_combout\) # (!\Equal4~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111101111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~1_combout\,
	datab => \ALT_INV_Equal4~2_combout\,
	datac => \ALT_INV_draw.x[4]~2_combout\,
	datad => \ALT_INV_Equal4~0_combout\,
	dataf => \ALT_INV_draw.y[6]~5_combout\,
	combout => \draw.y[3]~7_combout\);

-- Location: FF_X39_Y71_N49
\draw.y[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector15~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(0));

-- Location: MLABCELL_X39_Y71_N3
\Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~21_sumout\ = SUM(( \draw.y\(1) ) + ( GND ) + ( \Add0~26\ ))
-- \Add0~22\ = CARRY(( \draw.y\(1) ) + ( GND ) + ( \Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(1),
	cin => \Add0~26\,
	sumout => \Add0~21_sumout\,
	cout => \Add0~22\);

-- Location: MLABCELL_X39_Y71_N54
\Selector14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector14~0_combout\ = ( \puck_one.y\(1) & ( \draw.y[3]~1_combout\ & ( (!\draw.y[6]~4_combout\ & (\Add0~21_sumout\)) # (\draw.y[6]~4_combout\ & ((!\Selector9~0_combout\))) ) ) ) # ( !\puck_one.y\(1) & ( \draw.y[3]~1_combout\ & ( (!\draw.y[6]~4_combout\ 
-- & (\Add0~21_sumout\)) # (\draw.y[6]~4_combout\ & ((!\Selector9~0_combout\))) ) ) ) # ( \puck_one.y\(1) & ( !\draw.y[3]~1_combout\ & ( (\puck_two.y\(1)) # (\draw.y[6]~4_combout\) ) ) ) # ( !\puck_one.y\(1) & ( !\draw.y[3]~1_combout\ & ( 
-- (!\draw.y[6]~4_combout\ & \puck_two.y\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100001111110011111101110111010001000111011101000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add0~21_sumout\,
	datab => \ALT_INV_draw.y[6]~4_combout\,
	datac => \ALT_INV_puck_two.y\(1),
	datad => \ALT_INV_Selector9~0_combout\,
	datae => \ALT_INV_puck_one.y\(1),
	dataf => \ALT_INV_draw.y[3]~1_combout\,
	combout => \Selector14~0_combout\);

-- Location: LABCELL_X37_Y71_N30
\draw.y[6]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~6_combout\ = ( \state.ERASE_PADDLE_LOOP~q\ & ( \draw.y[6]~5_combout\ & ( (!\Equal4~0_combout\) # ((!\Equal4~1_combout\) # (!\Equal4~2_combout\)) ) ) ) # ( !\state.ERASE_PADDLE_LOOP~q\ & ( \draw.y[6]~5_combout\ & ( (!\Equal4~0_combout\) # 
-- ((!\state.DRAW_PADDLE_LOOP~q\) # ((!\Equal4~1_combout\) # (!\Equal4~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111101111111111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~0_combout\,
	datab => \ALT_INV_state.DRAW_PADDLE_LOOP~q\,
	datac => \ALT_INV_Equal4~1_combout\,
	datad => \ALT_INV_Equal4~2_combout\,
	datae => \ALT_INV_state.ERASE_PADDLE_LOOP~q\,
	dataf => \ALT_INV_draw.y[6]~5_combout\,
	combout => \draw.y[6]~6_combout\);

-- Location: FF_X39_Y71_N55
\draw.y[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector14~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(1));

-- Location: MLABCELL_X39_Y71_N6
\Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~17_sumout\ = SUM(( \draw.y\(2) ) + ( GND ) + ( \Add0~22\ ))
-- \Add0~18\ = CARRY(( \draw.y\(2) ) + ( GND ) + ( \Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(2),
	cin => \Add0~22\,
	sumout => \Add0~17_sumout\,
	cout => \Add0~18\);

-- Location: MLABCELL_X39_Y71_N9
\Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~13_sumout\ = SUM(( \draw.y\(3) ) + ( GND ) + ( \Add0~18\ ))
-- \Add0~14\ = CARRY(( \draw.y\(3) ) + ( GND ) + ( \Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(3),
	cin => \Add0~18\,
	sumout => \Add0~13_sumout\,
	cout => \Add0~14\);

-- Location: MLABCELL_X39_Y71_N42
\Selector12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector12~0_combout\ = ( \Add0~13_sumout\ & ( (!\draw.y[3]~1_combout\ & ((!\draw.y[6]~4_combout\ & (!\puck_two.y\(3))) # (\draw.y[6]~4_combout\ & ((!\puck_one.y\(3)))))) # (\draw.y[3]~1_combout\ & (!\draw.y[6]~4_combout\)) ) ) # ( !\Add0~13_sumout\ & ( 
-- (!\draw.y[3]~1_combout\ & ((!\draw.y[6]~4_combout\ & (!\puck_two.y\(3))) # (\draw.y[6]~4_combout\ & ((!\puck_one.y\(3)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010001010000000101000101000000011100110110001001110011011000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y[3]~1_combout\,
	datab => \ALT_INV_draw.y[6]~4_combout\,
	datac => \ALT_INV_puck_two.y\(3),
	datad => \ALT_INV_puck_one.y\(3),
	dataf => \ALT_INV_Add0~13_sumout\,
	combout => \Selector12~0_combout\);

-- Location: FF_X39_Y71_N44
\draw.y[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector12~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(3));

-- Location: MLABCELL_X39_Y71_N12
\Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~9_sumout\ = SUM(( \draw.y\(4) ) + ( GND ) + ( \Add0~14\ ))
-- \Add0~10\ = CARRY(( \draw.y\(4) ) + ( GND ) + ( \Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(4),
	cin => \Add0~14\,
	sumout => \Add0~9_sumout\,
	cout => \Add0~10\);

-- Location: MLABCELL_X39_Y71_N36
\Selector11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector11~0_combout\ = ( \draw.y[6]~4_combout\ & ( \Add0~9_sumout\ & ( (!\draw.y[3]~1_combout\ & ((!\puck_one.y\(4)))) # (\draw.y[3]~1_combout\ & (!\Selector9~0_combout\)) ) ) ) # ( !\draw.y[6]~4_combout\ & ( \Add0~9_sumout\ & ( (!\puck_two.y\(4)) # 
-- (\draw.y[3]~1_combout\) ) ) ) # ( \draw.y[6]~4_combout\ & ( !\Add0~9_sumout\ & ( (!\draw.y[3]~1_combout\ & ((!\puck_one.y\(4)))) # (\draw.y[3]~1_combout\ & (!\Selector9~0_combout\)) ) ) ) # ( !\draw.y[6]~4_combout\ & ( !\Add0~9_sumout\ & ( 
-- (!\draw.y[3]~1_combout\ & !\puck_two.y\(4)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000111011100100010011110101111101011110111001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y[3]~1_combout\,
	datab => \ALT_INV_Selector9~0_combout\,
	datac => \ALT_INV_puck_two.y\(4),
	datad => \ALT_INV_puck_one.y\(4),
	datae => \ALT_INV_draw.y[6]~4_combout\,
	dataf => \ALT_INV_Add0~9_sumout\,
	combout => \Selector11~0_combout\);

-- Location: FF_X39_Y71_N38
\draw.y[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector11~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(4));

-- Location: MLABCELL_X39_Y71_N15
\Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~5_sumout\ = SUM(( \draw.y\(5) ) + ( GND ) + ( \Add0~10\ ))
-- \Add0~6\ = CARRY(( \draw.y\(5) ) + ( GND ) + ( \Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(5),
	cin => \Add0~10\,
	sumout => \Add0~5_sumout\,
	cout => \Add0~6\);

-- Location: MLABCELL_X39_Y71_N30
\Selector10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector10~0_combout\ = ( \draw.y[6]~4_combout\ & ( \Add0~5_sumout\ & ( (!\draw.y[3]~1_combout\ & ((!\puck_one.y\(5)))) # (\draw.y[3]~1_combout\ & (!\Selector9~0_combout\)) ) ) ) # ( !\draw.y[6]~4_combout\ & ( \Add0~5_sumout\ & ( (!\puck_two.y\(5)) # 
-- (\draw.y[3]~1_combout\) ) ) ) # ( \draw.y[6]~4_combout\ & ( !\Add0~5_sumout\ & ( (!\draw.y[3]~1_combout\ & ((!\puck_one.y\(5)))) # (\draw.y[3]~1_combout\ & (!\Selector9~0_combout\)) ) ) ) # ( !\draw.y[6]~4_combout\ & ( !\Add0~5_sumout\ & ( 
-- (!\draw.y[3]~1_combout\ & !\puck_two.y\(5)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000111011100100010011110101111101011110111001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y[3]~1_combout\,
	datab => \ALT_INV_Selector9~0_combout\,
	datac => \ALT_INV_puck_two.y\(5),
	datad => \ALT_INV_puck_one.y\(5),
	datae => \ALT_INV_draw.y[6]~4_combout\,
	dataf => \ALT_INV_Add0~5_sumout\,
	combout => \Selector10~0_combout\);

-- Location: FF_X39_Y71_N31
\draw.y[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector10~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(5));

-- Location: MLABCELL_X39_Y71_N18
\Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~1_sumout\ = SUM(( \draw.y\(6) ) + ( GND ) + ( \Add0~6\ ))
-- \Add0~2\ = CARRY(( \draw.y\(6) ) + ( GND ) + ( \Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(6),
	cin => \Add0~6\,
	sumout => \Add0~1_sumout\,
	cout => \Add0~2\);

-- Location: MLABCELL_X39_Y71_N24
\Selector9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector9~1_combout\ = ( \draw.y[6]~4_combout\ & ( \puck_one.y\(6) & ( (!\draw.y[3]~1_combout\) # (!\Selector9~0_combout\) ) ) ) # ( !\draw.y[6]~4_combout\ & ( \puck_one.y\(6) & ( (!\draw.y[3]~1_combout\ & ((\puck_two.y\(6)))) # (\draw.y[3]~1_combout\ & 
-- (\Add0~1_sumout\)) ) ) ) # ( \draw.y[6]~4_combout\ & ( !\puck_one.y\(6) & ( (\draw.y[3]~1_combout\ & !\Selector9~0_combout\) ) ) ) # ( !\draw.y[6]~4_combout\ & ( !\puck_one.y\(6) & ( (!\draw.y[3]~1_combout\ & ((\puck_two.y\(6)))) # (\draw.y[3]~1_combout\ 
-- & (\Add0~1_sumout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011010100110101000011110000000000110101001101011111111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add0~1_sumout\,
	datab => \ALT_INV_puck_two.y\(6),
	datac => \ALT_INV_draw.y[3]~1_combout\,
	datad => \ALT_INV_Selector9~0_combout\,
	datae => \ALT_INV_draw.y[6]~4_combout\,
	dataf => \ALT_INV_puck_one.y\(6),
	combout => \Selector9~1_combout\);

-- Location: FF_X39_Y71_N26
\draw.y[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector9~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(6));

-- Location: MLABCELL_X39_Y72_N39
\Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = ( \draw.y\(6) & ( (\draw.y\(5) & \draw.y\(4)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(5),
	datad => \ALT_INV_draw.y\(4),
	dataf => \ALT_INV_draw.y\(6),
	combout => \Equal1~0_combout\);

-- Location: LABCELL_X35_Y71_N9
\draw.x[4]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~8_combout\ = ( \Equal1~0_combout\ & ( (\Equal1~1_combout\ & \state.DRAW_RIGHT_LOOP~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~1_combout\,
	datad => \ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \draw.x[4]~8_combout\);

-- Location: FF_X35_Y71_N11
\state.DRAW_LEFT_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.x[4]~8_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_LEFT_ENTER~q\);

-- Location: MLABCELL_X34_Y71_N33
\WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \WideOr1~0_combout\ = ( !\state.DRAW_RIGHT_ENTER~q\ & ( (!\state.DRAW_TOP_ENTER~q\ & !\state.DRAW_LEFT_ENTER~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010001000100010001000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datab => \ALT_INV_state.DRAW_LEFT_ENTER~q\,
	dataf => \ALT_INV_state.DRAW_RIGHT_ENTER~q\,
	combout => \WideOr1~0_combout\);

-- Location: LABCELL_X35_Y71_N48
\Selector7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~1_combout\ = ( \Equal1~0_combout\ & ( (!\Equal1~1_combout\ & (((!\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & !\state.DRAW_LEFT_LOOP~q\)))) # (\Equal1~1_combout\ & ((!\draw.x\(0)) # ((!\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & 
-- !\state.DRAW_LEFT_LOOP~q\)))) ) ) # ( !\Equal1~0_combout\ & ( (!\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & !\state.DRAW_LEFT_LOOP~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000011110100010001001111010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~1_combout\,
	datab => \ALT_INV_draw.x\(0),
	datac => \ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\,
	datad => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \Selector7~1_combout\);

-- Location: LABCELL_X36_Y71_N27
\Selector7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~2_combout\ = ( \vga_u0|writeEn~1_combout\ & ( (\state.DRAW_TOP_LOOP~DUPLICATE_q\ & (((!\draw.x\(2) & \Equal2~0_combout\)) # (\Add1~13_sumout\))) ) ) # ( !\vga_u0|writeEn~1_combout\ & ( (\state.DRAW_TOP_LOOP~DUPLICATE_q\ & \Add1~13_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000010001100110000001000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(2),
	datab => \ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\,
	datac => \ALT_INV_Equal2~0_combout\,
	datad => \ALT_INV_Add1~13_sumout\,
	dataf => \vga_u0|ALT_INV_writeEn~1_combout\,
	combout => \Selector7~2_combout\);

-- Location: LABCELL_X35_Y71_N51
\Selector7~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~3_combout\ = ( \draw.y[3]~0_combout\ & ( (\draw.x\(0) & \state.IDLE~q\) ) ) # ( !\draw.y[3]~0_combout\ & ( ((\draw.x\(0) & \state.IDLE~q\)) # (\puck_two.x\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111111111000000111111111100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x\(0),
	datac => \ALT_INV_state.IDLE~q\,
	datad => \ALT_INV_puck_two.x\(0),
	dataf => \ALT_INV_draw.y[3]~0_combout\,
	combout => \Selector7~3_combout\);

-- Location: LABCELL_X36_Y71_N30
\Selector7~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~4_combout\ = ( !\Selector7~3_combout\ & ( \puck_one.x\(0) & ( (\WideOr1~0_combout\ & (\Selector7~1_combout\ & (!\Selector7~2_combout\ & \draw~0_combout\))) ) ) ) # ( !\Selector7~3_combout\ & ( !\puck_one.x\(0) & ( (\WideOr1~0_combout\ & 
-- (\Selector7~1_combout\ & !\Selector7~2_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010000000000000000000000000000000100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_WideOr1~0_combout\,
	datab => \ALT_INV_Selector7~1_combout\,
	datac => \ALT_INV_Selector7~2_combout\,
	datad => \ALT_INV_draw~0_combout\,
	datae => \ALT_INV_Selector7~3_combout\,
	dataf => \ALT_INV_puck_one.x\(0),
	combout => \Selector7~4_combout\);

-- Location: LABCELL_X37_Y71_N27
\draw.x[4]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~7_combout\ = ( !\state.DRAW_PADDLE_LOOP~q\ & ( !\state.ERASE_PADDLE_LOOP~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_state.ERASE_PADDLE_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_LOOP~q\,
	combout => \draw.x[4]~7_combout\);

-- Location: LABCELL_X35_Y71_N45
\Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = ( \state.START~q\ & ( (!\Equal0~0_combout\ & ((\Add1~13_sumout\))) # (\Equal0~0_combout\ & (\Equal1~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101111101010000010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~2_combout\,
	datac => \ALT_INV_Equal0~0_combout\,
	datad => \ALT_INV_Add1~13_sumout\,
	dataf => \ALT_INV_state.START~q\,
	combout => \Selector7~0_combout\);

-- Location: LABCELL_X36_Y71_N0
\Selector7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~5_combout\ = ( \draw.x\(0) & ( \Selector7~0_combout\ ) ) # ( !\draw.x\(0) & ( \Selector7~0_combout\ ) ) # ( \draw.x\(0) & ( !\Selector7~0_combout\ & ( (!\Selector7~4_combout\) # ((!\draw.x[4]~7_combout\ & ((\Equal4~3_combout\) # 
-- (\Add1~13_sumout\)))) ) ) ) # ( !\draw.x\(0) & ( !\Selector7~0_combout\ & ( (!\Selector7~4_combout\) # ((\Add1~13_sumout\ & (!\draw.x[4]~7_combout\ & !\Equal4~3_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101110011001100110111001111110011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add1~13_sumout\,
	datab => \ALT_INV_Selector7~4_combout\,
	datac => \ALT_INV_draw.x[4]~7_combout\,
	datad => \ALT_INV_Equal4~3_combout\,
	datae => \ALT_INV_draw.x\(0),
	dataf => \ALT_INV_Selector7~0_combout\,
	combout => \Selector7~5_combout\);

-- Location: FF_X36_Y71_N1
\draw.x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector7~5_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(0));

-- Location: LABCELL_X36_Y72_N3
\Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~17_sumout\ = SUM(( \draw.x\(1) ) + ( GND ) + ( \Add1~14\ ))
-- \Add1~18\ = CARRY(( \draw.x\(1) ) + ( GND ) + ( \Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(1),
	cin => \Add1~14\,
	sumout => \Add1~17_sumout\,
	cout => \Add1~18\);

-- Location: LABCELL_X37_Y72_N21
\Selector6~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~2_combout\ = ( !\state.DRAW_RIGHT_ENTER~q\ & ( (!\state.DRAW_RIGHT_LOOP~q\ & ((!\puck_one.x\(1)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000011110000101000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_one.x\(1),
	datac => \ALT_INV_state.DRAW_RIGHT_LOOP~q\,
	datad => \ALT_INV_draw~0_combout\,
	dataf => \ALT_INV_state.DRAW_RIGHT_ENTER~q\,
	combout => \Selector6~2_combout\);

-- Location: LABCELL_X37_Y72_N12
\Selector6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~1_combout\ = ( paddle_x(1) & ( ((\puck_two.x\(1) & !\draw.y[3]~0_combout\)) # (\state.ERASE_PADDLE_ENTER~q\) ) ) # ( !paddle_x(1) & ( (\puck_two.x\(1) & !\draw.y[3]~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000010100000101000001010000111111110101000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_two.x\(1),
	datac => \ALT_INV_draw.y[3]~0_combout\,
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => ALT_INV_paddle_x(1),
	combout => \Selector6~1_combout\);

-- Location: LABCELL_X37_Y72_N51
\Selector6~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~3_combout\ = ( !\Selector6~1_combout\ & ( (\Selector6~2_combout\ & ((!\Add1~17_sumout\) # (\draw.x[4]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110011000000001111001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x[4]~3_combout\,
	datac => \ALT_INV_Add1~17_sumout\,
	datad => \ALT_INV_Selector6~2_combout\,
	dataf => \ALT_INV_Selector6~1_combout\,
	combout => \Selector6~3_combout\);

-- Location: LABCELL_X36_Y72_N33
\Selector6~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~4_combout\ = ( paddle_x(1) & ( (!\Selector6~3_combout\) # ((\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((\Selector6~0_combout\) # (\paddle_x~5_combout\)))) ) ) # ( !paddle_x(1) & ( (!\Selector6~3_combout\) # ((!\paddle_x~5_combout\ & 
-- (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & \Selector6~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000010111111110000001011111111000100111111111100010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_paddle_x~5_combout\,
	datab => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datac => \ALT_INV_Selector6~0_combout\,
	datad => \ALT_INV_Selector6~3_combout\,
	dataf => ALT_INV_paddle_x(1),
	combout => \Selector6~4_combout\);

-- Location: LABCELL_X35_Y71_N42
\draw.x[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~5_combout\ = ( !\draw.x[4]~0_combout\ & ( (!\draw.x[4]~4_combout\ & (\draw.x[4]~1_combout\ & ((!\Equal1~2_combout\) # (!\state.DRAW_LEFT_LOOP~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001000000011000000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~2_combout\,
	datab => \ALT_INV_draw.x[4]~4_combout\,
	datac => \ALT_INV_draw.x[4]~1_combout\,
	datad => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	dataf => \ALT_INV_draw.x[4]~0_combout\,
	combout => \draw.x[4]~5_combout\);

-- Location: LABCELL_X37_Y71_N6
\draw.x[4]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~6_combout\ = ( \draw.x[4]~5_combout\ & ( (!\Equal4~2_combout\) # ((!\draw.x[4]~2_combout\) # ((!\Equal4~1_combout\) # (!\Equal4~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111101111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~2_combout\,
	datab => \ALT_INV_draw.x[4]~2_combout\,
	datac => \ALT_INV_Equal4~1_combout\,
	datad => \ALT_INV_Equal4~0_combout\,
	dataf => \ALT_INV_draw.x[4]~5_combout\,
	combout => \draw.x[4]~6_combout\);

-- Location: FF_X36_Y72_N35
\draw.x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector6~4_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.x[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(1));

-- Location: LABCELL_X36_Y72_N6
\Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~21_sumout\ = SUM(( \draw.x\(2) ) + ( GND ) + ( \Add1~18\ ))
-- \Add1~22\ = CARRY(( \draw.x\(2) ) + ( GND ) + ( \Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x\(2),
	cin => \Add1~18\,
	sumout => \Add1~21_sumout\,
	cout => \Add1~22\);

-- Location: LABCELL_X36_Y72_N9
\Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~25_sumout\ = SUM(( \draw.x\(3) ) + ( GND ) + ( \Add1~22\ ))
-- \Add1~26\ = CARRY(( \draw.x\(3) ) + ( GND ) + ( \Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(3),
	cin => \Add1~22\,
	sumout => \Add1~25_sumout\,
	cout => \Add1~26\);

-- Location: LABCELL_X33_Y71_N21
\Selector4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~1_combout\ = ( paddle_x(3) & ( ((!\draw.y[3]~0_combout\ & !\puck_two.x\(3))) # (\state.ERASE_PADDLE_ENTER~q\) ) ) # ( !paddle_x(3) & ( (!\draw.y[3]~0_combout\ & !\puck_two.x\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110000001100000011000000111111111100000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y[3]~0_combout\,
	datac => \ALT_INV_puck_two.x\(3),
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => ALT_INV_paddle_x(3),
	combout => \Selector4~1_combout\);

-- Location: LABCELL_X37_Y72_N54
\Selector4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~2_combout\ = ( !\state.DRAW_RIGHT_ENTER~q\ & ( (!\state.DRAW_RIGHT_LOOP~q\ & ((\puck_one.x\(3)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100110001001100010011000100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_state.DRAW_RIGHT_LOOP~q\,
	datac => \ALT_INV_puck_one.x\(3),
	dataf => \ALT_INV_state.DRAW_RIGHT_ENTER~q\,
	combout => \Selector4~2_combout\);

-- Location: LABCELL_X36_Y72_N27
\Selector4~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~3_combout\ = ( \Selector4~2_combout\ & ( (!\Selector4~1_combout\ & ((!\Add1~25_sumout\) # (\draw.x[4]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110101000000001111010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x[4]~3_combout\,
	datac => \ALT_INV_Add1~25_sumout\,
	datad => \ALT_INV_Selector4~1_combout\,
	dataf => \ALT_INV_Selector4~2_combout\,
	combout => \Selector4~3_combout\);

-- Location: LABCELL_X36_Y72_N30
\Selector4~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~4_combout\ = ( \Selector4~0_combout\ & ( (!\Selector4~3_combout\) # ((\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((!\paddle_x~5_combout\) # (paddle_x(3))))) ) ) # ( !\Selector4~0_combout\ & ( (!\Selector4~3_combout\) # ((\paddle_x~5_combout\ & 
-- (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & paddle_x(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000001111111110000000111111111001000111111111100100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_paddle_x~5_combout\,
	datab => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datac => ALT_INV_paddle_x(3),
	datad => \ALT_INV_Selector4~3_combout\,
	dataf => \ALT_INV_Selector4~0_combout\,
	combout => \Selector4~4_combout\);

-- Location: FF_X36_Y72_N32
\draw.x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector4~4_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.x[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(3));

-- Location: LABCELL_X36_Y72_N12
\Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~29_sumout\ = SUM(( \draw.x\(4) ) + ( GND ) + ( \Add1~26\ ))
-- \Add1~30\ = CARRY(( \draw.x\(4) ) + ( GND ) + ( \Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(4),
	cin => \Add1~26\,
	sumout => \Add1~29_sumout\,
	cout => \Add1~30\);

-- Location: LABCELL_X37_Y72_N57
\Selector3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~1_combout\ = ( !\state.DRAW_RIGHT_ENTER~q\ & ( (!\state.DRAW_RIGHT_LOOP~q\ & ((!\puck_one.x\(4)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100010011000100110001001100010000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_state.DRAW_RIGHT_LOOP~q\,
	datac => \ALT_INV_puck_one.x\(4),
	dataf => \ALT_INV_state.DRAW_RIGHT_ENTER~q\,
	combout => \Selector3~1_combout\);

-- Location: LABCELL_X37_Y72_N48
\Selector3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~2_combout\ = ( \Selector3~1_combout\ & ( (!\Selector3~0_combout\ & ((!\Add1~29_sumout\) # (\draw.x[4]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000001100001111000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.x[4]~3_combout\,
	datac => \ALT_INV_Selector3~0_combout\,
	datad => \ALT_INV_Add1~29_sumout\,
	dataf => \ALT_INV_Selector3~1_combout\,
	combout => \Selector3~2_combout\);

-- Location: LABCELL_X37_Y72_N6
\Selector3~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~3_combout\ = ( \Selector3~2_combout\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((!\paddle_x~5_combout\ & ((\paddle_x~4_combout\))) # (\paddle_x~5_combout\ & (!paddle_x(4))))) ) ) # ( !\Selector3~2_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100010000010101000001000001010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_paddle_x~5_combout\,
	datac => ALT_INV_paddle_x(4),
	datad => \ALT_INV_paddle_x~4_combout\,
	dataf => \ALT_INV_Selector3~2_combout\,
	combout => \Selector3~3_combout\);

-- Location: FF_X37_Y72_N8
\draw.x[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector3~3_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.x[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(4));

-- Location: LABCELL_X36_Y72_N15
\Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~9_sumout\ = SUM(( \draw.x\(5) ) + ( GND ) + ( \Add1~30\ ))
-- \Add1~10\ = CARRY(( \draw.x\(5) ) + ( GND ) + ( \Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(5),
	cin => \Add1~30\,
	sumout => \Add1~9_sumout\,
	cout => \Add1~10\);

-- Location: LABCELL_X36_Y72_N18
\Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~5_sumout\ = SUM(( \draw.x\(6) ) + ( GND ) + ( \Add1~10\ ))
-- \Add1~6\ = CARRY(( \draw.x\(6) ) + ( GND ) + ( \Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.x\(6),
	cin => \Add1~10\,
	sumout => \Add1~5_sumout\,
	cout => \Add1~6\);

-- Location: LABCELL_X37_Y72_N39
\Selector1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector1~1_combout\ = ( \Add1~5_sumout\ & ( (\draw.x[4]~3_combout\ & (!\Selector1~0_combout\ & ((!\puck_one.x\(6)) # (\draw~0_combout\)))) ) ) # ( !\Add1~5_sumout\ & ( (!\Selector1~0_combout\ & ((!\puck_one.x\(6)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000001010000111100000101000000110000000100000011000000010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_draw.x[4]~3_combout\,
	datac => \ALT_INV_Selector1~0_combout\,
	datad => \ALT_INV_puck_one.x\(6),
	dataf => \ALT_INV_Add1~5_sumout\,
	combout => \Selector1~1_combout\);

-- Location: MLABCELL_X34_Y72_N15
\Add8~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~5_sumout\ = SUM(( !paddle_x(6) ) + ( VCC ) + ( \Add8~10\ ))
-- \Add8~6\ = CARRY(( !paddle_x(6) ) + ( VCC ) + ( \Add8~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(6),
	cin => \Add8~10\,
	sumout => \Add8~5_sumout\,
	cout => \Add8~6\);

-- Location: LABCELL_X35_Y72_N15
\Add7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~5_sumout\ = SUM(( !paddle_x(6) ) + ( GND ) + ( \Add7~10\ ))
-- \Add7~6\ = CARRY(( !paddle_x(6) ) + ( GND ) + ( \Add7~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(6),
	cin => \Add7~10\,
	sumout => \Add7~5_sumout\,
	cout => \Add7~6\);

-- Location: LABCELL_X37_Y72_N3
\paddle_x~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_x~3_combout\ = ( \KEY[0]~input_o\ & ( \Add8~5_sumout\ ) ) # ( !\KEY[0]~input_o\ & ( \Add7~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add8~5_sumout\,
	datad => \ALT_INV_Add7~5_sumout\,
	dataf => \ALT_INV_KEY[0]~input_o\,
	combout => \paddle_x~3_combout\);

-- Location: LABCELL_X37_Y72_N0
\Selector1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector1~2_combout\ = ( paddle_x(6) & ( (!\Selector1~1_combout\) # ((\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & (\paddle_x~3_combout\ & !\paddle_x~5_combout\))) ) ) # ( !paddle_x(6) & ( (!\Selector1~1_combout\) # ((\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & 
-- ((\paddle_x~5_combout\) # (\paddle_x~3_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110111011101110011011101110111001101110011001100110111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_Selector1~1_combout\,
	datac => \ALT_INV_paddle_x~3_combout\,
	datad => \ALT_INV_paddle_x~5_combout\,
	dataf => ALT_INV_paddle_x(6),
	combout => \Selector1~2_combout\);

-- Location: FF_X37_Y72_N2
\draw.x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector1~2_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.x[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(6));

-- Location: LABCELL_X37_Y71_N42
\Equal4~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~4_combout\ = !\Add13~21_sumout\ $ (!\draw.x\(6))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110000000011111111000000001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add13~21_sumout\,
	datad => \ALT_INV_draw.x\(6),
	combout => \Equal4~4_combout\);

-- Location: FF_X37_Y71_N13
\state.DRAW_PADDLE_LOOP~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector102~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_LOOP~DUPLICATE_q\);

-- Location: LABCELL_X37_Y71_N39
\draw.y[6]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~9_combout\ = ( \Equal4~1_combout\ & ( \state.DRAW_PADDLE_LOOP~DUPLICATE_q\ & ( (!\Equal4~4_combout\ & (\Equal4~0_combout\ & (!\draw.x\(7) $ (\Add13~25_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000010010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(7),
	datab => \ALT_INV_Add13~25_sumout\,
	datac => \ALT_INV_Equal4~4_combout\,
	datad => \ALT_INV_Equal4~0_combout\,
	datae => \ALT_INV_Equal4~1_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_LOOP~DUPLICATE_q\,
	combout => \draw.y[6]~9_combout\);

-- Location: FF_X37_Y71_N41
\state.ERASE_PUCK_ONE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.y[6]~9_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PUCK_ONE~q\);

-- Location: MLABCELL_X34_Y71_N36
\draw~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw~0_combout\ = ( !\state.DRAW_PUCK_ONE~q\ & ( !\state.ERASE_PUCK_ONE~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	dataf => \ALT_INV_state.DRAW_PUCK_ONE~q\,
	combout => \draw~0_combout\);

-- Location: LABCELL_X33_Y71_N18
\Selector2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~1_combout\ = ( \puck_two.x\(5) & ( (paddle_x(5) & \state.ERASE_PADDLE_ENTER~q\) ) ) # ( !\puck_two.x\(5) & ( (!\draw.y[3]~0_combout\) # ((paddle_x(5) & \state.ERASE_PADDLE_ENTER~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001111110011001100111100000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y[3]~0_combout\,
	datac => ALT_INV_paddle_x(5),
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => \ALT_INV_puck_two.x\(5),
	combout => \Selector2~1_combout\);

-- Location: LABCELL_X37_Y72_N36
\Selector2~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~2_combout\ = ( \Add1~9_sumout\ & ( (\draw.x[4]~3_combout\ & (!\Selector2~1_combout\ & ((\puck_one.x\(5)) # (\draw~0_combout\)))) ) ) # ( !\Add1~9_sumout\ & ( (!\Selector2~1_combout\ & ((\puck_one.x\(5)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011110000010100001111000000010000001100000001000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw~0_combout\,
	datab => \ALT_INV_draw.x[4]~3_combout\,
	datac => \ALT_INV_Selector2~1_combout\,
	datad => \ALT_INV_puck_one.x\(5),
	dataf => \ALT_INV_Add1~9_sumout\,
	combout => \Selector2~2_combout\);

-- Location: LABCELL_X37_Y72_N9
\Selector2~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector2~3_combout\ = ( \Selector2~0_combout\ & ( (!\Selector2~2_combout\) # ((\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((!\paddle_x~5_combout\) # (paddle_x(5))))) ) ) # ( !\Selector2~0_combout\ & ( (!\Selector2~2_combout\) # 
-- ((\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & (\paddle_x~5_combout\ & paddle_x(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000001111111110000000111111111010001011111111101000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_paddle_x~5_combout\,
	datac => ALT_INV_paddle_x(5),
	datad => \ALT_INV_Selector2~2_combout\,
	dataf => \ALT_INV_Selector2~0_combout\,
	combout => \Selector2~3_combout\);

-- Location: FF_X37_Y72_N11
\draw.x[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector2~3_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.x[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(5));

-- Location: LABCELL_X36_Y72_N36
\Equal4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~0_combout\ = ( \draw.x\(4) & ( \draw.x\(3) & ( (\Add13~1_sumout\ & (\Add13~5_sumout\ & (!\Add13~9_sumout\ $ (\draw.x\(5))))) ) ) ) # ( !\draw.x\(4) & ( \draw.x\(3) & ( (\Add13~1_sumout\ & (!\Add13~5_sumout\ & (!\Add13~9_sumout\ $ (\draw.x\(5))))) 
-- ) ) ) # ( \draw.x\(4) & ( !\draw.x\(3) & ( (!\Add13~1_sumout\ & (\Add13~5_sumout\ & (!\Add13~9_sumout\ $ (\draw.x\(5))))) ) ) ) # ( !\draw.x\(4) & ( !\draw.x\(3) & ( (!\Add13~1_sumout\ & (!\Add13~5_sumout\ & (!\Add13~9_sumout\ $ (\draw.x\(5))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001000000000000000001000001001000001000000000000000001000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~1_sumout\,
	datab => \ALT_INV_Add13~9_sumout\,
	datac => \ALT_INV_draw.x\(5),
	datad => \ALT_INV_Add13~5_sumout\,
	datae => \ALT_INV_draw.x\(4),
	dataf => \ALT_INV_draw.x\(3),
	combout => \Equal4~0_combout\);

-- Location: LABCELL_X37_Y71_N54
\Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = ( \draw.x\(2) & ( \Equal4~1_combout\ & ( (!\draw.x[4]~7_combout\ & (((\Equal4~0_combout\ & \Equal4~2_combout\)) # (\Add1~21_sumout\))) ) ) ) # ( !\draw.x\(2) & ( \Equal4~1_combout\ & ( (\Add1~21_sumout\ & (!\draw.x[4]~7_combout\ & 
-- ((!\Equal4~0_combout\) # (!\Equal4~2_combout\)))) ) ) ) # ( \draw.x\(2) & ( !\Equal4~1_combout\ & ( (\Add1~21_sumout\ & !\draw.x[4]~7_combout\) ) ) ) # ( !\draw.x\(2) & ( !\Equal4~1_combout\ & ( (\Add1~21_sumout\ & !\draw.x[4]~7_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000000110010000000000011011100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~0_combout\,
	datab => \ALT_INV_Add1~21_sumout\,
	datac => \ALT_INV_Equal4~2_combout\,
	datad => \ALT_INV_draw.x[4]~7_combout\,
	datae => \ALT_INV_draw.x\(2),
	dataf => \ALT_INV_Equal4~1_combout\,
	combout => \Selector5~0_combout\);

-- Location: LABCELL_X35_Y71_N15
\Selector5~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~2_combout\ = ( \state.START~q\ & ( (!\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & ((!\draw.x\(2)) # ((!\Equal2~0_combout\) # (!\vga_u0|writeEn~1_combout\)))) ) ) # ( !\state.START~q\ & ( !\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111110000000001111111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(2),
	datab => \ALT_INV_Equal2~0_combout\,
	datac => \vga_u0|ALT_INV_writeEn~1_combout\,
	datad => \ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\,
	dataf => \ALT_INV_state.START~q\,
	combout => \Selector5~2_combout\);

-- Location: LABCELL_X35_Y72_N33
\Selector5~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~3_combout\ = (!\state.IDLE~q\ & !\state.DRAW_LEFT_LOOP~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101000000000101010100000000010101010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.IDLE~q\,
	datad => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	combout => \Selector5~3_combout\);

-- Location: MLABCELL_X34_Y71_N30
\Selector5~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~7_combout\ = ( \state.DRAW_PUCK_ONE~q\ & ( (!\state.DRAW_TOP_ENTER~q\ & (!\state.DRAW_LEFT_ENTER~q\ & !\puck_one.x\(2))) ) ) # ( !\state.DRAW_PUCK_ONE~q\ & ( (!\state.DRAW_TOP_ENTER~q\ & (!\state.DRAW_LEFT_ENTER~q\ & 
-- ((!\state.ERASE_PUCK_ONE~q\) # (!\puck_one.x\(2))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010000000100010001000000010001000000000001000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datab => \ALT_INV_state.DRAW_LEFT_ENTER~q\,
	datac => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	datad => \ALT_INV_puck_one.x\(2),
	dataf => \ALT_INV_state.DRAW_PUCK_ONE~q\,
	combout => \Selector5~7_combout\);

-- Location: MLABCELL_X39_Y70_N18
\Selector5~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~6_combout\ = ( paddle_x(2) & ( \state.ERASE_PADDLE_ENTER~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => ALT_INV_paddle_x(2),
	combout => \Selector5~6_combout\);

-- Location: MLABCELL_X34_Y71_N39
\Selector5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~5_combout\ = ( \puck_two.x\(2) & ( (\state.ERASE_PUCK_TWO~q\) # (\state.DRAW_PUCK_TWO~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.DRAW_PUCK_TWO~q\,
	datad => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	dataf => \ALT_INV_puck_two.x\(2),
	combout => \Selector5~5_combout\);

-- Location: MLABCELL_X34_Y71_N54
\Selector5~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~8_combout\ = ( \state.DRAW_LEFT_LOOP~q\ & ( \Equal1~0_combout\ & ( (\Equal1~1_combout\ & (\Selector5~7_combout\ & (!\Selector5~6_combout\ & !\Selector5~5_combout\))) ) ) ) # ( !\state.DRAW_LEFT_LOOP~q\ & ( \Equal1~0_combout\ & ( 
-- (\Selector5~7_combout\ & (!\Selector5~6_combout\ & !\Selector5~5_combout\)) ) ) ) # ( !\state.DRAW_LEFT_LOOP~q\ & ( !\Equal1~0_combout\ & ( (\Selector5~7_combout\ & (!\Selector5~6_combout\ & !\Selector5~5_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000000000000000000000110000000000000001000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~1_combout\,
	datab => \ALT_INV_Selector5~7_combout\,
	datac => \ALT_INV_Selector5~6_combout\,
	datad => \ALT_INV_Selector5~5_combout\,
	datae => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \Selector5~8_combout\);

-- Location: LABCELL_X36_Y71_N6
\Selector5~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~4_combout\ = ( \Add1~21_sumout\ & ( \vga_u0|writeEn~1_combout\ & ( (!\Equal2~0_combout\ & (((\state.DRAW_TOP_LOOP~DUPLICATE_q\)) # (\state.START~q\))) # (\Equal2~0_combout\ & ((!\draw.x\(2) & (\state.START~q\)) # (\draw.x\(2) & 
-- ((\state.DRAW_TOP_LOOP~DUPLICATE_q\))))) ) ) ) # ( \Add1~21_sumout\ & ( !\vga_u0|writeEn~1_combout\ & ( (\state.DRAW_TOP_LOOP~DUPLICATE_q\) # (\state.START~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101011111111100000000000000000101010011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_Equal2~0_combout\,
	datac => \ALT_INV_draw.x\(2),
	datad => \ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\,
	datae => \ALT_INV_Add1~21_sumout\,
	dataf => \vga_u0|ALT_INV_writeEn~1_combout\,
	combout => \Selector5~4_combout\);

-- Location: LABCELL_X35_Y72_N48
\Selector5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~9_combout\ = ( \Selector5~8_combout\ & ( !\Selector5~4_combout\ & ( (!\draw.x\(2)) # ((\Selector5~3_combout\ & ((!\Equal1~2_combout\) # (\Selector5~2_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~2_combout\,
	datab => \ALT_INV_Selector5~2_combout\,
	datac => \ALT_INV_Selector5~3_combout\,
	datad => \ALT_INV_draw.x\(2),
	datae => \ALT_INV_Selector5~8_combout\,
	dataf => \ALT_INV_Selector5~4_combout\,
	combout => \Selector5~9_combout\);

-- Location: LABCELL_X35_Y72_N54
\Selector5~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~10_combout\ = ( \paddle_x~5_combout\ & ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( ((!\Selector5~9_combout\) # (paddle_x(2))) # (\Selector5~0_combout\) ) ) ) # ( !\paddle_x~5_combout\ & ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( 
-- ((!\Selector5~9_combout\) # (\Selector5~1_combout\)) # (\Selector5~0_combout\) ) ) ) # ( \paddle_x~5_combout\ & ( !\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\Selector5~9_combout\) # (\Selector5~0_combout\) ) ) ) # ( !\paddle_x~5_combout\ & ( 
-- !\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\Selector5~9_combout\) # (\Selector5~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101111101011111010111110111111101111111010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Selector5~0_combout\,
	datab => \ALT_INV_Selector5~1_combout\,
	datac => \ALT_INV_Selector5~9_combout\,
	datad => ALT_INV_paddle_x(2),
	datae => \ALT_INV_paddle_x~5_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector5~10_combout\);

-- Location: FF_X35_Y72_N56
\draw.x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector5~10_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(2));

-- Location: LABCELL_X35_Y71_N30
\draw.x[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~4_combout\ = ( \vga_u0|writeEn~1_combout\ & ( \Equal1~0_combout\ & ( (\draw.x\(2) & (\Equal2~0_combout\ & (\state.START~q\ & \Equal1~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(2),
	datab => \ALT_INV_Equal2~0_combout\,
	datac => \ALT_INV_state.START~q\,
	datad => \ALT_INV_Equal1~1_combout\,
	datae => \vga_u0|ALT_INV_writeEn~1_combout\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \draw.x[4]~4_combout\);

-- Location: FF_X35_Y71_N32
\state.DRAW_TOP_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.x[4]~4_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_TOP_ENTER~q\);

-- Location: LABCELL_X36_Y71_N36
\Selector93~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector93~0_combout\ = ( \Equal2~0_combout\ & ( (\state.DRAW_TOP_LOOP~DUPLICATE_q\ & ((!\vga_u0|writeEn~1_combout\) # (\draw.x\(2)))) ) ) # ( !\Equal2~0_combout\ & ( \state.DRAW_TOP_LOOP~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000110111010000000011011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(2),
	datab => \vga_u0|ALT_INV_writeEn~1_combout\,
	datad => \ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\,
	dataf => \ALT_INV_Equal2~0_combout\,
	combout => \Selector93~0_combout\);

-- Location: LABCELL_X36_Y71_N45
\Selector93~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector93~1_combout\ = ( \Selector93~0_combout\ ) # ( !\Selector93~0_combout\ & ( \state.DRAW_TOP_ENTER~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101111111111111111101010101010101011111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datae => \ALT_INV_Selector93~0_combout\,
	combout => \Selector93~1_combout\);

-- Location: FF_X36_Y71_N47
\state.DRAW_TOP_LOOP~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector93~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_TOP_LOOP~DUPLICATE_q\);

-- Location: LABCELL_X36_Y71_N18
\draw.y[6]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~2_combout\ = ( !\state.ERASE_PADDLE_LOOP~q\ & ( !\state.DRAW_PADDLE_LOOP~DUPLICATE_q\ & ( !\state.DRAW_TOP_LOOP~DUPLICATE_q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.DRAW_TOP_LOOP~DUPLICATE_q\,
	datae => \ALT_INV_state.ERASE_PADDLE_LOOP~q\,
	dataf => \ALT_INV_state.DRAW_PADDLE_LOOP~DUPLICATE_q\,
	combout => \draw.y[6]~2_combout\);

-- Location: LABCELL_X35_Y71_N0
\draw.x[4]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.x[4]~3_combout\ = ( \Equal2~0_combout\ & ( (\draw.y[6]~2_combout\ & ((!\state.START~q\) # ((\draw.x\(2) & \vga_u0|writeEn~1_combout\)))) ) ) # ( !\Equal2~0_combout\ & ( (!\state.START~q\ & \draw.y[6]~2_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100010001000100010001000100010001000110010001000100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_draw.y[6]~2_combout\,
	datac => \ALT_INV_draw.x\(2),
	datad => \vga_u0|ALT_INV_writeEn~1_combout\,
	dataf => \ALT_INV_Equal2~0_combout\,
	combout => \draw.x[4]~3_combout\);

-- Location: LABCELL_X36_Y72_N57
\Selector0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~1_combout\ = ( \draw.y[3]~0_combout\ & ( (paddle_x(7) & \state.ERASE_PADDLE_ENTER~q\) ) ) # ( !\draw.y[3]~0_combout\ & ( ((paddle_x(7) & \state.ERASE_PADDLE_ENTER~q\)) # (\puck_two.x\(7)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011111000011110101111100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(7),
	datac => \ALT_INV_puck_two.x\(7),
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => \ALT_INV_draw.y[3]~0_combout\,
	combout => \Selector0~1_combout\);

-- Location: LABCELL_X36_Y72_N21
\Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~1_sumout\ = SUM(( \draw.x\(7) ) + ( GND ) + ( \Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(7),
	cin => \Add1~6\,
	sumout => \Add1~1_sumout\);

-- Location: LABCELL_X37_Y72_N45
\Selector0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~2_combout\ = ( !\state.DRAW_RIGHT_ENTER~q\ & ( (!\state.DRAW_RIGHT_LOOP~q\ & ((!\puck_one.x\(7)) # (\draw~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000011110000101000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_puck_one.x\(7),
	datac => \ALT_INV_state.DRAW_RIGHT_LOOP~q\,
	datad => \ALT_INV_draw~0_combout\,
	dataf => \ALT_INV_state.DRAW_RIGHT_ENTER~q\,
	combout => \Selector0~2_combout\);

-- Location: LABCELL_X36_Y72_N51
\Selector0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~3_combout\ = ( \Selector0~2_combout\ & ( (!\Selector0~1_combout\ & ((!\Add1~1_sumout\) # (\draw.x[4]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000010100001111000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x[4]~3_combout\,
	datac => \ALT_INV_Selector0~1_combout\,
	datad => \ALT_INV_Add1~1_sumout\,
	dataf => \ALT_INV_Selector0~2_combout\,
	combout => \Selector0~3_combout\);

-- Location: LABCELL_X36_Y72_N54
\Selector0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~4_combout\ = ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\Selector0~3_combout\) # ((!\paddle_x~5_combout\ & ((\Selector0~0_combout\))) # (\paddle_x~5_combout\ & (paddle_x(7)))) ) ) # ( !\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( 
-- !\Selector0~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011110011111101011111001111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(7),
	datab => \ALT_INV_Selector0~0_combout\,
	datac => \ALT_INV_Selector0~3_combout\,
	datad => \ALT_INV_paddle_x~5_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector0~4_combout\);

-- Location: FF_X36_Y72_N56
\draw.x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector0~4_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.x[4]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.x\(7));

-- Location: LABCELL_X37_Y71_N36
\draw.y[6]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \draw.y[6]~8_combout\ = ( \state.ERASE_PADDLE_LOOP~q\ & ( \Equal4~1_combout\ & ( (\Equal4~0_combout\ & (!\Equal4~4_combout\ & (!\draw.x\(7) $ (\Add13~25_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000100100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.x\(7),
	datab => \ALT_INV_Add13~25_sumout\,
	datac => \ALT_INV_Equal4~0_combout\,
	datad => \ALT_INV_Equal4~4_combout\,
	datae => \ALT_INV_state.ERASE_PADDLE_LOOP~q\,
	dataf => \ALT_INV_Equal4~1_combout\,
	combout => \draw.y[6]~8_combout\);

-- Location: FF_X37_Y71_N37
\state.DRAW_PADDLE_ENTER~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \draw.y[6]~8_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_PADDLE_ENTER~DUPLICATE_q\);

-- Location: LABCELL_X37_Y72_N30
\Selector6~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~5_combout\ = (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((!\paddle_x~5_combout\ & (\Selector6~0_combout\)) # (\paddle_x~5_combout\ & ((paddle_x(1))))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000010101000001000001010100000100000101010000010000010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datab => \ALT_INV_paddle_x~5_combout\,
	datac => \ALT_INV_Selector6~0_combout\,
	datad => ALT_INV_paddle_x(1),
	combout => \Selector6~5_combout\);

-- Location: FF_X37_Y72_N32
\paddle_x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector6~5_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(1));

-- Location: LABCELL_X35_Y72_N3
\Add7~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~17_sumout\ = SUM(( paddle_x(2) ) + ( GND ) + ( \Add7~14\ ))
-- \Add7~18\ = CARRY(( paddle_x(2) ) + ( GND ) + ( \Add7~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => ALT_INV_paddle_x(2),
	cin => \Add7~14\,
	sumout => \Add7~17_sumout\,
	cout => \Add7~18\);

-- Location: LABCELL_X35_Y72_N39
\Selector5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~1_combout\ = (!\KEY[0]~input_o\ & ((\Add7~17_sumout\))) # (\KEY[0]~input_o\ & (\Add8~17_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111000001011010111100000101101011110000010110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[0]~input_o\,
	datac => \ALT_INV_Add8~17_sumout\,
	datad => \ALT_INV_Add7~17_sumout\,
	combout => \Selector5~1_combout\);

-- Location: LABCELL_X35_Y72_N30
\Selector5~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~11_combout\ = ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\paddle_x~5_combout\ & (\Selector5~1_combout\)) # (\paddle_x~5_combout\ & ((paddle_x(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Selector5~1_combout\,
	datac => \ALT_INV_paddle_x~5_combout\,
	datad => ALT_INV_paddle_x(2),
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector5~11_combout\);

-- Location: FF_X35_Y72_N32
\paddle_x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector5~11_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(2));

-- Location: LABCELL_X35_Y72_N27
\Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~0_combout\ = ( \KEY[0]~input_o\ & ( \Add8~21_sumout\ ) ) # ( !\KEY[0]~input_o\ & ( \Add7~21_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add7~21_sumout\,
	datad => \ALT_INV_Add8~21_sumout\,
	dataf => \ALT_INV_KEY[0]~input_o\,
	combout => \Selector4~0_combout\);

-- Location: LABCELL_X36_Y72_N24
\Selector4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector4~5_combout\ = ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\paddle_x~5_combout\ & (\Selector4~0_combout\)) # (\paddle_x~5_combout\ & ((paddle_x(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Selector4~0_combout\,
	datac => \ALT_INV_paddle_x~5_combout\,
	datad => ALT_INV_paddle_x(3),
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector4~5_combout\);

-- Location: FF_X36_Y72_N26
\paddle_x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector4~5_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(3));

-- Location: IOIBUF_X36_Y0_N18
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: MLABCELL_X34_Y72_N30
\paddle_x~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_x~0_combout\ = (!paddle_x(5) & paddle_x(6))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100010001000100010001000100010001000100010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(5),
	datab => ALT_INV_paddle_x(6),
	combout => \paddle_x~0_combout\);

-- Location: LABCELL_X36_Y69_N18
\paddle_x~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_x~1_combout\ = ( paddle_width(0) & ( \pflag~q\ & ( (!paddle_x(1) & paddle_width(1)) ) ) ) # ( !paddle_width(0) & ( \pflag~q\ & ( (!paddle_x(1) & ((!\paddle_width[3]~DUPLICATE_q\ & (!paddle_width(1))) # (\paddle_width[3]~DUPLICATE_q\ & 
-- ((paddle_width(2)) # (paddle_width(1)))))) ) ) ) # ( paddle_width(0) & ( !\pflag~q\ & ( (!paddle_x(1) & paddle_width(1)) ) ) ) # ( !paddle_width(0) & ( !\pflag~q\ & ( (!paddle_x(1) & paddle_width(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101010000010101000100000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(1),
	datab => \ALT_INV_paddle_width[3]~DUPLICATE_q\,
	datac => ALT_INV_paddle_width(1),
	datad => ALT_INV_paddle_width(2),
	datae => ALT_INV_paddle_width(0),
	dataf => \ALT_INV_pflag~q\,
	combout => \paddle_x~1_combout\);

-- Location: MLABCELL_X34_Y72_N36
\paddle_x~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_x~2_combout\ = ( paddle_x(3) & ( \paddle_width~1_combout\ & ( (!paddle_x(4)) # ((!\paddle_x~1_combout\ & (\paddle_width~0_combout\ & paddle_x(2)))) ) ) ) # ( !paddle_x(3) & ( \paddle_width~1_combout\ & ( (!paddle_x(4) & ((!\paddle_x~1_combout\) # 
-- ((paddle_x(2)) # (\paddle_width~0_combout\)))) ) ) ) # ( paddle_x(3) & ( !\paddle_width~1_combout\ & ( (!paddle_x(4) & ((!\paddle_x~1_combout\) # ((paddle_x(2)) # (\paddle_width~0_combout\)))) ) ) ) # ( !paddle_x(3) & ( !\paddle_width~1_combout\ & ( 
-- (!\paddle_x~1_combout\ & (\paddle_width~0_combout\ & (paddle_x(2) & !paddle_x(4)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000000101111110000000010111111000000001111111100000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_paddle_x~1_combout\,
	datab => \ALT_INV_paddle_width~0_combout\,
	datac => ALT_INV_paddle_x(2),
	datad => ALT_INV_paddle_x(4),
	datae => ALT_INV_paddle_x(3),
	dataf => \ALT_INV_paddle_width~1_combout\,
	combout => \paddle_x~2_combout\);

-- Location: MLABCELL_X34_Y72_N54
\paddle_x~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_x~5_combout\ = ( !\KEY[0]~input_o\ & ( (((paddle_x(7) & ((!\paddle_x~0_combout\) # (\paddle_x~2_combout\))))) ) ) # ( \KEY[0]~input_o\ & ( ((!paddle_x(3) & (paddle_x(4) & (!paddle_x(7) & \paddle_x~0_combout\)))) # (\KEY[1]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000011111111000011110000111100000000000011110010111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_x(3),
	datab => ALT_INV_paddle_x(4),
	datac => \ALT_INV_KEY[1]~input_o\,
	datad => ALT_INV_paddle_x(7),
	datae => \ALT_INV_KEY[0]~input_o\,
	dataf => \ALT_INV_paddle_x~0_combout\,
	datag => \ALT_INV_paddle_x~2_combout\,
	combout => \paddle_x~5_combout\);

-- Location: LABCELL_X37_Y72_N18
\Selector17~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector17~0_combout\ = ( \paddle_x~3_combout\ & ( (\paddle_x~5_combout\ & (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & paddle_x(6))) ) ) # ( !\paddle_x~3_combout\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ((!\paddle_x~5_combout\) # (paddle_x(6)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001111000011000000111100000000000000110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_paddle_x~5_combout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datad => ALT_INV_paddle_x(6),
	dataf => \ALT_INV_paddle_x~3_combout\,
	combout => \Selector17~0_combout\);

-- Location: FF_X37_Y72_N20
\paddle_x[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector17~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(6));

-- Location: MLABCELL_X34_Y72_N18
\Add8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add8~1_sumout\ = SUM(( paddle_x(7) ) + ( VCC ) + ( \Add8~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(7),
	cin => \Add8~6\,
	sumout => \Add8~1_sumout\);

-- Location: LABCELL_X35_Y72_N18
\Add7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~1_sumout\ = SUM(( paddle_x(7) ) + ( GND ) + ( \Add7~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(7),
	cin => \Add7~6\,
	sumout => \Add7~1_sumout\);

-- Location: LABCELL_X35_Y72_N36
\Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = ( \Add7~1_sumout\ & ( (!\KEY[0]~input_o\) # (\Add8~1_sumout\) ) ) # ( !\Add7~1_sumout\ & ( (\KEY[0]~input_o\ & \Add8~1_sumout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010110101111101011111010111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_KEY[0]~input_o\,
	datac => \ALT_INV_Add8~1_sumout\,
	dataf => \ALT_INV_Add7~1_sumout\,
	combout => \Selector0~0_combout\);

-- Location: LABCELL_X36_Y72_N48
\Selector0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~5_combout\ = ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\paddle_x~5_combout\ & (\Selector0~0_combout\)) # (\paddle_x~5_combout\ & ((paddle_x(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Selector0~0_combout\,
	datac => \ALT_INV_paddle_x~5_combout\,
	datad => ALT_INV_paddle_x(7),
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector0~5_combout\);

-- Location: FF_X36_Y72_N50
\paddle_x[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector0~5_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_x(7));

-- Location: LABCELL_X36_Y69_N48
\Add13~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add13~25_sumout\ = SUM(( paddle_x(7) ) + ( GND ) + ( \Add13~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_paddle_x(7),
	cin => \Add13~22\,
	sumout => \Add13~25_sumout\);

-- Location: LABCELL_X33_Y69_N36
\LessThan7~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan7~3_combout\ = ( \Add15~21_sumout\ & ( \Add13~21_sumout\ & ( (\Add13~25_sumout\ & (\Add15~25_sumout\ & (!\Add15~29_sumout\ $ (\Add13~9_sumout\)))) ) ) ) # ( !\Add15~21_sumout\ & ( \Add13~21_sumout\ & ( (!\Add13~25_sumout\ & (\Add15~25_sumout\ & 
-- (!\Add15~29_sumout\ $ (\Add13~9_sumout\)))) ) ) ) # ( \Add15~21_sumout\ & ( !\Add13~21_sumout\ & ( (\Add13~25_sumout\ & (!\Add15~25_sumout\ & (!\Add15~29_sumout\ $ (\Add13~9_sumout\)))) ) ) ) # ( !\Add15~21_sumout\ & ( !\Add13~21_sumout\ & ( 
-- (!\Add13~25_sumout\ & (!\Add15~25_sumout\ & (!\Add15~29_sumout\ $ (\Add13~9_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000100000010000000001000000001000000000100000010000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~25_sumout\,
	datab => \ALT_INV_Add15~29_sumout\,
	datac => \ALT_INV_Add15~25_sumout\,
	datad => \ALT_INV_Add13~9_sumout\,
	datae => \ALT_INV_Add15~21_sumout\,
	dataf => \ALT_INV_Add13~21_sumout\,
	combout => \LessThan7~3_combout\);

-- Location: LABCELL_X33_Y69_N54
\LessThan7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan7~1_combout\ = ( \Add15~13_sumout\ & ( \Add13~17_sumout\ & ( (\Add15~9_sumout\ & ((!\Add13~13_sumout\) # ((!paddle_width(0) & \Add15~17_sumout\)))) ) ) ) # ( !\Add15~13_sumout\ & ( \Add13~17_sumout\ & ( (!paddle_width(0) & (\Add15~9_sumout\ & 
-- (\Add15~17_sumout\ & !\Add13~13_sumout\))) ) ) ) # ( \Add15~13_sumout\ & ( !\Add13~17_sumout\ & ( (!\Add15~9_sumout\ & ((!\Add13~13_sumout\) # ((!paddle_width(0) & \Add15~17_sumout\)))) ) ) ) # ( !\Add15~13_sumout\ & ( !\Add13~17_sumout\ & ( 
-- (!paddle_width(0) & (!\Add15~9_sumout\ & (\Add15~17_sumout\ & !\Add13~13_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000000110011000000100000000010000000000011001100000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_width(0),
	datab => \ALT_INV_Add15~9_sumout\,
	datac => \ALT_INV_Add15~17_sumout\,
	datad => \ALT_INV_Add13~13_sumout\,
	datae => \ALT_INV_Add15~13_sumout\,
	dataf => \ALT_INV_Add13~17_sumout\,
	combout => \LessThan7~1_combout\);

-- Location: LABCELL_X33_Y69_N42
\LessThan7~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan7~4_combout\ = ( \Add15~21_sumout\ & ( \Add13~21_sumout\ & ( (!\Add13~25_sumout\) # ((\Add15~29_sumout\ & (\Add15~25_sumout\ & !\Add13~9_sumout\))) ) ) ) # ( !\Add15~21_sumout\ & ( \Add13~21_sumout\ & ( (!\Add13~25_sumout\ & (\Add15~29_sumout\ & 
-- (\Add15~25_sumout\ & !\Add13~9_sumout\))) ) ) ) # ( \Add15~21_sumout\ & ( !\Add13~21_sumout\ & ( (!\Add13~25_sumout\) # (((\Add15~29_sumout\ & !\Add13~9_sumout\)) # (\Add15~25_sumout\)) ) ) ) # ( !\Add15~21_sumout\ & ( !\Add13~21_sumout\ & ( 
-- (!\Add13~25_sumout\ & (((\Add15~29_sumout\ & !\Add13~9_sumout\)) # (\Add15~25_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010101000001010101111111010111100000010000000001010101110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~25_sumout\,
	datab => \ALT_INV_Add15~29_sumout\,
	datac => \ALT_INV_Add15~25_sumout\,
	datad => \ALT_INV_Add13~9_sumout\,
	datae => \ALT_INV_Add15~21_sumout\,
	dataf => \ALT_INV_Add13~21_sumout\,
	combout => \LessThan7~4_combout\);

-- Location: LABCELL_X33_Y69_N30
\LessThan7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan7~2_combout\ = ( \Add15~1_sumout\ & ( \Add13~1_sumout\ & ( (!\Add13~5_sumout\) # ((\Add15~9_sumout\ & (!\Add13~17_sumout\ & \Add15~5_sumout\))) ) ) ) # ( !\Add15~1_sumout\ & ( \Add13~1_sumout\ & ( (!\Add13~5_sumout\ & (\Add15~9_sumout\ & 
-- (!\Add13~17_sumout\ & \Add15~5_sumout\))) ) ) ) # ( \Add15~1_sumout\ & ( !\Add13~1_sumout\ & ( (!\Add13~5_sumout\) # (((\Add15~9_sumout\ & !\Add13~17_sumout\)) # (\Add15~5_sumout\)) ) ) ) # ( !\Add15~1_sumout\ & ( !\Add13~1_sumout\ & ( (!\Add13~5_sumout\ 
-- & (((\Add15~9_sumout\ & !\Add13~17_sumout\)) # (\Add15~5_sumout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000010101010101110101111111100000000001000001010101010111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~5_sumout\,
	datab => \ALT_INV_Add15~9_sumout\,
	datac => \ALT_INV_Add13~17_sumout\,
	datad => \ALT_INV_Add15~5_sumout\,
	datae => \ALT_INV_Add15~1_sumout\,
	dataf => \ALT_INV_Add13~1_sumout\,
	combout => \LessThan7~2_combout\);

-- Location: LABCELL_X33_Y71_N57
\LessThan7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan7~0_combout\ = ( \Add13~1_sumout\ & ( (\Add15~5_sumout\ & (!\Add15~1_sumout\ $ (\Add13~5_sumout\))) ) ) # ( !\Add13~1_sumout\ & ( (!\Add15~5_sumout\ & (!\Add15~1_sumout\ $ (\Add13~5_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000001010101000000000101001010000000001010101000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add15~5_sumout\,
	datac => \ALT_INV_Add15~1_sumout\,
	datad => \ALT_INV_Add13~5_sumout\,
	dataf => \ALT_INV_Add13~1_sumout\,
	combout => \LessThan7~0_combout\);

-- Location: LABCELL_X30_Y71_N54
\LessThan7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan7~5_combout\ = ( \LessThan7~0_combout\ & ( ((\LessThan7~3_combout\ & ((\LessThan7~2_combout\) # (\LessThan7~1_combout\)))) # (\LessThan7~4_combout\) ) ) # ( !\LessThan7~0_combout\ & ( ((\LessThan7~3_combout\ & \LessThan7~2_combout\)) # 
-- (\LessThan7~4_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011111000011110101111100011111010111110001111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LessThan7~3_combout\,
	datab => \ALT_INV_LessThan7~1_combout\,
	datac => \ALT_INV_LessThan7~4_combout\,
	datad => \ALT_INV_LessThan7~2_combout\,
	dataf => \ALT_INV_LessThan7~0_combout\,
	combout => \LessThan7~5_combout\);

-- Location: LABCELL_X33_Y71_N30
\state~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \state~53_combout\ = ( \LessThan6~0_combout\ & ( !\Equal12~0_combout\ ) ) # ( !\LessThan6~0_combout\ & ( (!\LessThan7~5_combout\) # (!\Equal12~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111100111111001111110011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_LessThan7~5_combout\,
	datac => \ALT_INV_Equal12~0_combout\,
	dataf => \ALT_INV_LessThan6~0_combout\,
	combout => \state~53_combout\);

-- Location: LABCELL_X33_Y71_N33
\Selector90~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector90~1_combout\ = ( \Selector90~0_combout\ & ( (!\state.ERASE_PUCK_TWO~q\) # (\state~53_combout\) ) ) # ( !\Selector90~0_combout\ & ( (!\state.ERASE_PUCK_ONE~q\ & ((!\state.ERASE_PUCK_TWO~q\) # (\state~53_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010100000000111101010000000011110101111101011111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state~53_combout\,
	datac => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	datad => \ALT_INV_state.ERASE_PUCK_ONE~q\,
	dataf => \ALT_INV_Selector90~0_combout\,
	combout => \Selector90~1_combout\);

-- Location: FF_X33_Y71_N34
\state.INIT~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector90~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.INIT~DUPLICATE_q\);

-- Location: LABCELL_X35_Y71_N18
\Selector91~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector91~0_combout\ = ( \Equal0~0_combout\ & ( (!\state.INIT~DUPLICATE_q\) # ((!\Equal1~2_combout\ & \state.START~q\)) ) ) # ( !\Equal0~0_combout\ & ( (!\state.INIT~DUPLICATE_q\) # (\state.START~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011111111110011001111111111001100111011101100110011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~2_combout\,
	datab => \ALT_INV_state.INIT~DUPLICATE_q\,
	datad => \ALT_INV_state.START~q\,
	dataf => \ALT_INV_Equal0~0_combout\,
	combout => \Selector91~0_combout\);

-- Location: FF_X35_Y71_N20
\state.START\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector91~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.START~q\);

-- Location: LABCELL_X35_Y71_N54
\Selector13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~0_combout\ = ( !\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & ( \Add0~17_sumout\ & ( (!\state.DRAW_LEFT_LOOP~q\ & ((!\state.START~q\) # (!\Equal0~0_combout\))) ) ) ) # ( \state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & ( !\Add0~17_sumout\ & ( !\Equal1~2_combout\ 
-- ) ) ) # ( !\state.DRAW_RIGHT_LOOP~DUPLICATE_q\ & ( !\Add0~17_sumout\ & ( (!\Equal1~2_combout\) # ((!\state.DRAW_LEFT_LOOP~q\ & ((!\state.START~q\) # (!\Equal0~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111011110000111100001111000011101110000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.START~q\,
	datab => \ALT_INV_Equal0~0_combout\,
	datac => \ALT_INV_Equal1~2_combout\,
	datad => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	datae => \ALT_INV_state.DRAW_RIGHT_LOOP~DUPLICATE_q\,
	dataf => \ALT_INV_Add0~17_sumout\,
	combout => \Selector13~0_combout\);

-- Location: LABCELL_X36_Y71_N24
\Selector13~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~2_combout\ = ( \draw~0_combout\ & ( \WideOr1~0_combout\ ) ) # ( !\draw~0_combout\ & ( (\puck_one.y\(2) & \WideOr1~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_puck_one.y\(2),
	datad => \ALT_INV_WideOr1~0_combout\,
	dataf => \ALT_INV_draw~0_combout\,
	combout => \Selector13~2_combout\);

-- Location: LABCELL_X33_Y71_N42
\Selector13~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~1_combout\ = ( !\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (!\state.ERASE_PADDLE_ENTER~q\ & ((\draw.y[3]~0_combout\) # (\puck_two.y\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011001100000011001100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	datac => \ALT_INV_puck_two.y\(2),
	datad => \ALT_INV_draw.y[3]~0_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector13~1_combout\);

-- Location: LABCELL_X36_Y71_N12
\Selector13~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~3_combout\ = ( !\Selector93~0_combout\ & ( \Selector13~1_combout\ & ( (\Selector13~2_combout\ & ((!\draw.y\(2)) # ((!\state.IDLE~q\ & \draw.x[4]~3_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001010000011100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datab => \ALT_INV_state.IDLE~q\,
	datac => \ALT_INV_Selector13~2_combout\,
	datad => \ALT_INV_draw.x[4]~3_combout\,
	datae => \ALT_INV_Selector93~0_combout\,
	dataf => \ALT_INV_Selector13~1_combout\,
	combout => \Selector13~3_combout\);

-- Location: LABCELL_X37_Y71_N15
\Selector13~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector13~4_combout\ = ( \draw.x[4]~7_combout\ & ( (!\Selector13~0_combout\) # (!\Selector13~3_combout\) ) ) # ( !\draw.x[4]~7_combout\ & ( (!\Equal4~3_combout\) # ((!\Selector13~0_combout\) # (!\Selector13~3_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111010111111111111101011111111111100001111111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~3_combout\,
	datac => \ALT_INV_Selector13~0_combout\,
	datad => \ALT_INV_Selector13~3_combout\,
	dataf => \ALT_INV_draw.x[4]~7_combout\,
	combout => \Selector13~4_combout\);

-- Location: FF_X37_Y71_N16
\draw.y[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector13~4_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(2));

-- Location: FF_X39_Y71_N43
\draw.y[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector12~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y[3]~DUPLICATE_q\);

-- Location: MLABCELL_X39_Y71_N21
\Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~29_sumout\ = SUM(( \draw.y\(7) ) + ( GND ) + ( \Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(7),
	cin => \Add0~2\,
	sumout => \Add0~29_sumout\);

-- Location: MLABCELL_X39_Y71_N45
\Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~0_combout\ = ( \puck_one.y\(7) & ( (!\draw.y[3]~1_combout\ & (((\puck_two.y\(7))) # (\draw.y[6]~4_combout\))) # (\draw.y[3]~1_combout\ & (!\draw.y[6]~4_combout\ & (\Add0~29_sumout\))) ) ) # ( !\puck_one.y\(7) & ( (!\draw.y[6]~4_combout\ & 
-- ((!\draw.y[3]~1_combout\ & ((\puck_two.y\(7)))) # (\draw.y[3]~1_combout\ & (\Add0~29_sumout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010010001100000001001000110000100110101011100010011010101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y[3]~1_combout\,
	datab => \ALT_INV_draw.y[6]~4_combout\,
	datac => \ALT_INV_Add0~29_sumout\,
	datad => \ALT_INV_puck_two.y\(7),
	dataf => \ALT_INV_puck_one.y\(7),
	combout => \Selector8~0_combout\);

-- Location: FF_X39_Y71_N46
\draw.y[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector8~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \draw.y[3]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \draw.y\(7));

-- Location: MLABCELL_X39_Y72_N48
\Equal1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = ( !\draw.y\(7) & ( (\draw.y\(2) & (\draw.y\(1) & (\draw.y\(0) & !\draw.y[3]~DUPLICATE_q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000000000000010000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(2),
	datab => \ALT_INV_draw.y\(1),
	datac => \ALT_INV_draw.y\(0),
	datad => \ALT_INV_draw.y[3]~DUPLICATE_q\,
	dataf => \ALT_INV_draw.y\(7),
	combout => \Equal1~1_combout\);

-- Location: LABCELL_X35_Y71_N27
\Equal1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = ( \Equal1~0_combout\ & ( \Equal1~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~1_combout\,
	dataf => \ALT_INV_Equal1~0_combout\,
	combout => \Equal1~2_combout\);

-- Location: LABCELL_X35_Y71_N21
\Selector97~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector97~0_combout\ = ((!\Equal1~2_combout\ & \state.DRAW_LEFT_LOOP~q\)) # (\state.DRAW_LEFT_ENTER~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111110101111000011111010111100001111101011110000111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~2_combout\,
	datac => \ALT_INV_state.DRAW_LEFT_ENTER~q\,
	datad => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	combout => \Selector97~0_combout\);

-- Location: FF_X35_Y71_N23
\state.DRAW_LEFT_LOOP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector97~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.DRAW_LEFT_LOOP~q\);

-- Location: LABCELL_X35_Y69_N51
\Selector98~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector98~1_combout\ = ( \Selector98~0_combout\ & ( ((\state.DRAW_LEFT_LOOP~q\ & \Equal1~2_combout\)) # (\state.DRAW_PUCK_TWO~q\) ) ) # ( !\Selector98~0_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100001111010111110000111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	datac => \ALT_INV_state.DRAW_PUCK_TWO~q\,
	datad => \ALT_INV_Equal1~2_combout\,
	dataf => \ALT_INV_Selector98~0_combout\,
	combout => \Selector98~1_combout\);

-- Location: FF_X35_Y69_N53
\state.IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector98~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.IDLE~q\);

-- Location: LABCELL_X35_Y69_N3
\Add2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~1_sumout\ = SUM(( clock_counter(21) ) + ( GND ) + ( \Add2~54\ ))
-- \Add2~2\ = CARRY(( clock_counter(21) ) + ( GND ) + ( \Add2~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(21),
	cin => \Add2~54\,
	sumout => \Add2~1_sumout\,
	cout => \Add2~2\);

-- Location: LABCELL_X35_Y69_N6
\Add2~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~73_sumout\ = SUM(( clock_counter(22) ) + ( GND ) + ( \Add2~2\ ))
-- \Add2~74\ = CARRY(( clock_counter(22) ) + ( GND ) + ( \Add2~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(22),
	cin => \Add2~2\,
	sumout => \Add2~73_sumout\,
	cout => \Add2~74\);

-- Location: LABCELL_X35_Y71_N6
\clock_counter[5]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \clock_counter[5]~0_combout\ = ( \KEY[3]~input_o\ & ( (!\state.DRAW_LEFT_LOOP~q\ & (((\state.IDLE~q\)))) # (\state.DRAW_LEFT_LOOP~q\ & (\Equal1~1_combout\ & (\Equal1~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000001110011010000000111001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal1~1_combout\,
	datab => \ALT_INV_state.DRAW_LEFT_LOOP~q\,
	datac => \ALT_INV_Equal1~0_combout\,
	datad => \ALT_INV_state.IDLE~q\,
	dataf => \ALT_INV_KEY[3]~input_o\,
	combout => \clock_counter[5]~0_combout\);

-- Location: FF_X35_Y69_N8
\clock_counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~73_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(22));

-- Location: LABCELL_X35_Y69_N9
\Add2~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~101_sumout\ = SUM(( clock_counter(23) ) + ( GND ) + ( \Add2~74\ ))
-- \Add2~102\ = CARRY(( clock_counter(23) ) + ( GND ) + ( \Add2~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(23),
	cin => \Add2~74\,
	sumout => \Add2~101_sumout\,
	cout => \Add2~102\);

-- Location: FF_X35_Y69_N10
\clock_counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~101_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(23));

-- Location: LABCELL_X35_Y69_N12
\Add2~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~105_sumout\ = SUM(( clock_counter(24) ) + ( GND ) + ( \Add2~102\ ))
-- \Add2~106\ = CARRY(( clock_counter(24) ) + ( GND ) + ( \Add2~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(24),
	cin => \Add2~102\,
	sumout => \Add2~105_sumout\,
	cout => \Add2~106\);

-- Location: FF_X35_Y69_N14
\clock_counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~105_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(24));

-- Location: LABCELL_X35_Y69_N15
\Add2~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~109_sumout\ = SUM(( clock_counter(25) ) + ( GND ) + ( \Add2~106\ ))
-- \Add2~110\ = CARRY(( clock_counter(25) ) + ( GND ) + ( \Add2~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(25),
	cin => \Add2~106\,
	sumout => \Add2~109_sumout\,
	cout => \Add2~110\);

-- Location: FF_X35_Y69_N17
\clock_counter[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~109_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(25));

-- Location: LABCELL_X35_Y69_N18
\Add2~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~81_sumout\ = SUM(( clock_counter(26) ) + ( GND ) + ( \Add2~110\ ))
-- \Add2~82\ = CARRY(( clock_counter(26) ) + ( GND ) + ( \Add2~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(26),
	cin => \Add2~110\,
	sumout => \Add2~81_sumout\,
	cout => \Add2~82\);

-- Location: FF_X35_Y69_N20
\clock_counter[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~81_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(26));

-- Location: LABCELL_X35_Y69_N21
\Add2~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~85_sumout\ = SUM(( clock_counter(27) ) + ( GND ) + ( \Add2~82\ ))
-- \Add2~86\ = CARRY(( clock_counter(27) ) + ( GND ) + ( \Add2~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(27),
	cin => \Add2~82\,
	sumout => \Add2~85_sumout\,
	cout => \Add2~86\);

-- Location: FF_X35_Y69_N23
\clock_counter[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~85_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(27));

-- Location: LABCELL_X35_Y69_N24
\Add2~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~89_sumout\ = SUM(( clock_counter(28) ) + ( GND ) + ( \Add2~86\ ))
-- \Add2~90\ = CARRY(( clock_counter(28) ) + ( GND ) + ( \Add2~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(28),
	cin => \Add2~86\,
	sumout => \Add2~89_sumout\,
	cout => \Add2~90\);

-- Location: FF_X35_Y69_N25
\clock_counter[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~89_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(28));

-- Location: LABCELL_X35_Y69_N27
\Add2~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~93_sumout\ = SUM(( clock_counter(29) ) + ( GND ) + ( \Add2~90\ ))
-- \Add2~94\ = CARRY(( clock_counter(29) ) + ( GND ) + ( \Add2~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(29),
	cin => \Add2~90\,
	sumout => \Add2~93_sumout\,
	cout => \Add2~94\);

-- Location: FF_X35_Y69_N29
\clock_counter[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~93_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(29));

-- Location: LABCELL_X35_Y69_N30
\Add2~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~97_sumout\ = SUM(( clock_counter(30) ) + ( GND ) + ( \Add2~94\ ))
-- \Add2~98\ = CARRY(( clock_counter(30) ) + ( GND ) + ( \Add2~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(30),
	cin => \Add2~94\,
	sumout => \Add2~97_sumout\,
	cout => \Add2~98\);

-- Location: FF_X35_Y69_N31
\clock_counter[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~97_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(30));

-- Location: LABCELL_X35_Y69_N48
\LessThan0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~4_combout\ = ( !clock_counter(25) & ( (!clock_counter(24) & (!clock_counter(30) & !clock_counter(23))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_clock_counter(24),
	datac => ALT_INV_clock_counter(30),
	datad => ALT_INV_clock_counter(23),
	dataf => ALT_INV_clock_counter(25),
	combout => \LessThan0~4_combout\);

-- Location: LABCELL_X35_Y69_N42
\LessThan0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~5_combout\ = ( \LessThan0~4_combout\ & ( (!clock_counter(29) & (!clock_counter(28) & (!clock_counter(26) & !clock_counter(27)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(29),
	datab => ALT_INV_clock_counter(28),
	datac => ALT_INV_clock_counter(26),
	datad => ALT_INV_clock_counter(27),
	dataf => \ALT_INV_LessThan0~4_combout\,
	combout => \LessThan0~5_combout\);

-- Location: LABCELL_X35_Y69_N33
\Add2~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~77_sumout\ = SUM(( clock_counter(31) ) + ( GND ) + ( \Add2~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(31),
	cin => \Add2~98\,
	sumout => \Add2~77_sumout\);

-- Location: FF_X35_Y69_N35
\clock_counter[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~77_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(31));

-- Location: LABCELL_X36_Y70_N42
\LessThan0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = ( clock_counter(16) & ( (clock_counter(18) & (clock_counter(20) & (clock_counter(17) & clock_counter(19)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(18),
	datab => ALT_INV_clock_counter(20),
	datac => ALT_INV_clock_counter(17),
	datad => ALT_INV_clock_counter(19),
	dataf => ALT_INV_clock_counter(16),
	combout => \LessThan0~2_combout\);

-- Location: LABCELL_X36_Y70_N3
\LessThan0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = ( clock_counter(12) & ( (clock_counter(10) & (clock_counter(11) & clock_counter(9))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(10),
	datac => ALT_INV_clock_counter(11),
	datad => ALT_INV_clock_counter(9),
	dataf => ALT_INV_clock_counter(12),
	combout => \LessThan0~1_combout\);

-- Location: LABCELL_X36_Y70_N36
\LessThan0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = ( !clock_counter(7) & ( !clock_counter(8) & ( (!clock_counter(6) & (!clock_counter(4) & !clock_counter(5))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => ALT_INV_clock_counter(6),
	datac => ALT_INV_clock_counter(4),
	datad => ALT_INV_clock_counter(5),
	datae => ALT_INV_clock_counter(7),
	dataf => ALT_INV_clock_counter(8),
	combout => \LessThan0~0_combout\);

-- Location: LABCELL_X36_Y70_N48
\LessThan0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = ( \LessThan0~0_combout\ & ( clock_counter(15) & ( \LessThan0~2_combout\ ) ) ) # ( !\LessThan0~0_combout\ & ( clock_counter(15) & ( \LessThan0~2_combout\ ) ) ) # ( \LessThan0~0_combout\ & ( !clock_counter(15) & ( (clock_counter(13) 
-- & (\LessThan0~2_combout\ & clock_counter(14))) ) ) ) # ( !\LessThan0~0_combout\ & ( !clock_counter(15) & ( (\LessThan0~2_combout\ & (clock_counter(14) & ((\LessThan0~1_combout\) # (clock_counter(13))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010011000000000001000100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(13),
	datab => \ALT_INV_LessThan0~2_combout\,
	datac => \ALT_INV_LessThan0~1_combout\,
	datad => ALT_INV_clock_counter(14),
	datae => \ALT_INV_LessThan0~0_combout\,
	dataf => ALT_INV_clock_counter(15),
	combout => \LessThan0~3_combout\);

-- Location: LABCELL_X35_Y69_N54
\Selector98~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector98~0_combout\ = ( clock_counter(21) & ( \LessThan0~3_combout\ & ( (!\state.IDLE~q\) # ((!clock_counter(31) & ((!\LessThan0~5_combout\) # (clock_counter(22))))) ) ) ) # ( !clock_counter(21) & ( \LessThan0~3_combout\ & ( (!\state.IDLE~q\) # 
-- ((!clock_counter(31) & ((!\LessThan0~5_combout\) # (clock_counter(22))))) ) ) ) # ( clock_counter(21) & ( !\LessThan0~3_combout\ & ( (!\state.IDLE~q\) # ((!clock_counter(31) & ((!\LessThan0~5_combout\) # (clock_counter(22))))) ) ) ) # ( !clock_counter(21) 
-- & ( !\LessThan0~3_combout\ & ( (!\state.IDLE~q\) # ((!\LessThan0~5_combout\ & !clock_counter(31))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110101011101010111010101111101011101010111110101110101011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.IDLE~q\,
	datab => \ALT_INV_LessThan0~5_combout\,
	datac => ALT_INV_clock_counter(31),
	datad => ALT_INV_clock_counter(22),
	datae => ALT_INV_clock_counter(21),
	dataf => \ALT_INV_LessThan0~3_combout\,
	combout => \Selector98~0_combout\);

-- Location: FF_X35_Y70_N2
\clock_counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~125_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(0));

-- Location: LABCELL_X35_Y70_N3
\Add2~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~121_sumout\ = SUM(( clock_counter(1) ) + ( GND ) + ( \Add2~126\ ))
-- \Add2~122\ = CARRY(( clock_counter(1) ) + ( GND ) + ( \Add2~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(1),
	cin => \Add2~126\,
	sumout => \Add2~121_sumout\,
	cout => \Add2~122\);

-- Location: FF_X35_Y70_N5
\clock_counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~121_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(1));

-- Location: LABCELL_X35_Y70_N6
\Add2~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~117_sumout\ = SUM(( clock_counter(2) ) + ( GND ) + ( \Add2~122\ ))
-- \Add2~118\ = CARRY(( clock_counter(2) ) + ( GND ) + ( \Add2~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(2),
	cin => \Add2~122\,
	sumout => \Add2~117_sumout\,
	cout => \Add2~118\);

-- Location: FF_X35_Y70_N8
\clock_counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~117_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(2));

-- Location: LABCELL_X35_Y70_N9
\Add2~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~113_sumout\ = SUM(( clock_counter(3) ) + ( GND ) + ( \Add2~118\ ))
-- \Add2~114\ = CARRY(( clock_counter(3) ) + ( GND ) + ( \Add2~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(3),
	cin => \Add2~118\,
	sumout => \Add2~113_sumout\,
	cout => \Add2~114\);

-- Location: FF_X35_Y70_N11
\clock_counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~113_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(3));

-- Location: LABCELL_X35_Y70_N12
\Add2~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~33_sumout\ = SUM(( clock_counter(4) ) + ( GND ) + ( \Add2~114\ ))
-- \Add2~34\ = CARRY(( clock_counter(4) ) + ( GND ) + ( \Add2~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(4),
	cin => \Add2~114\,
	sumout => \Add2~33_sumout\,
	cout => \Add2~34\);

-- Location: FF_X35_Y70_N13
\clock_counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~33_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(4));

-- Location: LABCELL_X35_Y70_N15
\Add2~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~29_sumout\ = SUM(( clock_counter(5) ) + ( GND ) + ( \Add2~34\ ))
-- \Add2~30\ = CARRY(( clock_counter(5) ) + ( GND ) + ( \Add2~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(5),
	cin => \Add2~34\,
	sumout => \Add2~29_sumout\,
	cout => \Add2~30\);

-- Location: FF_X35_Y70_N16
\clock_counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~29_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(5));

-- Location: LABCELL_X35_Y70_N18
\Add2~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~17_sumout\ = SUM(( clock_counter(6) ) + ( GND ) + ( \Add2~30\ ))
-- \Add2~18\ = CARRY(( clock_counter(6) ) + ( GND ) + ( \Add2~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(6),
	cin => \Add2~30\,
	sumout => \Add2~17_sumout\,
	cout => \Add2~18\);

-- Location: FF_X35_Y70_N19
\clock_counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~17_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(6));

-- Location: LABCELL_X35_Y70_N21
\Add2~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~25_sumout\ = SUM(( clock_counter(7) ) + ( GND ) + ( \Add2~18\ ))
-- \Add2~26\ = CARRY(( clock_counter(7) ) + ( GND ) + ( \Add2~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(7),
	cin => \Add2~18\,
	sumout => \Add2~25_sumout\,
	cout => \Add2~26\);

-- Location: FF_X35_Y70_N22
\clock_counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~25_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(7));

-- Location: LABCELL_X35_Y70_N24
\Add2~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~21_sumout\ = SUM(( clock_counter(8) ) + ( GND ) + ( \Add2~26\ ))
-- \Add2~22\ = CARRY(( clock_counter(8) ) + ( GND ) + ( \Add2~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(8),
	cin => \Add2~26\,
	sumout => \Add2~21_sumout\,
	cout => \Add2~22\);

-- Location: FF_X35_Y70_N25
\clock_counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~21_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(8));

-- Location: LABCELL_X35_Y70_N27
\Add2~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~49_sumout\ = SUM(( clock_counter(9) ) + ( GND ) + ( \Add2~22\ ))
-- \Add2~50\ = CARRY(( clock_counter(9) ) + ( GND ) + ( \Add2~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(9),
	cin => \Add2~22\,
	sumout => \Add2~49_sumout\,
	cout => \Add2~50\);

-- Location: FF_X35_Y70_N28
\clock_counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~49_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(9));

-- Location: LABCELL_X35_Y70_N30
\Add2~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~45_sumout\ = SUM(( clock_counter(10) ) + ( GND ) + ( \Add2~50\ ))
-- \Add2~46\ = CARRY(( clock_counter(10) ) + ( GND ) + ( \Add2~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(10),
	cin => \Add2~50\,
	sumout => \Add2~45_sumout\,
	cout => \Add2~46\);

-- Location: FF_X35_Y70_N31
\clock_counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~45_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(10));

-- Location: LABCELL_X35_Y70_N33
\Add2~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~41_sumout\ = SUM(( clock_counter(11) ) + ( GND ) + ( \Add2~46\ ))
-- \Add2~42\ = CARRY(( clock_counter(11) ) + ( GND ) + ( \Add2~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(11),
	cin => \Add2~46\,
	sumout => \Add2~41_sumout\,
	cout => \Add2~42\);

-- Location: FF_X35_Y70_N34
\clock_counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~41_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(11));

-- Location: LABCELL_X35_Y70_N36
\Add2~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~37_sumout\ = SUM(( clock_counter(12) ) + ( GND ) + ( \Add2~42\ ))
-- \Add2~38\ = CARRY(( clock_counter(12) ) + ( GND ) + ( \Add2~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(12),
	cin => \Add2~42\,
	sumout => \Add2~37_sumout\,
	cout => \Add2~38\);

-- Location: FF_X35_Y70_N37
\clock_counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~37_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(12));

-- Location: LABCELL_X35_Y70_N39
\Add2~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~13_sumout\ = SUM(( clock_counter(13) ) + ( GND ) + ( \Add2~38\ ))
-- \Add2~14\ = CARRY(( clock_counter(13) ) + ( GND ) + ( \Add2~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(13),
	cin => \Add2~38\,
	sumout => \Add2~13_sumout\,
	cout => \Add2~14\);

-- Location: FF_X35_Y70_N40
\clock_counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~13_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(13));

-- Location: LABCELL_X35_Y70_N42
\Add2~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~9_sumout\ = SUM(( clock_counter(14) ) + ( GND ) + ( \Add2~14\ ))
-- \Add2~10\ = CARRY(( clock_counter(14) ) + ( GND ) + ( \Add2~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(14),
	cin => \Add2~14\,
	sumout => \Add2~9_sumout\,
	cout => \Add2~10\);

-- Location: FF_X35_Y70_N43
\clock_counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~9_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(14));

-- Location: LABCELL_X35_Y70_N45
\Add2~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~5_sumout\ = SUM(( clock_counter(15) ) + ( GND ) + ( \Add2~10\ ))
-- \Add2~6\ = CARRY(( clock_counter(15) ) + ( GND ) + ( \Add2~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(15),
	cin => \Add2~10\,
	sumout => \Add2~5_sumout\,
	cout => \Add2~6\);

-- Location: FF_X35_Y70_N46
\clock_counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~5_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(15));

-- Location: LABCELL_X35_Y70_N48
\Add2~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~69_sumout\ = SUM(( clock_counter(16) ) + ( GND ) + ( \Add2~6\ ))
-- \Add2~70\ = CARRY(( clock_counter(16) ) + ( GND ) + ( \Add2~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(16),
	cin => \Add2~6\,
	sumout => \Add2~69_sumout\,
	cout => \Add2~70\);

-- Location: FF_X35_Y70_N49
\clock_counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~69_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(16));

-- Location: LABCELL_X35_Y70_N51
\Add2~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~65_sumout\ = SUM(( clock_counter(17) ) + ( GND ) + ( \Add2~70\ ))
-- \Add2~66\ = CARRY(( clock_counter(17) ) + ( GND ) + ( \Add2~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(17),
	cin => \Add2~70\,
	sumout => \Add2~65_sumout\,
	cout => \Add2~66\);

-- Location: FF_X35_Y70_N52
\clock_counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~65_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(17));

-- Location: LABCELL_X35_Y70_N54
\Add2~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~61_sumout\ = SUM(( clock_counter(18) ) + ( GND ) + ( \Add2~66\ ))
-- \Add2~62\ = CARRY(( clock_counter(18) ) + ( GND ) + ( \Add2~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(18),
	cin => \Add2~66\,
	sumout => \Add2~61_sumout\,
	cout => \Add2~62\);

-- Location: FF_X35_Y70_N55
\clock_counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~61_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(18));

-- Location: LABCELL_X35_Y70_N57
\Add2~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~57_sumout\ = SUM(( clock_counter(19) ) + ( GND ) + ( \Add2~62\ ))
-- \Add2~58\ = CARRY(( clock_counter(19) ) + ( GND ) + ( \Add2~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(19),
	cin => \Add2~62\,
	sumout => \Add2~57_sumout\,
	cout => \Add2~58\);

-- Location: FF_X35_Y70_N58
\clock_counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~57_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(19));

-- Location: LABCELL_X35_Y69_N0
\Add2~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add2~53_sumout\ = SUM(( clock_counter(20) ) + ( GND ) + ( \Add2~58\ ))
-- \Add2~54\ = CARRY(( clock_counter(20) ) + ( GND ) + ( \Add2~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_clock_counter(20),
	cin => \Add2~58\,
	sumout => \Add2~53_sumout\,
	cout => \Add2~54\);

-- Location: FF_X35_Y69_N1
\clock_counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~53_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(20));

-- Location: FF_X35_Y69_N5
\clock_counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add2~1_sumout\,
	sclr => \Selector98~0_combout\,
	ena => \clock_counter[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => clock_counter(21));

-- Location: LABCELL_X35_Y69_N36
\Selector94~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector94~0_combout\ = ( \LessThan0~5_combout\ & ( \state.IDLE~q\ & ( (!clock_counter(31) & (clock_counter(22) & ((\LessThan0~3_combout\) # (clock_counter(21))))) ) ) ) # ( !\LessThan0~5_combout\ & ( \state.IDLE~q\ & ( !clock_counter(31) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000111100000000000001110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_clock_counter(21),
	datab => \ALT_INV_LessThan0~3_combout\,
	datac => ALT_INV_clock_counter(31),
	datad => ALT_INV_clock_counter(22),
	datae => \ALT_INV_LessThan0~5_combout\,
	dataf => \ALT_INV_state.IDLE~q\,
	combout => \Selector94~0_combout\);

-- Location: FF_X35_Y69_N37
\state.ERASE_PADDLE_ENTER\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector94~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ERASE_PADDLE_ENTER~q\);

-- Location: LABCELL_X37_Y70_N0
\Add3~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~73_sumout\ = SUM(( paddle_counter(0) ) + ( VCC ) + ( !VCC ))
-- \Add3~74\ = CARRY(( paddle_counter(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(0),
	cin => GND,
	sumout => \Add3~73_sumout\,
	cout => \Add3~74\);

-- Location: LABCELL_X36_Y70_N27
\paddle_counter[10]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_counter[10]~0_combout\ = ( \KEY[3]~input_o\ & ( \state.ERASE_PADDLE_ENTER~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => \ALT_INV_KEY[3]~input_o\,
	combout => \paddle_counter[10]~0_combout\);

-- Location: FF_X37_Y70_N2
\paddle_counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~73_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(0));

-- Location: LABCELL_X37_Y70_N3
\Add3~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~29_sumout\ = SUM(( paddle_counter(1) ) + ( GND ) + ( \Add3~74\ ))
-- \Add3~30\ = CARRY(( paddle_counter(1) ) + ( GND ) + ( \Add3~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(1),
	cin => \Add3~74\,
	sumout => \Add3~29_sumout\,
	cout => \Add3~30\);

-- Location: FF_X37_Y70_N4
\paddle_counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~29_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(1));

-- Location: LABCELL_X37_Y70_N6
\Add3~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~33_sumout\ = SUM(( paddle_counter(2) ) + ( GND ) + ( \Add3~30\ ))
-- \Add3~34\ = CARRY(( paddle_counter(2) ) + ( GND ) + ( \Add3~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(2),
	cin => \Add3~30\,
	sumout => \Add3~33_sumout\,
	cout => \Add3~34\);

-- Location: FF_X37_Y70_N7
\paddle_counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~33_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(2));

-- Location: LABCELL_X37_Y70_N9
\Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~5_sumout\ = SUM(( paddle_counter(3) ) + ( GND ) + ( \Add3~34\ ))
-- \Add3~6\ = CARRY(( paddle_counter(3) ) + ( GND ) + ( \Add3~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(3),
	cin => \Add3~34\,
	sumout => \Add3~5_sumout\,
	cout => \Add3~6\);

-- Location: FF_X37_Y70_N11
\paddle_counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~5_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(3));

-- Location: LABCELL_X37_Y70_N12
\Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~9_sumout\ = SUM(( paddle_counter(4) ) + ( GND ) + ( \Add3~6\ ))
-- \Add3~10\ = CARRY(( paddle_counter(4) ) + ( GND ) + ( \Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(4),
	cin => \Add3~6\,
	sumout => \Add3~9_sumout\,
	cout => \Add3~10\);

-- Location: FF_X37_Y70_N14
\paddle_counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~9_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(4));

-- Location: LABCELL_X37_Y70_N15
\Add3~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~121_sumout\ = SUM(( paddle_counter(5) ) + ( GND ) + ( \Add3~10\ ))
-- \Add3~122\ = CARRY(( paddle_counter(5) ) + ( GND ) + ( \Add3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(5),
	cin => \Add3~10\,
	sumout => \Add3~121_sumout\,
	cout => \Add3~122\);

-- Location: FF_X37_Y70_N16
\paddle_counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~121_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(5));

-- Location: LABCELL_X37_Y70_N18
\Add3~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~125_sumout\ = SUM(( paddle_counter(6) ) + ( GND ) + ( \Add3~122\ ))
-- \Add3~126\ = CARRY(( paddle_counter(6) ) + ( GND ) + ( \Add3~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(6),
	cin => \Add3~122\,
	sumout => \Add3~125_sumout\,
	cout => \Add3~126\);

-- Location: FF_X37_Y70_N19
\paddle_counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~125_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(6));

-- Location: LABCELL_X37_Y70_N21
\Add3~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~61_sumout\ = SUM(( paddle_counter(7) ) + ( GND ) + ( \Add3~126\ ))
-- \Add3~62\ = CARRY(( paddle_counter(7) ) + ( GND ) + ( \Add3~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(7),
	cin => \Add3~126\,
	sumout => \Add3~61_sumout\,
	cout => \Add3~62\);

-- Location: FF_X37_Y70_N23
\paddle_counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~61_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(7));

-- Location: LABCELL_X37_Y70_N24
\Add3~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~69_sumout\ = SUM(( paddle_counter(8) ) + ( GND ) + ( \Add3~62\ ))
-- \Add3~70\ = CARRY(( paddle_counter(8) ) + ( GND ) + ( \Add3~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(8),
	cin => \Add3~62\,
	sumout => \Add3~69_sumout\,
	cout => \Add3~70\);

-- Location: FF_X37_Y70_N25
\paddle_counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~69_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(8));

-- Location: LABCELL_X37_Y70_N27
\Add3~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~37_sumout\ = SUM(( paddle_counter(9) ) + ( GND ) + ( \Add3~70\ ))
-- \Add3~38\ = CARRY(( paddle_counter(9) ) + ( GND ) + ( \Add3~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(9),
	cin => \Add3~70\,
	sumout => \Add3~37_sumout\,
	cout => \Add3~38\);

-- Location: FF_X37_Y70_N29
\paddle_counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~37_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(9));

-- Location: LABCELL_X37_Y70_N30
\Add3~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~77_sumout\ = SUM(( paddle_counter(10) ) + ( GND ) + ( \Add3~38\ ))
-- \Add3~78\ = CARRY(( paddle_counter(10) ) + ( GND ) + ( \Add3~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(10),
	cin => \Add3~38\,
	sumout => \Add3~77_sumout\,
	cout => \Add3~78\);

-- Location: FF_X37_Y70_N32
\paddle_counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~77_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(10));

-- Location: LABCELL_X37_Y70_N33
\Add3~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~81_sumout\ = SUM(( paddle_counter(11) ) + ( GND ) + ( \Add3~78\ ))
-- \Add3~82\ = CARRY(( paddle_counter(11) ) + ( GND ) + ( \Add3~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(11),
	cin => \Add3~78\,
	sumout => \Add3~81_sumout\,
	cout => \Add3~82\);

-- Location: FF_X37_Y70_N35
\paddle_counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~81_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(11));

-- Location: LABCELL_X37_Y70_N36
\Add3~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~85_sumout\ = SUM(( paddle_counter(12) ) + ( GND ) + ( \Add3~82\ ))
-- \Add3~86\ = CARRY(( paddle_counter(12) ) + ( GND ) + ( \Add3~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(12),
	cin => \Add3~82\,
	sumout => \Add3~85_sumout\,
	cout => \Add3~86\);

-- Location: FF_X37_Y70_N37
\paddle_counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~85_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(12));

-- Location: LABCELL_X37_Y70_N39
\Add3~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~65_sumout\ = SUM(( paddle_counter(13) ) + ( GND ) + ( \Add3~86\ ))
-- \Add3~66\ = CARRY(( paddle_counter(13) ) + ( GND ) + ( \Add3~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(13),
	cin => \Add3~86\,
	sumout => \Add3~65_sumout\,
	cout => \Add3~66\);

-- Location: FF_X37_Y70_N40
\paddle_counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~65_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(13));

-- Location: LABCELL_X37_Y70_N42
\Add3~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~41_sumout\ = SUM(( paddle_counter(14) ) + ( GND ) + ( \Add3~66\ ))
-- \Add3~42\ = CARRY(( paddle_counter(14) ) + ( GND ) + ( \Add3~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(14),
	cin => \Add3~66\,
	sumout => \Add3~41_sumout\,
	cout => \Add3~42\);

-- Location: FF_X37_Y70_N43
\paddle_counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~41_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(14));

-- Location: LABCELL_X37_Y70_N45
\Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~25_sumout\ = SUM(( paddle_counter(15) ) + ( GND ) + ( \Add3~42\ ))
-- \Add3~26\ = CARRY(( paddle_counter(15) ) + ( GND ) + ( \Add3~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(15),
	cin => \Add3~42\,
	sumout => \Add3~25_sumout\,
	cout => \Add3~26\);

-- Location: FF_X37_Y70_N46
\paddle_counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~25_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(15));

-- Location: LABCELL_X37_Y70_N48
\Add3~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~45_sumout\ = SUM(( paddle_counter(16) ) + ( GND ) + ( \Add3~26\ ))
-- \Add3~46\ = CARRY(( paddle_counter(16) ) + ( GND ) + ( \Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(16),
	cin => \Add3~26\,
	sumout => \Add3~45_sumout\,
	cout => \Add3~46\);

-- Location: FF_X37_Y70_N49
\paddle_counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~45_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(16));

-- Location: LABCELL_X37_Y70_N51
\Add3~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~49_sumout\ = SUM(( paddle_counter(17) ) + ( GND ) + ( \Add3~46\ ))
-- \Add3~50\ = CARRY(( paddle_counter(17) ) + ( GND ) + ( \Add3~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(17),
	cin => \Add3~46\,
	sumout => \Add3~49_sumout\,
	cout => \Add3~50\);

-- Location: FF_X37_Y70_N52
\paddle_counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~49_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(17));

-- Location: LABCELL_X37_Y70_N54
\Add3~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~53_sumout\ = SUM(( paddle_counter(18) ) + ( GND ) + ( \Add3~50\ ))
-- \Add3~54\ = CARRY(( paddle_counter(18) ) + ( GND ) + ( \Add3~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(18),
	cin => \Add3~50\,
	sumout => \Add3~53_sumout\,
	cout => \Add3~54\);

-- Location: FF_X37_Y70_N56
\paddle_counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~53_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(18));

-- Location: LABCELL_X37_Y69_N42
\Equal3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~1_combout\ = ( !paddle_counter(14) & ( (!paddle_counter(16) & (!paddle_counter(17) & (!paddle_counter(9) & !paddle_counter(18)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_counter(16),
	datab => ALT_INV_paddle_counter(17),
	datac => ALT_INV_paddle_counter(9),
	datad => ALT_INV_paddle_counter(18),
	dataf => ALT_INV_paddle_counter(14),
	combout => \Equal3~1_combout\);

-- Location: LABCELL_X37_Y70_N57
\Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~1_sumout\ = SUM(( paddle_counter(19) ) + ( GND ) + ( \Add3~54\ ))
-- \Add3~2\ = CARRY(( paddle_counter(19) ) + ( GND ) + ( \Add3~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(19),
	cin => \Add3~54\,
	sumout => \Add3~1_sumout\,
	cout => \Add3~2\);

-- Location: FF_X37_Y70_N58
\paddle_counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~1_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(19));

-- Location: LABCELL_X37_Y69_N0
\Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~13_sumout\ = SUM(( paddle_counter(20) ) + ( GND ) + ( \Add3~2\ ))
-- \Add3~14\ = CARRY(( paddle_counter(20) ) + ( GND ) + ( \Add3~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(20),
	cin => \Add3~2\,
	sumout => \Add3~13_sumout\,
	cout => \Add3~14\);

-- Location: FF_X37_Y69_N2
\paddle_counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~13_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(20));

-- Location: LABCELL_X37_Y69_N3
\Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~17_sumout\ = SUM(( paddle_counter(21) ) + ( GND ) + ( \Add3~14\ ))
-- \Add3~18\ = CARRY(( paddle_counter(21) ) + ( GND ) + ( \Add3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(21),
	cin => \Add3~14\,
	sumout => \Add3~17_sumout\,
	cout => \Add3~18\);

-- Location: FF_X37_Y69_N5
\paddle_counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~17_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(21));

-- Location: LABCELL_X37_Y69_N6
\Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~21_sumout\ = SUM(( paddle_counter(22) ) + ( GND ) + ( \Add3~18\ ))
-- \Add3~22\ = CARRY(( paddle_counter(22) ) + ( GND ) + ( \Add3~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(22),
	cin => \Add3~18\,
	sumout => \Add3~21_sumout\,
	cout => \Add3~22\);

-- Location: FF_X37_Y69_N8
\paddle_counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~21_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(22));

-- Location: LABCELL_X37_Y69_N9
\Add3~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~89_sumout\ = SUM(( paddle_counter(23) ) + ( GND ) + ( \Add3~22\ ))
-- \Add3~90\ = CARRY(( paddle_counter(23) ) + ( GND ) + ( \Add3~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(23),
	cin => \Add3~22\,
	sumout => \Add3~89_sumout\,
	cout => \Add3~90\);

-- Location: FF_X37_Y69_N10
\paddle_counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~89_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(23));

-- Location: LABCELL_X37_Y69_N12
\Add3~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~93_sumout\ = SUM(( paddle_counter(24) ) + ( GND ) + ( \Add3~90\ ))
-- \Add3~94\ = CARRY(( paddle_counter(24) ) + ( GND ) + ( \Add3~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(24),
	cin => \Add3~90\,
	sumout => \Add3~93_sumout\,
	cout => \Add3~94\);

-- Location: FF_X37_Y69_N14
\paddle_counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~93_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(24));

-- Location: LABCELL_X37_Y69_N15
\Add3~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~97_sumout\ = SUM(( paddle_counter(25) ) + ( GND ) + ( \Add3~94\ ))
-- \Add3~98\ = CARRY(( paddle_counter(25) ) + ( GND ) + ( \Add3~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(25),
	cin => \Add3~94\,
	sumout => \Add3~97_sumout\,
	cout => \Add3~98\);

-- Location: FF_X37_Y69_N17
\paddle_counter[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~97_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(25));

-- Location: LABCELL_X37_Y69_N18
\Add3~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~101_sumout\ = SUM(( paddle_counter(26) ) + ( GND ) + ( \Add3~98\ ))
-- \Add3~102\ = CARRY(( paddle_counter(26) ) + ( GND ) + ( \Add3~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(26),
	cin => \Add3~98\,
	sumout => \Add3~101_sumout\,
	cout => \Add3~102\);

-- Location: FF_X37_Y69_N20
\paddle_counter[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~101_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(26));

-- Location: LABCELL_X37_Y69_N21
\Add3~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~117_sumout\ = SUM(( paddle_counter(27) ) + ( GND ) + ( \Add3~102\ ))
-- \Add3~118\ = CARRY(( paddle_counter(27) ) + ( GND ) + ( \Add3~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(27),
	cin => \Add3~102\,
	sumout => \Add3~117_sumout\,
	cout => \Add3~118\);

-- Location: FF_X37_Y69_N23
\paddle_counter[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~117_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(27));

-- Location: LABCELL_X37_Y69_N24
\Add3~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~105_sumout\ = SUM(( paddle_counter(28) ) + ( GND ) + ( \Add3~118\ ))
-- \Add3~106\ = CARRY(( paddle_counter(28) ) + ( GND ) + ( \Add3~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(28),
	cin => \Add3~118\,
	sumout => \Add3~105_sumout\,
	cout => \Add3~106\);

-- Location: FF_X37_Y69_N26
\paddle_counter[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~105_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(28));

-- Location: LABCELL_X37_Y69_N27
\Add3~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~57_sumout\ = SUM(( paddle_counter(29) ) + ( GND ) + ( \Add3~106\ ))
-- \Add3~58\ = CARRY(( paddle_counter(29) ) + ( GND ) + ( \Add3~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(29),
	cin => \Add3~106\,
	sumout => \Add3~57_sumout\,
	cout => \Add3~58\);

-- Location: FF_X37_Y69_N28
\paddle_counter[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~57_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(29));

-- Location: LABCELL_X37_Y69_N30
\Add3~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~109_sumout\ = SUM(( paddle_counter(30) ) + ( GND ) + ( \Add3~58\ ))
-- \Add3~110\ = CARRY(( paddle_counter(30) ) + ( GND ) + ( \Add3~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(30),
	cin => \Add3~58\,
	sumout => \Add3~109_sumout\,
	cout => \Add3~110\);

-- Location: FF_X37_Y69_N32
\paddle_counter[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~109_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(30));

-- Location: LABCELL_X37_Y69_N33
\Add3~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~113_sumout\ = SUM(( paddle_counter(31) ) + ( GND ) + ( \Add3~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => ALT_INV_paddle_counter(31),
	cin => \Add3~110\,
	sumout => \Add3~113_sumout\);

-- Location: FF_X37_Y69_N35
\paddle_counter[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Add3~113_sumout\,
	sclr => \Equal3~6_combout\,
	ena => \paddle_counter[10]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_counter(31));

-- Location: LABCELL_X37_Y69_N54
\Equal3~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~4_combout\ = ( !paddle_counter(27) & ( !paddle_counter(6) & ( (paddle_counter(5) & (!paddle_counter(30) & !paddle_counter(31))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_counter(5),
	datab => ALT_INV_paddle_counter(30),
	datac => ALT_INV_paddle_counter(31),
	datae => ALT_INV_paddle_counter(27),
	dataf => ALT_INV_paddle_counter(6),
	combout => \Equal3~4_combout\);

-- Location: LABCELL_X37_Y69_N48
\Equal3~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~3_combout\ = ( !paddle_counter(23) & ( !paddle_counter(25) & ( (!paddle_counter(28) & (!paddle_counter(24) & !paddle_counter(26))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_counter(28),
	datab => ALT_INV_paddle_counter(24),
	datac => ALT_INV_paddle_counter(26),
	datae => ALT_INV_paddle_counter(23),
	dataf => ALT_INV_paddle_counter(25),
	combout => \Equal3~3_combout\);

-- Location: LABCELL_X36_Y70_N24
\Equal3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~2_combout\ = ( !paddle_counter(10) & ( (!paddle_counter(11) & (!paddle_counter(12) & (!paddle_counter(8) & !paddle_counter(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_counter(11),
	datab => ALT_INV_paddle_counter(12),
	datac => ALT_INV_paddle_counter(8),
	datad => ALT_INV_paddle_counter(0),
	dataf => ALT_INV_paddle_counter(10),
	combout => \Equal3~2_combout\);

-- Location: LABCELL_X36_Y70_N30
\Equal3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~5_combout\ = ( paddle_counter(7) & ( !paddle_counter(13) & ( (\Equal3~4_combout\ & (\Equal3~3_combout\ & (\Equal3~2_combout\ & !paddle_counter(29)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000010000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal3~4_combout\,
	datab => \ALT_INV_Equal3~3_combout\,
	datac => \ALT_INV_Equal3~2_combout\,
	datad => ALT_INV_paddle_counter(29),
	datae => ALT_INV_paddle_counter(7),
	dataf => ALT_INV_paddle_counter(13),
	combout => \Equal3~5_combout\);

-- Location: LABCELL_X37_Y69_N36
\Equal3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~0_combout\ = ( !paddle_counter(20) & ( !paddle_counter(2) & ( (!paddle_counter(15) & (!paddle_counter(22) & (!paddle_counter(21) & !paddle_counter(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_counter(15),
	datab => ALT_INV_paddle_counter(22),
	datac => ALT_INV_paddle_counter(21),
	datad => ALT_INV_paddle_counter(1),
	datae => ALT_INV_paddle_counter(20),
	dataf => ALT_INV_paddle_counter(2),
	combout => \Equal3~0_combout\);

-- Location: LABCELL_X36_Y70_N6
\Equal3~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal3~6_combout\ = ( !paddle_counter(3) & ( !paddle_counter(4) & ( (\Equal3~1_combout\ & (\Equal3~5_combout\ & (!paddle_counter(19) & \Equal3~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal3~1_combout\,
	datab => \ALT_INV_Equal3~5_combout\,
	datac => ALT_INV_paddle_counter(19),
	datad => \ALT_INV_Equal3~0_combout\,
	datae => ALT_INV_paddle_counter(3),
	dataf => ALT_INV_paddle_counter(4),
	combout => \Equal3~6_combout\);

-- Location: LABCELL_X36_Y70_N0
\pflag~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \pflag~0_combout\ = ( \Equal3~6_combout\ & ( ((\state.ERASE_PADDLE_ENTER~q\ & \KEY[3]~input_o\)) # (\pflag~q\) ) ) # ( !\Equal3~6_combout\ & ( (\pflag~q\ & ((!\state.ERASE_PADDLE_ENTER~q\) # (!\KEY[3]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111100000000001111110000000011111111110000001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	datac => \ALT_INV_KEY[3]~input_o\,
	datad => \ALT_INV_pflag~q\,
	dataf => \ALT_INV_Equal3~6_combout\,
	combout => \pflag~0_combout\);

-- Location: FF_X36_Y70_N2
pflag : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \pflag~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \pflag~q\);

-- Location: LABCELL_X36_Y69_N0
\paddle_width~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \paddle_width~2_combout\ = ( paddle_width(2) & ( (\pflag~q\ & (((!paddle_width(3)) # (!paddle_width(1))) # (paddle_width(0)))) ) ) # ( !paddle_width(2) & ( (\pflag~q\ & !paddle_width(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000110011001100010011001100110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_width(0),
	datab => \ALT_INV_pflag~q\,
	datac => ALT_INV_paddle_width(3),
	datad => ALT_INV_paddle_width(1),
	dataf => ALT_INV_paddle_width(2),
	combout => \paddle_width~2_combout\);

-- Location: MLABCELL_X34_Y71_N18
\Selector110~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector110~0_combout\ = ( \state.INIT~DUPLICATE_q\ & ( !paddle_width(0) $ (((!\paddle_width~2_combout\) # (!\state.DRAW_PADDLE_ENTER~DUPLICATE_q\))) ) ) # ( !\state.INIT~DUPLICATE_q\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & 
-- (!\paddle_width~2_combout\ $ (!paddle_width(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100001100000000110000110000000011111111000000001111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_paddle_width~2_combout\,
	datac => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datad => ALT_INV_paddle_width(0),
	dataf => \ALT_INV_state.INIT~DUPLICATE_q\,
	combout => \Selector110~0_combout\);

-- Location: FF_X34_Y71_N19
\paddle_width[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector110~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_width(0));

-- Location: LABCELL_X36_Y69_N3
\Selector109~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector109~0_combout\ = ( \paddle_width~2_combout\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & (!paddle_width(0) $ (paddle_width(1)))) ) ) # ( !\paddle_width~2_combout\ & ( (\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & paddle_width(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100001010000001010000101000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_paddle_width(0),
	datac => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	datad => ALT_INV_paddle_width(1),
	dataf => \ALT_INV_paddle_width~2_combout\,
	combout => \Selector109~0_combout\);

-- Location: FF_X36_Y69_N5
\paddle_width[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector109~0_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	ena => \WideOr5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => paddle_width(1));

-- Location: LABCELL_X37_Y71_N48
\Equal4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~1_combout\ = ( \draw.x\(1) & ( \draw.x\(0) & ( (\Add13~13_sumout\ & (paddle_width(0) & (!\Add13~17_sumout\ $ (\draw.x\(2))))) ) ) ) # ( !\draw.x\(1) & ( \draw.x\(0) & ( (!\Add13~13_sumout\ & (paddle_width(0) & (!\Add13~17_sumout\ $ 
-- (\draw.x\(2))))) ) ) ) # ( \draw.x\(1) & ( !\draw.x\(0) & ( (\Add13~13_sumout\ & (!paddle_width(0) & (!\Add13~17_sumout\ $ (\draw.x\(2))))) ) ) ) # ( !\draw.x\(1) & ( !\draw.x\(0) & ( (!\Add13~13_sumout\ & (!paddle_width(0) & (!\Add13~17_sumout\ $ 
-- (\draw.x\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000001000010000000000010000100000000000100001000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add13~13_sumout\,
	datab => ALT_INV_paddle_width(0),
	datac => \ALT_INV_Add13~17_sumout\,
	datad => \ALT_INV_draw.x\(2),
	datae => \ALT_INV_draw.x\(1),
	dataf => \ALT_INV_draw.x\(0),
	combout => \Equal4~1_combout\);

-- Location: LABCELL_X37_Y71_N18
\Equal4~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal4~3_combout\ = ( \Add13~21_sumout\ & ( \Add13~25_sumout\ & ( (\Equal4~1_combout\ & (\draw.x\(6) & (\Equal4~0_combout\ & \draw.x\(7)))) ) ) ) # ( !\Add13~21_sumout\ & ( \Add13~25_sumout\ & ( (\Equal4~1_combout\ & (!\draw.x\(6) & (\Equal4~0_combout\ & 
-- \draw.x\(7)))) ) ) ) # ( \Add13~21_sumout\ & ( !\Add13~25_sumout\ & ( (\Equal4~1_combout\ & (\draw.x\(6) & (\Equal4~0_combout\ & !\draw.x\(7)))) ) ) ) # ( !\Add13~21_sumout\ & ( !\Add13~25_sumout\ & ( (\Equal4~1_combout\ & (!\draw.x\(6) & 
-- (\Equal4~0_combout\ & !\draw.x\(7)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000000000000010000000000000000000001000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~1_combout\,
	datab => \ALT_INV_draw.x\(6),
	datac => \ALT_INV_Equal4~0_combout\,
	datad => \ALT_INV_draw.x\(7),
	datae => \ALT_INV_Add13~21_sumout\,
	dataf => \ALT_INV_Add13~25_sumout\,
	combout => \Equal4~3_combout\);

-- Location: MLABCELL_X34_Y71_N27
\Selector89~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector89~0_combout\ = ( !\WideOr6~0_combout\ & ( (!\state.DRAW_PUCK_ONE~q\ & (!\state.DRAW_PUCK_TWO~q\ & (!\state.ERASE_PUCK_TWO~q\ & !\state.ERASE_PADDLE_ENTER~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000100000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PUCK_ONE~q\,
	datab => \ALT_INV_state.DRAW_PUCK_TWO~q\,
	datac => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => \ALT_INV_WideOr6~0_combout\,
	combout => \Selector89~0_combout\);

-- Location: LABCELL_X37_Y71_N3
\Selector89~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector89~1_combout\ = ( \state.IDLE~q\ & ( (\Selector89~0_combout\ & (((!\state.DRAW_PADDLE_LOOP~q\) # (\plot~q\)) # (\Equal4~3_combout\))) ) ) # ( !\state.IDLE~q\ & ( (\Selector89~0_combout\ & (((\Equal4~3_combout\ & \state.DRAW_PADDLE_LOOP~q\)) # 
-- (\plot~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100110011000000010011001100110001001100110011000100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Equal4~3_combout\,
	datab => \ALT_INV_Selector89~0_combout\,
	datac => \ALT_INV_state.DRAW_PADDLE_LOOP~q\,
	datad => \ALT_INV_plot~q\,
	dataf => \ALT_INV_state.IDLE~q\,
	combout => \Selector89~1_combout\);

-- Location: FF_X37_Y71_N4
plot : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector89~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \plot~q\);

-- Location: MLABCELL_X39_Y72_N36
\vga_u0|writeEn~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|writeEn~0_combout\ = ( \draw.x\(7) & ( (!\plot~q\ & (!\draw.x\(6) & !\draw.x\(5))) ) ) # ( !\draw.x\(7) & ( !\plot~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_plot~q\,
	datac => \ALT_INV_draw.x\(6),
	datad => \ALT_INV_draw.x\(5),
	dataf => \ALT_INV_draw.x\(7),
	combout => \vga_u0|writeEn~0_combout\);

-- Location: MLABCELL_X39_Y72_N0
\vga_u0|user_input_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~9_sumout\ = SUM(( !\draw.y\(0) $ (!\draw.x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~10\ = CARRY(( !\draw.y\(0) $ (!\draw.x\(5)) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|user_input_translator|Add1~11\ = SHARE((\draw.y\(0) & \draw.x\(5)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(0),
	datac => \ALT_INV_draw.x\(5),
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|user_input_translator|Add1~9_sumout\,
	cout => \vga_u0|user_input_translator|Add1~10\,
	shareout => \vga_u0|user_input_translator|Add1~11\);

-- Location: MLABCELL_X39_Y72_N3
\vga_u0|user_input_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~13_sumout\ = SUM(( !\draw.y\(1) $ (!\draw.x\(6)) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~14\ = CARRY(( !\draw.y\(1) $ (!\draw.x\(6)) ) + ( \vga_u0|user_input_translator|Add1~11\ ) + ( \vga_u0|user_input_translator|Add1~10\ ))
-- \vga_u0|user_input_translator|Add1~15\ = SHARE((\draw.y\(1) & \draw.x\(6)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(1),
	datad => \ALT_INV_draw.x\(6),
	cin => \vga_u0|user_input_translator|Add1~10\,
	sharein => \vga_u0|user_input_translator|Add1~11\,
	sumout => \vga_u0|user_input_translator|Add1~13_sumout\,
	cout => \vga_u0|user_input_translator|Add1~14\,
	shareout => \vga_u0|user_input_translator|Add1~15\);

-- Location: MLABCELL_X39_Y72_N6
\vga_u0|user_input_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~17_sumout\ = SUM(( !\draw.y\(0) $ (!\draw.x\(7) $ (\draw.y\(2))) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~18\ = CARRY(( !\draw.y\(0) $ (!\draw.x\(7) $ (\draw.y\(2))) ) + ( \vga_u0|user_input_translator|Add1~15\ ) + ( \vga_u0|user_input_translator|Add1~14\ ))
-- \vga_u0|user_input_translator|Add1~19\ = SHARE((!\draw.y\(0) & (\draw.x\(7) & \draw.y\(2))) # (\draw.y\(0) & ((\draw.y\(2)) # (\draw.x\(7)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(0),
	datac => \ALT_INV_draw.x\(7),
	datad => \ALT_INV_draw.y\(2),
	cin => \vga_u0|user_input_translator|Add1~14\,
	sharein => \vga_u0|user_input_translator|Add1~15\,
	sumout => \vga_u0|user_input_translator|Add1~17_sumout\,
	cout => \vga_u0|user_input_translator|Add1~18\,
	shareout => \vga_u0|user_input_translator|Add1~19\);

-- Location: MLABCELL_X39_Y72_N9
\vga_u0|user_input_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~21_sumout\ = SUM(( !\draw.y[3]~DUPLICATE_q\ $ (!\draw.y\(1)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~22\ = CARRY(( !\draw.y[3]~DUPLICATE_q\ $ (!\draw.y\(1)) ) + ( \vga_u0|user_input_translator|Add1~19\ ) + ( \vga_u0|user_input_translator|Add1~18\ ))
-- \vga_u0|user_input_translator|Add1~23\ = SHARE((\draw.y[3]~DUPLICATE_q\ & \draw.y\(1)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y[3]~DUPLICATE_q\,
	datac => \ALT_INV_draw.y\(1),
	cin => \vga_u0|user_input_translator|Add1~18\,
	sharein => \vga_u0|user_input_translator|Add1~19\,
	sumout => \vga_u0|user_input_translator|Add1~21_sumout\,
	cout => \vga_u0|user_input_translator|Add1~22\,
	shareout => \vga_u0|user_input_translator|Add1~23\);

-- Location: MLABCELL_X39_Y72_N12
\vga_u0|user_input_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~25_sumout\ = SUM(( !\draw.y\(4) $ (!\draw.y\(2)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~26\ = CARRY(( !\draw.y\(4) $ (!\draw.y\(2)) ) + ( \vga_u0|user_input_translator|Add1~23\ ) + ( \vga_u0|user_input_translator|Add1~22\ ))
-- \vga_u0|user_input_translator|Add1~27\ = SHARE((\draw.y\(4) & \draw.y\(2)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(4),
	datad => \ALT_INV_draw.y\(2),
	cin => \vga_u0|user_input_translator|Add1~22\,
	sharein => \vga_u0|user_input_translator|Add1~23\,
	sumout => \vga_u0|user_input_translator|Add1~25_sumout\,
	cout => \vga_u0|user_input_translator|Add1~26\,
	shareout => \vga_u0|user_input_translator|Add1~27\);

-- Location: MLABCELL_X39_Y72_N15
\vga_u0|user_input_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~29_sumout\ = SUM(( !\draw.y\(5) $ (!\draw.y[3]~DUPLICATE_q\) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~30\ = CARRY(( !\draw.y\(5) $ (!\draw.y[3]~DUPLICATE_q\) ) + ( \vga_u0|user_input_translator|Add1~27\ ) + ( \vga_u0|user_input_translator|Add1~26\ ))
-- \vga_u0|user_input_translator|Add1~31\ = SHARE((\draw.y\(5) & \draw.y[3]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_draw.y\(5),
	datac => \ALT_INV_draw.y[3]~DUPLICATE_q\,
	cin => \vga_u0|user_input_translator|Add1~26\,
	sharein => \vga_u0|user_input_translator|Add1~27\,
	sumout => \vga_u0|user_input_translator|Add1~29_sumout\,
	cout => \vga_u0|user_input_translator|Add1~30\,
	shareout => \vga_u0|user_input_translator|Add1~31\);

-- Location: MLABCELL_X39_Y72_N18
\vga_u0|user_input_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~33_sumout\ = SUM(( !\draw.y\(6) $ (!\draw.y\(4)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~34\ = CARRY(( !\draw.y\(6) $ (!\draw.y\(4)) ) + ( \vga_u0|user_input_translator|Add1~31\ ) + ( \vga_u0|user_input_translator|Add1~30\ ))
-- \vga_u0|user_input_translator|Add1~35\ = SHARE((\draw.y\(6) & \draw.y\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000111100000000000000000000111111110000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(6),
	datad => \ALT_INV_draw.y\(4),
	cin => \vga_u0|user_input_translator|Add1~30\,
	sharein => \vga_u0|user_input_translator|Add1~31\,
	sumout => \vga_u0|user_input_translator|Add1~33_sumout\,
	cout => \vga_u0|user_input_translator|Add1~34\,
	shareout => \vga_u0|user_input_translator|Add1~35\);

-- Location: MLABCELL_X39_Y72_N21
\vga_u0|user_input_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~37_sumout\ = SUM(( \draw.y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~38\ = CARRY(( \draw.y\(5) ) + ( \vga_u0|user_input_translator|Add1~35\ ) + ( \vga_u0|user_input_translator|Add1~34\ ))
-- \vga_u0|user_input_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(5),
	cin => \vga_u0|user_input_translator|Add1~34\,
	sharein => \vga_u0|user_input_translator|Add1~35\,
	sumout => \vga_u0|user_input_translator|Add1~37_sumout\,
	cout => \vga_u0|user_input_translator|Add1~38\,
	shareout => \vga_u0|user_input_translator|Add1~39\);

-- Location: MLABCELL_X39_Y72_N24
\vga_u0|user_input_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~5_sumout\ = SUM(( \draw.y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~6\ = CARRY(( \draw.y\(6) ) + ( \vga_u0|user_input_translator|Add1~39\ ) + ( \vga_u0|user_input_translator|Add1~38\ ))
-- \vga_u0|user_input_translator|Add1~7\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_draw.y\(6),
	cin => \vga_u0|user_input_translator|Add1~38\,
	sharein => \vga_u0|user_input_translator|Add1~39\,
	sumout => \vga_u0|user_input_translator|Add1~5_sumout\,
	cout => \vga_u0|user_input_translator|Add1~6\,
	shareout => \vga_u0|user_input_translator|Add1~7\);

-- Location: MLABCELL_X39_Y72_N27
\vga_u0|user_input_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|user_input_translator|Add1~1_sumout\ = SUM(( GND ) + ( \vga_u0|user_input_translator|Add1~7\ ) + ( \vga_u0|user_input_translator|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|user_input_translator|Add1~6\,
	sharein => \vga_u0|user_input_translator|Add1~7\,
	sumout => \vga_u0|user_input_translator|Add1~1_sumout\);

-- Location: MLABCELL_X39_Y72_N33
\vga_u0|LessThan3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|LessThan3~0_combout\ = ( \draw.y\(6) & ( (\draw.y\(4) & (\draw.y[3]~DUPLICATE_q\ & \draw.y\(5))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_draw.y\(4),
	datac => \ALT_INV_draw.y[3]~DUPLICATE_q\,
	datad => \ALT_INV_draw.y\(5),
	dataf => \ALT_INV_draw.y\(6),
	combout => \vga_u0|LessThan3~0_combout\);

-- Location: MLABCELL_X39_Y72_N42
\vga_u0|VideoMemory|auto_generated|decode2|w_anode126w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2) = ( !\vga_u0|LessThan3~0_combout\ & ( (\vga_u0|writeEn~0_combout\ & (!\vga_u0|user_input_translator|Add1~5_sumout\ & \vga_u0|user_input_translator|Add1~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000001000000010000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|ALT_INV_writeEn~0_combout\,
	datab => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	datac => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|ALT_INV_LessThan3~0_combout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2));

-- Location: PLLREFCLKSELECT_X0_Y21_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT\ : cyclonev_pll_refclk_select
-- pragma translate_off
GENERIC MAP (
	pll_auto_clk_sw_en => "false",
	pll_clk_loss_edge => "both_edges",
	pll_clk_loss_sw_en => "false",
	pll_clk_sw_dly => 0,
	pll_clkin_0_src => "clk_0",
	pll_clkin_1_src => "ref_clk1",
	pll_manu_clk_sw_en => "false",
	pll_sw_refclk_src => "clk_0")
-- pragma translate_on
PORT MAP (
	clkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_CLKIN_bus\,
	clkout => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	extswitchbuf => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\);

-- Location: FRACTIONALPLL_X0_Y15_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL\ : cyclonev_fractional_pll
-- pragma translate_off
GENERIC MAP (
	dsm_accumulator_reset_value => 0,
	forcelock => "false",
	mimic_fbclk_type => "gclk_far",
	nreset_invert => "true",
	output_clock_frequency => "300.0 mhz",
	pll_atb => 0,
	pll_bwctrl => 4000,
	pll_cmp_buf_dly => "0 ps",
	pll_cp_comp => "true",
	pll_cp_current => 10,
	pll_ctrl_override_setting => "false",
	pll_dsm_dither => "disable",
	pll_dsm_out_sel => "disable",
	pll_dsm_reset => "false",
	pll_ecn_bypass => "false",
	pll_ecn_test_en => "false",
	pll_enable => "true",
	pll_fbclk_mux_1 => "glb",
	pll_fbclk_mux_2 => "fb_1",
	pll_fractional_carry_out => 32,
	pll_fractional_division => 1,
	pll_fractional_division_string => "'0'",
	pll_fractional_value_ready => "true",
	pll_lf_testen => "false",
	pll_lock_fltr_cfg => 25,
	pll_lock_fltr_test => "false",
	pll_m_cnt_bypass_en => "false",
	pll_m_cnt_coarse_dly => "0 ps",
	pll_m_cnt_fine_dly => "0 ps",
	pll_m_cnt_hi_div => 6,
	pll_m_cnt_in_src => "ph_mux_clk",
	pll_m_cnt_lo_div => 6,
	pll_m_cnt_odd_div_duty_en => "false",
	pll_m_cnt_ph_mux_prst => 0,
	pll_m_cnt_prst => 1,
	pll_n_cnt_bypass_en => "false",
	pll_n_cnt_coarse_dly => "0 ps",
	pll_n_cnt_fine_dly => "0 ps",
	pll_n_cnt_hi_div => 1,
	pll_n_cnt_lo_div => 1,
	pll_n_cnt_odd_div_duty_en => "false",
	pll_ref_buf_dly => "0 ps",
	pll_reg_boost => 0,
	pll_regulator_bypass => "false",
	pll_ripplecap_ctrl => 0,
	pll_slf_rst => "false",
	pll_tclk_mux_en => "false",
	pll_tclk_sel => "n_src",
	pll_test_enable => "false",
	pll_testdn_enable => "false",
	pll_testup_enable => "false",
	pll_unlock_fltr_cfg => 2,
	pll_vco_div => 2,
	pll_vco_ph0_en => "true",
	pll_vco_ph1_en => "true",
	pll_vco_ph2_en => "true",
	pll_vco_ph3_en => "true",
	pll_vco_ph4_en => "true",
	pll_vco_ph5_en => "true",
	pll_vco_ph6_en => "true",
	pll_vco_ph7_en => "true",
	pll_vctrl_test_voltage => 750,
	reference_clock_frequency => "50.0 mhz",
	vccd0g_atb => "disable",
	vccd0g_output => 0,
	vccd1g_atb => "disable",
	vccd1g_output => 0,
	vccm1g_tap => 2,
	vccr_pd => "false",
	vcodiv_override => "false",
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	coreclkfb => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	ecnc1test => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_EXTSWITCHBUF\,
	nresync => GND,
	refclkin => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_REFCLK_SELECT_O_CLKOUT\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftdonein => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	fbclk => \vga_u0|mypll|altpll_component|auto_generated|fb_clkin\,
	tclk => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	vcoph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_VCOPH_bus\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_MHI_bus\);

-- Location: PLLRECONFIG_X0_Y19_N0
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG\ : cyclonev_pll_reconfig
-- pragma translate_off
GENERIC MAP (
	fractional_pll_index => 0)
-- pragma translate_on
PORT MAP (
	cntnen => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	mhi => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_MHI_bus\,
	shift => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiftenm => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFTENM\,
	up => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_SHIFTEN_bus\);

-- Location: PLLOUTPUTCOUNTER_X0_Y20_N1
\vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER\ : cyclonev_pll_output_counter
-- pragma translate_off
GENERIC MAP (
	c_cnt_coarse_dly => "0 ps",
	c_cnt_fine_dly => "0 ps",
	c_cnt_in_src => "ph_mux_clk",
	c_cnt_ph_mux_prst => 0,
	c_cnt_prst => 1,
	cnt_fpll_src => "fpll_0",
	dprio0_cnt_bypass_en => "false",
	dprio0_cnt_hi_div => 6,
	dprio0_cnt_lo_div => 6,
	dprio0_cnt_odd_div_even_duty_en => "false",
	duty_cycle => 50,
	output_clock_frequency => "25.0 mhz",
	phase_shift => "0 ps",
	fractional_pll_index => 0,
	output_counter_index => 6)
-- pragma translate_on
PORT MAP (
	nen0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_CNTNEN\,
	shift0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_SHIFT\,
	shiften => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIGSHIFTEN6\,
	tclk0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~FRACTIONAL_PLL_O_TCLK\,
	up0 => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_RECONFIG_O_UP\,
	vco0ph => \vga_u0|mypll|altpll_component|auto_generated|generic_pll1~PLL_OUTPUT_COUNTER_VCO0PH_bus\,
	divclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0));

-- Location: CLKCTRL_G6
\vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \vga_u0|mypll|altpll_component|auto_generated|clk\(0),
	outclk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\);

-- Location: LABCELL_X29_Y73_N30
\vga_u0|controller|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~37_sumout\ = SUM(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add0~38\ = CARRY(( \vga_u0|controller|xCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add0~37_sumout\,
	cout => \vga_u0|controller|Add0~38\);

-- Location: FF_X29_Y73_N32
\vga_u0|controller|xCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~37_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(0));

-- Location: LABCELL_X29_Y73_N33
\vga_u0|controller|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~13_sumout\ = SUM(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))
-- \vga_u0|controller|Add0~14\ = CARRY(( \vga_u0|controller|xCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(1),
	cin => \vga_u0|controller|Add0~38\,
	sumout => \vga_u0|controller|Add0~13_sumout\,
	cout => \vga_u0|controller|Add0~14\);

-- Location: FF_X29_Y73_N35
\vga_u0|controller|xCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~13_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(1));

-- Location: LABCELL_X29_Y73_N36
\vga_u0|controller|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~29_sumout\ = SUM(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))
-- \vga_u0|controller|Add0~30\ = CARRY(( \vga_u0|controller|xCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(2),
	cin => \vga_u0|controller|Add0~14\,
	sumout => \vga_u0|controller|Add0~29_sumout\,
	cout => \vga_u0|controller|Add0~30\);

-- Location: FF_X29_Y73_N38
\vga_u0|controller|xCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~29_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(2));

-- Location: LABCELL_X29_Y73_N39
\vga_u0|controller|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~33_sumout\ = SUM(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))
-- \vga_u0|controller|Add0~34\ = CARRY(( \vga_u0|controller|xCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	cin => \vga_u0|controller|Add0~30\,
	sumout => \vga_u0|controller|Add0~33_sumout\,
	cout => \vga_u0|controller|Add0~34\);

-- Location: FF_X29_Y73_N40
\vga_u0|controller|xCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~33_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(3));

-- Location: LABCELL_X29_Y73_N42
\vga_u0|controller|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~17_sumout\ = SUM(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))
-- \vga_u0|controller|Add0~18\ = CARRY(( \vga_u0|controller|xCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(4),
	cin => \vga_u0|controller|Add0~34\,
	sumout => \vga_u0|controller|Add0~17_sumout\,
	cout => \vga_u0|controller|Add0~18\);

-- Location: LABCELL_X29_Y73_N45
\vga_u0|controller|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~21_sumout\ = SUM(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))
-- \vga_u0|controller|Add0~22\ = CARRY(( \vga_u0|controller|xCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	cin => \vga_u0|controller|Add0~18\,
	sumout => \vga_u0|controller|Add0~21_sumout\,
	cout => \vga_u0|controller|Add0~22\);

-- Location: FF_X29_Y73_N46
\vga_u0|controller|xCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~21_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(5));

-- Location: LABCELL_X29_Y73_N48
\vga_u0|controller|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~25_sumout\ = SUM(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))
-- \vga_u0|controller|Add0~26\ = CARRY(( \vga_u0|controller|xCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(6),
	cin => \vga_u0|controller|Add0~22\,
	sumout => \vga_u0|controller|Add0~25_sumout\,
	cout => \vga_u0|controller|Add0~26\);

-- Location: FF_X29_Y73_N49
\vga_u0|controller|xCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~25_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(6));

-- Location: LABCELL_X29_Y73_N27
\vga_u0|controller|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~1_combout\ = (\vga_u0|controller|xCounter\(1) & (\vga_u0|controller|xCounter\(0) & (!\vga_u0|controller|xCounter\(5) & !\vga_u0|controller|xCounter\(6))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000000100000000000000010000000000000001000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(1),
	datab => \vga_u0|controller|ALT_INV_xCounter\(0),
	datac => \vga_u0|controller|ALT_INV_xCounter\(5),
	datad => \vga_u0|controller|ALT_INV_xCounter\(6),
	combout => \vga_u0|controller|Equal0~1_combout\);

-- Location: LABCELL_X29_Y73_N51
\vga_u0|controller|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~9_sumout\ = SUM(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))
-- \vga_u0|controller|Add0~10\ = CARRY(( \vga_u0|controller|xCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(7),
	cin => \vga_u0|controller|Add0~26\,
	sumout => \vga_u0|controller|Add0~9_sumout\,
	cout => \vga_u0|controller|Add0~10\);

-- Location: FF_X29_Y73_N53
\vga_u0|controller|xCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~9_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(7));

-- Location: LABCELL_X29_Y73_N54
\vga_u0|controller|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~5_sumout\ = SUM(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))
-- \vga_u0|controller|Add0~6\ = CARRY(( \vga_u0|controller|xCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(8),
	cin => \vga_u0|controller|Add0~10\,
	sumout => \vga_u0|controller|Add0~5_sumout\,
	cout => \vga_u0|controller|Add0~6\);

-- Location: FF_X29_Y73_N56
\vga_u0|controller|xCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(8));

-- Location: LABCELL_X29_Y73_N57
\vga_u0|controller|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add0~1_sumout\ = SUM(( \vga_u0|controller|xCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_xCounter\(9),
	cin => \vga_u0|controller|Add0~6\,
	sumout => \vga_u0|controller|Add0~1_sumout\);

-- Location: FF_X29_Y73_N59
\vga_u0|controller|xCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~1_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(9));

-- Location: LABCELL_X29_Y73_N6
\vga_u0|controller|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~0_combout\ = ( !\vga_u0|controller|xCounter\(7) & ( (\vga_u0|controller|xCounter\(8) & (\vga_u0|controller|xCounter\(9) & (\vga_u0|controller|xCounter\(2) & \vga_u0|controller|xCounter\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000000000000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_xCounter\(2),
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(7),
	combout => \vga_u0|controller|Equal0~0_combout\);

-- Location: LABCELL_X30_Y73_N45
\vga_u0|controller|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Equal0~2_combout\ = ( \vga_u0|controller|Equal0~0_combout\ & ( (\vga_u0|controller|xCounter[4]~DUPLICATE_q\ & \vga_u0|controller|Equal0~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|ALT_INV_xCounter[4]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	dataf => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	combout => \vga_u0|controller|Equal0~2_combout\);

-- Location: FF_X29_Y73_N44
\vga_u0|controller|xCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~17_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter\(4));

-- Location: FF_X29_Y73_N43
\vga_u0|controller|xCounter[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~17_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[4]~DUPLICATE_q\);

-- Location: LABCELL_X30_Y73_N0
\vga_u0|controller|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))
-- \vga_u0|controller|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(0),
	cin => GND,
	sumout => \vga_u0|controller|Add1~37_sumout\,
	cout => \vga_u0|controller|Add1~38\);

-- Location: FF_X30_Y73_N2
\vga_u0|controller|yCounter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~37_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(0));

-- Location: LABCELL_X30_Y73_N3
\vga_u0|controller|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~33_sumout\ = SUM(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))
-- \vga_u0|controller|Add1~34\ = CARRY(( \vga_u0|controller|yCounter\(1) ) + ( GND ) + ( \vga_u0|controller|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(1),
	cin => \vga_u0|controller|Add1~38\,
	sumout => \vga_u0|controller|Add1~33_sumout\,
	cout => \vga_u0|controller|Add1~34\);

-- Location: FF_X30_Y73_N5
\vga_u0|controller|yCounter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~33_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(1));

-- Location: LABCELL_X30_Y73_N6
\vga_u0|controller|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~29_sumout\ = SUM(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))
-- \vga_u0|controller|Add1~30\ = CARRY(( \vga_u0|controller|yCounter\(2) ) + ( GND ) + ( \vga_u0|controller|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(2),
	cin => \vga_u0|controller|Add1~34\,
	sumout => \vga_u0|controller|Add1~29_sumout\,
	cout => \vga_u0|controller|Add1~30\);

-- Location: FF_X30_Y73_N8
\vga_u0|controller|yCounter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~29_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(2));

-- Location: LABCELL_X30_Y73_N9
\vga_u0|controller|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~25_sumout\ = SUM(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))
-- \vga_u0|controller|Add1~26\ = CARRY(( \vga_u0|controller|yCounter\(3) ) + ( GND ) + ( \vga_u0|controller|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(3),
	cin => \vga_u0|controller|Add1~30\,
	sumout => \vga_u0|controller|Add1~25_sumout\,
	cout => \vga_u0|controller|Add1~26\);

-- Location: FF_X30_Y73_N10
\vga_u0|controller|yCounter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~25_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(3));

-- Location: LABCELL_X30_Y73_N12
\vga_u0|controller|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~21_sumout\ = SUM(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))
-- \vga_u0|controller|Add1~22\ = CARRY(( \vga_u0|controller|yCounter\(4) ) + ( GND ) + ( \vga_u0|controller|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|Add1~26\,
	sumout => \vga_u0|controller|Add1~21_sumout\,
	cout => \vga_u0|controller|Add1~22\);

-- Location: FF_X30_Y73_N13
\vga_u0|controller|yCounter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~21_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(4));

-- Location: LABCELL_X30_Y73_N15
\vga_u0|controller|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~17_sumout\ = SUM(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))
-- \vga_u0|controller|Add1~18\ = CARRY(( \vga_u0|controller|yCounter\(5) ) + ( GND ) + ( \vga_u0|controller|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(5),
	cin => \vga_u0|controller|Add1~22\,
	sumout => \vga_u0|controller|Add1~17_sumout\,
	cout => \vga_u0|controller|Add1~18\);

-- Location: FF_X30_Y73_N17
\vga_u0|controller|yCounter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(5));

-- Location: LABCELL_X30_Y73_N18
\vga_u0|controller|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~13_sumout\ = SUM(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))
-- \vga_u0|controller|Add1~14\ = CARRY(( \vga_u0|controller|yCounter\(6) ) + ( GND ) + ( \vga_u0|controller|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(6),
	cin => \vga_u0|controller|Add1~18\,
	sumout => \vga_u0|controller|Add1~13_sumout\,
	cout => \vga_u0|controller|Add1~14\);

-- Location: FF_X30_Y73_N20
\vga_u0|controller|yCounter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~13_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(6));

-- Location: LABCELL_X30_Y73_N21
\vga_u0|controller|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~9_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))
-- \vga_u0|controller|Add1~10\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( GND ) + ( \vga_u0|controller|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|Add1~14\,
	sumout => \vga_u0|controller|Add1~9_sumout\,
	cout => \vga_u0|controller|Add1~10\);

-- Location: FF_X30_Y73_N22
\vga_u0|controller|yCounter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~9_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(7));

-- Location: LABCELL_X30_Y73_N24
\vga_u0|controller|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~5_sumout\ = SUM(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))
-- \vga_u0|controller|Add1~6\ = CARRY(( \vga_u0|controller|yCounter\(8) ) + ( GND ) + ( \vga_u0|controller|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(8),
	cin => \vga_u0|controller|Add1~10\,
	sumout => \vga_u0|controller|Add1~5_sumout\,
	cout => \vga_u0|controller|Add1~6\);

-- Location: LABCELL_X30_Y73_N27
\vga_u0|controller|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter\(9) ) + ( GND ) + ( \vga_u0|controller|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \vga_u0|controller|ALT_INV_yCounter\(9),
	cin => \vga_u0|controller|Add1~6\,
	sumout => \vga_u0|controller|Add1~1_sumout\);

-- Location: FF_X30_Y73_N29
\vga_u0|controller|yCounter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~1_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(9));

-- Location: LABCELL_X30_Y73_N30
\vga_u0|controller|always1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~0_combout\ = ( !\vga_u0|controller|yCounter\(5) & ( (!\vga_u0|controller|yCounter\(6) & (!\vga_u0|controller|yCounter\(7) & (!\vga_u0|controller|yCounter\(8) & \vga_u0|controller|yCounter\(9)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(6),
	datab => \vga_u0|controller|ALT_INV_yCounter\(7),
	datac => \vga_u0|controller|ALT_INV_yCounter\(8),
	datad => \vga_u0|controller|ALT_INV_yCounter\(9),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(5),
	combout => \vga_u0|controller|always1~0_combout\);

-- Location: LABCELL_X30_Y73_N42
\vga_u0|controller|always1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~1_combout\ = ( \vga_u0|controller|yCounter\(2) & ( (!\vga_u0|controller|yCounter\(1) & (!\vga_u0|controller|yCounter\(4) & (\vga_u0|controller|yCounter\(3) & !\vga_u0|controller|yCounter\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000000000000000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(1),
	datab => \vga_u0|controller|ALT_INV_yCounter\(4),
	datac => \vga_u0|controller|ALT_INV_yCounter\(3),
	datad => \vga_u0|controller|ALT_INV_yCounter\(0),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(2),
	combout => \vga_u0|controller|always1~1_combout\);

-- Location: LABCELL_X30_Y73_N48
\vga_u0|controller|always1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|always1~2_combout\ = ( \vga_u0|controller|Equal0~0_combout\ & ( \vga_u0|controller|always1~1_combout\ & ( (\vga_u0|controller|xCounter[4]~DUPLICATE_q\ & (\vga_u0|controller|Equal0~1_combout\ & \vga_u0|controller|always1~0_combout\)) ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_xCounter[4]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_Equal0~1_combout\,
	datad => \vga_u0|controller|ALT_INV_always1~0_combout\,
	datae => \vga_u0|controller|ALT_INV_Equal0~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_always1~1_combout\,
	combout => \vga_u0|controller|always1~2_combout\);

-- Location: FF_X30_Y73_N26
\vga_u0|controller|yCounter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~5_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter\(8));

-- Location: FF_X30_Y73_N25
\vga_u0|controller|yCounter[8]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~5_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[8]~DUPLICATE_q\);

-- Location: FF_X30_Y73_N19
\vga_u0|controller|yCounter[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~13_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[6]~DUPLICATE_q\);

-- Location: FF_X30_Y73_N16
\vga_u0|controller|yCounter[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~17_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[5]~DUPLICATE_q\);

-- Location: FF_X30_Y73_N7
\vga_u0|controller|yCounter[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~29_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[2]~DUPLICATE_q\);

-- Location: FF_X29_Y73_N55
\vga_u0|controller|xCounter[8]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~5_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[8]~DUPLICATE_q\);

-- Location: FF_X29_Y73_N52
\vga_u0|controller|xCounter[7]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~9_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[7]~DUPLICATE_q\);

-- Location: LABCELL_X31_Y73_N30
\vga_u0|controller|controller_translator|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~9_sumout\ = SUM(( !\vga_u0|controller|xCounter[7]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter[2]~DUPLICATE_q\) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~10\ = CARRY(( !\vga_u0|controller|xCounter[7]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter[2]~DUPLICATE_q\) ) + ( !VCC ) + ( !VCC ))
-- \vga_u0|controller|controller_translator|Add1~11\ = SHARE((\vga_u0|controller|xCounter[7]~DUPLICATE_q\ & \vga_u0|controller|yCounter[2]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\,
	cin => GND,
	sharein => GND,
	sumout => \vga_u0|controller|controller_translator|Add1~9_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~10\,
	shareout => \vga_u0|controller|controller_translator|Add1~11\);

-- Location: LABCELL_X31_Y73_N33
\vga_u0|controller|controller_translator|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~13_sumout\ = SUM(( !\vga_u0|controller|xCounter[8]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(3)) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~10\ ))
-- \vga_u0|controller|controller_translator|Add1~14\ = CARRY(( !\vga_u0|controller|xCounter[8]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(3)) ) + ( \vga_u0|controller|controller_translator|Add1~11\ ) + ( \vga_u0|controller|controller_translator|Add1~10\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~15\ = SHARE((\vga_u0|controller|xCounter[8]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(3)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter[8]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(3),
	cin => \vga_u0|controller|controller_translator|Add1~10\,
	sharein => \vga_u0|controller|controller_translator|Add1~11\,
	sumout => \vga_u0|controller|controller_translator|Add1~13_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~14\,
	shareout => \vga_u0|controller|controller_translator|Add1~15\);

-- Location: LABCELL_X31_Y73_N36
\vga_u0|controller|controller_translator|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~17_sumout\ = SUM(( !\vga_u0|controller|yCounter[2]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(4) $ (\vga_u0|controller|xCounter\(9))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~18\ = CARRY(( !\vga_u0|controller|yCounter[2]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(4) $ (\vga_u0|controller|xCounter\(9))) ) + ( \vga_u0|controller|controller_translator|Add1~15\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~14\ ))
-- \vga_u0|controller|controller_translator|Add1~19\ = SHARE((!\vga_u0|controller|yCounter[2]~DUPLICATE_q\ & (\vga_u0|controller|yCounter\(4) & \vga_u0|controller|xCounter\(9))) # (\vga_u0|controller|yCounter[2]~DUPLICATE_q\ & 
-- ((\vga_u0|controller|xCounter\(9)) # (\vga_u0|controller|yCounter\(4)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010101111100000000000000000101101010100101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter[2]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(4),
	datad => \vga_u0|controller|ALT_INV_xCounter\(9),
	cin => \vga_u0|controller|controller_translator|Add1~14\,
	sharein => \vga_u0|controller|controller_translator|Add1~15\,
	sumout => \vga_u0|controller|controller_translator|Add1~17_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~18\,
	shareout => \vga_u0|controller|controller_translator|Add1~19\);

-- Location: LABCELL_X31_Y73_N39
\vga_u0|controller|controller_translator|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~21_sumout\ = SUM(( !\vga_u0|controller|yCounter[5]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(3)) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~18\ ))
-- \vga_u0|controller|controller_translator|Add1~22\ = CARRY(( !\vga_u0|controller|yCounter[5]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(3)) ) + ( \vga_u0|controller|controller_translator|Add1~19\ ) + ( \vga_u0|controller|controller_translator|Add1~18\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~23\ = SHARE((\vga_u0|controller|yCounter[5]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(3)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(3),
	cin => \vga_u0|controller|controller_translator|Add1~18\,
	sharein => \vga_u0|controller|controller_translator|Add1~19\,
	sumout => \vga_u0|controller|controller_translator|Add1~21_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~22\,
	shareout => \vga_u0|controller|controller_translator|Add1~23\);

-- Location: LABCELL_X31_Y73_N42
\vga_u0|controller|controller_translator|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~25_sumout\ = SUM(( !\vga_u0|controller|yCounter[6]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~22\ ))
-- \vga_u0|controller|controller_translator|Add1~26\ = CARRY(( !\vga_u0|controller|yCounter[6]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter\(4)) ) + ( \vga_u0|controller|controller_translator|Add1~23\ ) + ( \vga_u0|controller|controller_translator|Add1~22\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~27\ = SHARE((\vga_u0|controller|yCounter[6]~DUPLICATE_q\ & \vga_u0|controller|yCounter\(4)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter\(4),
	cin => \vga_u0|controller|controller_translator|Add1~22\,
	sharein => \vga_u0|controller|controller_translator|Add1~23\,
	sumout => \vga_u0|controller|controller_translator|Add1~25_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~26\,
	shareout => \vga_u0|controller|controller_translator|Add1~27\);

-- Location: LABCELL_X31_Y73_N45
\vga_u0|controller|controller_translator|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~29_sumout\ = SUM(( !\vga_u0|controller|yCounter\(7) $ (!\vga_u0|controller|yCounter[5]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~26\ ))
-- \vga_u0|controller|controller_translator|Add1~30\ = CARRY(( !\vga_u0|controller|yCounter\(7) $ (!\vga_u0|controller|yCounter[5]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~27\ ) + ( \vga_u0|controller|controller_translator|Add1~26\ 
-- ))
-- \vga_u0|controller|controller_translator|Add1~31\ = SHARE((\vga_u0|controller|yCounter\(7) & \vga_u0|controller|yCounter[5]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001010000010100000000000000000101101001011010",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(7),
	datac => \vga_u0|controller|ALT_INV_yCounter[5]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~26\,
	sharein => \vga_u0|controller|controller_translator|Add1~27\,
	sumout => \vga_u0|controller|controller_translator|Add1~29_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~30\,
	shareout => \vga_u0|controller|controller_translator|Add1~31\);

-- Location: LABCELL_X31_Y73_N48
\vga_u0|controller|controller_translator|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~33_sumout\ = SUM(( !\vga_u0|controller|yCounter[8]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter[6]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~34\ = CARRY(( !\vga_u0|controller|yCounter[8]~DUPLICATE_q\ $ (!\vga_u0|controller|yCounter[6]~DUPLICATE_q\) ) + ( \vga_u0|controller|controller_translator|Add1~31\ ) + ( 
-- \vga_u0|controller|controller_translator|Add1~30\ ))
-- \vga_u0|controller|controller_translator|Add1~35\ = SHARE((\vga_u0|controller|yCounter[8]~DUPLICATE_q\ & \vga_u0|controller|yCounter[6]~DUPLICATE_q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000011110000111100",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[8]~DUPLICATE_q\,
	datac => \vga_u0|controller|ALT_INV_yCounter[6]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~30\,
	sharein => \vga_u0|controller|controller_translator|Add1~31\,
	sumout => \vga_u0|controller|controller_translator|Add1~33_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~34\,
	shareout => \vga_u0|controller|controller_translator|Add1~35\);

-- Location: LABCELL_X31_Y73_N51
\vga_u0|controller|controller_translator|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~37_sumout\ = SUM(( \vga_u0|controller|yCounter\(7) ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~38\ = CARRY(( \vga_u0|controller|yCounter\(7) ) + ( \vga_u0|controller|controller_translator|Add1~35\ ) + ( \vga_u0|controller|controller_translator|Add1~34\ ))
-- \vga_u0|controller|controller_translator|Add1~39\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(7),
	cin => \vga_u0|controller|controller_translator|Add1~34\,
	sharein => \vga_u0|controller|controller_translator|Add1~35\,
	sumout => \vga_u0|controller|controller_translator|Add1~37_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~38\,
	shareout => \vga_u0|controller|controller_translator|Add1~39\);

-- Location: LABCELL_X31_Y73_N54
\vga_u0|controller|controller_translator|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~1_sumout\ = SUM(( \vga_u0|controller|yCounter[8]~DUPLICATE_q\ ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~2\ = CARRY(( \vga_u0|controller|yCounter[8]~DUPLICATE_q\ ) + ( \vga_u0|controller|controller_translator|Add1~39\ ) + ( \vga_u0|controller|controller_translator|Add1~38\ ))
-- \vga_u0|controller|controller_translator|Add1~3\ = SHARE(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|ALT_INV_yCounter[8]~DUPLICATE_q\,
	cin => \vga_u0|controller|controller_translator|Add1~38\,
	sharein => \vga_u0|controller|controller_translator|Add1~39\,
	sumout => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	cout => \vga_u0|controller|controller_translator|Add1~2\,
	shareout => \vga_u0|controller|controller_translator|Add1~3\);

-- Location: LABCELL_X31_Y73_N57
\vga_u0|controller|controller_translator|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|controller_translator|Add1~5_sumout\ = SUM(( GND ) + ( \vga_u0|controller|controller_translator|Add1~3\ ) + ( \vga_u0|controller|controller_translator|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "on")
-- pragma translate_on
PORT MAP (
	cin => \vga_u0|controller|controller_translator|Add1~2\,
	sharein => \vga_u0|controller|controller_translator|Add1~3\,
	sumout => \vga_u0|controller|controller_translator|Add1~5_sumout\);

-- Location: LABCELL_X31_Y73_N3
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2) = ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ & ( \vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000000000000000001111000011110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	datae => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2));

-- Location: MLABCELL_X34_Y71_N42
\Selector88~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector88~0_combout\ = ( !\WideOr6~0_combout\ & ( (!\state.ERASE_PUCK_TWO~q\ & (colour(1) & !\state.ERASE_PADDLE_ENTER~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_state.ERASE_PUCK_TWO~q\,
	datac => ALT_INV_colour(1),
	datad => \ALT_INV_state.ERASE_PADDLE_ENTER~q\,
	dataf => \ALT_INV_WideOr6~0_combout\,
	combout => \Selector88~0_combout\);

-- Location: MLABCELL_X34_Y71_N24
\Selector88~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector88~1_combout\ = ( \state.DRAW_PADDLE_ENTER~DUPLICATE_q\ ) # ( !\state.DRAW_PADDLE_ENTER~DUPLICATE_q\ & ( (((\Selector88~0_combout\) # (\state.DRAW_TOP_ENTER~q\)) # (\state.DRAW_PUCK_TWO~q\)) # (\state.DRAW_PUCK_ONE~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111111111111011111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_state.DRAW_PUCK_ONE~q\,
	datab => \ALT_INV_state.DRAW_PUCK_TWO~q\,
	datac => \ALT_INV_state.DRAW_TOP_ENTER~q\,
	datad => \ALT_INV_Selector88~0_combout\,
	dataf => \ALT_INV_state.DRAW_PADDLE_ENTER~DUPLICATE_q\,
	combout => \Selector88~1_combout\);

-- Location: FF_X34_Y71_N26
\colour[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \Selector88~1_combout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => colour(1));

-- Location: FF_X29_Y73_N37
\vga_u0|controller|xCounter[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add0~29_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|xCounter[2]~DUPLICATE_q\);

-- Location: M10K_X38_Y73_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a7\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a7_PORTBDATAOUT_bus\);

-- Location: FF_X31_Y73_N58
\vga_u0|VideoMemory|auto_generated|address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~5_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1));

-- Location: FF_X33_Y73_N32
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1));

-- Location: FF_X31_Y73_N55
\vga_u0|VideoMemory|auto_generated|address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|controller_translator|Add1~1_sumout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0));

-- Location: FF_X33_Y73_N8
\vga_u0|VideoMemory|auto_generated|out_address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|VideoMemory|auto_generated|address_reg_b\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0));

-- Location: MLABCELL_X39_Y72_N30
\vga_u0|VideoMemory|auto_generated|decode2|w_anode105w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2) = ( !\vga_u0|LessThan3~0_combout\ & ( (!\vga_u0|user_input_translator|Add1~5_sumout\ & (\vga_u0|writeEn~0_combout\ & !\vga_u0|user_input_translator|Add1~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	datac => \vga_u0|ALT_INV_writeEn~0_combout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|ALT_INV_LessThan3~0_combout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2));

-- Location: LABCELL_X31_Y73_N15
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2) = ( !\vga_u0|controller|controller_translator|Add1~1_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000000000000000000011110000111100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	datae => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2));

-- Location: M10K_X41_Y73_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a2\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a2_PORTBDATAOUT_bus\);

-- Location: MLABCELL_X39_Y72_N45
\vga_u0|VideoMemory|auto_generated|decode2|w_anode118w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2) = ( !\vga_u0|LessThan3~0_combout\ & ( (\vga_u0|writeEn~0_combout\ & (\vga_u0|user_input_translator|Add1~5_sumout\ & !\vga_u0|user_input_translator|Add1~1_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100000000000100010000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|ALT_INV_writeEn~0_combout\,
	datab => \vga_u0|user_input_translator|ALT_INV_Add1~5_sumout\,
	datad => \vga_u0|user_input_translator|ALT_INV_Add1~1_sumout\,
	dataf => \vga_u0|ALT_INV_LessThan3~0_combout\,
	combout => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2));

-- Location: LABCELL_X31_Y73_N6
\vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2) = ( \vga_u0|controller|controller_translator|Add1~1_sumout\ & ( !\vga_u0|controller|controller_translator|Add1~5_sumout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \vga_u0|controller|controller_translator|ALT_INV_Add1~5_sumout\,
	datae => \vga_u0|controller|controller_translator|ALT_INV_Add1~1_sumout\,
	combout => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2));

-- Location: M10K_X26_Y72_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a5\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a5_PORTBDATAOUT_bus\);

-- Location: LABCELL_X29_Y73_N0
\vga_u0|controller|on_screen~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~0_combout\ = ( !\vga_u0|controller|xCounter\(6) & ( !\vga_u0|controller|xCounter\(4) & ( (!\vga_u0|controller|xCounter\(2) & (!\vga_u0|controller|xCounter\(3) & (!\vga_u0|controller|xCounter\(1) & 
-- !\vga_u0|controller|xCounter\(5)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(2),
	datab => \vga_u0|controller|ALT_INV_xCounter\(3),
	datac => \vga_u0|controller|ALT_INV_xCounter\(1),
	datad => \vga_u0|controller|ALT_INV_xCounter\(5),
	datae => \vga_u0|controller|ALT_INV_xCounter\(6),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|on_screen~0_combout\);

-- Location: LABCELL_X30_Y73_N33
\vga_u0|controller|LessThan7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|LessThan7~0_combout\ = ( \vga_u0|controller|yCounter\(8) & ( (!\vga_u0|controller|yCounter\(9) & ((!\vga_u0|controller|yCounter\(6)) # ((!\vga_u0|controller|yCounter\(7)) # (!\vga_u0|controller|yCounter\(5))))) ) ) # ( 
-- !\vga_u0|controller|yCounter\(8) & ( !\vga_u0|controller|yCounter\(9) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111110000000001111111000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(6),
	datab => \vga_u0|controller|ALT_INV_yCounter\(7),
	datac => \vga_u0|controller|ALT_INV_yCounter\(5),
	datad => \vga_u0|controller|ALT_INV_yCounter\(9),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(8),
	combout => \vga_u0|controller|LessThan7~0_combout\);

-- Location: LABCELL_X29_Y73_N9
\vga_u0|controller|on_screen~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|on_screen~1_combout\ = ( \vga_u0|controller|LessThan7~0_combout\ & ( (!\vga_u0|controller|xCounter\(9)) # ((!\vga_u0|controller|xCounter\(8) & ((!\vga_u0|controller|xCounter[7]~DUPLICATE_q\) # 
-- (\vga_u0|controller|on_screen~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011101100111011101110110011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\,
	datad => \vga_u0|controller|ALT_INV_on_screen~0_combout\,
	dataf => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	combout => \vga_u0|controller|on_screen~1_combout\);

-- Location: LABCELL_X33_Y73_N15
\vga_u0|controller|VGA_R[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_R[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & ( \vga_u0|controller|on_screen~1_combout\ & ( (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & 
-- (((\vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0))))) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a8\)) ) ) ) # ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a5~portbdataout\ & ( \vga_u0|controller|on_screen~1_combout\ & ( (!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & 
-- \vga_u0|VideoMemory|auto_generated|ram_block1a2~portbdataout\)))) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a8\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000010001110100010001110111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a8\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datad => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a2~portbdataout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a5~portbdataout\,
	dataf => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	combout => \vga_u0|controller|VGA_R[0]~0_combout\);

-- Location: M10K_X26_Y73_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a4\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a4_PORTBDATAOUT_bus\);

-- Location: M10K_X41_Y72_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a1\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a1_PORTBDATAOUT_bus\);

-- Location: LABCELL_X33_Y73_N30
\vga_u0|controller|VGA_G[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_G[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & 
-- \vga_u0|controller|on_screen~1_combout\) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a7~portbdataout\ & \vga_u0|controller|on_screen~1_combout\) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a1~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|ram_block1a4~portbdataout\ & (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & \vga_u0|controller|on_screen~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010001000000000000111100000000110111010000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a4~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a7~portbdataout\,
	datad => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a1~portbdataout\,
	combout => \vga_u0|controller|VGA_G[0]~0_combout\);

-- Location: M10K_X26_Y71_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a3\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode118w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode157w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a3_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y71_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a6\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 2,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 2,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode126w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode166w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a6_PORTBDATAOUT_bus\);

-- Location: M10K_X38_Y72_N0
\vga_u0|VideoMemory|auto_generated|ram_block1a0\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_core_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vga_adapter:vga_u0|altsyncram:VideoMemory|altsyncram_pnm1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 3,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 3,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	clk1 => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	ena0 => \vga_u0|VideoMemory|auto_generated|decode2|w_anode105w\(2),
	ena1 => \vga_u0|VideoMemory|auto_generated|rden_decode_b|w_anode143w\(2),
	portadatain => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \vga_u0|VideoMemory|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LABCELL_X33_Y73_N6
\vga_u0|controller|VGA_B[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_B[0]~0_combout\ = ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\)) # (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & 
-- ((\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( \vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & 
-- ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1)) # (\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))) ) ) ) # ( \vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|controller|on_screen~1_combout\ & ((!\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a3~portbdataout\)) # 
-- (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & ((\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\))))) ) ) ) # ( !\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(0) & ( 
-- !\vga_u0|VideoMemory|auto_generated|ram_block1a0~portbdataout\ & ( (\vga_u0|VideoMemory|auto_generated|out_address_reg_b\(1) & (\vga_u0|VideoMemory|auto_generated|ram_block1a6~portbdataout\ & \vga_u0|controller|on_screen~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000100011100000000110011110000000001000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a3~portbdataout\,
	datab => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(1),
	datac => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a6~portbdataout\,
	datad => \vga_u0|controller|ALT_INV_on_screen~1_combout\,
	datae => \vga_u0|VideoMemory|auto_generated|ALT_INV_out_address_reg_b\(0),
	dataf => \vga_u0|VideoMemory|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	combout => \vga_u0|controller|VGA_B[0]~0_combout\);

-- Location: LABCELL_X29_Y73_N24
\vga_u0|controller|VGA_HS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~0_combout\ = ( \vga_u0|controller|xCounter\(4) & ( (((\vga_u0|controller|xCounter\(1) & \vga_u0|controller|xCounter\(0))) # (\vga_u0|controller|xCounter\(3))) # (\vga_u0|controller|xCounter\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000011111111111110001111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(1),
	datab => \vga_u0|controller|ALT_INV_xCounter\(0),
	datac => \vga_u0|controller|ALT_INV_xCounter\(2),
	datad => \vga_u0|controller|ALT_INV_xCounter\(3),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(4),
	combout => \vga_u0|controller|VGA_HS1~0_combout\);

-- Location: LABCELL_X29_Y73_N12
\vga_u0|controller|VGA_HS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_HS1~1_combout\ = ( \vga_u0|controller|xCounter\(5) & ( \vga_u0|controller|xCounter\(7) & ( ((!\vga_u0|controller|xCounter\(9)) # ((\vga_u0|controller|VGA_HS1~0_combout\ & \vga_u0|controller|xCounter\(6)))) # 
-- (\vga_u0|controller|xCounter\(8)) ) ) ) # ( !\vga_u0|controller|xCounter\(5) & ( \vga_u0|controller|xCounter\(7) & ( ((!\vga_u0|controller|xCounter\(9)) # ((!\vga_u0|controller|VGA_HS1~0_combout\ & !\vga_u0|controller|xCounter\(6)))) # 
-- (\vga_u0|controller|xCounter\(8)) ) ) ) # ( \vga_u0|controller|xCounter\(5) & ( !\vga_u0|controller|xCounter\(7) ) ) # ( !\vga_u0|controller|xCounter\(5) & ( !\vga_u0|controller|xCounter\(7) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111101110111011101110111011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datac => \vga_u0|controller|ALT_INV_VGA_HS1~0_combout\,
	datad => \vga_u0|controller|ALT_INV_xCounter\(6),
	datae => \vga_u0|controller|ALT_INV_xCounter\(5),
	dataf => \vga_u0|controller|ALT_INV_xCounter\(7),
	combout => \vga_u0|controller|VGA_HS1~1_combout\);

-- Location: FF_X29_Y73_N13
\vga_u0|controller|VGA_HS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_HS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS1~q\);

-- Location: FF_X29_Y73_N16
\vga_u0|controller|VGA_HS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_HS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_HS~q\);

-- Location: FF_X30_Y73_N4
\vga_u0|controller|yCounter[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|Add1~33_sumout\,
	clrn => \KEY[3]~inputCLKENA0_outclk\,
	sclr => \vga_u0|controller|always1~2_combout\,
	ena => \vga_u0|controller|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|yCounter[1]~DUPLICATE_q\);

-- Location: LABCELL_X30_Y73_N36
\vga_u0|controller|VGA_VS1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~0_combout\ = ( \vga_u0|controller|yCounter\(3) & ( \vga_u0|controller|yCounter\(2) & ( (!\vga_u0|controller|yCounter\(9) & (!\vga_u0|controller|yCounter\(4) & (!\vga_u0|controller|yCounter\(0) $ 
-- (!\vga_u0|controller|yCounter[1]~DUPLICATE_q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000100010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(9),
	datab => \vga_u0|controller|ALT_INV_yCounter\(4),
	datac => \vga_u0|controller|ALT_INV_yCounter\(0),
	datad => \vga_u0|controller|ALT_INV_yCounter[1]~DUPLICATE_q\,
	datae => \vga_u0|controller|ALT_INV_yCounter\(3),
	dataf => \vga_u0|controller|ALT_INV_yCounter\(2),
	combout => \vga_u0|controller|VGA_VS1~0_combout\);

-- Location: LABCELL_X30_Y73_N54
\vga_u0|controller|VGA_VS1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_VS1~1_combout\ = ( \vga_u0|controller|yCounter[8]~DUPLICATE_q\ & ( \vga_u0|controller|yCounter\(5) & ( (!\vga_u0|controller|yCounter\(6)) # ((!\vga_u0|controller|yCounter\(7)) # (!\vga_u0|controller|VGA_VS1~0_combout\)) ) ) ) # ( 
-- !\vga_u0|controller|yCounter[8]~DUPLICATE_q\ & ( \vga_u0|controller|yCounter\(5) ) ) # ( \vga_u0|controller|yCounter[8]~DUPLICATE_q\ & ( !\vga_u0|controller|yCounter\(5) ) ) # ( !\vga_u0|controller|yCounter[8]~DUPLICATE_q\ & ( 
-- !\vga_u0|controller|yCounter\(5) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_yCounter\(6),
	datab => \vga_u0|controller|ALT_INV_yCounter\(7),
	datad => \vga_u0|controller|ALT_INV_VGA_VS1~0_combout\,
	datae => \vga_u0|controller|ALT_INV_yCounter[8]~DUPLICATE_q\,
	dataf => \vga_u0|controller|ALT_INV_yCounter\(5),
	combout => \vga_u0|controller|VGA_VS1~1_combout\);

-- Location: FF_X30_Y73_N55
\vga_u0|controller|VGA_VS1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_VS1~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS1~q\);

-- Location: FF_X30_Y73_N40
\vga_u0|controller|VGA_VS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_VS1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_VS~q\);

-- Location: LABCELL_X29_Y73_N18
\vga_u0|controller|VGA_BLANK1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \vga_u0|controller|VGA_BLANK1~0_combout\ = ( \vga_u0|controller|xCounter[7]~DUPLICATE_q\ & ( \vga_u0|controller|LessThan7~0_combout\ & ( !\vga_u0|controller|xCounter\(9) ) ) ) # ( !\vga_u0|controller|xCounter[7]~DUPLICATE_q\ & ( 
-- \vga_u0|controller|LessThan7~0_combout\ & ( (!\vga_u0|controller|xCounter\(8)) # (!\vga_u0|controller|xCounter\(9)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011101110111011101100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \vga_u0|controller|ALT_INV_xCounter\(8),
	datab => \vga_u0|controller|ALT_INV_xCounter\(9),
	datae => \vga_u0|controller|ALT_INV_xCounter[7]~DUPLICATE_q\,
	dataf => \vga_u0|controller|ALT_INV_LessThan7~0_combout\,
	combout => \vga_u0|controller|VGA_BLANK1~0_combout\);

-- Location: FF_X29_Y73_N19
\vga_u0|controller|VGA_BLANK1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	d => \vga_u0|controller|VGA_BLANK1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK1~q\);

-- Location: FF_X29_Y73_N22
\vga_u0|controller|VGA_BLANK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \vga_u0|mypll|altpll_component|auto_generated|clk[0]~CLKENA0_outclk\,
	asdata => \vga_u0|controller|VGA_BLANK1~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \vga_u0|controller|VGA_BLANK~q\);

-- Location: IOIBUF_X40_Y0_N1
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);


pll_reconfig_inst_tasks : altera_pll_reconfig_tasks
-- pragma translate_off
GENERIC MAP (
		number_of_fplls => 1);
-- pragma translate_on
END structure;


